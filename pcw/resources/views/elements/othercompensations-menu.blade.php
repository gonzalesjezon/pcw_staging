
<?php

(strtolower($module) == 'communicationexpense') ? $communicationexpense = 'active' : $communicationexpense = '';
(strtolower($module) == 'eme') ? $eme = 'active' : $eme = '';
(strtolower($module) == 'cashmidyearbonus') ? $cashmidyearbonus = 'active' : $cashmidyearbonus = '';
?>
<div class="side-menu">
    <div class="row">
        <div class="col-sm-8">
            <nav class="nav-sidebar">
                <ul class="nav">
                    <li class="{{ $communicationexpense }}">
                        <a href="{{ url('payrolls/reports/othercompensations/communicationexpense') }}"  >
                        COMMUNICATION EXPENSE ALLOTMENT</a>
                    </li>
                    <li class="{{ $eme }}">
                        <a href="{{ url('payrolls/reports/othercompensations/eme') }}"  >
                        EXTRAORDINARY AND MISCELLANEOUS EXPENSES</a>
                    </li>
                    <li class="{{ $cashmidyearbonus }}">
                        <a href="{{ url('payrolls/reports/othercompensations/cashmidyearbonus') }}"  >
                        CASH GIFT AND MID YEAR BONUS</a>
                    </li>
                    <!-- <li class="nav-divider"></li> -->

                </ul>
            </nav>
        </div>
<!--         <div class="col-sm-2 col-sm-offset-8">

        </div> -->
    </div>

</div>




