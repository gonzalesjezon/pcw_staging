
<?php

(strtolower($module) == 'yearendtransmital') ? $yearendtransmital = 'active' : $yearendtransmital = '';
(strtolower($module) == 'yearendgeneralpayroll') ? $yearendgeneralpayroll = 'active' : $yearendgeneralpayroll = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $yearendgeneralpayroll }}">
            <a href="{{ url('payrolls/reports/yearendreports/yearendgeneralpayroll') }}"  >
                <span>GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $yearendtransmital }}">
            <a href="{{ url('payrolls/reports/yearendreports/yearendtransmital') }}"  >
                <span>TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




