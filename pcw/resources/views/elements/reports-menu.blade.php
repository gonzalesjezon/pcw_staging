<?php
(strtolower($module) == 'payslips') ? $payslips = 'active' : $payslips = '';
(strtolower($module) == 'generalpayroll') ? $generalpayroll = 'active' : $generalpayroll = '';
(strtolower($module) == 'peipayroll') ? $peipayroll = 'active' : $peipayroll = '';
(strtolower($module) == 'ratapayroll') ? $ratapayroll = 'active' : $ratapayroll = '';
(strtolower($module) == 'midyearpayroll') ? $midyearpayroll = 'active' : $midyearpayroll = '';
(strtolower($module) == 'yearendpayroll') ? $yearendpayroll = 'active' : $yearendpayroll = '';
(strtolower($module) == 'pbbpayroll') ? $pbbpayroll = 'active' : $pbbpayroll = '';
(strtolower($module) == 'cospayroll') ? $cospayroll = 'active' : $cospayroll = '';
(strtolower($module) == 'gpanniversary') ? $gpanniversary = 'active' : $gpanniversary = '';
(strtolower($module) == 'gpuniformallowance') ? $gpuniformallowance = 'active' : $gpuniformallowance = '';
(strtolower($module) == 'gpovertime') ? $gpovertime = 'active' : $gpovertime = '';
(strtolower($module) == 'recomputationtax') ? $recomputationtax = 'active' : $recomputationtax = '';
?>
<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $payslips }}">
            <a href="{{ url('payrolls/reports/payslips') }}"  >
                <span>PAYSLIP</span>
            </a>
        </li>
        <li class="{{ $generalpayroll }}">
            <a href="{{ url('payrolls/reports/generalpayroll') }}"  >
                <span>GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $cospayroll }}">
            <a href="{{ url('payrolls/reports/cospayroll') }}"  >
                <span>COS PAYROLL</span>
            </a>
        </li>
        <li class="{{ $peipayroll }}">
            <a href="{{ url('payrolls/reports/peipayroll') }}"  >
                <span>PEI PAYROLL</span>
            </a>
        </li>
        <li class="{{ $pbbpayroll }}">
            <a href="{{ url('payrolls/reports/pbbpayroll') }}"  >
                <span>PBB PAYROLL</span>
            </a>
        </li>
        <li class="{{ $ratapayroll }}">
            <a href="{{ url('payrolls/reports/ratapayroll') }}"  >
                <span>RATA PAYROLL</span>
            </a>
        </li>
        <li class="{{ $midyearpayroll }}">
            <a href="{{ url('payrolls/reports/midyearpayroll') }}"  >
                <span>MID YEAR PAYROLL</span>
            </a>
        </li>
        <li class="{{ $yearendpayroll }}">
            <a href="{{ url('payrolls/reports/yearendpayroll') }}"  >
                <span>YEAR END PAYROLL</span>
            </a>
        </li>
        <li class="{{ $gpanniversary }}">
            <a href="{{ url('payrolls/reports/gpanniversary') }}"  >
                <span>GENERAL PAYROLL ANNIVERSARY</span>
            </a>
        </li>
        <li class="{{ $gpuniformallowance }}">
            <a href="{{ url('payrolls/reports/gpuniformallowance') }}"  >
                <span>GENERAL PAYROLL UNIFORM ALLOWANCE</span>
            </a>
        </li>
        <li class="{{ $gpovertime }}">
            <a href="{{ url('payrolls/reports/gpovertime') }}"  >
                <span>GENERAL PAYROLL OVERTIME</span>
            </a>
        </li>
        <li class="{{ $recomputationtax }}">
            <a href="{{ url('payrolls/reports/recomputationtax') }}"  >
                <span>RECOMPUTATION OF TAX</span>
            </a>
        </li>

    </ul>
</nav>




