
<?php
(strtolower($module) == 'gsis') ? $gsis = 'active' : $gsis = '';
(strtolower($module) == 'ecip') ? $ecip = 'active' : $ecip = '';
(strtolower($module) == 'pagibig') ? $pagibig = 'active' : $pagibig = '';
(strtolower($module) == 'philhealth') ? $philhealth = 'active' : $philhealth = '';
?>
<div class="side-menu">
    <div class="row">
        <div class="col-sm-8">
            <nav class="nav-sidebar">
                <ul class="nav">
                    <li class="{{ $gsis }}">
                    	<a href="{{ url('payrolls/reports/remittances/gsis') }}"  >
						GSIS</a>
                    </li>
                     <li class="{{ $ecip }}">
                    	<a href="{{ url('payrolls/reports/remittances/ecip') }}"  >
						ECIP</a>
                    </li>
                     <li class="{{ $pagibig }}">
                    	<a href="{{ url('payrolls/reports/remittances/pagibig') }}"  >
						PAGIBIG</a>
                    </li>
                    <li class="{{ $philhealth }}">
                    	<a href="{{ url('payrolls/reports/remittances/philhealth') }}"  >
						PHILHEALTH</a>
                    </li>
                    <!-- <li class="nav-divider"></li> -->

                </ul>
            </nav>
        </div>
<!--         <div class="col-sm-2 col-sm-offset-8">

        </div> -->
    </div>

</div>




