<div class="row p-2">
	<div class="col-md-6">
		<span class="font-weight-bold">Transaction Date</span>
		<div class="form-group mb-0 pb-0">
			<select class="employee-type form-control font-style2 select2" id="select_month" name="month" placeholder="Month">
				@foreach($months as $key => $month)
				<option value="{{$key}}" {{ ($key == $current_month) ? 'selected' : '' }}>{{ $month }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<span>&nbsp;</span>
		<div class="form-group mb-0 pb-0">
			<select class="employee-type form-control font-style2 select2" id="select_year" name="year">
				@foreach( range($latest_year,$earliest_year) as $i)
				<option value="{{$i}}">{{$i}}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>