@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row pl-4 pr-4">
					@include('payrolls.includes._months-year')
				</div>
				@include('payrolls.reports.includes._signatory')
			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table" style=" width: 100%;" class="table">
	       					<thead class="text-center" style="font-weight: bold;">
	       						<tr>
	       							<td rowspan="3" style="border-right: none;">
	       								<img src="{{ url('images/reportlogo.png') }}" style="height: 80px;position: absolute;">
	       							</td>
	       							<td rowspan="3" class="text-center" colspan="7" style="border-left: none;">
	       								<h3>GENERAL PAYROLL</h3>
	       								<h5 style="font-weight: bold;">RATA for the month of <span id="month_year"></span></h5>
	       							</td>
	       							<td colspan="3" class="text-left">Document Code: <span id="doc_code">HRDMS-R-009</span></td>
	       						</tr>
	       						<tr>
	       							<td colspan="3" class="text-left">Date: <span id="print_date"></span></td>
	       						</tr>
	       						<tr class="text-left">
	       							<td colspan="2">Rev. No</td>
	       							<td>Page</td>
	       						</tr>
	       						<tr>
	       							<td>NO</td>
	       							<td>NAME OF EMPLOYEE</td>
	       							<td>POSITION</td>
	       							<td>EMPLOYEE NO.</td>
	       							<td>ACCOUNT NUMBER</td>
	       							<td>Monthly Representation Allowance (RA)</td>
	       							<td>Monthly Transportation Allowance (TA)</td>
	       							<td>Additional RATA</td>
	       							<td>Refund</td>
	       							<td>Deduction base on Used of Vehicle</td>
	       							<td>Net Amount Received</td>
	       						</tr>
	       					</thead>
	       					<tbody  id="payroll_transfer"></tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						ctr2 = 1;
						netPEIAmount = 0;

						subRAAmount 	= 0;
						subTAAmount  	= 0;
						subRATAAmount 	= 0;
						subRefundAmount 		= 0;
						subUsedVehicleAmount 	= 0;
						subAddRATAAmount 		= 0;
						is_oic = false;
						$.each(data.transaction,function(k,v){

							// ======== BODY =======

							lastname = (v.employees) ? v.employees.lastname : '';
							firstname = (v.employees) ? v.employees.firstname : '';
							middlename = (v.employees.middlename) ? v.employees.middlename : '';
							fullname = lastname+' '+firstname+' '+middlename;
							position = (v.positions) ? v.positions.Name : '';
							employee_number = (v.employee_number) ? v.employee_number : '';
							account_number = (v.account_number) ? v.account_number : '';

							raAmount = (v.representation_amount) ? v.representation_amount : 0;
							taAmount = (v.transportation_amount) ? v.transportation_amount : 0;

							refundAmount 		= 0;
							usedVehicleAmount 	= 0;
							addRaAmount 		= 0;
							addTaAmount 		= 0;
							if(v.rataadjustment){
								refundAmount = (v.rataadjustment.refund_amount) ? v.rataadjustment.refund_amount : 0;
								usedVehicleAmount = (v.rataadjustment.used_vehicle_amount) ? v.rataadjustment.used_vehicle_amount : 0;
								addRaAmount = (v.rataadjustment.ra_additional_amount) ? v.rataadjustment.ra_additional_amount : 0;
								addTaAmount = (v.rataadjustment.ta_additional_amount) ? v.rataadjustment.ta_additional_amount : 0;
							}

							addRATAAmount = parseFloat(addRaAmount) + parseFloat(addTaAmount);

							// ===== COMPUTATION =====

							// ===== SUB TOTAL COMPUTATION =====

							totalRATAAmount = parseFloat(raAmount) + parseFloat(taAmount) + parseFloat(addRATAAmount) + parseFloat(refundAmount) - parseFloat(usedVehicleAmount);

							subRAAmount += parseFloat(raAmount);
							subTAAmount += parseFloat(taAmount);
							subRATAAmount += parseFloat(totalRATAAmount);
							subRefundAmount += parseFloat(refundAmount);
							subUsedVehicleAmount += parseFloat(usedVehicleAmount);
							subAddRATAAmount += parseFloat(addRATAAmount);
							// ===== SUB TOTAL COMPUTATION =====

							// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
							ra_amount = (raAmount !== 0) ? commaSeparateNumber(parseFloat(raAmount).toFixed(2)) : '';
							ta_amount = (taAmount !== 0) ? commaSeparateNumber(parseFloat(taAmount).toFixed(2)) : '';
							total_rata_amount = (totalRATAAmount !== 0) ? commaSeparateNumber(parseFloat(totalRATAAmount).toFixed(2)) : '';
							add_rata_amount = (addRATAAmount !== 0) ? commaSeparateNumber(parseFloat(addRATAAmount).toFixed(2)) : '';
							used_vehicle_amount = (usedVehicleAmount !== 0) ? commaSeparateNumber(parseFloat(usedVehicleAmount).toFixed(2)) : '';
							refund_amount = (refundAmount !== 0) ? commaSeparateNumber(parseFloat(refundAmount).toFixed(2)) : '';
							account_number = (account_number !== null) ? account_number : '';
							// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

							if(v.is_oic == 0){
								body += '<tr>';
								body += '<td>'+ctr+'</td>'
								body += '<td>'+fullname+'</td>'
								body += '<td>'+position+'</td>'
								body += '<td class="text-center">'+employee_number+'</td>'
								body += '<td class="text-center">'+account_number+'</td>'
								body += '<td  class="text-right">'+ra_amount+'</td>'
								body += '<td  class="text-right">'+ta_amount+'</td>'
								body += '<td  class="text-right">'+add_rata_amount+'</td>'
								body += '<td  class="text-right">'+refund_amount+'</td>'
								body += '<td  class="text-right">'+used_vehicle_amount+'</td>'
								body += '<td  class="text-right">'+total_rata_amount+'</td>'
								body += '</tr>';
							}

							ctr++;


						});

						if(data.transaction2.length !== 0){
							$.each(data.transaction2,function(k,v){
								lastname = (v.employees) ? v.employees.lastname : '';
								firstname = (v.employees) ? v.employees.firstname : '';
								middlename = (v.employees.middlename) ? v.employees.middlename : '';
								fullname = lastname+' '+firstname+' '+middlename;
								position = (v.positions) ? v.positions.Name : '';
								employee_number = (v.employee_number) ? v.employee_number : '';
								account_number = (v.account_number) ? v.account_number : '';

								raAmount = (v.representation_amount) ? v.representation_amount : 0;
								taAmount = (v.transportation_amount) ? v.transportation_amount : 0;

								refundAmount 		= 0;
								usedVehicleAmount 	= 0;
								addRaAmount 		= 0;
								addTaAmount 		= 0;
								remarks 			= '';
								if(v.rataadjustment){
									refundAmount = (v.rataadjustment.refund_amount) ? v.rataadjustment.refund_amount : 0;
									usedVehicleAmount = (v.rataadjustment.used_vehicle_amount) ? v.rataadjustment.used_vehicle_amount : 0;
									addRaAmount = (v.rataadjustment.ra_additional_amount) ? v.rataadjustment.ra_additional_amount : 0;
									addTaAmount = (v.rataadjustment.ta_additional_amount) ? v.rataadjustment.ta_additional_amount : 0;
									remarks = (v.rataadjustment.remarks) ? v.rataadjustment.remarks : 0;
								}
								addRATAAmount = parseFloat(addRaAmount) + parseFloat(addTaAmount);
								totalRATAAmount = parseFloat(raAmount) + parseFloat(taAmount) + parseFloat(addRATAAmount) + parseFloat(refundAmount) - parseFloat(usedVehicleAmount);
								ra_amount = (raAmount !== 0) ? commaSeparateNumber(parseFloat(raAmount).toFixed(2)) : '';
								ta_amount = (taAmount !== 0) ? commaSeparateNumber(parseFloat(taAmount).toFixed(2)) : '';
								total_rata_amount = (totalRATAAmount !== 0) ? commaSeparateNumber(parseFloat(totalRATAAmount).toFixed(2)) : '';
								add_rata_amount = (addRATAAmount !== 0) ? commaSeparateNumber(parseFloat(addRATAAmount).toFixed(2)) : '';
								used_vehicle_amount = (usedVehicleAmount !== 0) ? commaSeparateNumber(parseFloat(usedVehicleAmount).toFixed(2)) : '';
								refund_amount = (refundAmount !== 0) ? commaSeparateNumber(parseFloat(refundAmount).toFixed(2)) : '';
								account_number = (account_number !== null) ? account_number : '';
								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
								if(is_oic == false){
									body += '<tr>';
									body += '<td colspan="11" id="attach_here">OIC</td>'
									body += '</tr>';
									is_oic = true;
								}

								body += '<tr>';
								body += '<td>'+ctr2+'</td>'
								body += '<td>'+fullname+'</td>'
								body += '<td>'+position+'</td>'
								body += '<td class="text-center">'+employee_number+'</td>'
								body += '<td class="text-center">'+account_number+'</td>'
								body += '<td  class="text-right">'+ra_amount+'</td>'
								body += '<td  class="text-right">'+ta_amount+'</td>'
								body += '<td  class="text-right">'+add_rata_amount+'</td>'
								body += '<td  class="text-right">'+refund_amount+'</td>'
								body += '<td  class="text-right">'+used_vehicle_amount+'</td>'
								body += '<td  class="text-right">'+total_rata_amount+'</td>'
								body += '</tr>';

								ctr2++;
							});
						}



						// ======= GRAND TOTAL ======

						sub_ra_amount = (subRAAmount !== 0) ? commaSeparateNumber(parseFloat(subRAAmount).toFixed(2)) : '';
						sub_ta_amount = (subTAAmount !== 0) ? commaSeparateNumber(parseFloat(subTAAmount).toFixed(2)) : '';
						sub_rata_amount = (subRATAAmount !== 0) ? commaSeparateNumber(parseFloat(subRATAAmount).toFixed(2)) : '';
						sub_add_rata_amount = (subAddRATAAmount !== 0) ? commaSeparateNumber(parseFloat(subAddRATAAmount).toFixed(2)) : '';
						sub_refund_amount = (subRefundAmount !== 0) ? commaSeparateNumber(parseFloat(subRefundAmount).toFixed(2)) : '';
						sub_used_vehicle_amount = (subUsedVehicleAmount !== 0) ? commaSeparateNumber(parseFloat(subUsedVehicleAmount).toFixed(2)) : '';

						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
						body += '<tr>';
						body += '<td></td>'
						body += '<td></td>'
						body += '<td></td>'
						body += '<td class="text-center">GRAND TOTAL</td>'
						body += '<td class="text-center"></td>'
						body += '<td  class="text-right">'+sub_ra_amount+'</td>'
						body += '<td  class="text-right">'+sub_ta_amount+'</td>'
						body += '<td  class="text-right">'+sub_add_rata_amount+'</td>'
						body += '<td  class="text-right">'+sub_refund_amount+'</td>'
						body += '<td  class="text-right">'+sub_used_vehicle_amount+'</td>'
						body += '<td  class="text-right">'+sub_rata_amount+'</td>'
						body += '</tr>';

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';

						// ======= TOTAL DIVISION AMOUNT ======

						amountInWord = toWords(sub_rata_amount).toUpperCase();


						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;" colspan="3">CERTIFIED:  Services duly rendered as stated.</td>'
						body += '<td style="border:none;" colspan="7">APPROVED FOR PAYMENT: '+amountInWord+' ONLY PHP ('+sub_rata_amount+')</td>'
						body += '</tr>';

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td style="border:none;" ></td>'
						body += '<td style="border:none;padding-top:20px;" colspan="3">'+signatoryOne+' <br> '+positionOne+'</td>'
						body += '<td style="border:none;padding-top:20px;" colspan="7">'+signatoryTwo+' <br> '+positionTwo+'</td>'
						body += '</tr>';

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;padding-top:20px;" colspan="3">CERTIFIED:  Supporting documents complete and proper; and cash available in the amount of PHP '+sub_rata_amount+'</td>'
						body += '<td style="border:none;" colspan="7"></td>'
						body += '</tr>';

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;padding-top:20px;" colspan="3">'+signatoryThree+' <br> '+positionThree+'</td>';
						body += '<td style="border:none;" colspan="7"></td>';
						body += '</tr>';

						$('#payroll_transfer').html(body);

						$('#month_year').text(months[_Month]+' '+_Year);

						printDate = data.print_date;
						$('#print_date').text(printDate);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection