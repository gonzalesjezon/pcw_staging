@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Select Employee Status</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="employee_status" id="employee_status">
								<option value=""></option>
								<option value="permanent">Permanent</option>
								<option value="project">Project</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row pl-4 pr-4">
					@include('payrolls.includes._months-year')
				</div>
				@include('payrolls.reports.includes._signatory')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-left: -5px; margin-right: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _status;
	$(document).on('change','#employee_status',function(){
		_status = "";
		_status = $(this).find(':selected').val();

	});


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'status':_status
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						ctr2 = 0;

						ctr3 = 0;
						$.each(data.transaction,function(key,val){
							$.each(val,function(k1,v1){
								ctr3++;
							})
						})

						netAbAmount = 0;
						printDate = data.print_date;

       					$.each(data.transaction,function(key,val){
       						$.each(val,function(k1,v1){

       							body += '<div class="row">';
								body += '<div class="col-md-12">'
								body += '<table class="table table2">';
								body += '<thead class="text-center" style="font-weight: bold;">'
		       					body += '<tr>';
		       					body += '<td rowspan="3" style="border-right: none;">';
		       					body += 	'<img src="{{ url("images/reportlogo.png") }}" style="height: 80px;position: absolute;">';
		       					body += '</td>';
		       					body += '<td rowspan="3" class="text-center" colspan="6" style="border-left: none;">';
		       					body +=	'<h3>GENERAL PAYROLLL</h3>';
		       					body +=	'<h5 style="font-weight: bold;">HIRING ANNIVERSARY  RECOGNITION FOR <span id="month_year">'+_Year+'</span></h5>';
		       					body += '</td>';
		       					body += '<td colspan="2" class="text-left">Document Code: HRDMS-R-023</td>';
		       					body += '</tr>';
		       					body += '<tr>';
		       					body += '<td colspan="2" class="text-left">Date: '+printDate+'</td>';
		       					body += '</tr>';
		       					body += '<tr class="text-left">';
		       					body += '<td>Rev. No</td>';
		       					body += '<td>Page</td>';
		       					body +=	'</tr>';
		       					body += '<tr>';
		       					body += '<td colspan="9" class="text-left">WE ACKNOWLEDGE receipt of the sums shown opposite our names as full compensation  for the period covered.</td>';
		       					body +=	'</tr>';
		       					body +=	'<tr>';
		       					body += '<td>NO</td>';
		       					body += '<td>NAME OF EMPLOYEE</td>';
		       					body += '<td>POSITION</td>';
		       					body += '<td>EMPLOYEE NO.</td>';
		       					body += '<td>ACCOUNT NUMBER</td>';
		       					body += '<td>'+_Year+'</td>';
		       					body += '<td>NET AMOUNT RECEIVED</td>';
		       					body += '<td>SURNAME</td>';
		       					body += '<td>SIGNATURE</td>';
		       					body +=	'</tr>';
		       					body += '</thead>';

		       					body += '<tbody>';

		       					subAbAmount = 0;

       							$.each(v1,function(k,v){

       								firstname = (v.employees.firstname) ? v.employees.firstname : '';
       								lastname = (v.employees.lastname) ? v.employees.lastname : '';
       								middlename = (v.employees.middlename) ? v.employees.middlename : '';
       								employee_number = (v.employees.employee_number) ? v.employees.employee_number : '';
       								position = (v.positions.Name) ? v.positions.Name : '';
       								account_number = (v.employeeinfo.account_number) ? v.employeeinfo.account_number : '';

       								fullname = lastname+' '+firstname+' '+middlename;

       								abAmount = (v.amount) ? v.amount : 0;

       								subAbAmount += parseFloat(abAmount);

       								ab_amount = (abAmount) ? commaSeparateNumber(parseFloat(abAmount).toFixed(2)) : '';

       								body += '<tr class="text-right">';
       								body += '<td class="text-center">'+ctr+'</td>';
       								body += '<td class="text-left">'+fullname+'</td>';
       								body += '<td class="text-center">'+position+'</td>';
       								body += '<td class="text-center">'+employee_number+'</td>';
       								body += '<td class="text-center">'+account_number+'</td>';
       								body += '<td >'+ab_amount+'</td>';
       								body += '<td >'+ab_amount+'</td>';
       								body += '<td ></td>'; // surname
       								body += '<td ></td>'; // signature
       								body += '</tr>';
       								ctr++;
       								body += '<div style="page-break-after: always;display:block;"></div>';
       							})

       							ctr = 1;

       							netAbAmount += parseFloat(subAbAmount);

       							sub_ab_amount = (subAbAmount) ? commaSeparateNumber(parseFloat(subAbAmount).toFixed(2)) : '';

       							ctr2++;

								if(ctr2 >= ctr3){
									body += '<tr id="lastrow" style="font-weight:bold;" class="text-right">';
								}else{
									body += '<tr style="font-weight:bold;" class="text-right">';
								}
   								body += '<td class="text-center"></td>';
   								body += '<td class="text-left"></td>';
   								body += '<td class="text-center">SUB TOTAL</td>';
   								body += '<td class="text-center"></td>';
   								body += '<td class="text-center"></td>';
   								body += '<td >'+sub_ab_amount+'</td>';
   								body += '<td >'+sub_ab_amount+'</td>';
   								body += '<td ></td>'; // surname
   								body += '<td ></td>'; // signature
   								body += '</tr>';

       						});

       					});

						net_ab_amount = (netAbAmount) ? commaSeparateNumber(parseFloat(netAbAmount).toFixed(2)) : '';

						lastrow = [];

						lastrow += '<tr style="font-weight:bold;" class="text-right">';
						lastrow += '<td class="text-center"></td>';
						lastrow += '<td class="text-left"></td>';
						lastrow += '<td class="text-center">GRAND TOTAL</td>';
						lastrow += '<td class="text-center"></td>';
						lastrow += '<td class="text-center"></td>';
						lastrow += '<td >'+net_ab_amount+'</td>';
						lastrow += '<td >'+net_ab_amount+'</td>';
						lastrow += '<td ></td>'; // surname
						lastrow += '<td ></td>'; // signature
						lastrow += '</tr>';


						amountInWord = toWords(net_ab_amount).toUpperCase();

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="4">CERTIFIED:  Services duly rendered as stated.</td>'
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="6" >APPROVED FOR PAYMENT: '+amountInWord+' ONLY PHP '+net_ab_amount+'</td>';
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="4">'+signatoryOne+' <br> '+positionOne+'</td>';
						lastrow += '<td colspan="6" style="border:none" ></td>';
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="4">CERTIFIED:  Supporting documents complete and proper; <br> and cash available in the amount of Php '+net_ab_amount+'</td>';
						lastrow += '<td colspan="6" style="border:none" ></td>';
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;border:none;">';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="4">'+signatoryTwo+' ';
						lastrow += '<br> '+positionTwo;
						lastrow += '</td>';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="6" >'+signatoryThree+' <br>'+positionThree+'</td>';
						lastrow += '</tr>';



       					body += '</table></div></div>'
						$('#payroll_transfer').html(body);
						$('#lastrow').after(lastrow);



						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection