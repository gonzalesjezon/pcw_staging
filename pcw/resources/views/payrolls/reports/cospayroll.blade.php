@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/legal-size.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<span>Covered Date</span>
				<div class="row pl-4 pr-4">
					@include('payrolls.includes._months-year')
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<span>Pay Period</span>
						<div class="form-group">
							<select class="form-control select2" name="pay_period" id="pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes._signatory')

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table" style=" width: 100%;border: 2px solid #333;">
	       					<thead class="text-center" style="font-weight: bold;">
	       						<tr>
	       							<td rowspan="3" colspan="3">
	       								<img src="{{ url('images/reportlogo.png') }}" style="height: 80px;">
	       							</td>
	       							<td rowspan="3" class="text-center" colspan="11">
	       								<h3>JO/COS PAYROLL</h3>
	       								<h4>For the month of <span id="month_year"></span></h4>
	       							</td>
	       							<td colspan="3">Document Code: HRDMS-R-008</td>
	       						</tr>
	       						<tr>
	       							<td colspan="3">Date: <span id="print_date"></span></td>
	       						</tr>
	       						<tr>
	       							<td>Revision No</td>
	       							<td colspan="2">Page</td>
	       						</tr>
	       						<tr>
	       							<td rowspan="2" style="vertical-align: middle;">NO</td>
	       							<td rowspan="2" style="vertical-align: middle;">NAME OF EMPLOYEE</td>
	       							<td rowspan="2" style="vertical-align: middle;">POSITION</td>
	       							<td rowspan="2" style="vertical-align: middle;">DAILY RATE</td>
	       							<td rowspan="2" style="vertical-align: middle;">NO. OF ACTUAL <br> DAYS RENDERES</td>
	       							<td rowspan="2" style="vertical-align: middle;">EXCESS <br> HOUR/S RENDERED <br> (in minutes)</td>
	       							<td rowspan="2" style="vertical-align: middle;">RATE PER <br> HOUR (in min.)</td>
	       							<td rowspan="2" style="vertical-align: middle;">TOTAL AMOUNT <br> OF EXCESS HOUR/S <br> RENDERED </td>
	       							<td rowspan="2" style="vertical-align: middle;">TOTAL EARNED <br> AMOUNT</td>
	       							<td rowspan="2" style="vertical-align: middle;">LATE/UT</td>
	       							<td rowspan="2" style="vertical-align: middle;">TOTAL AMOUNT <br> OF LATE/UT</td>
	       							<td colspan="2">ADJUSTMENT</td>
	       							<td rowspan="2" style="vertical-align: middle;">GROSS AMOUNT <br> EARNED</td>
	       							<td rowspan="2" style="vertical-align: middle;">TAX 3%</td>
	       							<td rowspan="2" style="vertical-align: middle;">AOM</td>
	       							<td rowspan="2" style="vertical-align: middle;">NET PAY</td>
	       						</tr>
	       						<tr>
	       							<td>ADD</td>
	       							<td>LESS</td>
	       						</tr>
	       					</thead>
	       					<tbody  id="payroll_transfer"></tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	var payPeriod;
	$(document).on('change','#pay_period',function(){
		payPeriod = "";
		payPeriod = $(this).find(':selected').val();

	})

	function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	}


	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':payPeriod,
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;

						days = daysInMonth(_Month,_Year)
						switch(payPeriod){
							case 'firsthalf':
								_coveredPeriod =  months[_Month]+' 1-15, '+_Year;
							break;
							default:
								_coveredPeriod = months[_Month]+' 16-'+days+', '+_Year;
							break;
						}

						netTotalLWOP 		= 0;
						netGrossAmount 		= 0;
						netTotalAmount 		= 0;
						netActualOvertime 	= 0;
						netTotalExcess 		= 0;
						netAomAmount 		= 0;
						netAddAdjustment 	= 0;
						netLessAdjustment 	= 0;
						netTaxAmountOne 	= 0;
						netTotalRendered = 0;

						$.each(data.transaction,function(key,val){

						// ======== BODY =======

						subTotalLWOP 		= 0;
						subGrossAmount 		= 0;
						subTaxAmount 		= 0;
						subTotalAmount 		= 0;
						subActualOvertime 	= 0;
						subTotalExcess 		= 0;
						subAomAmount 		= 0;
						subAddAdjustment 	= 0;
						subLessAdjustment 	= 0;
						subTaxAmountOne 	= 0;
						subTotalRendered = 0;

						$.each(val,function(k,v){

							lastname = (v.employees) ? v.employees.lastname : '';
							firstname = (v.employees) ? v.employees.firstname : '';
							middlename = (v.employees.middlename) ? v.employees.middlename : '';
							fullname = lastname+' '+firstname+' '+middlename;
							position = (v.positions) ? v.positions.Name : '';

							actualWorkDays = (v.actual_workdays) ? v.actual_workdays : 0;
							actualOvertime = (v.actual_overtime) ? v.actual_overtime : 0;
							totalExcess = (v.total_overtime_amount) ? v.total_overtime_amount : 0;
							actualTardiness = (v.actual_tardiness) ? v.actual_tardiness : 0;
							actualUndertime = (v.actual_undertime) ? v.actual_undertime : 0;

							addAdjustment = (v.adjustment_add) ? v.adjustment_add : 0;
							lessAdjustment = (v.adjustment_less) ? v.adjustment_less : 0;
							aomAmount = (v.aom_amount) ? v.aom_amount : 0;

							// totalAbsentAmount = (v.total_absences_amount) ? v.total_absences_amount : 0
							totalTardinessAmount = (v.total_tardiness_amount) ? v.total_tardiness_amount : 0
							totalUndertimeAmount = (v.total_undertime_amount) ? v.total_undertime_amount : 0

							totalLATE = Number(actualTardiness) + Number(actualUndertime);
							totalLWOP = parseFloat(totalTardinessAmount) + parseFloat(totalUndertimeAmount);

							dailyRate = (v.daily_rate) ? v.daily_rate : 0;
							taxAmountOne = (v.tax_rate_amount_one) ? v.tax_rate_amount_one : 0;
							hourlyRate = parseFloat(dailyRate) / 8;
							minuteRate = parseFloat(hourlyRate) / 60;


							salaryRate = parseFloat(dailyRate) * actualWorkDays;

							totalRendered = Number(salaryRate) + Number(totalExcess);

							grossAmount = (parseFloat(totalRendered) + parseFloat(addAdjustment)) - (parseFloat(totalLWOP) + parseFloat(lessAdjustment));

							totalAmount = parseFloat(grossAmount) - (parseFloat(taxAmountOne) + parseFloat(aomAmount));

							// ===== COMPUTATION =====

							// ===== SUB TOTAL COMPUTATION =====

							subTotalLWOP += parseFloat(totalLWOP);
							subGrossAmount += parseFloat(grossAmount);
							subTaxAmountOne += parseFloat(taxAmountOne);
							subTotalAmount += parseFloat(totalAmount);
							subActualOvertime += parseFloat(actualOvertime);
							subAddAdjustment += parseFloat(addAdjustment);
							subLessAdjustment += parseFloat(lessAdjustment);
							subAomAmount += parseFloat(aomAmount);
							subTotalExcess += parseFloat(totalExcess);
							subTotalRendered += Number(totalRendered);


							// ===== SUB TOTAL COMPUTATION =====

							// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
							totalLATE = (totalLATE) ? totalLATE : '';
							daily_rate_amount = (dailyRate !== 0) ? commaSeparateNumber(parseFloat(dailyRate).toFixed(2)) : '0.00';
							total_lwop_amount = (totalLWOP !== 0) ? commaSeparateNumber(parseFloat(totalLWOP).toFixed(2)) : '0.00';
							gross_amount = (grossAmount !== 0) ? commaSeparateNumber(parseFloat(grossAmount).toFixed(2)) : '0.00';
							tax_amount_one = (taxAmountOne !== 0) ? commaSeparateNumber(parseFloat(taxAmountOne).toFixed(2)) : '0.00';
							total_amount = (totalAmount !== 0) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '0.00';
							adjustment_add = (addAdjustment !== 0) ? commaSeparateNumber(parseFloat(addAdjustment).toFixed(2)) : '0.00';
							adjustment_less = (lessAdjustment !== 0) ? commaSeparateNumber(parseFloat(lessAdjustment).toFixed(2)) : '0.00';
							aom_amount = (aomAmount !== 0) ? commaSeparateNumber(parseFloat(aomAmount).toFixed(2)) : '0.00';
							actual_overtime = (actualOvertime !== 0) ? Number(actualOvertime).toFixed(0) : '';
							total_excess = (totalExcess !== 0) ? commaSeparateNumber(parseFloat(totalExcess).toFixed(2)) : '0.00';
							total_rendered = (totalRendered !== 0) ? commaSeparateNumber(parseFloat(totalRendered).toFixed(2)) : '0.00';
							minute_rate = parseFloat(minuteRate).toFixed(2);
							// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

							actual_overtime = (actual_overtime != 0) ? actual_overtime : '';

							body += '<tr>';
							body += '<td class="text-center">'+ctr+'</td>'
							body += '<td class="text-left" nowrap>'+fullname+'</td>'
							body += '<td class="text-left" nowrap>'+position+'</td>'
							body += '<td class="text-right">'+daily_rate_amount+'</td>'
							body += '<td class="text-center">'+actualWorkDays+'</td>'
							body += '<td class="text-center">'+actual_overtime+'</td>' // excess hours rendered in minute
							body += '<td class="text-center">'+minute_rate+'</td>'
							body += '<td class="text-right">'+total_excess+'</td>' // total amount hours excess rendered
							body += '<td class="text-right">'+total_rendered+'</td>' // total earned amount
							body += '<td class="text-center">'+totalLATE+'</td>' // total late
							body += '<td class="text-right">'+total_lwop_amount+'</td>'
							body += '<td class="text-right">'+adjustment_add+'</td>'
							body += '<td class="text-right">'+adjustment_less+'</td>'
							body += '<td class="text-right">'+gross_amount+'</td>'
							body += '<td class="text-right">'+tax_amount_one+'</td>'
							body += '<td class="text-right">'+aom_amount+'</td>' // AOM
							body += '<td class="text-right">'+total_amount+'</td>'
							body += '</tr>';

							ctr++;
						});

						// ===== SUB TOTAL COMPUTATION =====

						netTotalLWOP 		+= parseFloat(subTotalLWOP);
						netGrossAmount 		+= parseFloat(subGrossAmount);
						netTaxAmountOne 	+= parseFloat(subTaxAmount);
						netTotalAmount 		+= parseFloat(subTotalAmount);
						netActualOvertime 	+= parseFloat(subActualOvertime);
						netTotalExcess 		+= parseFloat(subTotalExcess);
						netAomAmount 		+= parseFloat(subAomAmount);
						netAddAdjustment 	+= parseFloat(subAddAdjustment);
						netLessAdjustment 	+= parseFloat(subLessAdjustment);
						netTotalRendered += Number(subTotalRendered);

						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
						sub_total_lwop_amount = (subTotalLWOP !== 0) ? commaSeparateNumber(parseFloat(subTotalLWOP).toFixed(2)) : '0.00';
						sub_gross_amount = (subGrossAmount !== 0) ? commaSeparateNumber(parseFloat(subGrossAmount).toFixed(2)) : '0.00';
						sub_tax_amount_one = (subTaxAmountOne !== 0) ? commaSeparateNumber(parseFloat(subTaxAmountOne).toFixed(2)) : '0.00';
						sub_total_amount = (subTotalAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalAmount).toFixed(2)) : '0.00';
						sub_add_adjustment = (subAddAdjustment !== 0) ? commaSeparateNumber(parseFloat(subAddAdjustment).toFixed(2)) : '0.00';
						sub_less_adjustment = (subLessAdjustment !== 0) ? commaSeparateNumber(parseFloat(subLessAdjustment).toFixed(2)) : '0.00';
						sub_aom_amount = (subAomAmount !== 0) ? commaSeparateNumber(parseFloat(subAomAmount).toFixed(2)) : '0.00';
						sub_actual_overtime = (subActualOvertime !== 0) ? commaSeparateNumber(parseFloat(subActualOvertime).toFixed(2)) : '0.00';
						sub_total_excess = (subTotalExcess !== 0) ? commaSeparateNumber(parseFloat(subTotalExcess).toFixed(2)) : '0.00';
						sub_total_rendered = (subTotalRendered !== 0) ? commaSeparateNumber(parseFloat(subTotalRendered).toFixed(2)) : '0.00';
						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

						body += '<tr style="font-weight:bold;">';
						body += '<td class="text-center"></td>'
						body += '<td class="text-center">SUB TOTAL</td>'
						body += '<td class="text-center"></td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-center"></td>'
						body += '<td class="text-center">'+sub_actual_overtime+'</td>' // excess hours rendered in minute
						body += '<td class="text-center"></td>'
						body += '<td class="text-right">'+sub_total_excess+'</td>' // total amount hours excess rendered
						body += '<td class="text-right">'+sub_total_rendered+'</td>' // total earned
						body += '<td class="text-right"></td>' // total late
						body += '<td class="text-right">'+total_lwop_amount+'</td>'
						body += '<td class="text-right">'+sub_add_adjustment+'</td>'
						body += '<td class="text-right">'+sub_less_adjustment+'</td>'
						body += '<td class="text-right">'+sub_gross_amount+'</td>'
						body += '<td class="text-right">'+sub_tax_amount_one+'</td>'
						body += '<td class="text-right">'+sub_aom_amount+'</td>' // AOM
						body += '<td class="text-right">'+sub_total_amount+'</td>'
						body += '</tr>';

						});

						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
						net_total_lwop_amount = (netTotalLWOP !== 0) ? commaSeparateNumber(parseFloat(netTotalLWOP).toFixed(2)) : '0.00';
						net_gross_amount = (netGrossAmount !== 0) ? commaSeparateNumber(parseFloat(netGrossAmount).toFixed(2)) : '0.00';
						net_tax_amount_one = (netTaxAmountOne !== 0) ? commaSeparateNumber(parseFloat(netTaxAmountOne).toFixed(2)) : '0.00';
						net_total_amount = (netTotalAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalAmount).toFixed(2)) : '0.00';
						net_add_adjustment = (netAddAdjustment !== 0) ? commaSeparateNumber(parseFloat(netAddAdjustment).toFixed(2)) : '0.00';
						net_less_adjustment = (netLessAdjustment !== 0) ? commaSeparateNumber(parseFloat(netLessAdjustment).toFixed(2)) : '0.00';
						net_aom_amount = (netAomAmount !== 0) ? commaSeparateNumber(parseFloat(netAomAmount).toFixed(2)) : '0.00';
						net_actual_overtime = (netActualOvertime !== 0) ? commaSeparateNumber(parseFloat(netActualOvertime).toFixed(2)) : '0.00';
						net_total_excess = (netTotalExcess !== 0) ? commaSeparateNumber(parseFloat(netTotalExcess).toFixed(2)) : '0.00';
						net_total_rendered = (netTotalRendered !== 0) ? commaSeparateNumber(parseFloat(netTotalRendered).toFixed(2)) : '0.00';
						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

						body += '<tr style="font-weight:bold;">';
						body += '<td class="text-center"></td>'
						body += '<td class="text-center">GRAND TOTAL</td>'
						body += '<td class="text-center"></td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-center"></td>'
						body += '<td class="text-center">'+net_actual_overtime+'</td>' // excess hours rendered in minute
						body += '<td class="text-center"></td>'
						body += '<td class="text-right">'+net_total_excess+'</td>' // total amount hours excess rendered
						body += '<td class="text-right">'+net_total_rendered+'</td>' // total earned
						body += '<td class="text-right"></td>' // total late
						body += '<td class="text-right">'+total_lwop_amount+'</td>'
						body += '<td class="text-right">'+net_add_adjustment+'</td>'
						body += '<td class="text-right">'+net_less_adjustment+'</td>'
						body += '<td class="text-right">'+net_gross_amount+'</td>'
						body += '<td class="text-right">'+net_tax_amount_one+'</td>'
						body += '<td class="text-right">'+net_aom_amount+'</td>' // AOM
						body += '<td class="text-right">'+net_total_amount+'</td>'
						body += '</tr>';

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';

						// ======= GRAND TOTAL ======

						body += '<tr style="font-weight:bold;">';
						body += '<td colspan="17" style="border:none;"></td>';
						body += '</tr">';

						toConvert = net_total_amount.split('.');

						amountInWord = toWords(toConvert[0]).toUpperCase();

						body += '<tr style="font-weight:bold;">';
						body += '<td colspan="6" style="border:none;padding-left:150px;">CERTIFIED:  Services duly rendered as stated.</td>';
						body += '<td colspan="11" style="border:none;padding-left:150px;">APPROVED FOR PAYMENT: '+amountInWord+' & '+toConvert[1]+'/100 PESOS ONLY (PHP '+net_total_amount+')</td>';
						body += '</tr">';

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td colspan="6" style="border:none;padding:30px;">'+signatoryOne+' <br> '+positionOne+'</td>';
						body += '<td colspan="11" style="border:none;"></td>';
						body += '</tr">';

						body += '<tr style="font-weight:bold;">';
						body += '<td colspan="8" style="border:none;padding-left:150px;">CERTIFIED:  Supporting documents complete and proper; <br> and cash available in the amount of  P '+net_total_amount+'</td>';
						body += '<td colspan="9" style="border:none;">'+signatoryTwo+' <br> '+positionTwo+'</td>';
						body += '</tr">';

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td colspan="6" style="border:none;padding:30px;">'+signatoryThree+' <br> '+positionThree+'</td>';
						body += '<td colspan="11" style="border:none;"></td>';
						body += '</tr">';


						$('#payroll_transfer').html(body);

						$('#month_year').text(_coveredPeriod);

						printDate = data.print_date;
						$('#print_date').text(printDate);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection