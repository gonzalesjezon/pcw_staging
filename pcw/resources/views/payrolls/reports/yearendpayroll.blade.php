@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Select Employee Status</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="employee_status" id="employee_status">
								<option value=""></option>
								<option value="permanent">Permanent</option>
								<option value="project">Project</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row pl-4 pr-4">
					@include('payrolls.includes._months-year')
				</div>
				@include('payrolls.reports.includes._signatory')
			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-left: -5px;margin-right: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _status;
	$(document).on('change','#employee_status',function(){
		_status = "";
		_status = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'status':_status
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						ctr2 = 0;

						netYearEndAmount 	= 0;
						netDefaultAmount 	= 0;
						netCashGiftAmount 	= 0;
						netSalaryAmount 	= 0;
						netTotalAmount 		= 0;

						ctr3 = 0;
						$.each(data.transaction,function(key,val){
							$.each(val,function(k1,v1){
								ctr3++;
							})
						})
						printDate = data.print_date;

						$.each(data.transaction,function(key,val){

							$.each(val,function(k1,v1){

							body += '<div class="row">';
							body += '<div class="col-md-12">';
							body += '<table class="table table2">';
							body +=	'<thead class="text-center" style="font-weight: bold;"><tr>';
	       					body += '<td rowspan="3" style="border-right: none;">';
	       					body += '<img src="{{ url("images/reportlogo.png") }}" style="height: 80px;position: absolute;">';
	       					body += '</td>';
	       					body += '<td rowspan="3" class="text-center" colspan="9" style="border-left: none;">';
	       					body += '<h3>GENERAL PAYROLLL</h3>';
	       					body += '<h5 style="font-weight: bold;">YEAR-END AND CASH GIFT BONUS <br> FOR THE YEAR <span id="month_year">'+_Year+'</span></h5></td>';
	       					body +=	'<td colspan="3" class="text-left">Document Code: HRDMS-R-009</td></tr>';
	       					body +=	'<tr><td colspan="3" class="text-left">Date: '+printDate+'</td></tr>';
	       					body +=	'<tr class="text-left"><td>Rev. No</td><td colspan="2">Page</td></tr>';
	       					body += '<tr>';
	       					body += '<td class="text-left" colspan="13">WE ACKNOWLEDGE receipt of the sums shown opposite our names as full compensation  for the period covered.</td>';
	       					body += '</tr>';
	       					body +=	'<tr>';
	       					body += '<td>NO</td>';
	       					body += '<td>NAME OF EMPLOYEE</td>';
	       					body += '<td>POSITION</td>';
	       					body += '<td>EMPLOYEE NO.</td>';
	       					body += '<td>ACCOUNT NUMBER</td>';
	       					body += '<td>ADJUSTED MONTHLY SALARY</td>';
	       					body += '<td>CASH GIFT</td>';
	       					body += '<td>Percentage of the Year End Bonus</td>';
	       					body += '<td>Percentage of the Cash Gift</td>';
	       					body += '<td>YEAR END BONUS</td>';
	       					body += '<td> CASH GIFT</td>';
	       					body += '<td>TAX</td>';
	       					body += '<td>NET AMOUNT RECEIVED</td>';
	       					body += '</tr>';
	       					body += '</thead>';

							// ======== BODY =======
							subYearEndAmount 	= 0;
							subCashGiftAmount 	= 0;
							cashGiftDefaultAmount = 5000;
							subTotalAmount = 0;
							subDefaultAmount = 0;
							subSalaryAmount = 0;

							body += '<tbody>';

							$.each(v1,function(k,v){

								lastname = (v.employees) ? v.employees.lastname : '';
								firstname = (v.employees) ? v.employees.firstname : '';
								middlename = (v.employees.middlename) ? v.employees.middlename : '';
								fullname = lastname+' '+firstname+' '+middlename;
								position = (v.positions) ? v.positions.Name : '';
								employee_number = (v.employees.employee_number) ? v.employees.employee_number : '';
								account_number = (v.employeeinfo) ? v.employeeinfo.account_number : '';
								step_inc = (v.salaryinfo.step_inc) ? v.salaryinfo.step_inc : '';
								salarygrade = (v.salaryinfo.salarygrade_id) ? v.salaryinfo.salarygrade.salary_grade : '';
								percentage = (v.percentage) ? v.percentage : 0;

								salaryAmount = (v.salaryinfo) ? v.salaryinfo.salary_new_rate : 0;
								yearEndAmount = (v.amount) ? v.amount : 0;
								cashGiftAmount = (v.cash_gift_amount) ? v.cash_gift_amount : 0;

								totalAmount = parseFloat(yearEndAmount) + parseFloat(cashGiftAmount);

								// ===== COMPUTATION =====

								// ===== SUB TOTAL COMPUTATION =====

								subDefaultAmount += parseFloat(cashGiftDefaultAmount);
								subYearEndAmount += parseFloat(yearEndAmount);
								subCashGiftAmount += parseFloat(cashGiftAmount);
								subSalaryAmount += parseFloat(salaryAmount);
								subTotalAmount += parseFloat(totalAmount);
								// ===== SUB TOTAL COMPUTATION =====

								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
								year_end_amount = (yearEndAmount !== 0) ? commaSeparateNumber(parseFloat(yearEndAmount).toFixed(2)) : '';
								default_amount = (cashGiftDefaultAmount !== 0) ? commaSeparateNumber(parseFloat(cashGiftDefaultAmount).toFixed(2)) : '';
								cash_gift_amount = (cashGiftAmount !== 0) ? commaSeparateNumber(parseFloat(cashGiftAmount).toFixed(2)) : '';
								salary_amount = (salaryAmount !== 0) ? commaSeparateNumber(parseFloat(salaryAmount).toFixed(2)) : '';
								total_amount = (totalAmount !== 0) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';
								account_number = (account_number !== null) ? account_number : '';
								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

								body += '<tr>';
								body += '<td>'+ctr+'</td>'
								body += '<td>'+fullname+'</td>'
								body += '<td>'+position+'</td>'
								body += '<td class="text-center">'+employee_number+'</td>'
								body += '<td class="text-center">'+account_number+'</td>'
								body += '<td  class="text-right">'+salary_amount+'</td>'
								body += '<td  class="text-right">'+default_amount+'</td>'
								body += '<td  class="text-center">'+percentage+'</td>'
								body += '<td  class="text-center">'+percentage+' %</td>'
								body += '<td  class="text-right">'+year_end_amount+'</td>'
								body += '<td  class="text-right">'+cash_gift_amount+'</td>'
								body += '<td  class="text-right"></td>' // TAX
								body += '<td  class="text-right">'+total_amount+'</td>'
								body += '</tr>';
							ctr++;
							body += '<div style="page-break-after: always;display:block;"></div>';
							});


							ctr = 1;
							ctr2++;

							netYearEndAmount += parseFloat(subYearEndAmount);
							netDefaultAmount += parseFloat(subDefaultAmount);
							netCashGiftAmount += parseFloat(subCashGiftAmount);
							netSalaryAmount += parseFloat(subSalaryAmount);
							netTotalAmount += parseFloat(subTotalAmount);

							sub_year_end_amount = (subYearEndAmount !== 0) ? commaSeparateNumber(parseFloat(subYearEndAmount).toFixed(2)) : '';
							sub_default_amount = (subDefaultAmount !== 0) ? commaSeparateNumber(parseFloat(subDefaultAmount).toFixed(2)) : '';
							sub_cash_gift_amount = (subCashGiftAmount !== 0) ? commaSeparateNumber(parseFloat(subCashGiftAmount).toFixed(2)) : '';
							sub_salary_amount = (subSalaryAmount !== 0) ? commaSeparateNumber(parseFloat(subSalaryAmount).toFixed(2)) : '';
							sub_total_amount = (subTotalAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalAmount).toFixed(2)) : '';

							if(ctr2 >= ctr3){
								body += '<tr id="lastrow" style="font-weight:bold;" class="text-right">';
							}else{
								body += '<tr style="font-weight:bold;" class="text-right">';
							}
							body += '<td></td>'
							body += '<td class="text-center">SUB TOTAL</td>'
							body += '<td></td>'
							body += '<td class="text-center"></td>'
							body += '<td class="text-center"></td>'
							body += '<td  class="text-right">'+sub_salary_amount+'</td>'
							body += '<td  class="text-right">'+sub_default_amount+'</td>'
							body += '<td  class="text-center"></td>'
							body += '<td  class="text-center"></td>'
							body += '<td  class="text-right">'+sub_year_end_amount+'</td>'
							body += '<td  class="text-right">'+sub_cash_gift_amount+'</td>'
							body += '<td  class="text-right"></td>' // TAX
							body += '<td  class="text-right">'+sub_total_amount+'</td>'
							body += '</tr>';

							});

						});

						net_year_end_amount = (netYearEndAmount !== 0) ? commaSeparateNumber(parseFloat(netYearEndAmount).toFixed(2)) : '';
						net_default_amount = (netDefaultAmount !== 0) ? commaSeparateNumber(parseFloat(netDefaultAmount).toFixed(2)) : '';
						net_cash_gift_amount = (netCashGiftAmount !== 0) ? commaSeparateNumber(parseFloat(netCashGiftAmount).toFixed(2)) : '';
						net_salary_amount = (netSalaryAmount !== 0) ? commaSeparateNumber(parseFloat(netSalaryAmount).toFixed(2)) : '';
						net_total_amount = (netTotalAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalAmount).toFixed(2)) : '';

						lastrow = [];

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td></td>'
						lastrow += '<td class="text-center">GRAND TOTAL</td>'
						lastrow += '<td></td>'
						lastrow += '<td class="text-center"></td>'
						lastrow += '<td class="text-center"></td>'
						lastrow += '<td  class="text-right">'+net_salary_amount+'</td>'
						lastrow += '<td  class="text-right">'+net_default_amount+'</td>'
						lastrow += '<td  class="text-center"></td>'
						lastrow += '<td  class="text-center"></td>'
						lastrow += '<td  class="text-right">'+net_year_end_amount+'</td>'
						lastrow += '<td  class="text-right">'+net_cash_gift_amount+'</td>'
						lastrow += '<td  class="text-right"></td>' // TAX
						lastrow += '<td  class="text-right">'+net_total_amount+'</td>'
						lastrow += '</tr>';

						// ======= TOTAL DIVISION AMOUNT ======

						lastrow += '<tr>';
						lastrow += '<td colspan="13"></td>'
						lastrow += '</tr>';

						netDivisionAmount = 0;
						$.each(data.transaction,function(key,val){

							$.each(val,function(k1,v1){

								subDivisionAmount  = 0;
								$.each(v1,function(k,v){
									yearEndAmount = (v.amount) ? v.amount : 0;
									cashGiftAmount = (v.cash_gift_amount) ? v.cash_gift_amount : 0;

									totalAmount = parseFloat(yearEndAmount) + parseFloat(cashGiftAmount);
									// ===== COMPUTATION =====

									// ===== SUB TOTAL COMPUTATION =====

									subDivisionAmount += parseFloat(totalAmount);

								});

								netDivisionAmount += parseFloat(subDivisionAmount);

								sub_div_amount = (subDivisionAmount) ? commaSeparateNumber(parseFloat(subDivisionAmount).toFixed(2)) : '';

								lastrow += '<tr style="font-weight:bold;">';
								lastrow += '<td class="text-right"></td>'
								lastrow += '<td class="text-left" colspan="4">'+key+'</td>'
								lastrow += '<td class="text-right"></td>'
								lastrow += '<td class="text-right"></td>'
								lastrow += '<td class="text-right">'+sub_div_amount+'</td>'
								lastrow += '<td class="text-right" colspan="5"></td>'
								lastrow += '</tr>';

								lastrow += '<tr style="font-weight:bold;">';
								lastrow += '<td colspan="13"></td>'
								lastrow += '</tr>';
							});


						});

						net_div_amount = (netDivisionAmount) ? commaSeparateNumber(parseFloat(netDivisionAmount).toFixed(2)) : '';

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td class="text-right"></td>'
						lastrow += '<td class="text-right" colspan="4"></td>'
						lastrow += '<td class="text-right"></td>'
						lastrow += '<td class="text-right"></td>'
						lastrow += '<td class="text-right">'+net_div_amount+'</td>'
						lastrow += '<td class="text-right" colspan="5"></td>'
						lastrow += '</tr>';

						amountInWord = toWords(net_total_amount).toUpperCase();

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';

						lastrow += '<tr style="font-weight:bold;" class="text-center">';
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;" colspan="4">CERTIFIED:  Services duly rendered as stated.</td>'
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;" colspan="5">APPROVED FOR PAYMENT: '+amountInWord+'ONLY PHP ('+net_total_amount+') </td>'
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;"></td>' // NET AMOUNT
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;" class="text-center">';
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;padding-top:20px;" colspan="4">'+signatoryOne+' <br> '+positionOne+'</td>'
						lastrow += '<td style="border:none;" colspan="8"></td>'
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;" class="text-center">';
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;padding-top:20px;" colspan="4">CERTIFIED:  Supporting documents complete and proper; and cash available in the amount of PHP '+net_total_amount+'</td>'
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;" colspan="5">'+signatoryTwo+' <br> '+positionTwo+'</td>'
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;"></td>' // NET AMOUNT
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;" class="text-center">';
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;padding-top:20px;" colspan="4">'+signatoryThree+' <br> '+positionThree+'</td>'
						lastrow += '<td style="border:none;" colspan="8"></td>'
						lastrow += '</tr>';

						body += '</table></div></div>';

						$('#payroll_transfer').html(body);
						$('#lastrow').after(lastrow)

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection