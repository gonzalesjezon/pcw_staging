<div class="row pl-2 pr-2">
	<div class="col-md-6">
		<span class="font-weight-bold">Signatory One</span>
		<div class="form-group">
			<select class="form-control select2" id="signatory_one">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{!! @$employee->employeeinformation->positions->Name !!}" >{!! @$employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-md-6">
		<span class="font-weight-bold">Signatory Two</span>
		<div class="form-group">
			<select class="form-control select2" id="signatory_two">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{!! @$employee->employeeinformation->positions->Name !!}" >{!! @$employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>

<div class="row pl-2">
	<div class="col-md-6">
		<span class="font-weight-bold">Signatory Three</span>
		<div class="form-group">
			<select class="form-control select2" id="signatory_three">
				<option></option>
				@foreach($employees as $employee)
				<option data-position="{!! @$employee->employeeinformation->positions->Name !!}" >{!! @$employee->getFullName() !!}</option>
				@endforeach
			</select>
		</div>
	</div>

</div>