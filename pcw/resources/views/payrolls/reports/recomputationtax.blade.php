@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<style type="text/css">
	.td-style1{
		border-right: 2px solid #333;
		background-color: #e4e0e0;
	    width: 140px;
	    font-size: 12px;
	    font-weight: bold;
	}
	.td-style2{
		border-right: 2px solid #333;
	    font-size: 12px;
	    font-weight: bold;
	}
	.td-style3{
		background-color: #e4e0e0;
		font-weight: bold;
		font-size: 14px;
	}
	.table-style1{
		width: 400px;
	    border: 2px solid #333;
	    margin-top: 20px;
	}
	.table-style2{
	    border: 2px solid #333;
	    margin-top: 20px;
	}
	.table-padding2>tbody>tr>td{
		padding: 4px !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row pl-4 pr-4">
					@include('payrolls.includes._months-year')
				</div>
				<!-- @include('payrolls.reports.includes._signatory') -->
			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="width: 960px;margin: auto;">
	       			<div class="col-md-12">
	       				<table class="table" style=" width: 960px;border: 2px solid #333;" class="table">
	       					<thead>
	       						<tr>
	       							<td rowspan="3">
	       								<img src="{{ url('images/reportlogo.png') }}" style="height: 80px;">
	       							</td>
	       							<td rowspan="3" class="text-center">
	       								<h3>Recomputation of Tax</h3>
	       							</td>
	       							<td colspan="3">Document Code:</td>
	       						</tr>
	       						<tr>
	       							<td colspan="3">Date: <span id="print_date"></span></td>
	       						</tr>
	       						<tr>
	       							<td>Revision No</td>
	       							<td colspan="2">Page</td>
	       						</tr>
	       					</thead>
						</table>
						<table class="table table-padding2 " style=" width: 960px;border: 2px solid #333;margin-top: 20px;" class="table">
	       					<tbody>
	       						<tr>
	       							<td class="td-style1">Year</td>
	       							<td><span id="for_year"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style1">Employee Name:</td>
	       							<td><span id="fullname"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style1">Employee No:</td>
	       							<td>
	       								<span id="employee_number"></span>
	       							</td>
	       						</tr>
	       						<tr>
	       							<td class="td-style1">Office/Division:</td>
	       							<td><span id="division"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style1">Position:</td>
	       							<td><span id="position"></span></td>
	       						</tr>
	       					</tbody>
						</table>
						<table class="table table-padding2" style=" width: 650px;border: 2px solid #333;margin-top: 20px;" class="table">
	       					<tbody>
	       						<tr>
	       							<td class="text-center td-style3" >COMPENSATION AND OTHER BENEFITS</td>
	       							<td class="text-center td-style3" >PER MONTH </td>
	       							<td class="text-center td-style3" >PER YEAR</td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">Salary (Jan – Dec)</td>
	       							<td class="text-right"><span id="monthly_basic"></span></td>
	       							<td class="text-right"><span id="annual_basic"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">Overtime</td>
	       							<td class="text-right"><span id="monthly_overtime"></span></td>
	       							<td class="text-right"><span id="annual_overtime"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">Taxable 12th Month</td>
	       							<td class="text-right"><span id="monthly_12month"></span></td>
	       							<td class="text-right"><span id="annual_12month"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">Mid Year</td>
	       							<td class="text-right"><span id="monthly_midyear"></span></td>
	       							<td class="text-right"><span id="annual_midyear"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">Year End</td>
	       							<td class="text-right"><span id="monthly_yearend"></span></td>
	       							<td class="text-right"><span id="annual_yearend"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">PBB</td>
	       							<td class="text-right"><span id="monthly_pbb"></span></td>
	       							<td class="text-right"><span id="annual_pbb"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">PEI</td>
	       							<td class="text-right"><span id="monthly_pei"></span></td>
	       							<td class="text-right"><span id="annual_pei"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">CNA</td>
	       							<td class="text-right"><span id="monthly_cna"></span></td>
	       							<td class="text-right"><span id="annual_cna"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-right">TOTAL</td>
	       							<td class="text-right"><span id="monthly_compensation"></span></td>
	       							<td class="text-right"><span id="annual_compensation"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="text-center td-style3" >ALLOWABLE DEDUCTIONS</td>
	       							<td class="text-center td-style3" >PER MONTH </td>
	       							<td class="text-center td-style3" >PER YEAR</td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">GSIS  (Jan – Dec)</td>
	       							<td class="text-right"><span id="monthly_gsis"></span></td>
	       							<td class="text-right"><span id="annual_gsis"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">HDMF  (Jan – Dec)</td>
	       							<td class="text-right"><span id="monthly_hdmf"></span></td>
	       							<td class="text-right"><span id="annual_hdmf"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">PHIC  (Jan – Dec)</td>
	       							<td class="text-right"><span id="monthly_phic"></span></td>
	       							<td class="text-right"><span id="annual_phic"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2">EA  (Jan – Dec)</td>
	       							<td class="text-right"><span id="monthly_ea"></span></td>
	       							<td class="text-right"><span id="annual_ea"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-right">TOTAL</td>
	       							<td class="text-right"><span id="monthly_deductions"></span></td>
	       							<td class="text-right"><span id="annual_deductions"></span></td>
	       						</tr>
	       					</tbody>
						</table>
						<table class="table table-padding2" style=" width: 650px;border: 2px solid #333;margin-top: 20px;" class="table">
	       					<tbody>
	       						<tr>
	       							<td class="td-style2 text-right" style="width: 532px;">TOTAL TAXABLE INCOME </td>
	       							<td class="text-right"><span id="total_taxable_income"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-right">LESS PERSONAL EXEMPTIONS</td>
	       							<td class="text-right"><span id="less_personal_exemptions"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-right">AMOUNT SUBJECT TO TAX </td>
	       							<td class="text-right"><span id="subject_to_tax"></span></td>
	       						</tr>
	       					</tbody>
						</table>
						<table class="table table-style1 table-padding2" style="width: 650px;">
	       					<tbody>
	       						<tr>
	       							<td class="text-center td-style3" ></td>
	       							<td class="text-center td-style3" >PER MONTH</td>
	       							<td class="text-center td-style3" >PER YEAR</td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-left">TFirst 400,000</td>
	       							<td class="text-right"><span id="first_per_month"></span></td>
	       							<td class="text-right"><span id="first_per_annual"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-left">Excess 25%</td>
	       							<td class="text-right"><span id="excess_month"></span></td>
	       							<td class="text-right"><span id="excess_annual"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-right" colspan="2">TAX DUE</td>
	       							<td class="text-right"><span id="taxdue_annual"></span></td>
	       						</tr>
	       					</tbody>
						</table>
						<table class="table table-style1 table-padding2" style="width: 650px;">
	       					<tbody>
	       						<tr>
	       							<td class="text-left" style="width: 221px;">Tax January 2018 </td>
	       							<td class="text-right" style="width: 224px;"><span class="tax_january"></span></td>
	       							<td class="text-right" ><span class="tax_january"></span></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-left" style="font-weight: bold;"><span style="padding-left: 15px;">FEB-APRIL</span></td>
	       							<td class="text-right"></td>
	       							<td class="text-right"></td>
	       						</tr>
	       						<tr>
	       							<td class="td-style2 text-left" style="font-weight: bold;"><span style="padding-left: 15px;">OT</span></td>
	       							<td class="text-right"></td>
	       							<td class="text-right"></td>
	       						</tr>
	       						<tr>
	       							<td class="text-left" style="font-weight: bold;">TAX STILL DUE </td>
	       							<td class="text-right" ></td>
	       							<td class="text-right" ></td>
	       						</tr>
	       						<tr>
	       							<td class="text-left" style="font-weight: bold;">TAX EFFECTIVE MAY 2018 </td>
	       							<td class="text-right" ></td>
	       							<td class="text-right" ></td>
	       						</tr>
	       					</tbody>
						</table>
						<div class="row" style="margin-left: -5px;margin-top: 25px;">
							<div class="col-md-4">
								<p style="color: #333;font-weight: bold;">Prepared By</p>
							</div>
							<div class="col-md-4">
								<p style="color: #333;font-weight: bold;">Noted By</p>
							</div>
						</div>
						<div class="row" style="margin-left: -5px;">
							<div class="col-md-4">
								<p style="color: #333;">FULLNAME <br> Position</p>
							</div>
							<div class="col-md-4">
								<p style="color: #333;">FULLNAME <br> Position</p>
							</div>
						</div>
						<div class="row" style="margin-left: -5px;margin-top: 25px;">
							<div class="col-md-12">
								<span style="font-size: 10px;padding-left: 15px;">
									___________________________ <br>
									1 Given to/Could be requested by employees whose basic salary increased due to promotion or getting the new step increment.
								</span>
							</div>
						</div>
						<table class="table table-style2 table-padding2">
	       					<tbody>
	       						<tr>
	       							<td style="font-size: 8px;">
	       								The only CONTROLLED copy of this template is the online version maintained in the Fileserver. The user must ensure that this or any other copy of a controlled document is current and complete prior to use. The MASTER copy of this document is with the Information Communication Technology Section (ICTS) of Corporate Affairs and Information Resource Management Division (CAIRMD). This document is UNCONTROLLED when downloaded and printed.
	       							</td>
	       						</tr>
	       					</tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){
		$('#btnModal').trigger('click');
		// if(!_Year && !_Month){
		// 	swal({
		// 		  title: "Select year and month first",
		// 		  type: "warning",
		// 		  showCancelButton: false,
		// 		  confirmButtonClass: "btn-danger",
		// 		  confirmButtonText: "Yes",
		// 		  closeOnConfirm: false
		// 	});
		// }else{
		// 	$.ajax({
		// 		url:base_url+module_prefix+module+'/show',
		// 		data:{
		// 			'month':_Month,
		// 			'year':_Year,
		// 			'employee_id':_empid,
		// 		},
		// 		type:'GET',
		// 		dataType:'JSON',
		// 		success:function(data){

		// 			if(data.transaction !== null){

		// 				body = [];

		// 				firstname = (data.transaction.employees) ? data.transaction.employees.firstname : ''
		// 				lastname = (data.transaction.employees) ? data.transaction.employees.lastname : ''
		// 				middlename = (data.transaction.employees.middlename) ? data.transaction.employees.middlename : ''
		// 				employee_number = (data.transaction.employee_number) ? data.transaction.employee_number : ''
		// 				position = (data.transaction.positions) ? data.transaction.positions.Name : ''
		// 				office = (data.transaction.offices) ? data.transaction.offices.Name : ''
		// 				division = (data.transaction.divisions) ? data.transaction.divisions.Name : '';

		// 				fullname = lastname+' '+firstname+' '+middlename;

		// 				basicAmount = (data.transaction.salaryinfo) ? data.transaction.salaryinfo.salary_new_rate : 0;
		// 				peraAmount = (data.transaction.benefit_transactions) ? data.transaction.benefit_transactions.amount : 0;
		// 				gsisAmount = (data.transaction.employeeinfo) ? data.transaction.employeeinfo.gsis_contribution : 0;
		// 				pagibigAmount = (data.transaction.employeeinfo) ? data.transaction.employeeinfo.pagibig_contribution : 0;
		// 				philhealthAmount = (data.transaction.employeeinfo) ? data.transaction.employeeinfo.philhealth_contribution : 0;
		// 				taxAmount = (data.transaction.employeeinfo) ? data.transaction.employeeinfo.tax_contribution : 0;
		// 				pagibigPersonalAmount = (data.transaction.employeeinfo) ? data.transaction.employeeinfo.pagibig_personal : 0;
		// 				pagibig2Amount = (data.transaction.employeeinfo) ? data.transaction.employeeinfo.pagibig2 : 0;

		// 				totalEarningsAmount = parseFloat(basicAmount) + parseFloat(peraAmount);


		// 				basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
		// 				pera_amount = (peraAmount) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
		// 				total_earnings_amount = (totalEarningsAmount) ? commaSeparateNumber(parseFloat(totalEarningsAmount).toFixed(2)) : '';
		// 				tax_amount = (taxAmount) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
		// 				pagibig_amount = (pagibigAmount) ? commaSeparateNumber(parseFloat(pagibigAmount).toFixed(2)) : '';
		// 				pagibig_personal_amount = (pagibigPersonalAmount) ? commaSeparateNumber(parseFloat(pagibigPersonalAmount).toFixed(2)) : '';
		// 				pagibig2_amount = (pagibig2Amount) ? commaSeparateNumber(parseFloat(pagibig2Amount).toFixed(2)) : '';
		// 				philhealth_amount = (philhealthAmount) ? commaSeparateNumber(parseFloat(philhealthAmount).toFixed(2)) : '';
		// 				gsis_amount = (gsisAmount) ? commaSeparateNumber(parseFloat(gsisAmount).toFixed(2)) : '';


		// 				$('#fullname').text(fullname);
		// 				$('#employee_number').text(employee_number);
		// 				$('#position').text(position);
		// 				$('#division').text(office+' / '+division);

		// 				$('#basic_amount').text(basic_amount);
		// 				$('#gross_amount').text(basic_amount);
		// 				$('#pera_amount').text(pera_amount);
		// 				$('#total_earning_amount').text(total_earnings_amount);


		// 				  // =========== //
		// 				 // DEDUCTIONS  //
		// 				// =========== //

		// 				body += '<tr>';
  //  						body +=	'<td colspan="2" class="text-center" style="background-color: #e4e0e0;font-weight: bold;font-size: 14px;">DEDUCTIONS</td></tr>';
  //  						body += '<tr>'
  //  						body += '<td class="td-style2">BIR Withholding Tax </td>';
  //  						body +=	'<td class="text-right"><span id="wtax_amount">'+tax_amount+'</span></td>';
  //  						body += '</tr>';
  //  						body += '<tr>'
  //  						body += '<td class="td-style2">GSIS Life & Ret. Contribution</td>';
  //  						body +=	'<td class="text-right"><span id="gsis_amount">'+gsis_amount+'</span></td>';
  //  						body += '</tr>';
  //  						totalGsisLoansAmount = 0;
  //  						$.each(data.transaction.gsisloans,function(k,v){
  //  							gsisLoanAmount = (v.amount) ? v.amount : 0;
  //  							totalGsisLoansAmount += parseFloat(gsisLoanAmount);
  //  							gsis_loan_amount = (gsisLoanAmount) ? commaSeparateNumber(parseFloat(gsisLoanAmount).toFixed(2)) : '';
  //  							body += '<tr>';
	 //   						body +=	'<td class="td-style2"><span style="margin-left:10px;">'+v.loans.name+'</span></td>';
	 //   						body +=	'<td class="text-right">'+gsis_loan_amount+'</td>';
	 //   						body += '</tr>';
  //  						});
  //  						body += '<tr>';
  //  						body +=	'<td class="td-style2">HDMF Contribution</td>';
  //  						body +=	'<td class="text-right"><span id="hdmf_cont_amount">'+pagibig_amount+'</span></td>';
  //  						body += '</tr>';

  //  						body += '<tr>';
  //  						body +=	'<td class="td-style2">Pagibig Personal</td>';
  //  						body +=	'<td class="text-right"><span>'+pagibig_personal_amount+'</span></td>';
  //  						body += '</tr>';

  //  						body += '<tr>';
  //  						body +=	'<td class="td-style2">MP2</td>';
  //  						body +=	'<td class="text-right"><span>'+pagibig2_amount+'</span></td>';
  //  						body += '</tr>';

  //  						totalPagibigLoansAmount = 0;
  //  						$.each(data.transaction.pagibigloans,function(k,v){
  //  							pagibigLoanAmount = (v.amount) ? v.amount : 0;
  //  							totalPagibigLoansAmount += parseFloat(pagibigLoanAmount);
  //  							pagibig_loan_amount = (pagibigLoanAmount) ? commaSeparateNumber(parseFloat(pagibigLoanAmount).toFixed(2)) : '';
  //  							body += '<tr>';
	 //   						body +=	'<td class="td-style2">'+v.loans.name+'</td>';
	 //   						body +=	'<td class="text-right">'+pagibig_loan_amount+'</td>';
	 //   						body += '</tr>';
  //  						});

  //  						totalDeductionAmount = parseFloat(taxAmount) + parseFloat(pagibigAmount) + parseFloat(philhealthAmount) + parseFloat(gsisAmount) + parseFloat(totalGsisLoansAmount) + parseFloat(totalPagibigLoansAmount) + parseFloat(pagibig2Amount) + parseFloat(pagibigPersonalAmount);

  //  						total_deduction_amount = (totalDeductionAmount) ? commaSeparateNumber(parseFloat(totalDeductionAmount).toFixed(2)) : '';
  //  						body += '<tr>';
  //  						body += '<td class="td-style2">PHILHEALTH Contribution</td>';
  //  						body +=	'<td class="text-right"><span id="philhealth_cont_amount">'+philhealth_amount+'</span></td>';
  //  						body += '</tr>';
  //  						body += '<tr>';
  //  						body +=	'<td class="td-style2 text-right">TOTAL DEDUCTIONS</td>';
  //  						body +=	'<td class="text-right"><span>'+total_deduction_amount+'</span></td>'
  //  						body += '</tr>';

  //  						monthlyPayAmount = parseFloat(totalEarningsAmount) - parseFloat(totalDeductionAmount);

  //  						monthly_pay_amount = (monthlyPayAmount) ? commaSeparateNumber(parseFloat(monthlyPayAmount).toFixed(2)) : '';

  //  						$('#monthly_pay_amount').text(monthly_pay_amount);

		// 				$('#deduction_list').html(body);

		// 				$('#pay_period').text(_Month+' '+_Year);
		// 				printDate = data.print_date;
		// 				$('#print_date').text(printDate);

		// 				$('#btnModal').trigger('click');


		// 			}else{
		// 				swal({
		// 					title: "No Records Found",
		// 					type: "warning",
		// 					showCancelButton: false,
		// 					confirmButtonClass: "btn-danger",
		// 					confirmButtonText: "Yes",
		// 					closeOnConfirm: false
		// 				});
		// 			}
		// 		}
		// 	})
		// }
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection