@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">
<style type="text/css">
	.table2>thead>tr>td{
		vertical-align: middle;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row p-2">
					<div class="col-md-6">
						<span class="font-weight-bold">Select Employee Status</span>
						<div class="form-group mb-0 pb-0">
							<select class="form-control select2" name="employee_status" id="employee_status">
							<option value=""></option>
							<option value="permanent">Permanent</option>
							<option value="project">Project</option>
						</select>
						</div>
					</div>
				</div>
				@include('payrolls.includes._months-year')
				@include('payrolls.reports.includes._signatory')
			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid payroll_transfer" id="reports">
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	var _status;
	$(document).on('change','#employee_status',function(){
		_status = "";
		_status = $(this).find(':selected').val();

	});


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'status':_status,
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					loanCount = (data.loancount) ? data.loancount : 0;
					deductionCount = (data.deductioncount) ? data.deductioncount : 0;

					if(data.transaction.length !== 0){
						body = [];
						ctr  = 1;
						ctr2 = 0;

						arr_loan = [];
						arr_deduction = [];
						arr_subtotal_loan = [];
						arr_subtotal_deduction = [];

						count1 = (parseFloat(loanCount) + parseFloat(deductionCount) + 7);

						netBasicAmount 			= 0;
						netPeraAmount 			= 0;
						netGsisContAmount 		= 0;
						netPagibigContAmount 	= 0;
						netPhilhealthContAmount = 0;
						netPagibig2Amount 		= 0;
						netTaxContAmount 		= 0;
						netGrossPayAmount 		= 0;
						netTotalDeductionAmount = 0;
						netNetAmount 			= 0;
						netFirstHalf 			= 0;
						netSecondHalf 			= 0;
						netPersonalShareAmount  = 0;
						netAddDeduction 		= 0;
						netSalaryDiffAmount 	= 0;
						netAddAdj 				= 0;

						ctr3 = 0;
						$.each(data.transaction,function(key,val){
							$.each(val,function(k1,v1){
								ctr3++;
							})
						})

						coveredPeriod = months[_Month]+' '+_Year;


						printDate = data.print_date;

						var docCode;
						switch(_status){
							case 'project':
								docCode = 'HRDMS-R-005';
								break;
							default:
								docCode = 'HRDMS-R-007';
								break;
						}


						body += '<div class="row">';
						body += '<div class="col-md-12">';
						body += '<table class="table table2">';
						body += '<thead>';
						body +=	'<tr>';
   						body +=	'<td rowspan="3" style="border-right:none;" colspan="2" ><img src="{{ url("images/reportlogo.png") }}" style="height: 80px;"></td>';
   						body +=	'<td rowspan="3" class="text-center" colspan="'+(parseInt(count1)+13)+'" style="border-left:none;">';
   						body +=	'<h3>GENERAL PAYROLL</h3>';
   						body +=	'<h4>For the month of <span id="month_year">'+coveredPeriod+'</span></h4>';
   						body +=	'</td>';
   						body +=	'<td colspan="3">Document Code: '+docCode+'</td>';
   						body += '</tr>';
   						body += '<tr>';
   						body += '<td colspan="3">Date: '+printDate+'</td>';
   						body += '</tr>';
   						body += '<tr>';
   						body += '<td>Revision No</td>';
   						body += '<td colspan="2">Page</td>';
   						body += '</tr>';
						body += '<tr class="text-center" style="font-weight:bold;">';
						body += '<td rowspan="2" >NO</td>';
						body += '<td rowspan="2">NAME OF EMPLOYEE</td>';
						body += '<td rowspan="2" >POSITION</td>';
						body += '<td rowspan="2">EMPLOYEE NO.</td>';
						body += '<td rowspan="2">ACCOUNT NO.</td>';
						body += '<td rowspan="2">SG-STEP</td>';
						body += '<td rowspan="2">MONTHLY SALARY</td>';
						body += '<td colspan="2" >OTHER COMPENSATION</td>';
						body += '<td colspan="2" >NEGATIVE</td>';
						body += '<td rowspan="2" >GROSS AMOUNT EARNED</td>';
						body += '<td rowspan="2" >TOTAL DEDUCTIONS</td>';
						body += '<td colspan="'+count1+'" ></td>';
						body += '<td rowspan="2" >ADDITIONAL <br> DEDUCTIONS <br> (TAX/ <br> PHIC/ <br> GSIS/ </br>HDMF)</td>';
						body += '<td rowspan="2" >NET AMT. RECEIVED</td>';
						body += '<td rowspan="2" >01 - 15</td>';
						body += '<td rowspan="2" >16 - 31</td>';
						body += '<td rowspan="2" >TOTAL</td>';
						body += '</tr>';
						body += '<tr class="text-center" style="font-weight:bold;">';
						body += '<td>PERA</td>';
						body += '<td>SALARY DIFF.</td>';
						body += '<td>SALARY</td>';
						body += '<td>PERA</td>';
						body += '<td>412( BIR WITHHOLDING TAX PAYABLE)</td>';
						body += '<td>413A(GSIS LIFE AND RETIREMENT CONTRIBUTION)</td>';
						if(data.loanlists.length !== 0){
							$.each(data.loanlists,function(k1,v1){
								body += '<td>'+v1.loans.name+'</td>';
								arr_loan[v1.loan_id+'_id'] = v1.loan_id;
								arr_subtotal_loan[v1.loan_id+'_id']  = 0;
							})
						}
						body += '<td> 414A <br> (PAG-IBIG CONTRIBUTION)</td>';
						body += '<td> PERSONAL SHARE</td>';
						body += '<td> MP2</td>';
						body += '<td> 415 <br> (PHILHEALTH)</td>';
						if(data.deductionlists.length !== 0){
							$.each(data.deductionlists,function(k1,v1){
								body += '<td>'+v1.deductions.name+'</td>';
								arr_deduction[v1.deduction_id+'_id'] = v1.deduction_id;
								arr_subtotal_deduction[v1.deduction_id+'_id']  = 0;
							})
						}
						body += '<td>EA Loans, <br> MEDICARD etc. </td>';
						body += '</tr>';
						body += '</thead>';

						$.each(data.transaction,function(key,val){


							subBasicAmount  		= 0;
							subPeraAmount  			= 0;
							subGsisContAmount  		= 0;
							subPagibigContAmount  	= 0;
							subPhilhealthContAmount = 0;
							subPagibig2Amount  		= 0;
							subTaxContAmount  		= 0;
							subGrossPayAmount  		= 0;
							subTotalDeductionAmount = 0;
							subNetAmount  			= 0;
							subFirstHalf 			= 0;
							subSecondHalf 			= 0;
							subPersonalShareAmount  = 0;
							subAddDeduction 		= 0;
							subSalaryDiffAmount 	= 0;
							subAddAdj 				= 0;


							body += '<tbody>';
							$.each(val,function(k,v){

								own_loan_id 			= 0;
								own_deduction_id 		= 0;

								// ======== BODY =======

								var lwopPeraAmount = 0;
								var lwopAmount 	   = 0;
								var lessGrossBasicAmount = 0;
								var lessGrossPeraAmount = 0;

								lastname 			= (v.employees) ? v.employees.lastname : '';
								firstname 			= (v.employees) ? v.employees.firstname : '';
								middlename 			= (v.employees.middlename) ? v.employees.middlename : '';
								fullname 			= lastname+' '+firstname+' '+middlename;
								position 			= (v.positions) ? v.positions.Name : '';
								employee_number 	= (v.employees.employee_number) ? v.employees.employee_number : '';
								account_number 		= (v.employeeinfo) ? v.employeeinfo.account_number : '';
								step_inc 			= (v.salaryinfo.step_inc) ? v.salaryinfo.step_inc : '';
								// jobGrade = (v.salaryinfo.salarygrade.Code) ? v.salaryinfo.salarygrade.Code : '';
								salarygrade 		= (v.salaryinfo.salarygrade_id) ? v.salaryinfo.salarygrade.Code : '';

								basicAmount 		= (v.actual_basicpay_amount) ? v.actual_basicpay_amount : 0;
								peraAmount 			= (v.benefitinfo) ? v.benefitinfo.benefit_amount : 0;
								gsisContAmount 		= (v.gsis_ee_share) ? v.gsis_ee_share : 0;
								personalShareAmount = (v.mp1_amount) ? v.mp1_amount : 0;
								pagibigContAmount 	= (v.pagibig_share) ? v.pagibig_share : 0;
								philhealthContAmount = (v.phic_share) ? v.phic_share : 0;
								pagibig2Amount 				= (v.mp2_amount) ? v.mp2_amount : 0;
								taxContAmount 				= (v.tax_amount) ? v.tax_amount : 0;
								salaryDiffAmount 			= (v.salary_diff_amount) ? v.salary_diff_amount : 0;
								totalLoanAmount 			= (v.total_loan) ? v.total_loan : 0;
								totalOtherDeductionAmount 	= (v.total_otherdeduct) ? v.total_otherdeduct : 0;
								hasAbsent 					= (v.actual_absences) ? v.actual_absences : 0;

								addDeduction = 0;
								if(v.deductioninfo_transaction.length > 0){
									$.each(v.deductioninfo_transaction,function(k2,v2){
										if(v2.not_include === 1){
											addDeduction += parseFloat(v2.amount);
										}
									})
								}

								addAdjAmount = 0;
								if(v.deductioninfo_transaction.length > 0){
									$.each(v.deductioninfo_transaction,function(k2,v2){
										if(v2.not_include === 2){
											addAdjAmount += parseFloat(v2.amount);
										}
									})
								}

								// ===== COMPUTATION =====

								totalPagibigAmount = parseFloat(pagibigContAmount) + parseFloat(pagibig2Amount) + parseFloat(personalShareAmount);

								if(hasAbsent){
									lwopPeraAmount = (v.benefit_transactions) ? v.benefit_transactions.amount : 0;
									lwopAmount = (v.total_absences_amount) ? v.total_absences_amount : 0;
									lessGrossBasicAmount = parseFloat(basicAmount) - parseFloat(lwopAmount);
									lessGrossPeraAmount = parseFloat(peraAmount) - parseFloat(lwopPeraAmount);
								}

								grossPayAmount = (parseFloat(basicAmount) + parseFloat(peraAmount) + parseFloat(salaryDiffAmount) - parseFloat(lwopAmount));

								totalDeductionAmount = (parseFloat(gsisContAmount) + parseFloat(totalPagibigAmount) + parseFloat(totalLoanAmount) + parseFloat(taxContAmount) + parseFloat(philhealthContAmount) + parseFloat(totalOtherDeductionAmount));

								netAmount = (parseFloat(grossPayAmount) - parseFloat(totalDeductionAmount));

								dividedNet = parseFloat(netAmount) / 2;
								// FIRST HALF
								firstHalf = parseInt(dividedNet).toFixed(2);

								// SECOND HALF
								getDecimal = parseFloat(dividedNet).toFixed(2).split('.');
								getDecimal = parseFloat(getDecimal[1])/100;
								secondHalf = (parseFloat(dividedNet) + getDecimal);
								secondHalf = Math.round(secondHalf * 100) /100;

								// ===== COMPUTATION =====

								// ===== SUB TOTAL COMPUTATION =====

								subBasicAmount += parseFloat(basicAmount);
								subPeraAmount += parseFloat(peraAmount);
								subGsisContAmount += parseFloat(gsisContAmount);
								subPagibigContAmount += parseFloat(pagibigContAmount);
								subPhilhealthContAmount += parseFloat(philhealthContAmount);
								subPagibig2Amount += parseFloat(pagibig2Amount);
								subPersonalShareAmount += parseFloat(personalShareAmount);
								subTaxContAmount += parseFloat(taxContAmount);
								subGrossPayAmount += parseFloat(grossPayAmount);
								subTotalDeductionAmount += parseFloat(totalDeductionAmount);
								subNetAmount += parseFloat(netAmount);
								subFirstHalf += parseFloat(firstHalf);
								subSecondHalf += parseFloat(secondHalf);
								subAddDeduction += parseFloat(addDeduction);
								subSalaryDiffAmount += parseFloat(salaryDiffAmount);
								subAddAdj += parseFloat(addAdjAmount);
								// ===== SUB TOTAL COMPUTATION =====

								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
								basic_salary_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '0.00';
								pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '0.00';
								gross_earned_amount = (grossPayAmount !== 0) ? commaSeparateNumber(parseFloat(grossPayAmount).toFixed(2)) : '0.00';
								gsis_cont_amount = (gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(gsisContAmount).toFixed(2)) : '0.00';
								pagibig2_amount = (pagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(pagibig2Amount).toFixed(2)) : '0.00';
								tax_cont_amount = (taxContAmount !== 0) ? commaSeparateNumber(parseFloat(taxContAmount).toFixed(2)) : '0.00';
								total_deduction_amount = (totalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(totalDeductionAmount).toFixed(2)) : '0.00';
								net_amount = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '0.00';
								lwop_amount = (lwopAmount !== 0) ? commaSeparateNumber(parseFloat(lwopAmount).toFixed(2)) : '0.00';
								philhealth_cont_amount = (philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(philhealthContAmount).toFixed(2)) : '0.00';
								pagibig_cont_amount = (pagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(pagibigContAmount).toFixed(2)) : '0.00';

								total_pagibig = (totalPagibigAmount !== 0) ? commaSeparateNumber(parseFloat(totalPagibigAmount).toFixed(2)) : '0.00';
								less_gross_basic = (lessGrossBasicAmount !== 0) ? commaSeparateNumber(parseFloat(lessGrossBasicAmount).toFixed(2)) : '0.00';
								lwop_pera_amount = (lwopPeraAmount !== 0) ? commaSeparateNumber(parseFloat(lwopPeraAmount).toFixed(2)) : '0.00';
								less_gross_pera_amount = (lessGrossPeraAmount !== 0) ? commaSeparateNumber(parseFloat(lessGrossPeraAmount).toFixed(2)) : '0.00';
								first_half = (firstHalf !== 0) ? commaSeparateNumber(parseFloat(firstHalf).toFixed(2)) : '0.00';
								second_half = (secondHalf !== 0) ? commaSeparateNumber(parseFloat(secondHalf).toFixed(2)) : '0.00';
								personal_share_amount = (personalShareAmount !== 0) ? commaSeparateNumber(parseFloat(personalShareAmount).toFixed(2)) : '0.00';
								add_deduction = (addDeduction !== 0) ? commaSeparateNumber(parseFloat(addDeduction).toFixed(2)) : '0.00';
								salary_diff_amount = (salaryDiffAmount !== 0) ? commaSeparateNumber(parseFloat(salaryDiffAmount).toFixed(2)) : '0.00';
								add_adjustment = (addAdjAmount !== 0) ? commaSeparateNumber(parseFloat(addAdjAmount).toFixed(2)) : '0.00';
								account_number = (account_number !== null) ? account_number : '';
								salarygrade = (salarygrade !== null) ? salarygrade : '';
								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
								body += '<tr>';
								body += '<td>'+ctr+'</td>'
								body += '<td nowrap>'+fullname.toUpperCase()+'</td>'
								// body += '<td>'+jobGrade+'</td>'
								body += '<td nowrap>'+position+'</td>'
								body += '<td class="text-center">'+employee_number+'</td>'
								body += '<td class="text-center" nowrap>'+account_number+'</td>'
								body += '<td class="text-center" nowrap>'+salarygrade+' - '+step_inc+'</td>'
								body += '<td  class="text-right">'+basic_salary_amount+'</td>'
								body += '<td  class="text-right">'+pera_amount+'</td>'
								body += '<td class="text-right">'+salary_diff_amount+'</td>' // SALARY DIFFERENTIAL
								body += '<td class="text-right">'+less_gross_basic+'</td>' // SALARY
								body += '<td class="text-right">'+lwop_pera_amount+'</td>' // PERA
								body += '<td  class="text-right">'+gross_earned_amount+'</td>' // GROSS EARNED AMOUNT
								body += '<td  class="text-right">'+total_deduction_amount+'</td>' // TOTAL DEDUCTIONS
								body += '<td class="text-right">'+tax_cont_amount+'</td>' // tax amount
								body += '<td class="text-right">'+gsis_cont_amount+'</td>' // gsis cont amount
								if(data.loanlists.length !== 0){
									$.each(data.loanlists,function(k2,v2){
										loan = [];
										loan_dispay = [];
										loan_id = v2.loan_id;
										$.each(v.loaninfo_transaction,function(k3,v3){
											own_loan_id = v3.loan_id;
											loan_dispay['loan_amount_'+own_loan_id] = v3.amount;
										});

										if(loan_id){
											if (loan_dispay['loan_amount_'+loan_id]) {
												arr_subtotal_loan[loan_id+'_id'] += parseFloat(loan_dispay['loan_amount_'+loan_id]);
											}
											loan_dispay['loan_amount_'+loan_id] = (loan_dispay['loan_amount_'+loan_id]) ? commaSeparateNumber(parseFloat(loan_dispay['loan_amount_'+loan_id]).toFixed(2)) : '0.00';
											body += '<td class="text-right" >'+loan_dispay['loan_amount_'+loan_id]+'</td>'
										}else{
											body += '<td class="text-right"></td>';
										}

									});
								}


								body += '<td class="text-right">'+pagibig_cont_amount+'</td>'
								body += '<td class="text-right">'+personal_share_amount+'</td>'
								body += '<td class="text-right">'+pagibig2_amount+'</td>'
								body += '<td class="text-right">'+philhealth_cont_amount+'</td>';
								if(data.deductionlists.length !== 0){
									$.each(data.deductionlists,function(k2,v2){
										deduction = [];
										deduction_dispay = [];
										deduction_id = v2.deduction_id;
										$.each(v.deductioninfo_transaction,function(k3,v3){
											own_deduction_id = v3.deduction_id;
											deduction_dispay['deduction_amount_'+own_deduction_id] = v3.amount;
										});

										if(deduction_id){
											if (deduction_dispay['deduction_amount_'+deduction_id]) {
												arr_subtotal_deduction[deduction_id+'_id'] += parseFloat(deduction_dispay['deduction_amount_'+deduction_id]);
											}
											deduction_dispay['deduction_amount_'+deduction_id] = (deduction_dispay['deduction_amount_'+deduction_id]) ? commaSeparateNumber(parseFloat(deduction_dispay['deduction_amount_'+deduction_id]).toFixed(2)) : '0.00';
											body += '<td class="sub_total text-right">'+deduction_dispay['deduction_amount_'+deduction_id]+'</td>'
										}else{
											body += '<td class="text-right"></td>'
										}
									});
								}

								body += '<td class="text-right">'+add_deduction+'</td>' // EA Loans, Medicard etc.
								body += '<td class="text-right">'+add_adjustment+'</td>' // ADDITIONAL DEDUCTIONS
								body += '<td class="text-right">'+net_amount+'</td>' // NET AMOUNT
								body += '<td class="text-right">'+first_half+'</td>' // NET AMOUNT ONE
								body += '<td class="text-right">'+second_half+'</td>' // NET AMOUNT TWO
								body += '<td class="text-right">'+net_amount+'</td>' // NET AMOUNT
								body += '</tr>';

								ctr++;

							});

							netBasicAmount += parseFloat(subBasicAmount);
							netPeraAmount += parseFloat(subPeraAmount);
							netGsisContAmount += parseFloat(subGsisContAmount);
							netPagibigContAmount += parseFloat(subPagibigContAmount);
							netPhilhealthContAmount += parseFloat(subPhilhealthContAmount);
							netPagibig2Amount += parseFloat(subPagibig2Amount);
							netPersonalShareAmount += parseFloat(subPersonalShareAmount);
							netTaxContAmount += parseFloat(subTaxContAmount);
							netGrossPayAmount += parseFloat(subGrossPayAmount);
							netTotalDeductionAmount += parseFloat(subTotalDeductionAmount);
							netNetAmount += parseFloat(subNetAmount);
							netFirstHalf += parseFloat(subFirstHalf);
							netSecondHalf += parseFloat(subSecondHalf);
							netAddDeduction += parseFloat(subAddDeduction);
							netSalaryDiffAmount += parseFloat(subSalaryDiffAmount);
							netAddAdj += parseFloat(subAddAdj);
							// ===== SUB TOTAL COMPUTATION =====

							// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
							sub_basic_amount = (subBasicAmount !== 0) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '0.00';
							sub_pera_amount = (subPeraAmount !== 0) ? commaSeparateNumber(parseFloat(subPeraAmount).toFixed(2)) : '0.00';
							sub_gross_earned_amount = (subGrossPayAmount !== 0) ? commaSeparateNumber(parseFloat(subGrossPayAmount).toFixed(2)) : '0.00';
							sub_gsis_cont_amount = (subGsisContAmount !== 0) ? commaSeparateNumber(parseFloat(subGsisContAmount).toFixed(2)) : '0.00';
							sub_pagibig2_amount = (subPagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(subPagibig2Amount).toFixed(2)) : '0.00';
							sub_pagibig_amount = (subPagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(subPagibigContAmount).toFixed(2)) : '0.00';
							sub_philhealth_amount = (subPhilhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(subPhilhealthContAmount).toFixed(2)) : '0.00';
							sub_tax_cont_amount = (subTaxContAmount !== 0) ? commaSeparateNumber(parseFloat(subTaxContAmount).toFixed(2)) : '0.00';
							sub_deduction_amount = (subTotalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalDeductionAmount).toFixed(2)) : '0.00';
							sub_net_amount = (subNetAmount !== 0) ? commaSeparateNumber(parseFloat(subNetAmount).toFixed(2)) : '0.00';
							sub_first = (subFirstHalf !== 0) ? commaSeparateNumber(parseFloat(subFirstHalf).toFixed(2)) : '0.00';
							sub_second = (subSecondHalf !== 0) ? commaSeparateNumber(parseFloat(subSecondHalf).toFixed(2)) : '0.00';
							sub_add_deduction = (subAddDeduction !== 0) ? commaSeparateNumber(parseFloat(subAddDeduction).toFixed(2)) : '0.00';
							sub_ps_amount = (subPersonalShareAmount !== 0) ? commaSeparateNumber(parseFloat(subPersonalShareAmount).toFixed(2)) : '0.00';
							sub_salary_diff_amount = (subSalaryDiffAmount !== 0) ? commaSeparateNumber(parseFloat(subSalaryDiffAmount).toFixed(2)) : '0.00';
							sub_adj = (subAddAdj !== 0) ? commaSeparateNumber(parseFloat(subAddAdj).toFixed(2)) : '0.00';

							ctr2++;

							if(ctr2 >= ctr3){
								body += '<tr id="lastrow" style="font-weight:bold;">';
							}else{
								body += '<tr style="font-weight:bold;">';
							}

							body += '<td></td>'
							body += '<td></td>'
							body += '<td>SUB TOTAL</td>'
							body += '<td></td>'
							body += '<td></td>'
							body += '<td></td>'
							body += '<td class="text-right">'+sub_basic_amount+'</td>'
							body += '<td  class="text-right">'+sub_pera_amount+'</td>'
							body += '<td class="text-right">'+sub_salary_diff_amount+'</td>' // SALARY DIFFERENTIAL
							body += '<td class="text-right"></td>' // SALARY
							body += '<td class="text-right"></td>' // PERA
							body += '<td  class="text-right">'+sub_gross_earned_amount+'</td>' // GROSS EARNED AMOUNT
							body += '<td  class="text-right">'+sub_deduction_amount+'</td>' // TOTAL DEDUCTIONS
							body += '<td class="text-right">'+sub_tax_cont_amount+'</td>' // tax amount
							body += '<td class="text-right">'+sub_gsis_cont_amount+'</td>' // gsis cont amount
							loanId = 0;
							if(data.loanlists.length !== 0){
								for (var k in arr_loan){
								    if (typeof arr_loan[k] !== 'function') {
								    	arr_subtotal_loan[arr_loan[k]+'_id'] = (arr_subtotal_loan[arr_loan[k]+'_id'] !== 0) ? commaSeparateNumber(parseFloat(arr_subtotal_loan[arr_loan[k]+'_id']).toFixed(2)) : '0.00';
								         body += '<td  class="text-right">'+arr_subtotal_loan[arr_loan[k]+'_id']+'</td>';
								         arr_subtotal_loan[arr_loan[k]+'_id'] = 0;
								    }
								}
							}


							body += '<td class="text-right">'+sub_pagibig_amount+'</td>'
							body += '<td class="text-right">'+sub_ps_amount+'</td>'
							body += '<td class="text-right">'+sub_pagibig2_amount+'</td>'
							body += '<td class="text-right">'+sub_philhealth_amount+'</td>'
							deductionId = 0;
							if(data.deductionlists.length !== 0){
								for (var k in arr_deduction){
								    if (typeof arr_deduction[k] !== 'function') {
								    	arr_subtotal_deduction[arr_deduction[k]+'_id'] = (arr_subtotal_deduction[arr_deduction[k]+'_id']) ? commaSeparateNumber(parseFloat(arr_subtotal_deduction[arr_deduction[k]+'_id']).toFixed(2)) : '0.00';
								        body += '<td  class="text-right">'+arr_subtotal_deduction[arr_deduction[k]+'_id']+'</td>';
								        arr_subtotal_deduction[arr_deduction[k]+'_id'] = 0;
								    }
								}
							}
							body += '<td class="text-right">'+sub_add_deduction+'</td>' // EA Loans, Medicard etc.
							body += '<td class="text-right">'+sub_adj+'</td>' // ADDITIONAL DEDUCTIONS
							body += '<td class="text-right">'+sub_net_amount+'</td>' // NET AMOUNT
							body += '<td class="text-right">'+sub_first+'</td>' // NET AMOUNT ONE
							body += '<td class="text-right">'+sub_second+'</td>' // NET AMOUNT TWO
							body += '<td class="text-right">'+sub_net_amount+'</td>' // NET AMOUNT
							body += '</tr>';

						});

						// ======= GRAND TOTAL ======

						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
						net_basic_amount = (netBasicAmount !== 0) ? commaSeparateNumber(parseFloat(netBasicAmount).toFixed(2)) : '0.00';
						net_pera_amount = (netPeraAmount !== 0) ? commaSeparateNumber(parseFloat(netPeraAmount).toFixed(2)) : '0.00';
						net_gross_earned_amount = (netGrossPayAmount !== 0) ? commaSeparateNumber(parseFloat(netGrossPayAmount).toFixed(2)) : '0.00';
						net_gsis_cont_amount = (netGsisContAmount !== 0) ? commaSeparateNumber(parseFloat(netGsisContAmount).toFixed(2)) : '0.00';
						net_pagibig2_amount = (netPagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(netPagibig2Amount).toFixed(2)) : '0.00';
						net_pagibig_amount = (netPagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(netPagibigContAmount).toFixed(2)) : '0.00';
						net_philhealth_amount = (netPhilhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(netPhilhealthContAmount).toFixed(2)) : '0.00';
						net_tax_cont_amount = (netTaxContAmount !== 0) ? commaSeparateNumber(parseFloat(netTaxContAmount).toFixed(2)) : '0.00';
						net_deduction_amount = (netTotalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalDeductionAmount).toFixed(2)) : '0.00';
						net_net_amount = (netNetAmount !== 0) ? commaSeparateNumber(parseFloat(netNetAmount).toFixed(2)) : '0.00';
						net_first = (netFirstHalf !== 0) ? commaSeparateNumber(parseFloat(netFirstHalf).toFixed(2)) : '0.00';
						net_second = (netSecondHalf !== 0) ? commaSeparateNumber(parseFloat(netSecondHalf).toFixed(2)) : '0.00';
						net_add_deduction = (netAddDeduction !== 0) ? commaSeparateNumber(parseFloat(netAddDeduction).toFixed(2)) : '0.00';
						net_ps_amount = (netPersonalShareAmount !== 0) ? commaSeparateNumber(parseFloat(netPersonalShareAmount).toFixed(2)) : '0.00';
						net_salary_diff_amount = (netSalaryDiffAmount !== 0) ? commaSeparateNumber(parseFloat(netSalaryDiffAmount).toFixed(2)) : '0.00';
						net_adj = (netAddAdj !== 0) ? commaSeparateNumber(parseFloat(netAddAdj).toFixed(2)) : '0.00';

						body += '<tr style="font-weight:bold;">';
						body += '<td></td>'
						body += '<td></td>'
						body += '<td>GRAND TOTAL</td>'
						body += '<td></td>'
						body += '<td></td>'
						body += '<td></td>'
						body += '<td class="text-right">'+net_basic_amount+'</td>'
						body += '<td  class="text-right">'+net_pera_amount+'</td>'
						body += '<td class="text-right">'+net_salary_diff_amount+'</td>' // SALARY DIFFERENTIAL
						body += '<td class="text-right"></td>' // SALARY
						body += '<td class="text-right"></td>' // PERA
						body += '<td  class="text-right">'+net_gross_earned_amount+'</td>' // GROSS EARNED AMOUNT
						body += '<td  class="text-right">'+net_deduction_amount+'</td>' // TOTAL DEDUCTIONS
						body += '<td class="text-right">'+net_tax_cont_amount+'</td>' // tax amount
						body += '<td class="text-right">'+net_gsis_cont_amount+'</td>' // gsis cont amount
						if(data.loanlists.length !== 0){
							$.each(data.loanlists,function(k,v){
								body += '<td class="text-right">'+commaSeparateNumber(parseFloat(v.net_amount).toFixed(2))+'</td>';
							});
						}


						body += '<td class="text-right">'+net_pagibig_amount+'</td>'
						body += '<td class="text-right">'+net_ps_amount+'</td>'
						body += '<td class="text-right">'+net_pagibig2_amount+'</td>'
						body += '<td class="text-right">'+net_philhealth_amount+'</td>'
						if(data.deductionlists.length !== 0){
							$.each(data.deductionlists,function(k,v){
								body += '<td class="text-right">'+commaSeparateNumber(parseFloat(v.net_amount).toFixed(2))+'</td>';
							});
						}
						body += '<td class="text-right">'+net_add_deduction+'</td>' // EA Loans Medicard
						body += '<td class="text-right">'+net_adj+'</td>' // ADDITIONAL DEDUCTIONS
						body += '<td class="text-right">'+net_net_amount+'</td>' // NET AMOUNT
						body += '<td class="text-right">'+net_first+'</td>' // NET AMOUNT ONE
						body += '<td class="text-right">'+net_second+'</td>' // NET AMOUNT TWO
						body += '<td class="text-right">'+net_net_amount+'</td>' // NET AMOUNT
						body += '</tr>';

						// ======= TOTAL DIVISION AMOUNT ======

						netDivisionAmount = 0;
						$.each(data.transaction,function(key,val){

							// $.each(val,function(k1,v1){

								subDivisionAmount  = 0;
								subGrossPeraAmount = 0;
								$.each(val,function(k,v){

									lessGrossPeraAmount = 0;
									basicAmount = (v.salaryinfo) ? v.salaryinfo.salary_new_rate : 0;
									peraAmount = (v.benefitinfo) ? v.benefitinfo.benefit_amount : 0;
									lwopPeraAmount = (v.benefit_transactions) ? v.benefit_transactions.amount : 0;
									hasAbsent = (v.actual_absences) ? v.actual_absences : 0;

									if(hasAbsent){
										lessGrossPeraAmount = parseFloat(peraAmount) - parseFloat(lwopPeraAmount);
									}else{
										lessGrossPeraAmount = parseFloat(peraAmount);
									}
									subGrossPeraAmount += parseFloat(lessGrossPeraAmount);
									subDivisionAmount += parseFloat(basicAmount);

								})
								netDivisionAmount += parseFloat(subDivisionAmount);

								sub_div_amount = (subDivisionAmount) ? commaSeparateNumber(parseFloat(subDivisionAmount).toFixed(2)) : '';
								sub_pera_amount = (subGrossPeraAmount) ? commaSeparateNumber(parseFloat(subGrossPeraAmount).toFixed(2)) : '';

								body += '<tr style="font-weight:bold;">';
								body += '<td></td>';
								if(key == 'PJT'){
									body += '<td colspan="3"></td>'
								}else{
									body += '<td colspan="3">'+key+'</td>'
								}
								body += '<td class="text-right">01-00</td>'
								body += '<td  class="text-right"></td>'
								body += '<td  class="text-right"></td>'
								body += '<td  class="text-right"></td>'
								body += '<td class="text-right">'+sub_div_amount+'</td>' // SALARY DIFFERENTIAL
								body += '<td  class="text-right"></td>' // PERA
								body += '<td class="text-right"></td>' // SALARY
								body += '<td class="text-right"></td>' // PERA
								body += '<td  class="text-right"></td>' // GROSS EARNED AMOUNT
								body += '<td  class="text-right"></td>' // TOTAL DEDUCTIONS
								body += '<td class="text-right"></td>' // tax amount
								body += '<td class="text-right"></td>' // gsis cont amount
								body += '<td class="text-right" colspan="'+loanCount+'"></td>';
								body += '<td class="text-right"></td>'
								body += '<td class="text-right"></td>'
								body += '<td class="text-right"></td>'
								body += '<td class="text-right"></td>'
								body += '<td class="text-right" colspan="'+deductionCount+'"></td>';
								body += '<td class="text-right"></td>' // ADDITIONAL DEDUCTIONS
								body += '<td class="text-right"></td>' // NET AMOUNT
								body += '<td class="text-right"></td>' // NET AMOUNT ONE
								body += '<td class="text-right"></td>' // NET AMOUNT TWO
								body += '<td class="text-right"></td>' // NET AMOUNT
								body += '</tr>';

								body += '<tr style="font-weight:bold;">';
								body += '<td></td>'
								body += '<td colspan="3"></td>'
								body += '<td  class="text-right">01-10</td>'
								body += '<td  class="text-right"></td>'
								body += '<td  class="text-right"></td>'
								body += '<td  class="text-right"></td>'
								body += '<td class="text-right">'+sub_pera_amount+'</td>' // SALARY DIFFERENTIAL
								body += '<td  class="text-right"></td>' // PERA
								body += '<td class="text-right"></td>' // SALARY
								body += '<td class="text-right"></td>' // PERA
								body += '<td  class="text-right"></td>' // GROSS EARNED AMOUNT
								body += '<td  class="text-right"></td>' // TOTAL DEDUCTIONS
								body += '<td class="text-right"></td>' // tax amount
								body += '<td class="text-right"></td>' // gsis cont amount
								body += '<td class="text-right" colspan="'+loanCount+'"></td>';
								body += '<td class="text-right"></td>'
								body += '<td class="text-right"></td>'
								body += '<td class="text-right"></td>'
								body += '<td class="text-right"></td>'
								body += '<td class="text-right" colspan="'+deductionCount+'"></td>';
								body += '<td class="text-right"></td>' // ADDITIONAL DEDUCTIONS
								body += '<td class="text-right"></td>' // NET AMOUNT
								body += '<td class="text-right"></td>' // NET AMOUNT ONE
								body += '<td class="text-right"></td>' // NET AMOUNT TWO
								body += '<td class="text-right"></td>' // NET AMOUNT
								body += '</tr>';
						});



						// });

						net_div_amount = (netDivisionAmount) ? commaSeparateNumber(parseFloat(netDivisionAmount).toFixed(2)) : '';

						body += '<tr style="font-weight:bold;">';
						body += '<td></td>'
						body += '<td colspan="5"></td>'
						body += '<td class="text-right"></td>'
						body += '<td  class="text-right"></td>'
						body += '<td class="text-right">'+net_div_amount+'</td>' // SALARY DIFFERENTIAL
						body += '<td  class="text-right"></td>' // PERA
						body += '<td class="text-right"></td>' // SALARY
						body += '<td class="text-right"></td>' // PERA
						body += '<td  class="text-right"></td>' // GROSS EARNED AMOUNT
						body += '<td  class="text-right"></td>' // TOTAL DEDUCTIONS
						body += '<td class="text-right"></td>' // tax amount
						body += '<td class="text-right"></td>' // gsis cont amount
						body += '<td class="text-right" colspan="'+loanCount+'"></td>';
						body += '<td class="text-right"></td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-right" colspan="'+deductionCount+'"></td>';
						body += '<td class="text-right"></td>' // ADDITIONAL DEDUCTIONS
						body += '<td class="text-right"></td>' // NET AMOUNT
						body += '<td class="text-right"></td>' // NET AMOUNT ONE
						body += '<td class="text-right"></td>' // NET AMOUNT TWO
						body += '<td class="text-right"></td>' // NET AMOUNT
						body += '</tr>';

						amountInWord = toWords(net_net_amount).toUpperCase();

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';

						body += '<tr style="font-weight:bold;">';
						body += '<td style="border:none;padding:20px;" colspan="7" class="text-center">CERTIFIED:  Services duly rendered as stated.</td>'
						body += '<td style="border:none;padding:20px;" class="text-center" colspan="7" >APPROVED FOR PAYMENT: '+amountInWord+' ONLY PHP '+net_net_amount+'</td>';
						body += '<td style="border:none;" colspan="'+loanCount+'"></td>'
						body += '<td style="border:none;" colspan="'+deductionCount+'"></td>'
						body += '<td style="border:none;" colspan="11"></td>'
						body += '</tr>';

						body += '<tr style="font-weight:bold;">';
						body += '<td style="border:none;padding:20px;" colspan="7" class="text-center">'+signatoryOne+' <br> '+positionOne+'</td>'
						body += '<td style="border:none;" colspan="'+loanCount+'"></td>'
						body += '<td style="border:none;" colspan="'+deductionCount+'"></td>';
						body += '<td style="border:none;" colspan="18"></td>'
						body += '</tr>';

						body += '<tr style="font-weight:bold;">';
						body += '<td style="border:none;padding:20px;" colspan="7" class="text-center">CERTIFIED:  Supporting documents complete and proper; <br> and cash available in the amount of Php '+net_net_amount;
						body += '<td style="border:none;padding:20px;" class="text-center" colspan="7">'+signatoryTwo+' <br> '+positionTwo+'</td>';
						body += '<td style="border:none;" colspan="'+loanCount+'"></td>'
						body += '<td style="border:none;" colspan="'+deductionCount+'"></td>'
						body += '<td style="border:none;" colspan="11"></td>'
						body += '</tr>';

						body += '<tr style="font-weight:bold;border:none;">';
						body += '<td style="border:none;padding:20px;" colspan="7" class="text-center">'+signatoryThree;
						body += '<br>'+positionThree;
						body += '</td>';
						body += '<td style="border:none;" class="text-center" colspan="7"></td>';
						body += '<td style="border:none;" colspan="'+loanCount+'"></td>'
						body += '<td style="border:none;" colspan="'+deductionCount+'"></td>'
						body += '<td style="border:none;" colspan="11"></td>'
						body += '</tr>';
						body += '</tbody>';
						body += '</table>';
						body += '</div></div>';

						$('.payroll_transfer').html(body);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection