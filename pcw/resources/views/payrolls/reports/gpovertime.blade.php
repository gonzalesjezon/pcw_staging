@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row pl-4 pr-4">
					@include('payrolls.includes._months-year')
				</div>
				@include('payrolls.reports.includes._signatory')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-left: -5px; margin-right: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _status;
	$(document).on('change','#employee_status',function(){
		_status = "";
		_status = $(this).find(':selected').val();

	})

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'employee_id':_empid
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						ctr2 = 0;

						ctr3 = 0;
						$.each(data.transaction,function(key,val){
							$.each(val,function(k1,v1){
								ctr3++;
							})
						})

						netBasicAmount = 0;

						coveredPeriod = months[_Month]+' '+_Year;
						printDate = data.print_date;

       					$.each(data.transaction,function(key,val){
       						$.each(val,function(k1,v1){

       							body += '<div class="row">';
								body += '<div class="col-md-12">'
								body += '<table class="table table2">';
								body += '<thead class="text-center" style="font-weight: bold;">'
		       					body += '<tr>';
		       					body += '<td rowspan="3" style="border-right: none;">';
		       					body += 	'<img src="{{ url("images/reportlogo.png") }}" style="height: 80px;position: absolute;">';
		       					body += '</td>';
		       					body += '<td rowspan="3" class="text-center" colspan="11" style="border-left: none;">';
		       					body +=	'<h3>GENERAL PAYROLLL</h3>';
		       					body +=	'<h5 style="font-weight: bold;">OVERTIME AS OF <span id="month_year">'+coveredPeriod+'</span></h5>';
		       					body += '</td>';
		       					body += '<td colspan="2" class="text-left">Document Code:HRDMS-R-021</td>';
		       					body += '</tr>';
		       					body += '<tr>';
		       					body += '<td colspan="2" class="text-left">Date: '+printDate+'</td>';
		       					body += '</tr>';
		       					body += '<tr class="text-left">';
		       					body += '<td>Rev. No</td>';
		       					body += '<td>Page</td>';
		       					body +=	'</tr>';
		       					body += '<tr>';
		       					body += '<td colspan="14" class="text-left">WE ACKNOWLEDGE receipt of the sums shown opposite our names as full compensation  for the period covered.</td>';
		       					body +=	'</tr>';
		       					body +=	'<tr>';
		       					body += '<td rowspan="2" >NO</td>';
		       					body += '<td rowspan="2" >NAME OF EMPLOYEE</td>';
		       					body += '<td rowspan="2" >POSITION</td>';
		       					body += '<td rowspan="2" >EMPLOYEE NO.</td>';
		       					body += '<td rowspan="2" >ACCOUNT NUMBER</td>';
		       					body += '<td rowspan="2" >MONTHLY SALARY</td>';
		       					body += '<td rowspan="2" >RATE PER HOUR</td>';
		       					body += '<td colspan="2" >WEEKDAYS</td>';
		       					body += '<td colspan="2" >SATURDAYS, SUNDAYS AND HOLIDAYS</td>';
		       					body += '<td rowspan="2">TOTAL AMOUNT OF OT RENDERED</td>';
		       					body += '<td rowspan="2">TAX</td>';
		       					body += '<td rowspan="2">NET AMOUNT RECEIVED</td>';
		       					body +=	'</tr>';
		       					body +=	'<tr>';
		       					body += '<td >No of Hours</td>';
		       					body += '<td >Multiplier (1.25) </td>';
		       					body += '<td >No of Hours</td>';
		       					body += '<td >Multiplier (1.50) </td>';
		       					body +=	'</tr>';
		       					body += '</thead>';

		       					body += '<tbody>';

		       					subBasicAmount = 0;

       							$.each(v1,function(k,v){

       								firstname = (v.employees.firstname) ? v.employees.firstname : '';
       								lastname = (v.employees.lastname) ? v.employees.lastname : '';
       								middlename = (v.employees.middlename) ? v.employees.middlename : '';
       								employee_number = (v.employees.employee_number) ? v.employees.employee_number : '';
       								position = (v.positions) ? v.positions.Name : '';
       								account_number = (v.employeeinfo) ? v.employeeinfo.account_number : '';
       								basicAmount = (v.actual_basicpay_amount) ? v.actual_basicpay_amount : '';
       								dailyAmount = Number(basicAmount) / 22 : '';
       								hourRate = parseFloat(dailyAmount) / 8;

       								fullname = lastname+' '+firstname+' '+middlename;

       								subBasicAmount += parseFloat(basicAmount);

       								account_number = (account_number) ? account_number : '';
       								basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
       								hour_rate = (hourRate) ? commaSeparateNumber(parseFloat(hourRate).toFixed(2)) : '';

       								body += '<tr class="text-right">';
       								body += '<td class="text-center">'+ctr+'</td>';
       								body += '<td class="text-left">'+fullname+'</td>';
       								body += '<td class="text-center">'+position+'</td>';
       								body += '<td class="text-center">'+employee_number+'</td>';
       								body += '<td class="text-center">'+account_number+'</td>';
       								body += '<td >'+basic_amount+'</td>';
       								body += '<td >'+hour_rate+'</td>';
       								body += '<td ></td>';
       								body += '<td ></td>';
       								body += '<td ></td>';
       								body += '<td ></td>';
       								body += '<td ></td>';
       								body += '<td ></td>';
       								body += '<td ></td>';
       								body += '</tr>';
       								ctr++;
       								body += '<div style="page-break-after: always;display:block;"></div>';
       							})

       							ctr = 1;

       							netBasicAmount += parseFloat(subBasicAmount);

       							sub_basic_amount = (subBasicAmount) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '';

       							ctr2++;

								if(ctr2 >= ctr3){
									body += '<tr id="lastrow" style="font-weight:bold;" class="text-right">';
								}else{
									body += '<tr style="font-weight:bold;" class="text-right">';
								}
   								body += '<td class="text-center"></td>';
   								body += '<td class="text-left"></td>';
   								body += '<td class="text-center">SUB TOTAL</td>';
   								body += '<td class="text-center"></td>';
   								body += '<td class="text-center"></td>';
   								body += '<td >'+sub_basic_amount+'</td>';
   								body += '<td ></td>';
   								body += '<td ></td>';
   								body += '<td ></td>';
   								body += '<td ></td>';
   								body += '<td ></td>';
   								body += '<td ></td>';
   								body += '<td ></td>';
   								body += '<td ></td>';
   								body += '</tr>';

       						});

       					});

						net_basic_amount = (netBasicAmount) ? commaSeparateNumber(parseFloat(netBasicAmount).toFixed(2)) : '';

						lastrow = [];

						lastrow += '<tr style="font-weight:bold;" class="text-right">';
						lastrow += '<td class="text-center"></td>';
						lastrow += '<td class="text-left"></td>';
						lastrow += '<td class="text-center">GRAND TOTAL</td>';
						lastrow += '<td class="text-center"></td>';
						lastrow += '<td class="text-center"></td>';
						lastrow += '<td >'+net_basic_amount+'</td>';
						lastrow += '<td ></td>';
						lastrow += '<td ></td>';
						lastrow += '<td ></td>';
						lastrow += '<td ></td>';
						lastrow += '<td ></td>';
						lastrow += '<td ></td>';
						lastrow += '<td ></td>';
						lastrow += '<td ></td>';
						lastrow += '</tr>';


						amountInWord = toWords(net_basic_amount).toUpperCase();

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="7">CERTIFIED:  Services duly rendered as stated.</td>'
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="7" >APPROVED FOR PAYMENT: '+amountInWord+' ONLY PHP '+net_basic_amount+'</td>';
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="7">'+signatoryOne+' <br> '+positionOne+'</td>';
						lastrow += '<td colspan="7" style="border:none" ></td>';
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="7">CERTIFIED:  Supporting documents complete and proper; <br> and cash available in the amount of Php '+net_basic_amount+'</td>';
						lastrow += '<td colspan="7" style="border:none" ></td>';
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;border:none;">';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="7">'+signatoryTwo+' ';
						lastrow += '<br> '+positionTwo;
						lastrow += '</td>';
						lastrow += '<td style="border:none;padding:20px;" class="text-center" colspan="7" >'+signatoryThree+' <br>'+positionThree+'</td>';
						lastrow += '</tr>';



       					body += '</table></div></div>'
						$('#payroll_transfer').html(body);
						$('#lastrow').after(lastrow);



						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection