@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row pl-4 pr-4">
					@include('payrolls.includes._months-year')
				</div>
				@include('payrolls.reports.includes._signatory')
			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table" style=" width: 960px;border: 2px solid #333;" class="table">
	       					<thead class="text-center" style="font-weight: bold;">
	       						<tr>
	       							<td rowspan="3" style="border-right: none;">
	       								<img src="{{ url('images/reportlogo.png') }}" style="height: 80px;position: absolute;">
	       							</td>
	       							<td rowspan="3" class="text-center" colspan="7" style="border-left: none;">
	       								<h3>GENERAL PAYROLLL</h3>
	       								<h5 style="font-weight: bold;">PERFORMANCE - BASED BONUS  <br> FOR YEAR <span id="year"></span></h5>
	       							</td>
	       							<td colspan="3" class="text-left">Document Code: HRDMS-R-009</td>
	       						</tr>
	       						<tr>
	       							<td colspan="3" class="text-left">Date: <span id="print_date"></span></td>
	       						</tr>
	       						<tr class="text-left">
	       							<td>Rev. No</td>
	       							<td colspan="2">Page</td>
	       						</tr>
	       						<tr>
	       							<td colspan="11" class="text-left">
	       								WE ACKNOWLEDGE receipt of the sums shown opposite our names as full compensation  for the period covered.
	       							</td>
	       						</tr>
	       						<tr>
	       							<td>NO</td>
	       							<td>NAME OF EMPLOYEE</td>
	       							<td>POSITION</td>
	       							<td>EMPLOYEE NO.</td>
	       							<td>LBP ACCOUNT NUMBER</td>
	       							<td>Monthly Salary as of <span id="month_year"></span></td>
	       							<td>DIVISION/UNIT PBB RATE</td>
	       							<td>PBB RATE</td>
	       							<td>PERFORMANCE-BASED BONUS</td>
	       							<td>TAX</td>
	       							<td>TOTAL AMOUNT RECEIVED</td>
	       						</tr>
	       					</thead>
	       					<tbody  id="payroll_transfer"></tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});

	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						netPBBAmount 		= 0;
						netSalaryAmount 	= 0;
						netTotalAmount 		= 0;

						$.each(data.transaction,function(key,val){

							subPBBAmount = 0;
							subSalaryAmount = 0;
							subTotalAmount = 0;
							$.each(val,function(k,v){

								// ======== BODY =======

								lastname = (v.employees) ? v.employees.lastname : '';
								firstname = (v.employees) ? v.employees.firstname : '';
								middlename = (v.employees.middlename) ? v.employees.middlename : '';
								fullname = lastname+' '+firstname+' '+middlename;
								position = (v.positions) ? v.positions.Name : '';
								employee_number = (v.employees.employee_number) ? v.employees.employee_number : '';
								account_number = (v.employeeinfo) ? v.employeeinfo.account_number : '';
								step_inc = (v.salaryinfo.step_inc) ? v.salaryinfo.step_inc : '';

								salaryAmount = (v.salaryinfo) ? v.salaryinfo.salary_new_rate : 0;
								pbbAmount = (v.amount) ? v.amount : 0;

								// ===== COMPUTATION =====

								// ===== SUB TOTAL COMPUTATION =====

								totalAmount = pbbAmount;

								subPBBAmount += parseFloat(pbbAmount);
								subSalaryAmount += parseFloat(salaryAmount);
								subTotalAmount += parseFloat(totalAmount);
								// ===== SUB TOTAL COMPUTATION =====

								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
								salary_amount = (salaryAmount !== 0) ? commaSeparateNumber(parseFloat(salaryAmount).toFixed(2)) : '';
								pbb_amount = (pbbAmount !== 0) ? commaSeparateNumber(parseFloat(pbbAmount).toFixed(2)) : '';
								total_amount = (totalAmount !== 0) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';
								account_number = (account_number !== null) ? account_number : '';
								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

								body += '<tr>';
								body += '<td>'+ctr+'</td>'
								body += '<td>'+fullname+'</td>'
								body += '<td>'+position+'</td>'
								body += '<td class="text-center">'+employee_number+'</td>'
								body += '<td class="text-center">'+account_number+'</td>'
								body += '<td  class="text-right">'+salary_amount+'</td>'
								body += '<td  class="text-right"></td>' // PBB DIVISION RATE
								body += '<td  class="text-right"></td>' // PBB  RATE
								body += '<td  class="text-right">'+pbb_amount+'</td>'
								body += '<td  class="text-right"></td>' // TAX
								body += '<td  class="text-right">'+total_amount+'</td>'
								body += '</tr>';

								ctr++;

							});

						netPBBAmount += parseFloat(subPBBAmount);
						netSalaryAmount += parseFloat(subSalaryAmount);
						netTotalAmount += parseFloat(subTotalAmount);
						// ===== SUB TOTAL COMPUTATION =====

						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
						sub_salary_amount = (subSalaryAmount !== 0) ? commaSeparateNumber(parseFloat(subSalaryAmount).toFixed(2)) : '';
						sub_pbb_amount = (subPBBAmount !== 0) ? commaSeparateNumber(parseFloat(subPBBAmount).toFixed(2)) : '';
						sub_total_amount = (subTotalAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalAmount).toFixed(2)) : '';

						body += '<tr style="font-weight:bold;">';
						body += '<td></td>'
						body += '<td>SUB TOTAL</td>'
						body += '<td></td>'
						body += '<td class="text-center"></td>'
						body += '<td class="text-center"></td>'
						body += '<td  class="text-right">'+sub_salary_amount+'</td>'
						body += '<td  class="text-right"></td>' // PBB DIVISION RATE
						body += '<td  class="text-right"></td>' // PBB  RATE
						body += '<td  class="text-right">'+sub_pbb_amount+'</td>'
						body += '<td  class="text-right"></td>' // TAX
						body += '<td  class="text-right">'+sub_total_amount+'</td>'
						body += '</tr>';


						});

						// ======= GRAND TOTAL ======

						net_salary_amount = (netSalaryAmount !== 0) ? commaSeparateNumber(parseFloat(netSalaryAmount).toFixed(2)) : '';
						net_pbb_amount = (netPBBAmount !== 0) ? commaSeparateNumber(parseFloat(netPBBAmount).toFixed(2)) : '';
						net_total_amount = (netTotalAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalAmount).toFixed(2)) : '';

						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
						body += '<tr style="font-weight:bold;">';
						body += '<td></td>'
						body += '<td>GRAND TOTAL</td>'
						body += '<td></td>'
						body += '<td class="text-center"></td>'
						body += '<td class="text-center"></td>'
						body += '<td  class="text-right">'+net_salary_amount+'</td>'
						body += '<td  class="text-right"></td>' // PBB DIVISION RATE
						body += '<td  class="text-right"></td>' // PBB  RATE
						body += '<td  class="text-right">'+net_pbb_amount+'</td>'
						body += '<td  class="text-right"></td>' // TAX
						body += '<td  class="text-right">'+net_total_amount+'</td>'
						body += '</tr>';

						// ======= TOTAL DIVISION AMOUNT ======

						body += '<tr>';
						body += '<td colspan="11"></td>'
						body += '</tr>';

						netDivisionAmount = 0;
						$.each(data.transaction,function(key,val){
							subDivisionAmount  = 0;
							$.each(val,function(k,v){
								pbbAmount = (v.amount) ? v.amount : 0;

								// ===== COMPUTATION =====

								// ===== SUB TOTAL COMPUTATION =====

								subDivisionAmount += parseFloat(pbbAmount);

							});

							netDivisionAmount += parseFloat(subDivisionAmount);

							sub_div_amount = (subDivisionAmount) ? commaSeparateNumber(parseFloat(subDivisionAmount).toFixed(2)) : '';

							body += '<tr style="font-weight:bold;">';
							body += '<td class="text-right"></td>'
							body += '<td class="text-left" colspan="3">'+key+'</td>'
							body += '<td class="text-right"></td>'
							body += '<td class="text-right"></td>'
							body += '<td class="text-right" colspan="4">'+sub_div_amount+'</td>'
							body += '<td class="text-right"></td>'
							body += '</tr>';

							body += '<tr style="font-weight:bold;">';
							body += '<td colspan="11"></td>'
							body += '</tr>';

						});

						net_div_amount = (netDivisionAmount) ? commaSeparateNumber(parseFloat(netDivisionAmount).toFixed(2)) : '';

						body += '<tr style="font-weight:bold;">';
						body += '<td class="text-right"></td>'
						body += '<td class="text-right" colspan="3"></td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-right" colspan="4">'+net_div_amount+'</td>'
						body += '<td class="text-right"></td>'
						body += '</tr>';

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';

						amountInWord = toWords(net_total_amount).toUpperCase();

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;" colspan="3">CERTIFIED:  Services duly rendered as stated.</td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;" colspan="4">APPROVED FOR PAYMENT: '+amountInWord+' ONLY PHP ('+net_total_amount+')</td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;"></td>' // NET AMOUNT
						body += '</tr>';

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;padding-top:20px;" colspan="3">'+signatoryOne+' <br> '+positionOne+' </td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;" colspan="4"></td>' // NET AMOUNT
						body += '</tr>';

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;padding-top:20px;" colspan="3">CERTIFIED:  Supporting documents complete and proper; and cash available in the amount of PHP '+net_total_amount+'</td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;" colspan="4">'+signatoryTwo+' <br> '+positionTwo+'</td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;"></td>' // NET AMOUNT
						body += '</tr>';

						body += '<tr style="font-weight:bold;" class="text-center">';
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;padding-top:20px;" colspan="3">'+signatoryThree+' <br> '+positionThree+'</td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;"></td>'
						body += '<td style="border:none;" colspan="4"></td>' // NET AMOUNT
						body += '</tr>';


						$('#payroll_transfer').html(body);

						printDate = data.print_date;
						$('#print_date').text(printDate)

						$('#year').text(_Year);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection