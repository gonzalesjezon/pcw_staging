@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<style type="text/css">
	.table > thead > tr >td, .table > tbody > tr >td{
		border: 1px solid #333 !important;
	}
	.td-style1{
		border-right: 2px solid #333;
		background-color: #e4e0e0;
    width: 140px;
    font-size: 12px;
    font-weight: bold;
	}
	.td-style2{
		border-right: 2px solid #333;
	    font-size: 12px;
	    width: 750px;
	    font-weight: bold;
	}
	.td-style3{
		background-color: #e4e0e0;
		font-weight: bold;
		font-size: 14px;
	}
	.table-style1{
		width: 400px;
	    border: 2px solid #333;
	    margin-top: 20px;
	}
	.table-style2{
	    border: 2px solid #333;
	    margin-top: 20px;
	}
	.table-padding2>tbody>tr>td{
		padding: 4px !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<div class="panel panel-default">
		<div class="panel-body">
			@include('payrolls.reports.includes._table-posted')
			<form action="{{ url($module_prefix.'/'.$module) }}" method="post" id="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row p-1">
					<div class="col-md-12">
						<span >Select Employee</span>
						<select class="form-control select2" name="employee_id" id="employee_id">
							<option value=""></option>
							@foreach($employee as $key => $value)
							<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row mb-0 pb-0">
					<div class="col-md-12">
						<span class="pl-2" >Covered Date</span>
					</div>
				</div>
				@include('payrolls.includes._months-year')
				<br>
				@include('payrolls.reports.includes._post-button')
				<!-- <div class="progress">
                    <div class="bar"></div >
                    <div class="percent">0%</div >
                </div> -->
			</form>
		</div>
	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="width: 960px;margin: auto;">
	       			<div class="col-md-12">
		       				<table class="table" style=" width: 960px;border: 2px solid #333;" class="table">
		       					<thead>
		       						<tr>
		       							<td style="width: 180px !important;">
		       								<img src="{{ url('images/reportlogo.png') }}" style="height: 80px;">
		       							</td>
		       							<td class="text-center">
		       								<h3>Payroll Payment Slip</h3>
		       							</td>
		       						</tr>
		       					</thead>
							</table>
							<table class="table table-padding2" style=" width: 960px;border: 2px solid #333;margin-top: 20px;" class="table">
		       					<tbody>
		       						<tr>
		       							<td class="td-style1">Pay Period</td>
		       							<td><span id="pay_period"></span></td>
		       						</tr>
		       						<tr>
		       							<td class="td-style1">Employee Name:</td>
		       							<td><span id="fullname"></span></td>
		       						</tr>
		       						<tr>
		       							<td class="td-style1">Employee No:</td>
		       							<td>
		       								<span id="employee_number"></span>
		       							</td>
		       						</tr>
		       						<tr>
		       							<td class="td-style1">Office/Division:</td>
		       							<td><span id="division"></span></td>
		       						</tr>
		       						<tr>
		       							<td class="td-style1">Position:</td>
		       							<td><span id="position"></span></td>
		       						</tr>
		       					</tbody>
							</table>
							<table class="table table-padding2" style=" width: 960px;border: 2px solid #333;margin-top: 20px;" class="table">
		       					<tbody>
		       						<tr>
		       							<td colspan="2" class="text-center td-style3" >EARNINGS</td>
		       						</tr>
		       						<tr>
		       							<td class="td-style2">Salary Basic</td>
		       							<td class="text-right"><span id="basic_amount"></span></td>
		       						</tr>
		       						<tr>
		       							<td class="td-style2">Salary Differential</td>
		       							<td class="text-right"><span id="salary_diff_amount"></span></td>
		       						</tr>
		       						<tr>
		       							<td class="td-style2">Gross Earnings:</td>
		       							<td class="text-right"><span id="gross_amount"></span></td>
		       						</tr>
		       						<tr>
		       							<td class="td-style2">Personnel Economic Relief Allowance</td>
		       							<td class="text-right"><span id="pera_amount"></span></td>
		       						</tr>
		       						<tr>
		       							<td class="td-style2 text-right">TOTAL EARNINGS</td>
		       							<td class="text-right"><span id="total_earning_amount"></span></td>
		       						</tr>
		       					</tbody>
							</table>
							<table class="table table-padding2" style=" width: 960px;border: 2px solid #333;margin-top: 20px;" class="table">
		       					<tbody id="deduction_list">
		       					</tbody>
							</table>
							<table class="table" style=" width: 960px;border: 2px solid #333;margin-top: 20px;" class="table">
		       					<tbody>
		       						<tr>
		       							<td class="td-style3 text-right">MONTHLY PAY</td>
		       							<td class="text-right td-style3"><span id="monthly_pay_amount"></span></td>
		       						</tr>
		       					</tbody>
							</table>

							</div>
	       		</div>

	       		<div class="row" style="width: 960px;margin: auto;">
	       			<div class="col-md-6"></div>
	       			<div class="col-md-6">
								<table class="table table-style1 table-padding2" style="width: 480px !important;">
			       					<tbody>
			       						<tr>
			       							<td class="text-center td-style3" >PERIOD</td>
			       							<td class="text-center td-style3" >PAY DATE</td>
			       							<td class="text-center td-style3" >AMOUNT</td>
			       						</tr>
			       					</tbody>
			       					<tbody>
			       						<tr>
			       							<td class="text-center">
			       								<span id="first_period"></span>
			       							</td>
			       							<td class="text-center"><span id="first_pay_date"></span></td>
			       							<td class="text-right"><span id="first_half_amount"></span></td>
			       						</tr>
			       						<tr>
			       							<td class="text-center">
			       								<span id="second_period"></span>
			       							</td>
			       							<td class="text-center"><span id="second_pay_date"></span></td>
			       							<td class="text-right"><span id="second_half_amount"></span></td>
			       						</tr>
			       					</tbody>
								</table>
								<table class="table table-style1 table-padding2" style="width: 480px !important;">
			       					<tbody>
			       						<tr>
			       							<td class="text-center td-style3" >LOAN/S</td>
			       							<td class="text-center td-style3" >AMOUNT </td>
			       							<td class="text-center td-style3" >ACCUM. <br> DEDUCTION</td>
			       							<td class="text-center td-style3" >BALANCE</td>
			       						</tr>
			       					</tbody>
			       					<tbody>
			       						<tr>
			       							<td></td>
			       							<td></td>
			       							<td></td>
			       							<td></td>
			       						</tr>
			       					</tbody>
								</table>
	       				
	       			</div>
	       		</div>

	       		<div class="row" style="width: 960px;margin: auto;">
	       			<div class="col-md-12">
								<table class="table table-style2 table-padding2">
			       					<tbody>
			       						<tr>
			       							<td>
			       								The only CONTROLLED copy of this template is the online version maintained in the Fileserver. The user must ensure that this or any other copy of a controlled document is current and complete prior to use. The MASTER copy of this document is with the Information Communication Technology Section (ICTS) of Corporate Affairs and Information Resource Management Division (CAIRMD). This document is UNCONTROLLED when downloaded and printed.
			       							</td>
			       						</tr>
			       					</tbody>
								</table>
	       				
	       			</div>
	       		</div>

	       			
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
@include('payrolls.reports.includes._post-button-script')
<script type="text/javascript">
$(document).ready(function(){


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	// View posted report
	$(document).on('click','.view',function(){
		_Year 	= $(this).data('year');
		_Month 	= $(this).data('month');
		_empid 	= $(this).data('employee_id');
		$('#preview').trigger('click');
	});

	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'employee_id':_empid,
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction !== null){

						body = [];

						firstname = (data.transaction.employees) ? data.transaction.employees.firstname : ''
						lastname = (data.transaction.employees) ? data.transaction.employees.lastname : ''
						middlename = (data.transaction.employees.middlename) ? data.transaction.employees.middlename : ''
						employee_number = (data.transaction.employee_number) ? data.transaction.employee_number : ''
						position = (data.transaction.positions) ? data.transaction.positions.Name : ''
						office = (data.transaction.offices) ? data.transaction.offices.Name : ''
						division = (data.transaction.divisions) ? data.transaction.divisions.Name : '';

						fullname = lastname+' '+firstname+' '+middlename;

						basicAmount = (data.transaction.actual_basicpay_amount) ? data.transaction.actual_basicpay_amount : 0;
						peraAmount = (data.transaction.benefit_transactions) ? data.transaction.benefit_transactions.amount : 0;
						gsisAmount = (data.transaction.gsis_ee_share) ? data.transaction.gsis_ee_share : 0;
						pagibigAmount = (data.transaction.pagibig_share) ? data.transaction.pagibig_share : 0;
						philhealthAmount = (data.transaction.phic_share) ? data.transaction.phic_share : 0;
						taxAmount = (data.transaction.tax_amount) ? data.transaction.tax_amount : 0;
						pagibigPersonalAmount = (data.transaction.mp1_amount) ? data.transaction.mp1_amount : 0;
						pagibig2Amount = (data.transaction.mp2_amount) ? data.transaction.mp2_amount : 0;

						totalEarningsAmount = parseFloat(basicAmount) + parseFloat(peraAmount);


						basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
						pera_amount = (peraAmount) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
						total_earnings_amount = (totalEarningsAmount) ? commaSeparateNumber(parseFloat(totalEarningsAmount).toFixed(2)) : '';
						tax_amount = (taxAmount) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
						pagibig_amount = (pagibigAmount) ? commaSeparateNumber(parseFloat(pagibigAmount).toFixed(2)) : '';
						pagibig_personal_amount = (pagibigPersonalAmount) ? commaSeparateNumber(parseFloat(pagibigPersonalAmount).toFixed(2)) : '';
						pagibig2_amount = (pagibig2Amount) ? commaSeparateNumber(parseFloat(pagibig2Amount).toFixed(2)) : '';
						philhealth_amount = (philhealthAmount) ? commaSeparateNumber(parseFloat(philhealthAmount).toFixed(2)) : '';
						gsis_amount = (gsisAmount) ? commaSeparateNumber(parseFloat(gsisAmount).toFixed(2)) : '';


						$('#fullname').text(fullname);
						$('#employee_number').text(employee_number);
						$('#position').text(position);
						$('#division').text(office+' / '+division);

						$('#basic_amount').text(basic_amount);
						$('#gross_amount').text(basic_amount);
						$('#pera_amount').text(pera_amount);
						$('#total_earning_amount').text(total_earnings_amount);


						  // =========== //
						 // DEDUCTIONS  //
						// =========== //

						body += '<tr>';
   						body +=	'<td colspan="2" class="text-center" style="background-color: #e4e0e0;font-weight: bold;font-size: 14px;">DEDUCTIONS</td></tr>';
   						body += '<tr>'
   						body += '<td class="td-style2">BIR Withholding Tax </td>';
   						body +=	'<td class="text-right"><span id="wtax_amount">'+tax_amount+'</span></td>';
   						body += '</tr>';
   						body += '<tr>'
   						body += '<td class="td-style2">GSIS Life & Ret. Contribution</td>';
   						body +=	'<td class="text-right"><span id="gsis_amount">'+gsis_amount+'</span></td>';
   						body += '</tr>';
   						totalGsisLoansAmount = 0;
   						$.each(data.transaction.gsisloans,function(k,v){
   							gsisLoanAmount = (v.amount) ? v.amount : 0;
   							totalGsisLoansAmount += parseFloat(gsisLoanAmount);
   							gsis_loan_amount = (gsisLoanAmount) ? commaSeparateNumber(parseFloat(gsisLoanAmount).toFixed(2)) : '';
   							body += '<tr>';
	   						body +=	'<td class="td-style2"><span style="margin-left:10px;">'+v.loans.name+'</span></td>';
	   						body +=	'<td class="text-right">'+gsis_loan_amount+'</td>';
	   						body += '</tr>';
   						});
   						body += '<tr>';
   						body +=	'<td class="td-style2">HDMF Contribution</td>';
   						body +=	'<td class="text-right"><span id="hdmf_cont_amount">'+pagibig_amount+'</span></td>';
   						body += '</tr>';

   						body += '<tr>';
   						body +=	'<td class="td-style2">Pagibig Personal</td>';
   						body +=	'<td class="text-right"><span>'+pagibig_personal_amount+'</span></td>';
   						body += '</tr>';

   						body += '<tr>';
   						body +=	'<td class="td-style2">MP2</td>';
   						body +=	'<td class="text-right"><span>'+pagibig2_amount+'</span></td>';
   						body += '</tr>';

   						totalPagibigLoansAmount = 0;
   						$.each(data.transaction.pagibigloans,function(k,v){
   							pagibigLoanAmount = (v.amount) ? v.amount : 0;
   							totalPagibigLoansAmount += parseFloat(pagibigLoanAmount);
   							pagibig_loan_amount = (pagibigLoanAmount) ? commaSeparateNumber(parseFloat(pagibigLoanAmount).toFixed(2)) : '';
   							body += '<tr>';
	   						body +=	'<td class="td-style2">'+v.loans.name+'</td>';
	   						body +=	'<td class="text-right">'+pagibig_loan_amount+'</td>';
	   						body += '</tr>';
   						});

   						totalDeductionAmount = parseFloat(taxAmount) + parseFloat(pagibigAmount) + parseFloat(philhealthAmount) + parseFloat(gsisAmount) + parseFloat(totalGsisLoansAmount) + parseFloat(totalPagibigLoansAmount) + parseFloat(pagibig2Amount) + parseFloat(pagibigPersonalAmount);

   						total_deduction_amount = (totalDeductionAmount) ? commaSeparateNumber(parseFloat(totalDeductionAmount).toFixed(2)) : '';
   						body += '<tr>';
   						body += '<td class="td-style2">PHILHEALTH Contribution</td>';
   						body +=	'<td class="text-right"><span id="philhealth_cont_amount">'+philhealth_amount+'</span></td>';
   						body += '</tr>';
   						body += '<tr>';
   						body +=	'<td class="td-style3 text-right">TOTAL DEDUCTIONS</td>';
   						body +=	'<td class="text-right td-style3"><span>'+total_deduction_amount+'</span></td>'
   						body += '</tr>';

   						body += '<tr>';
   						body +=	'<td class="td-style3 text-right">REFUND</td>';
   						body +=	'<td class="text-right td-style3"><span></span></td>'
   						body += '</tr>';

   						monthlyPayAmount = parseFloat(totalEarningsAmount) - parseFloat(totalDeductionAmount);

   						monthly_pay_amount = (monthlyPayAmount) ? commaSeparateNumber(parseFloat(monthlyPayAmount).toFixed(2)) : '';

   						$('#monthly_pay_amount').text(monthly_pay_amount);

   						netAmount = parseFloat(monthlyPayAmount) / 2;

   						// FIRST HALF
						firstHalf = parseInt(netAmount).toFixed(2);
						first_half_amount = (firstHalf) ? commaSeparateNumber(firstHalf) : '';

						// SECOND HALF
						getDecimal = parseFloat(netAmount).toFixed(2).split('.');
						getDecimal = parseFloat(getDecimal[1])/100;
						secondHalf = (parseFloat(netAmount) + getDecimal);
						secondHalf = Math.round(secondHalf * 100) /100;
						second_half_amount = (secondHalf) ? commaSeparateNumber(parseFloat(secondHalf).toFixed(2)) : '';

						$('#first_half_amount').text(first_half_amount);
						$('#second_half_amount').text(second_half_amount);
						$('#deduction_list').html(body);

						$('#pay_period').text(months[_Month]+' '+_Year);

						days = daysInMonth(_Month,_Year)

						monthly = months[_Month]+' 1-'+days+', '+_Year;
						first_period = months[_Month]+' 1-15, '+_Year;
						second_period = months[_Month]+' 16-'+days+', '+_Year;

						printDate = data.print_date;
						dateOne = printDate.split('-');

						monthOne = months[parseInt(dateOne[1])]
						dayOne = dateOne[2];
						yearOne = dateOne[0];

						printDate = monthOne+' '+dayOne+', '+yearOne;

						$('#print_date').text(printDate);
						$('#first_period').text(first_period);
						$('#second_period').text(second_period);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection