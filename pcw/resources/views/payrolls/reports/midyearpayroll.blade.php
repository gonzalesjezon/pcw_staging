@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Select Employee Status</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="employee_status" id="employee_status">
								<option value=""></option>
								<option value="permanent">Permanent</option>
								<option value="project">Project</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row pl-4 pr-4">
					@include('payrolls.includes._months-year')
				</div>
				@include('payrolls.reports.includes._signatory')
			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	/* SIGNATORY */
	var signatoryOne = "";
	var positionOne  = "";
	$('#signatory_one').change(function(){
		signatoryOne = $(this).find(':selected').text();
		positionOne  = $(this).find(':selected').data('position');
	});

	var signatoryTwo = "";
	var positionTwo  = "";
	$('#signatory_two').change(function(){
		signatoryTwo = $(this).find(':selected').text();
		positionTwo  = $(this).find(':selected').data('position');
	});

	var signatoryThree = "";
	var positionThree  = "";
	$('#signatory_three').change(function(){
		signatoryThree = $(this).find(':selected').text();
		positionThree  = $(this).find(':selected').data('position');
	});


	var months ={
			1:'January',
			2:'February',
			3:'March',
			4:'April',
			5:'May',
			6:'June',
			7:'July',
			8:'August',
			9:'September',
			10:'October',
			11:'November',
			12:'December',
		}

	var _status;
	$(document).on('change','#employee_status',function(){
		_status = "";
		_status = $(this).find(':selected').val();

	});


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'status':_status
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr  = 1;
						ctr2 = 0;

						netBasicAmount = 0;
						netMidYearAmount = 0;

						ctr3 = 0;
						$.each(data.transaction,function(key,val){
							$.each(val,function(k1,v1){
								ctr3++;
							})
						})
						printDate = data.print_date;
						$.each(data.transaction,function(key,val){


							$.each(val,function(k1,v1){

								body += '<div class="row">';
								body += '<div class="col-md-12">';
								body += '<table class="table table2">';
								body += '<thead class="text-center" style="font-weight: bold;"><tr>';
	       						body += '<td rowspan="3" style="border-right: none;">';
	       						body += '<img src="{{ url("images/reportlogo.png") }}" style="height: 80px;position: absolute;">';
	       						body += '</td>'
	       						body += '<td rowspan="3" class="text-center" colspan="5" style="border-left: none;">';
	       						body += '<h3>GENERAL PAYROLLL</h3>';
	       						body += '<h5 style="font-weight: bold;">MID - YEAR BONUS FOR THE YEAR <span id="month_year"></span></h5>';
	       						body += '</td>'
	       						body += '<td colspan="2" class="text-left">Document Code: HRDMS-R-009</td></tr>'
	       						body += '<tr><td colspan="2" class="text-left">Date: '+printDate+'</tr>';
	       						body += '<tr class="text-left"><td>Rev. No</td><td>Page</td></tr>';
	       						body += '<tr>';
	       						body +=	'<td>NO</td>';
	       						body +=	'<td>NAME OF EMPLOYEE</td>';
	       						body +=	'<td>POSITION</td>';
	       						body +=	'<td>EMPLOYEE NO.</td>';
	       						body +=	'<td>ACCOUNT NUMBER</td>';
	       						body +=	'<td>MONTLY SALARY</td>';
	       						body +=	'<td>MID YEAR BONUS</td>';
	       						body +=	'<td>NET AMOUNT</td>';
	       						body += '</tr>';
	       						body += '</thead>';

	       						body += '<tbody>';

	       						subBasicAmount = 0;
								subMidYearAmount = 0;

								$.each(v1,function(k,v){

									// ======== BODY =======

									lastname = (v.employees) ? v.employees.lastname : '';
									firstname = (v.employees) ? v.employees.firstname : '';
									middlename = (v.employees.middlename) ? v.employees.middlename : '';
									fullname = lastname+' '+firstname+' '+middlename;
									position = (v.positions) ? v.positions.Name : '';
									employee_number = (v.employees.employee_number) ? v.employees.employee_number : '';
									account_number = (v.employeeinfo) ? v.employeeinfo.account_number : '';
									basicAmount = (v.actual_basicpay_amount) ? v.actual_basicpay_amount : 0;

									midYearAmount = (v.amount) ? v.amount : 0;

									// ===== COMPUTATION =====

									// ===== SUB TOTAL COMPUTATION =====

									subBasicAmount += parseFloat(basicAmount);
									subMidYearAmount += parseFloat(midYearAmount);
									// ===== SUB TOTAL COMPUTATION =====

									// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
									basic_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
									midyear_amount = (midYearAmount !== 0) ? commaSeparateNumber(parseFloat(midYearAmount).toFixed(2)) : '';
									account_number = (account_number !== null) ? account_number : '';
									// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

									body += '<tr>';
									body += '<td>'+ctr+'</td>';
									body += '<td>'+fullname+'</td>';
									body += '<td>'+position+'</td>';
									body += '<td class="text-center">'+employee_number+'</td>';
									body += '<td class="text-center">'+account_number+'</td>';
									body += '<td  class="text-right">'+basic_amount+'</td>';
									body += '<td  class="text-right">'+midyear_amount+'</td>';
									body += '<td  class="text-right">'+midyear_amount+'</td>';
									body += '</tr>';

									ctr++;
									body += '<div style="page-break-after: always;display:block;"></div>';

								});

								ctr = 1;
								ctr2++;

								netBasicAmount += parseFloat(subBasicAmount);
								netMidYearAmount += parseFloat(subMidYearAmount);
								// ===== SUB TOTAL COMPUTATION =====

								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
								sub_basic_amount = (subBasicAmount !== 0) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '';
								sub_midyear_amount = (subMidYearAmount !== 0) ? commaSeparateNumber(parseFloat(subMidYearAmount).toFixed(2)) : '';

								if(ctr2 >= ctr3){
									body += '<tr id="lastrow" style="font-weight:bold;" class="text-right">';
								}else{
									body += '<tr style="font-weight:bold;" class="text-right">';
								}
								body += '<td></td>'
								body += '<td class="text-center" >SUB TOTAL</td>'
								body += '<td></td>'
								body += '<td></td>'
								body += '<td></td>'
								body += '<td></td>'
								body += '<td >'+sub_basic_amount+'</td>'
								body += '<td >'+sub_midyear_amount+'</td>'
								body += '</tr>';
							})

						});

						// ======= GRAND TOTAL ======

						net_basic_amount = (netBasicAmount !== 0) ? commaSeparateNumber(parseFloat(netBasicAmount).toFixed(2)) : '';
						net_midyear_amount = (netMidYearAmount !== 0) ? commaSeparateNumber(parseFloat(netMidYearAmount).toFixed(2)) : '';

						lastrow = [];

						// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td></td>'
						lastrow += '<td class="text-center">GRAND TOTAL</td>'
						lastrow += '<td></td>'
						lastrow += '<td></td>'
						lastrow += '<td></td>'
						lastrow += '<td></td>'
						lastrow += '<td class="text-right">'+net_basic_amount+'</td>'
						lastrow += '<td class="text-right">'+net_midyear_amount+'</td>'
						lastrow += '</tr>';

						// ======= TOTAL DIVISION AMOUNT ======

						netDivisionAmount = 0;
						$.each(data.transaction,function(key,val){
							$.each(val,function(k1,v1){
								subDivisionAmount  = 0;
								$.each(v1,function(k,v){
									midYearAmount = (v.amount) ? v.amount : 0;

									// ===== COMPUTATION =====

									// ===== SUB TOTAL COMPUTATION =====

									subDivisionAmount += parseFloat(midYearAmount);

								});

								netDivisionAmount += parseFloat(subDivisionAmount);
								sub_div_amount = (subDivisionAmount) ? commaSeparateNumber(parseFloat(subDivisionAmount).toFixed(2)) : '';

								lastrow += '<tr style="font-weight:bold;">';
								lastrow += '<td class="text-right"></td>'
								lastrow += '<td class="text-left" colspan="3">'+key+'</td>'
								lastrow += '<td class="text-right"></td>'
								lastrow += '<td class="text-right"></td>'
								lastrow += '<td class="text-right">'+sub_div_amount+'</td>'
								lastrow += '<td class="text-right"></td>'
								lastrow += '</tr>';

								lastrow += '<tr style="font-weight:bold;">';
								lastrow += '<td colspan="8"></td>'
								lastrow += '</tr>';
							})



						});

						net_div_amount = (netDivisionAmount) ? commaSeparateNumber(parseFloat(netDivisionAmount).toFixed(2)) : '';

						lastrow += '<tr style="font-weight:bold;">';
						lastrow += '<td class="text-right"></td>'
						lastrow += '<td class="text-right" colspan="3"></td>'
						lastrow += '<td class="text-right"></td>'
						lastrow += '<td class="text-right"></td>'
						lastrow += '<td class="text-right">'+net_div_amount+'</td>'
						lastrow += '<td class="text-right"></td>'
						lastrow += '</tr>';

						amountInWord = toWords(net_midyear_amount).toUpperCase();


						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';

						lastrow += '<tr style="font-weight:bold;" class="text-center">';
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;" colspan="2">CERTIFIED:  Services duly rendered as stated.</td>'
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;" colspan="4">APPROVED FOR PAYMENT: '+amountInWord+' ONLY (PHP '+net_midyear_amount+')</td>';
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;" class="text-center">';
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;padding-top:20px;" colspan="2">'+signatoryOne+' <br> '+positionOne+'</td>'
						lastrow += '<td style="border:none;" colspan="5"></td>'
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;" class="text-center">';
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;padding-top:20px;" colspan="2">CERTIFIED:  Supporting documents complete and proper; and cash available in the amount of PHP '+net_midyear_amount+'</td>'
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;" colspan="3">'+signatoryTwo+' <br> '+positionTwo+'</td>'
						lastrow += '<td style="border:none;"></td>'
						lastrow += '</tr>';

						lastrow += '<tr style="font-weight:bold;" class="text-center">';
						lastrow += '<td style="border:none;"></td>'
						lastrow += '<td style="border:none;padding-top:20px;" colspan="2">'+signatoryThree+' <br> '+positionThree+'</td>'
						lastrow += '<td style="border:none;" colspan="5"></td>'
						lastrow += '</tr>';

						body += '<table></div></div>';

						$('#payroll_transfer').html(body);
						$('#lastrow').after(lastrow);

						$('#month_year').text(_Year);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection