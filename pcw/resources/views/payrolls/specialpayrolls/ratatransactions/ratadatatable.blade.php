
<table class="table table-responsive datatable" id="tbl_rata" style="width: 809px;">
	<thead>
		<tr >
			<th>Representation</th>
			<th>Transportation</th>
			<th>Additional RATA</th>
			<th>Used Vehicle</th>
			<th>Refund Amount</th>
			<th>NET</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_rata').DataTable({
	 	'dom':'<lf<t>pi>',
	 	'responsive': true,
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_rata tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {
	        $(this).removeClass('selected');

	        employee_id = $(this).data('employee_id');
	        id 			  = $(this).data('id');
			add_ra_amount = $(this).data('add_ra_amount');
			add_ta_amount = $(this).data('add_ta_amount');
			vehicle_amount = $(this).data('vehicle_amount');
			refund_amount = $(this).data('refund_amount');

			$('#rata_id').val(id);
			$('#ra_deduction').val(add_ra_amount);
			$('#ta_deduction').val(add_ta_amount);
			$('#refund_amount').val(refund_amount);
			$('#number_of_used_vehicles').val(vehicle_amount);
			$('#employee_id').val(employee_id);

	        btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );


})
</script>
