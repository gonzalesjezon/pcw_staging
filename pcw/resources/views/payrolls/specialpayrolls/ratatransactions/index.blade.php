@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr>
						<td>
								@include('payrolls.includes._months-year')
						</td>
					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process_rata" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_rata" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wrata" id="wrata" value="wrata">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wrata" id="worata" value="worata">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="rata">
			<div class="row" style="margin-left: 15px;margin-right: 15px;">
				<label style="font-weight: 600;font-size: 15px;padding-left: 5px;" id="d-name"></label>
				<div class="col-md-12">
					<div class="sub-panel" style="z-index: 1;">
						{!! $controller->showRataDatatable() !!}
					</div>
				</div>
			</div>
			<div class="row" style="margin-left: 15px;margin-right: 15px;">
				<div class="col-md-12">
					<!-- <div class="benefits-content style-box2 " id="formAddRata">
						<div class="box-1 button-style-wrapper">
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savebanks"><i class="fa fa-save"></i> New</a>
							<a class="btn btn-xs btn-info btn-savebg btn_save hidden" id="btn_save_rata"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
						</div>
						<div class="col-md-6">
							<table class="table borderless" style="border: none;">
								<tr>
									<td><label>No. of Actual Work</label></td>
									<td>
										@if ($errors->has('no_of_actual_work'))
										<span class="text-danger">{{ $errors->first('no_of_actual_work') }}</span>
										@endif
										<input type="text" name="no_of_actual_work" id="no_of_actual_work" class="form-control font-style2 onlyNumber" readonly>
									</td>
								</tr>
								<tr>
									<td><label>No. of Leave Filed</label></td>
									<td>
										@if ($errors->has('number_of_leave_field'))
										<span class="text-danger">{{ $errors->first('number_of_leave_field') }}</span>
										@endif
										<input type="text" name="number_of_leave_field" id="no_of_leave_field" class="form-control font-style2 onlyNumber" maxlength="2" disabled>
									</td>
								</tr>
								<tr>
									<table class="table">
										<label>Inclusive Date</label>
										<thead>
											<tr>
												<th>Date Filed</th>
												<th>Leave Type</th>
											</tr>
										</thead>
										<tbody id="tbl_leave">
										</tbody>
									</table>
								</tr>
								<tr>
									<td><label>Date</label></td>
									<td>
										@if ($errors->has('inclusive_date'))
										<span class="text-danger">{{ $errors->first('inclusive_date') }}</span>
										@endif
										<input type="text" name="inclusive_leave_date" id="inclusive_leave_date" class="form-control font-style2 leave-field">
									</td>
								</tr>
								<tr>
									<td><label>Leave Type</label></td>
									<td>
										@if ($errors->has('leave_type'))
										<span class="text-danger">{{ $errors->first('leave_type') }}</span>
										@endif
										<select  name="leave_type" id="leave_type" class="form-control font-style2  leave-field">
											<option value=""></option>
											<option value="VL">Vacation Leave</option>
											<option value="SL">Sick Leave</option>
											<option value="SPL">Special Priviledge Leave</option>
											<option value="PL">Paternity Leave</option>
											<option value="CTO">CTO</option>
											<option value="OB">O.B</option>
											<option value="SO">S.O</option>
											<option value="FTO">FTO</option>

										</select>
									</td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
							<table class="table borderless" style="border: none;">
								<tr>
									<td><label>No. of Work Days</label></td>
									<td>
										@if ($errors->has('no_of_work_days'))
										<span class="text-danger">{{ $errors->first('no_of_work_days') }}</span>
										@endif
										<input type="text" name="no_of_work_days" id="no_of_work_days" class="form-control font-style2 onlyNumber" readonly>
									</td>
								</tr>
								<tr>
									<td><label>No. of Used Vehicle</label></td>
									<td>
										@if ($errors->has('no_of_used_vehicles'))
										<span class="text-danger">{{ $errors->first('no_of_used_vehicles') }}</span>
										@endif
										<input type="text" name="no_of_used_vehicles" id="no_of_used_vehicles" class="form-control font-style2 onlyNumber" disabled>
									</td>
								</tr>
								<tr>
									<td><label>Percentage of RATA</label></td>
									<td>
										@if ($errors->has('percentage_of_rata'))
										<span class="text-danger">{{ $errors->first('percentage_of_rata') }}</span>
										@endif
										<select class="form-control font-style2" name="percentage_of_rata" id="percentage_of_rata" disabled>

											<option value=""></option>
											<option value=".25">1 - 5 25% of Monthly RATA</option>
											<option value=".50">6 - 11 50% of Monthly RATA</option>
											<option value=".75">12 - 16 75% of Monthly RATA</option>
											<option value="1">17 & more 100% of Monthly RATA</option>
										</select>
									</td>
								</tr>
							</table>
						</div>

					</div> -->
					<div class="benefits-content style-box2 hidden" id="formAddDeduction">
						<div class="form-group">
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="newRata" data-btnnew="newRata" data-btncancel="cancelRata" data-btnedit="editRata" data-btnsave="saveRata"><i class="fa fa-save"></i> New</a>

							<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editRata" data-btnnew="newRata" data-btncancel="cancelRata" data-btnedit="editRata" data-btnsave="saveRata"><i class="fa fa-save"></i> Edit</a>

							<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newRata" data-btncancel="cancelRata" data-btnedit="editRata" data-btnsave="saveRata" id="saveRata"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newRata" data-btncancel="cancelRata" data-form="myform" data-btnedit="editRata" data-btnsave="saveRata"id="cancelRata"> Cancel</a>
						</div>
						<form method="post" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form">
							<div class="col-md-4">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="employee_id" id="employee_id">
								<input type="hidden" name="rata_id" id="rata_id">
								<input type="hidden" name="year" id="year">
								<input type="hidden" name="month" id="month">
								<input type="hidden" name="employee_number" id="employee_number">
								<div class="panel panel-default" style="padding: 12px;">
									<div class="form-group">
										<label>Representative Allowance</label>
										<input type="text" name="ra_deduction" id="ra_deduction" class="form-control onlyNumber">
									</div>
									<div class="form-group">
										<label>Transportation Allowance</label>
										<input type="text" name="ta_deduction" id="ta_deduction" class="form-control onlyNumber">
									</div>
									<div class="form-group">
										<label>Used Vehicle</label>
										<input type="text" name="number_of_used_vehicles" id="number_of_used_vehicles" class="form-control onlyNumber">
									</div>
									<div class="form-group">
										<label>Refund</label>
										<input type="text" name="refund_amount" id="refund_amount" class="form-control onlyNumber">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>


		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblRata = $('#tbl_rata').DataTable();
	var tblEoam = $('#tbl_eoam').DataTable();
	var tblCead = $('#tbl_cead').DataTable();
	var tblPei = $('#tbl_pe').DataTable();
	var tblCashGift = $('#tbl_cashgift').DataTable();
	number_of_actual_work = 0;

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

	var _bool = false;
	var _Year;
	var _Month;
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();
		if(_bool == true){
			$('.btnfilter').trigger('click');
		}

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
		if(_bool == true){
			$('.btnfilter').trigger('click');
		}
	})

    $('.select2').select2();

    $('#select_month').trigger('change');
	$('#select_year').trigger('change');


    $('#worata').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    	$('.btn_cancel').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	$('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_cancel').addClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });

    $(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _checkrata = ""
    $('input[type=radio][name=chk_wrata]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{


    		if (this.value == 'wrata') {
    			$('#btn_process_rata').prop('disabled',true);
    			$('#formAddDeduction').removeClass('hidden');
    			$('#formAddRata').addClass('hidden');
    			_bool = true;
    			_checkrata = 'wrata';

    		}
    		else if (this.value == 'worata') {
    			$('#btn_process_rata').prop('disabled',false);
    			$('#formAddDeduction').addClass('hidden');
    			$('#formAddRata').removeClass('hidden');
    			_checkrata = 'worata';

    		}
    		$('._searchname').trigger('keyup');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}

    });
    var number_of_leave = 0;
    var no_of_actual_work;
    $(document).on('keyup','#no_of_leave_field',function(){

    	number_of_leave = $(this).val();

    	no_of_actual_work = (number_of_leave) ? (number_of_actual_work - number_of_leave) : (number_of_actual_work + number_of_leave);

    	$('#no_of_actual_work').val(no_of_actual_work);
    })

    $(document).on('change','#leave_type',function(){
    	if($(this).val()){
    		$('.benefits-content :input').attr('disabled',false);
    	}else{
    		$('.benefits-content :input').attr('disabled',true);
    		$('.leave-field').attr('disabled',false);
    	}
    });
    var representation_amount = 0;
    var transportation_amount = 0;
    var rata_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');
    	employee_number = $(this).data('employee_number');
    	$('#employee_number').val(employee_number);

    	$('#d-name').text($(this).data('employee'));
    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getRataInfo',
    			data:{
    				'id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){

    				clear_form_elements('benefits-content');
    				$('#tbl_leave').html('');
    				rata_id = '';
    				if(data.rata !== null){

	    				employee_id = data.rata.employee_id;

						// <!-- GENERATE RATA TABLE --!>

						tblRata.clear().draw();

						raAmount = (data.rata.representation_amount) ? data.rata.representation_amount : 0;
						taAmount = (data.rata.transportation_amount) ? data.rata.transportation_amount : 0;

						usedVehicleAmount 		= 0;
						refundAmount 			= 0;
						additionalRaAmount 		= 0;
						additionalTaAmount 		= 0;
						adjustmentId 			= 0;

						if(data.adjustment !== null){
							adjustmentId = data.adjustment.id;
							usedVehicleAmount = (data.adjustment.used_vehicle_amount) ? data.adjustment.used_vehicle_amount : 0;
							refundAmount = (data.adjustment.refund_amount) ? data.adjustment.refund_amount : 0;
							additionalRaAmount = (data.adjustment.ra_additional_amount) ? data.adjustment.ra_additional_amount : 0;
							additionalTaAmount = (data.adjustment.ta_additional_amount) ? data.adjustment.ta_additional_amount : 0;
						}

						additionalTotalAmount = parseFloat(additionalRaAmount) + parseFloat(additionalTaAmount);

						totalRataAmount = parseFloat(raAmount) + parseFloat(taAmount);

						netAmount = parseFloat(totalRataAmount) + parseFloat(additionalTotalAmount) + parseFloat(refundAmount) - parseFloat(usedVehicleAmount);

						ra_amount = (raAmount) ? commaSeparateNumber(parseFloat(raAmount).toFixed(2)) : '';
						ta_amount = (taAmount) ? commaSeparateNumber(parseFloat(taAmount).toFixed(2)) : '';
						add_ta_amount = (additionalTaAmount) ? commaSeparateNumber(parseFloat(additionalTaAmount).toFixed(2)) : '';
						add_ra_amount = (additionalRaAmount) ? commaSeparateNumber(parseFloat(additionalRaAmount).toFixed(2)) : '';
						vehicle_amount = (usedVehicleAmount) ? commaSeparateNumber(parseFloat(usedVehicleAmount).toFixed(2)) : '';
						refund_amount = (refundAmount) ? commaSeparateNumber(parseFloat(refundAmount).toFixed(2)) : '';
						net_amount = (netAmount) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';
						addtional_rata_amount = (additionalTotalAmount) ? commaSeparateNumber(parseFloat(additionalTotalAmount).toFixed(2)) : '';


						tblRata.row.add( [
							ra_amount,
							ta_amount,
							addtional_rata_amount,
							vehicle_amount,
							refund_amount,
							net_amount

							]).draw( false );

						tblRata.rows(0).nodes().to$().attr("data-id", adjustmentId);
				        tblRata.rows(0).nodes().to$().attr("data-employee_id", employee_id);
				        tblRata.rows(0).nodes().to$().attr("data-ra_amount", ra_amount);
				        tblRata.rows(0).nodes().to$().attr("data-ta_amount", ta_amount);
				        tblRata.rows(0).nodes().to$().attr("data-add_ra_amount", add_ra_amount);
				        tblRata.rows(0).nodes().to$().attr("data-add_ta_amount", add_ta_amount);
				        tblRata.rows(0).nodes().to$().attr("data-vehicle_amount", vehicle_amount);
				        tblRata.rows(0).nodes().to$().attr("data-refund_amount", refund_amount);
				        tblRata.rows(0).nodes().to$().attr("data-btnnew", "newRata");
				        tblRata.rows(0).nodes().to$().attr("data-btnsave", "saveRata");
				        tblRata.rows(0).nodes().to$().attr("data-btnedit", "editRata");
				        tblRata.rows(0).nodes().to$().attr("data-btndelete", "deleteRata");
				        tblRata.rows(0).nodes().to$().attr("data-btncancel", "cancelRata");

				        $("#year").val(_Year);
				        $("#month").val(_Month);
				        $('#employee_id').val(employee_id);
    				}
				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'limit':$(".limit").val(),
					'check_rata':_checkrata,
					'year':_Year,
					'month':_Month,
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();

	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processRata();
			}else{
				return false;
			}
		});
	}

});

$.processRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processRata',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'rata_id':rata_id,
			'employee_id':employee_id
		},
		type:'POST',
		beforeSend:function(){
			$('#btn_process_rata').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
		},
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					// window.location.href = base_url+module_prefix+module;
					$('._searchname').trigger('keyup');
					$('#btn_process_rata').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
				}else{

					swal({
						title: par.response,
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-warning",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});

					$('#btn_process_rata').html('<i class="fa fa-save"></i> Process').prop('disabled',false);

				}
			}
	});
}

$(document).on('click','#btn_save_rata',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.saveRata();
	}

});

$.saveRata = function(){
	$.ajax({
		type:'POST',
		url:base_url+module_prefix+module+'/storeRata',
		data:{
			'_token':'{{ csrf_token() }}',
			'employee_id':employee_id,
			'rata_id':rata_id,
			'rata':{
				'no_of_actual_work':$('#no_of_actual_work').val(),
				'no_of_work_days':$('#no_of_work_days').val(),
				'no_of_used_vehicles':$('#no_of_used_vehicles').val(),
				'percentage_of_rata':$('#percentage_of_rata :selected').text(),
				'percentage_of_rata_value':$('#percentage_of_rata :selected').val(),
				'representation_amount':representation_amount,
				'transportation_amount':transportation_amount,
				'year':_Year,
				'month':_Month
			},
			'leave':{
				'inclusive_leave_date':$('#inclusive_leave_date').val(),
				'leave_type':$('#leave_type').val(),
				'no_of_leave_field':$('#no_of_leave_field').val(),
			}

		},
		success:function(data){
			par = JSON.parse(data);

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				}).then(function(){

					$('.benefits-content :input').prop('disabled',true);
					clear_form_elements('benefits-content');
					$('#tbl_leave').html('');
					$('.btn_save').addClass('hidden');
					$('.btn_new').addClass('hidden');
					$('.btn_edit').removeClass('hidden');

					employee_id = ''
					rata_id = '';
					number_of_actual_work = (par.rata.rata.number_of_actual_work) ? par.rata.rata.number_of_actual_work : par.rata.number_of_work_days;
					$('#no_of_actual_work').val(number_of_actual_work);
					$('#no_of_work_days').val(par.rata.rata.number_of_work_days);

					arr = [];
					leave_type = [];
					number_of_leave = [];
					$.each(par.rata.leave,function(k,v){
						arr += '<tr>';
						arr += '<td>'+v.leave_date+'</td>';

						arr += '<td>'+v.leave_type+'</td>';
						arr += '</tr>';

					})
					$('#tbl_leave').html(arr);
					item = '';
					$.each(par.rata.leave_list,function(k,v){
						item += v+', ('+k+')';
					});
						// GENERATE TR FOR BENEFITS TAB

						percentage = (par.rata.rata.percentage_of_rata_value) ? (par.rata.rata.percentage_of_rata_value * 100) : '100';

						tblRata.clear().draw();

						representation_amount = (par.rata.rata.representation_amount) ? par.rata.rata.representation_amount : 0;
						transportation_amount = (par.rata.rata.transportation_amount) ? par.rata.rata.transportation_amount : 0;
						total_rata = (parseFloat(representation_amount) + parseFloat(transportation_amount));

						tblRata.row.add( [
							representation_amount,
							transportation_amount,
							item,
							number_of_actual_work,
							percentage+'%',
							representation_amount,
							transportation_amount,
							total_rata.toFixed(2)

							]).draw( false );
					});


			}else{
				swal({
					title: par.response,
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-warning",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})
			}
		}
	});
}

$(document).on('click','#delete_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteRata();
				tblRata.clear().draw();
			}else{
				return false;
			}
		});
	}
})

$.deleteRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteRata',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_rata').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_rata').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('._searchname').trigger('keyup');
			}
		}
	});
}
var btn;
/*serialize All form ON SUBMIT*/
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);

	$("#form").ajaxForm({
		beforeSend:function(){

		},
		success:function(data){
			par  =  JSON.parse(data);
			if(par.status){

				swal({  title: par.response,
						text: '',
						type: "success",
						icon: 'success',

					});

			}else{

				swal({  title: par.response,
						text: '',
						type: "error",
						icon: 'error',

					});

			}

			$('.btn_cancel').addClass('hidden').trigger('click');
			_checkrata = 'wrata';
			$('._searchname').trigger('keyup');
			_listId = [];

			btn.button('reset');
		},
		error:function(data){
			$error = data.responseJSON;
			/*reset popover*/
			$('input[type="text"], select').popover('destroy');

			/*add popover*/
			block = 0;
			$(".error-msg").remove();
			$.each($error.errors,function(k,v){
				var messages = v.join(', ');
				msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
				$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
				if(block == 0){
					$('html, body').animate({
				        scrollTop: $('.err-'+k).offset().top - 250
				    }, 500);
				    block++;
				}
			})
			$('.saving').replaceWith(btn);
		},
		always:function(){
			setTimeout(function(){
					$('.saving').replaceWith(btn);
				},300)
		}
	}).submit();

});

});


</script>
@endsection