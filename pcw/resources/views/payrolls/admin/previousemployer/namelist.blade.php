<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody >
		@foreach($data as $key => $value)
		<tr data-empid="{{ $value->id }}" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary" data-empname="{{ strtoupper($value->lastname) }} {{ strtoupper($value->firstname) }} {{ strtoupper($value->middlename) }}">
			<td>[ {{ $value->id }} ] {{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
