<div class="col-md-12">
	<div class="box-1 button-style-wrapper" >
			<a class="btn btn-xs btn-info btn-savebg btn_new" id="newDeduct" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct"><i class="fa fa-save"></i> New</a>

			<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" id="editDeduct"><i class="fa fa-edit"></i> Edit</a>

			<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" data-form="form5" id="saveDeduct"><i class="fa fa-save"></i> Save</a>

			<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" id="cancelDeduct" data-form="myform5"> Cancel</a>
	</div>
	<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeDeductioninfo')}}" onsubmit="return false" id="form5" class="myform5">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="formcontent">
			<table class="table borderless">
				<tr>
					<td>Deduction Name</td>
					<td>
						<select class="form-control font-style2  newDeduct " name="deduction_id" id="deduction_id">
							<option value=""></option>
							@foreach($deductions as $value)
							<option value="{{ $value->id }}" data-code="{{ $value->code }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</td>
					<td>Amount</td>
					<td>
						<input type="text" name="deduct_amount" class="form-control font-style2 onlyNumber newDeduct" id="input_deductamount">
					</td>
				</tr>
				<tr>
					<td>Pay Period</td>
					<td>
						<select class="form-control font-style2  newDeduct " name="deduct_pay_period" id="select_deductperiod">
							<option value=""></option>
							<option value="semimonthly">Semi Monthly</option>
							<option value="monthly">Monthly</option>
						</select>
					</td>
					<td>

					</td>
					<td class="text-right"><label class="font-style2" style="color:#000;font-weight: normal;    position: relative;top: 20px;">
								<input type="checkbox" name="chk_deduct" id="chk_deduct" >
							Terminated
							</label></td>
				</tr>
				<tr>
					<td>Date Start</td>
					<td>
						<input type="text" name="deduct_date_start" id="date_deductstart" class="form-control font-style2 datepicker newDeduct">
					</td>
					<td >Date Terminated</td>
					<td>
						<input type="text" name="deduct_date_terminated" id="date_deductterminated" class="form-control font-style2 datepicker" disabled>
					</td>

				</tr>
				<tr>
					<td>Date End</td>
					<td>
						<input type="text" name="deduct_date_end" id="date_deductend" class="form-control font-style2 datepicker newDeduct">
					</td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
		<input type="hidden" name="id" id="deduction_info_id">
		<input type="hidden" name="employee_id" class="employee_id">
		<input type="hidden" name="employeeinfo_id" class="employeeinfo_id">
		<input type="hidden" name="employee_number" class="employee_number">
	</form>

	</div>