<div class="col-md-12">
	<div class="box-1 button-style-wrapper" >
		<a class="btn btn-xs btn-info btn-savebg btn_new" id="newBenefit" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit"><i class="fa fa-save"></i> New</a>
			<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" id="editBenefit"><i class="fa fa-edit"></i> Edit</a>

			<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="form3" id="saveBenefit"><i class="fa fa-save"></i> Save</a>

			<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="myform3" id="cancelBenefit"> Cancel</a>
	</div>
	<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeBenefitinfo')}}" onsubmit="return false" id="form3" class="myform3" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="formcontent">
			<table class="table borderless">
				<tr>
					<td>
						<span>Effective Date</span>
					</td>
					<td>
						<input type="text" name="benefit_effectivity_date" id="benefit_effectivedate" class="form-control font-style2 datepicker newBenefit">
					</td>
					<td>
						<span>Pay Period</span>
					</td>
					<td>
						<select class="form-control font-style2 newBenefit" name="benefit_pay_period" id="benefits_payperiod">
							<option value=""></option>
							<option value="Weekly">Weekly</option>
							<option value="Semi Monthly">Semi Monthly</option>
							<option value="Monthly">Monthly</option>
						</select>
					</td>

				</tr>
				<tr>
					<td>
						<span>Benefits</span>
					</td>
					<td>
						<select class="form-control font-style2 newBenefit" name="benefit_id" id="benefit_id">
							<option value=""></option>
							@foreach($benefit as $value)
							<option data-amount="{{ $value->amount }}"  value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</td>
					<td>


					</td>
					<td>
						<div class="weekly hidden">
							<div class="col-md-5">
								<span>
									<input type="radio" name="weekly" id="week1" value="Week 1" >Week 1
								</span>
							</div>
							<div class="col-md-5">
								<span>
									<input type="radio" name="weekly" id="week2" value="Week 2" >Week 2
								</span>
							</div>
						</div>
						<div class="semi-monthly hidden">
							<div class="col-md-5">
								<span>
									<input type="radio" name="semi_monthly" id="firsthalf" value="First Half" >First Half
								</span>
							</div>
							<div class="col-md-5">

								<span>
									<input type="radio" name="semi_monthly" id="secondhalf" value="Second Half" >Second Half
								</span>
							</div>
						</div>

					</td>
				</tr>
				<tr>
					<td>
						<span>Description</span>
					</td>
					<td>
						<input type="text" name="benefit_description" class="form-control font-style2 newBenefit" id="benefits_description">
					</td>
					<td></td>
					<td>
						<div class="weekly hidden">
							<div class="col-md-5">
								<span>
									<input type="radio" name="weekly" id="week3" value="Week 3" >Week 3
								</span>
							</div>
							<div class="col-md-5">
								<div>
								<span>
									<input type="radio" name="weekly" id="week4" value="Week 4" >Week 4
								</span>
							</div>

						</div>
					</td>
				</tr>
				<tr>
					<td>
						<span>Amount</span>
					</td>
					<td>
						<input type="text" name="benefit_amount" id="benefits_amount" class="form-control font-style2 onlyNumber newBenefit">
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Covered Period</td>
					<td>
						<div class="col-md-6">
							<input type="text" name="date_from" id="date_from" class="form-control newBenefit datepicker" placeholder="Date From">

						</div>
						<div class="col-md-6">

							<input type="text" name="date_to" id="date_to" class="form-control newBenefit datepicker" placeholder="Date To">
						</div>
					</td>
					<td colspan="2"></td>
				</tr>
			</table>
		</div>
		<input type="hidden" name="id" id="benefit_info_id">
		<input type="hidden" name="employeeinfo_id" id="employeeinfo_id" class="employeeinfo_id">
		<input type="hidden" name="employee_id"  class="employee_id">
		<input type="hidden" name="employee_number" class="employee_number">
	</form>
</div>