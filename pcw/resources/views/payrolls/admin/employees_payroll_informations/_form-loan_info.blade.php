<div class="col-md-12">
	<div class="box-1 button-style-wrapper" >
		<a class="btn btn-xs btn-info btn-savebg btn_new" id="newLoan" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan"><i class="fa fa-save"></i> New</a>

			<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan" id="editLoan"><i class="fa fa-edit"></i> Edit</a>

			<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan" data-form="form4" id="saveLoan"><i class="fa fa-save"></i> Save</a>

			<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan"   id="cancelLoan" data-form="myform4"> Cancel</a>
	</div>
	<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeLoaninfo')}}" onsubmit="return false" id="form4" class="myform4">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="formcontent">
			<table class="table borderless">
				<tr>
					<td>Loan Name</td>
					<td>
						<select class="form-control font-style2  newLoan " name="loan_id" id="loan_id">
							<option value=""></option>
							@foreach($loans as $value)
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</td>
					<td class="text-right">Pay Period</td>
					<td>
						<select class="form-control font-style2  newLoan " name="loan_pay_period" id="loan_payperiod">
							<option value=""></option>
							<option value="semimonthly">Semi Monthly</option>
							<option value="monthly">Monthly</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Date Granted</td>
					<td>
						<input type="text" name="loan_date_granted" class="form-control font-style2 datepicker newLoan" id="loan_dategranted">
					</td>
					<td class="text-right">Date Start</td>
					<td>
						<input type="text" name="loan_date_started" class="form-control font-style2 datepicker newLoan" id="loan_datestart">
					</td>

				</tr>
				<tr>
					<td>Total Loan Amount</td>
					<td>
						<input type="text" name="loan_totalamount" class="form-control font-style2 onlyNumber newLoan" id="total_loanamount">
					</td>
					<td class="text-right">Date End</td>
					<td>
						<input type="text" name="loan_date_end" id="loan_dateend" class="form-control font-style2 datepicker newLoan">
					</td>
				</tr>
				<tr>
					<td>Total Loan Balance</td>
					<td>
						<input type="text" name="loan_totalbalance" id="loan_totalbalance" class="form-control font-style2 onlyNumber newLoan">
					</td>
					<td></td>
					<td class="text-right" >
						<label class="font-style2" style="color:#000;font-weight: normal;    position: relative;top: 20px;">
								<input type="checkbox" name="chk_loanterminated" id="chk_laonterminated" >
							Terminated
							</label>
					</td>

				</tr>
				<tr>
					<td>Amortization</td>
					<td>
						<input type="text" name="loan_amortization" id="loan_amortization" class="form-control font-style2 onlyNumber newLoan">
					</td>
					<td class="text-right">Date Terminated</td>
					<td>
						<input type="text" name="loan_date_terminated" id="date_dateterminated" class="form-control font-style2 datepicker" disabled>
					</td>

				</tr>
			</table>

		</div>
		<input type="hidden" name="employee_id" class="employee_id">
		<input type="hidden" name="id" id="loan_info_id">
		<input type="hidden" name="employeeinfo_id" class="employeeinfo_id">
		<input type="hidden" name="employee_number" class="employee_number">
	</form>
</div>