<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_loans">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Type</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}" class="text-center" >
					<td>{{ $value->code }}</td>
					<td>{{ $value->name }}</td>
					<td>{{ $value->loan_type }}</td>
					<td>
						<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteItem" data-id="{{ $value->id }}"><i class="fa fa-trash"></i> Delete</a>'
					</td>
				</tr>
				@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_loans').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_loans tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

	$('#tbl_loans tr').on('click',function(){
		var id = 0;
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getItem',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#code').val(data.code);
				$('#name').val(data.name);
				$('#loan_type').val(data.loan_type);
				$('#category').val(data.category);
				$('#gsis_excel_col').val(data.gsis_excel_col);

				$('#for_update').val(data.id);
			}
		})
	});

})
</script>
