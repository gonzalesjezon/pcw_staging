<div class="col-md-12">
	<table id="tbl_benefits" class="table table-responsive datatable">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Type</th>
				<th>Computation Type</th>
				<th>Amount</th>
				<th>Remarks</th>
				<th>
					Action
				</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->code }}</td>
					<td>{{ $value->name }}</td>
					<td>{{ $value->tax_type }}</td>
					<td>{{ $value->computation_type }}</td>
					<td>{{ $value->amount }}</td>
					<td>{{ $value->remarks }}</td>
					<td>
						<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteItem" data-id="{{ $value->id }}"><i class="fa fa-trash"></i> Delete</a>'
					</td>
				</tr>
				@endforeach

		</tbody>
	</table>

</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_benefits').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_benefits tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	    }else {

	        table.$('tr.selected').removeClass('selected');

	        $(this).addClass('selected');
	    }
	});

})
</script>
