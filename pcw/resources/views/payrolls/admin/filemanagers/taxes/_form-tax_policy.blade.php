<div class="benefits-content style-box2 newPolicy">
	<div class="col-md-12">
		<div class="col-md-6">
			<table class="table borderless" style="border: none;">
				<tr>
					<td><label>Policy Name</label></td>
					<td>
						@if ($errors->has('policy_name'))
						    <span class="text-danger">{{ $errors->first('policy_name') }}</span>
						@endif
						<input type="text" name="policy_name" id="policy_name" class="form-control font-style2">
					</td>
				</tr>
				<tr>
					<td><label>Pay Period</label></td>
					<td>
						@if ($errors->has('pay_period'))
						    <span class="text-danger">{{ $errors->first('pay_period') }}</span>
						@endif
						<select name="pay_period" id="pay_period" class="form-control font-style2">
							<option value=""></option>
							<option value="Semi-Monthly">Semi-Monthly</option>
							<option value="Monthly">Monthly</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Deduction Period</label></td>
					<td>
						@if ($errors->has('deduction_period'))
						    <span class="text-danger">{{ $errors->first('deduction_period') }}</span>
						@endif
						<select name="deduction_period" id="deduction_period" class="form-control font-style2">
							<option value=""></option>
							<option value="Both">Both</option>
							<option value="First Half">First Half</option>
							<option value="Second Half">Second Half</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Policy Type</label></td>
					<td>
						@if ($errors->has('policy_type'))
						    <span class="text-danger">{{ $errors->first('policy_type') }}</span>
						@endif
						<select name="policy_type" id="policy_type" class="form-control font-style2">
							<option value=""></option>
							<option value="System Generated">System Generated</option>
							<option value="Inputted">Inputted</option>

						</select>
					</td>
				</tr>
				<tr>
					<td><label>Job Grade Rate</label></td>
					<td>
						@if ($errors->has('job_grade_rate'))
						    <span class="text-danger">{{ $errors->first('job_grade_rate') }}</span>
						@endif
						<select name="job_grade_rate" id="job_grade_rate" class="form-control font-style2">
							<option value=""></option>
							<option value="0.02">2 %</option>
							<option value="0.03">3 %</option>
							<option value="0.05">5 %</option>
							<option value="0.10">10 %</option>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="col-md-6">
			<table class="table borderless" style="border: none;">
				<tr>
					<td><label>Base On</label></td>
					<td>
						@if ($errors->has('based_on'))
						    <span class="text-danger">{{ $errors->first('based_on') }}</span>
						@endif
						<select name="based_on" id="based_on" class="form-control font-style2">
							<option value=""></option>
							<option value="Gross Salary">Gross Salary</option>
							<option value="Gross Taxable">Gross Taxable</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>Computation</label></td>
					<td>
						@if ($errors->has('computation'))
						    <span class="text-danger">{{ $errors->first('computation') }}</span>
						@endif
						<select name="computation" id="computation" class="form-control font-style2">
							<option value=""></option>
							<option value="Gross Taxable">Gross Taxable</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<input type="radio" name="is_withholding" id="chk_monthly" value="Monthly" >
						<label>Monthly</label>

					</td>
				</tr>
				<tr>
					<td>
						<input type="radio" name="is_withholding" id="chk_annual" value="Annual">
						<label>Annualized</label>

					</td>
				</tr>
			</table>
		</div>
	</div>
</div>