<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_banks">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Branch Name</th>
				<th>Account No</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->code }}</td>
					<td>{{ $value->name }}</td>
					<td>{{ $value->branch_name }}</td>
					<td>{{ $value->bank_accountno }}</td>
				</tr>
				@endforeach
			
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_banks').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_banks tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

	$('#tbl_banks tr').on('click',function(){
		var id = 0;	
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getItem',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#code').val(data.code);
				$('#name').val(data.name);
				$('#branch_name').val(data.branch_name);
				$('#bank_accountno').val(data.bank_accountno);
				$('#for_update').val(data.id);
			}
		})
	});

})
</script>
