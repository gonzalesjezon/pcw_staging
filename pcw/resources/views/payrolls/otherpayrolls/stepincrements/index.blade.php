@extends('app-front')

@section('content')
<style type="text/css">
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
.panel{
	padding: 25px;
}
</style>

<div class="row" style="padding: 40px 10px 0px 10px;">
	<div class="col-md-12">
		{!! $controller->showStepIncrement() !!}
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-3">
			<input type="text" name="filter_search" class="form-control search1" placeholder="Search here">
			<div style="height: 5px;"></div>
			<div class="sub-panelnamelist ">
				{!! $controller->show() !!}
			</div>
		</div>

		<div class="col-md-9">
			<div class="row" style="margin-left: 2px;">
				<div class="col-md-12">
					<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="newSalary" data-btnnew="newSalary" data-btncancel="cancelSalary" data-btnedit="editSalary" data-btnsave="saveSalary"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editSalary" data-btnnew="newSalary" data-btncancel="cancelSalary" data-btnedit="editSalary" data-btnsave="saveSalary"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newSalary" data-btncancel="cancelSalary" data-btnedit="editSalary" data-btnsave="saveSalary" id="saveSalary"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newSalary" data-btncancel="cancelSalary" data-form="myform" data-btnedit="editSalary" data-btnsave="saveSalary"id="cancelSalary"> Cancel</a>
					</div>
				</div>
			</div>
			<div class="row" style="padding: 20px;">
				<div class="col-md-12">
				<label id="employee_name" style="margin-left:5px; "></label>
					<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="employee_id" id="employee_id">
						<input type="hidden" name="year" id="year">
						<input type="hidden" name="month" id="month">
						<input type="hidden" name="salaryadjustment_id" id="salaryadjustment_id">
						<div class="col-md-5">
							<div class="panel">
								<table class="table noborder borderless">
									<tbody class="noborder-top">
										<tr>
											<td colspan="2">
												<span>Transaction Date</span>
											</td>
										</tr>
										<tr class="newSalary">
											<td colspan="2">
												<input type="text" name="transaction_date" id="transaction_date" class="form-control datepicker font-style2" >
											</td>
										</tr>
										<tr>
											<td>From</td>
											<td>To</td>
										</tr>
										<tr class="newSalary">
											<td>
												<input type="text" name="date_from" id="date_from" class="form-control font-style2 datepicker">
											</td>
											<td>
												<input type="text" name="date_to" id="date_to" class="form-control font-style2 datepicker">
											</td>
										</tr>
										<tr>
											<td colspan="2">Basic Pay</td>
										</tr>
										<tr class="newSummary">
											<td>
												<input type="text" name="new_position" id="new_position" class="form-control font-style2" readonly>
											</td>
											<td>
												<input type="text" name="new_rate_amount" id="new_rate_amount" class="form-control font-style2" readonly>
											</td>
										</tr>
										<tr class="newSummary">
											<td>
												<input type="text" name="old_position" id="old_position" class="form-control font-style2" readonly>
											</td>
											<td>
												<input type="text" name="old_rate_amount" id="old_rate_amount" class="form-control font-style2" readonly>
											</td>
										</tr>
										<tr>
											<td>Salary Adjustment</td>
											<td>
												<input type="text" name="adjustment_amount" id="adjustment_amount" class="form-control font-style2" readonly>
											</td>
										</tr>
										<tr>
											<td>Gross Basic Pay</td>
											<td>
												<input type="text" name="basic_amount" id="basic_amount" class="form-control font-style2" readonly>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="panel">
								<label>Deduction Period</label>
								<table class="table borderless noborder">
									<tbody class="noborder-top">
										<tr>
											<td>First Week</td>
											<td>
												<input type="text" name="first_amount" id="first_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
											</td>
										</tr>
										<tr>
											<td>Second Week</td>
											<td>
												<input type="text" name="second_amount" id="second_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
											</td>
										</tr>
										<tr>
											<td>Third Week</td>
											<td>
												<input type="text" name="third_amount" id="third_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
											</td>
										</tr>
										<tr>
											<td>Fourth Week</td>
											<td>
												<input type="text" name="fourth_amount" id="fourth_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel">
								<table class="table noborder borderless">
									<tbody class="noborder-top">
										<tr>
											<td colspan="2">
												<span>Less</span>
											</td>
										</tr>
										<tr>
											<td>Gsis Contribution</td>
											<td>
												<input type="text" name="gsis_cont_amount" id="gsis_cont_amount" class="form-control font-style2" readonly>
											</td>
										</tr>
										<!-- <tr>
											<td>Provident Fund Contribution</td>
											<td>
												<input type="text" name="pf_cont_amount" id="pf_cont_amount" class="form-control font-style2" readonly>
											</td>
										</tr> -->
										<tr>
											<td>Withholding Tax</td>
											<td>
												<input type="text" name="tax_amount" id="tax_amount" class="form-control font-style2" readonly>
											</td>
										</tr>
										<tr>
											<td>Total Deductions</td>
											<td>
												<input type="text" name="total_deduction_amount" id="total_deduction_amount" class="form-control font-style2" readonly>
											</td>
										</tr>
										<tr>
											<td>Net Amount</td>
											<td>
												<input type="text" name="net_amount" id="net_amount" class="form-control font-style2" readonly>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>


</div>
<!-- <div class="ajax-loader">
  <img src="{{ asset('images/ajax-loader1.gif') }}" class="img-responsive" />
</div> -->
<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
}
$('#select_month').html(mArr);


// ************************************************
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	$('#year').val(_Year);

})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	$('#month').val(_Month);
})

$('.select2').select2();

$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});

	var _monthlyRate;
	var taxAmountBR;
	var _taxDue;

	$('.newSalary :input').attr('disabled',true);
	$('.newSalary').attr('disabled',true);

	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
		_taxDue = 0;
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btndelete).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('#'+btndelete).addClass('hidden');
		$('.weekly').addClass('hidden');
		$('.semi-monthly').addClass('hidden');

		$('#employee_name').text('');
		$('#otherpayroll_id').val('');
		form = $(this).data('form');
		clear_form_elements(form);
		clear_form_elements('nonplantilla');
		$('.error-msg').remove();

	});

	$('.select2').select2();

	$('.onlyNumber').keypress(function (event) {
		return isNumber(event, this)
	});

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});


	// DATE PICKER
	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});

var basic_amount;
var pera_amount;
var gross_amount;
var total_deductions_amount;
var net_amount;
var gsis_premium_amount;
var philhealth_amount;;
var hdmf_amount;
var witholding_tax_amount;
$(document).on('keyup','#basic_salary_amount',function(){
	basic_amount = $(this).val().replace(',','');

	gross_amount = compute_gross_salary(basic_amount,pera_amount);

	net_amount = compute_netincome(gross_amount,total_deductions_amount);


	gross_amount = (gross_amount) ?  commaSeparateNumber(parseFloat(gross_amount)) : '';
	net_amount = (net_amount) ?  commaSeparateNumber(parseFloat(net_amount)) : '';

	$('#gross_salary_amount').val(gross_amount);
	$('#net_amount').val(net_amount);

})
$(document).on('keyup','#pera_amount',function(){
	pera_amount = $(this).val().replace(',','');

	gross_amount = compute_gross_salary(basic_amount,pera_amount);

	net_amount = compute_netincome(gross_amount,total_deductions_amount);


	gross_amount = (gross_amount) ?  commaSeparateNumber(parseFloat(gross_amount)) : '';
	net_amount = (net_amount) ?  commaSeparateNumber(parseFloat(net_amount)) : '';

	$('#gross_salary_amount').val(gross_amount);
	$('#net_amount').val(net_amount);

})
$(document).on('keyup','#gsis_premium_amount',function(){
	gsis_premium_amount = $(this).val().replace(',','');

	total_deductions_amount = compute_deductions(gsis_premium_amount,philhealth_amount,hdmf_amount,witholding_tax_amount);

	net_amount = compute_netincome(gross_amount.replace(',',''),total_deductions_amount);

	deduction_amount= (total_deductions_amount) ?  commaSeparateNumber(parseFloat(total_deductions_amount)) : '';
	net_amount = (net_amount) ?  commaSeparateNumber(parseFloat(net_amount)) : '';

	$('#total_deductions_amount').val(deduction_amount);
	$('#net_amount').val(net_amount);

})

$(document).on('keyup','#philhealth_amount',function(){
	philhealth_amount = $(this).val().replace(',','');

	total_deductions_amount = compute_deductions(gsis_premium_amount,philhealth_amount,hdmf_amount,witholding_tax_amount);
	net_amount = compute_netincome(gross_amount.replace(',',''),total_deductions_amount);

	deduction_amount= (total_deductions_amount) ?  commaSeparateNumber(parseFloat(total_deductions_amount)) : '';
	net_amount = (net_amount) ?  commaSeparateNumber(parseFloat(net_amount)) : '';

	$('#total_deductions_amount').val(deduction_amount);
	$('#net_amount').val(net_amount);

})
$(document).on('keyup','#hdmf_amount',function(){
	hdmf_amount = $(this).val().replace(',','');

	total_deductions_amount = compute_deductions(gsis_premium_amount,philhealth_amount,hdmf_amount,witholding_tax_amount);
	net_amount = compute_netincome(gross_amount.replace(',',''),total_deductions_amount);

	deduction_amount= (total_deductions_amount) ?  commaSeparateNumber(parseFloat(total_deductions_amount)) : '';
	net_amount = (net_amount) ?  commaSeparateNumber(parseFloat(net_amount)) : '';

	$('#total_deductions_amount').val(deduction_amount);
	$('#net_amount').val(net_amount);

})

$(document).on('keyup','#witholding_tax_amount',function(){
	witholding_tax_amount = $(this).val().replace(',','');

	total_deductions_amount = compute_deductions(gsis_premium_amount,philhealth_amount,hdmf_amount,witholding_tax_amount);
	net_amount = compute_netincome(gross_amount.replace(',',''),total_deductions_amount);

	deduction_amount= (total_deductions_amount) ?  commaSeparateNumber(parseFloat(total_deductions_amount)) : '';
	net_amount = (net_amount) ?  commaSeparateNumber(parseFloat(net_amount)) : '';

	$('#total_deductions_amount').val(deduction_amount);
	$('#net_amount').val(net_amount);

})

function compute_gross_salary(basic_amount,pera_amount){
	basic_amount = (basic_amount) ? basic_amount : 0;
	pera_amount = (pera_amount) ? pera_amount : 0;

	return (parseFloat(basic_amount) + parseFloat(pera_amount));

}

function compute_deductions(gsis_premium_amount,philhealth_amount,hdmf_amount,witholding_tax_amount){
	gsis_premium_amount = (gsis_premium_amount) ? gsis_premium_amount : 0;
	philhealth_amount = (philhealth_amount) ? philhealth_amount : 0;
	hdmf_amount = (hdmf_amount) ? hdmf_amount : 0;
	witholding_tax_amount = (witholding_tax_amount) ? witholding_tax_amount : 0;

	return (parseFloat(gsis_premium_amount) + parseFloat(philhealth_amount) + parseFloat(hdmf_amount) + parseFloat(witholding_tax_amount));
}

function compute_netincome(gross_income,deducutions){
	gross_income = (gross_income) ? gross_income : 0;
	deducutions = (deducutions) ? deducutions : 0;

	net_pay = (parseFloat(gross_income) - parseFloat(deducutions));
	return (net_pay > 0) ?  net_pay : 0;
}

var tStepIncrement = $('#tbl_initial_salary').DataTable();
$(document).on('click','#namelist tr',function(){

	employee_id = $(this).data('empid');
	$('#employee_id').val(employee_id);
	fullname = $(this).data('fullname');
	$('#employee_name').text(fullname);

	$.ajax({
		url:base_url+module_prefix+module+'/getStepIncrement',
		data:{
			'id':employee_id
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			clear_form_elements('myform');
			$('.error-msg').text('');
			$('.btn_edit').addClass('hidden');
			$('.btn_cancel').addClass('hidden');
			$('.btn_new').removeClass('hidden');

			if(data.salaryinfo !== null){

				daysInAMonth = data.days_in_month;
				daysFromStep = data.days_from_step - 1;
				// providentRate = (data.deductioninfo) ? data.deductioninfo.deduction_rate : 0;

				oldRateAmount = (data.salaryinfo.salary_old_rate) ? data.salaryinfo.salary_old_rate : 0;
				newRateAmount = (data.salaryinfo.salary_new_rate) ? data.salaryinfo.salary_new_rate : 0;
				salaryAdjustment = parseFloat(newRateAmount) - parseFloat(oldRateAmount);

				days = (daysFromStep / daysInAMonth);

				grossBasicPay = parseFloat(salaryAdjustment) * days;
				gsisContAmount = (parseFloat(salaryAdjustment) / daysInAMonth) * daysFromStep;
				// providentAmount = (parseFloat(grossBasicPay) * providentRate);
				taxAmount = (parseFloat(grossBasicPay) - parseFloat(gsisContAmount)) * .25;
				// deductionAmount = (parseFloat(taxAmount) + parseFloat(gsisContAmount) + parseFloat(providentAmount));
				deductionAmount = (parseFloat(taxAmount) + parseFloat(gsisContAmount));
				netAmount = (parseFloat(grossBasicPay) - parseFloat(deductionAmount));

				old_rate_amount = (oldRateAmount !== 0) ? commaSeparateNumber(parseFloat(oldRateAmount).toFixed(2)) : '';
				new_rate_amount = (newRateAmount !== 0) ? commaSeparateNumber(parseFloat(newRateAmount).toFixed(2)) : '';
				adjustment_amount = (salaryAdjustment !== 0) ? commaSeparateNumber(parseFloat(salaryAdjustment).toFixed(2)) : '';
				basic_amount = (grossBasicPay !== 0) ? commaSeparateNumber(parseFloat(grossBasicPay).toFixed(2)) : '';
				gsis_cont_amount = (gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(gsisContAmount).toFixed(2)) : '';
				tax_amount = (taxAmount !== 0) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
				deduction_amount = (deductionAmount !== 0) ? commaSeparateNumber(parseFloat(deductionAmount).toFixed(2)) : '';
				net_amount = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';
				// provident_amount = (providentAmount !== 0) ? commaSeparateNumber(parseFloat(providentAmount).toFixed(2)) : '';

				$('#new_rate_amount').val(new_rate_amount);
				$('#old_rate_amount').val(old_rate_amount);
				$('#adjustment_amount').val(adjustment_amount);
				$('#basic_amount').val(basic_amount);
				$('#gsis_cont_amount').val(gsis_cont_amount);
				// $('#pf_cont_amount').val(provident_amount);
				$('#tax_amount').val(tax_amount);
				$('#total_deduction_amount').val(deduction_amount);
				$('#net_amount').val(net_amount);

			}

			if(data.stepincrement.length !== 0){
				datatable(data);
			}
		}
	});
});

function datatable(data){
	tStepIncrement.clear().draw();

	$.each(data.stepincrement,function(k,v){

		adjustmentAmount = (v.salary_adjustment_amount) ? v.salary_adjustment_amount : 0;

		adjust_amount = (adjustmentAmount !== 0) ? commaSeparateNumber(parseFloat(adjustmentAmount).toFixed(2)) : '';

		tStepIncrement.row.add( [
        	adjust_amount,
        	'',
        	adjust_amount,
        	'',
        	'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteStepIncrement" data-id="'+v.id+'"  data-transaction_date="'+v.transaction_date+'" data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'
        ]).draw( false );

        tStepIncrement.rows(k).nodes().to$().attr("data-id", v.id);
        tStepIncrement.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
        tStepIncrement.rows(k).nodes().to$().attr("data-old_basic_pay_amount", v.old_basic_pay_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-new_basic_pay_amount", v.new_basic_pay_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-salary_adjustment_amount", v.salary_adjustment_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-gsis_cont_amount", v.gsis_cont_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-philhealth_cont_amount", v.philhealth_cont_amount);
        // tStepIncrement.rows(k).nodes().to$().attr("data-provident_fund_amount", v.provident_fund_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-wtax_amount", v.wtax_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-first_deduction_amount", v.first_deduction_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-second_deduction_amount", v.second_deduction_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-third_deduction_amount", v.third_deduction_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-fourth_deduction_amount", v.fourth_deduction_amount);
        tStepIncrement.rows(k).nodes().to$().attr("data-date_from", v.date_from);
        tStepIncrement.rows(k).nodes().to$().attr("data-date_to", v.date_to);
        tStepIncrement.rows(k).nodes().to$().attr("data-transaction_date", v.transaction_date);
        tStepIncrement.rows(k).nodes().to$().attr("data-btnnew", "newSalary");
        tStepIncrement.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
        tStepIncrement.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
        tStepIncrement.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
	});
}

//SUBMIT FORM
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);
	form = $(this).data('form');

		$('#'+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							// $('.btn_cancel').trigger('click');
							window.location.href = base_url+module_prefix+module;


						});// end swal

				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}// end of main IF STATUS

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error.errors,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});


var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	category  	= $('#select_searchvalue :selected').val();
	empstatus   = $('#emp_status :selected').val();
	emp_type    = $('#emp_type :selected').val();
	searchby    = $('#searchby :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {'category':category,'empstatus':empstatus,'emp_type':emp_type,'searchby':searchby },
					   beforeSend:function(){
					   		// $('#loading').removeClass('hidden');
					   },
					   complete:function(){
					   		// $('#loading').addClass('hidden');
					   },
					   success: function(res){
					      $(".sub-panelnamelist").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


$(document).on('keyup','.search1',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {"q":$('.search1').val(),'limit':$(".limit").val()},
			   beforeSend:function(){

			   },
			   success: function(res){
			      $(".sub-panelnamelist").html(res);

			   },
			   complete:function(){

			   }
			});
		},500);
});


$(document).on('click','.delete_item',function(){
	id 				= $(this).data('id');
	date 			= $(this).data('transaction_date');
	employee_id 	= $(this).data('employee_id');
	function_name 	= $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){

				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'date':date,
						'employee_id':employee_id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(data){

						swal({
							  title: 'Delete Successfully',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						})

						datatable(data);
					}

				})

			}else{
				return false;
			}
		});
	}
})


})
</script>
@endsection
