@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td><span>Employee Status</span></td>
					</tr>
					<tr>
						<td >
							<select class="employee-type form-control font-style2" id="employee_status" name="employee_status">
								<option value=""></option>
								@foreach($empstatus as $key => $value)
								<option value="{{ $value->RefId }}">{{ $value->Name }}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<tr>
						<td >
								@include('payrolls.includes._months-year')
						</td>
					</tr>
					<tr>
						<td>
							<b>Pay Period</b>
						</td>
					</tr>
					<tr>
						<td >
							<select class="form-control font-style2" id="pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</td>
					</tr>
				</table>
				<div class="col-md-4">
					<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				</div>
				<div class="col-md-8 text-right">
					<button class="btn btn-xs btn-info" id="process_payroll" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_payroll" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>

				</div>
				<div class="search-btn">

					<div class="col-md-12">
						<span>Search</span>

							<label class="radiobut-style radio-inline ">
								<input type="radio" name="chk_wpayroll" id="wpayroll" value="wpayroll">
								W/Payroll
							</label>

							<label class="radiobut-style radio-inline ">
								<input type="radio" name="chk_wpayroll" id="woutpayroll" value="wopayroll">
								W/ Out Payroll
							</label>

					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<br>
				<br>
				<div class="namelist" style="position: relative;top: -25px;">
					{!! $controller->show() !!}
				</div>
		</div>

		<div class="col-md-9">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="progress hidden">
						  <div class="progress-bar" role="progressbar" id="progressBar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
						  <span class="sr-only">0% Complete</span>
					</div>
					<div class="newSummary-name">
						<label id="d-name" style="text-transform: uppercase;"></label>
					</div>
				</div>
				<div class="col-md-6">

				</div>
			</div>
			<div class="button-wrapper hidden editButton" style="position: relative;top: 10px;left: 5px;">
				<!-- <a class="btn btn-xs btn-info btn-savebg btn_new hidden" id="newAttendance" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance"><i class="fa fa-save"></i> New</a> -->

				<a class="btn btn-xs btn-info btn-editbg btn_edit" id="editAttendance" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance"><i class="fa fa-save"></i> Edit</a>

				<a class="btn btn-xs btn-info btn-savebg btn_save update_payroll hidden" data-form="form" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance" id="saveAttendance"><i class="fa fa-save"></i> Save</a>
				<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-form="myform" data-btnedit="editAttendance" data-btnsave="saveAttendance"id="cancelAttendance"> Cancel</a>
			</div>
			<div class="tab-container" style="margin-top: 15px;">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#payrollsummary">Payroll Summary</a></li>
					<li><a href="#attendance">Attendance</a></li>
					<li><a href="#benefitsinfo">Benefits Info</a></li>
					<li><a href="#contribution">Contributions</a></li>
					<li><a href="#loansinfo">Loans Info</a></li>
					<li><a href="#deducinfo">Deduction Info</a></li>
				</ul>
			</div>
			<div class="tab-content myForm">
				<!-- PAYROLL SUMMARY -->
				<div id="payrollsummary" class="tab-pane fade in active">
					<input type="hidden" name="transaction_id" id="transaction_id">
					<input type="hidden" name="salaryinfo_id" id="salaryinfo_id">
					<div class="col-md-12">
						<div class="col-md-9">
							<div class="border-style2">
								<table class="table borderless">
									<tr class="text-center">
										<td></td>
										<td>Actual</td>
										<td>Adjustment</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>Basic Pay</td>
										<td>
											<input type="text" name="actual_basicpay" id="input_basicpayactual" class="form-control font-style2 onlyNumber computebasicpay" readonly>
										</td>
										<td>
											<input type="text" name="adjust_basicpay" id="input_basicpayadjust" class="form-control font-style2 onlyNumber computebasicpay" readonly>
										</td>
										<td>
											<input type="text" name="total_basicpay" id="input_basicpayatotal" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>LWOP</td>
										<td>
											<input type="text" name="actual_absences" id="input_absencesactual" class="form-control font-style2 onlyNumber computeabsences" readonly>
										</td>
										<td>
											<input type="text" name="adjust_absences" id="input_absencesadjust" class="form-control font-style2 onlyNumber computeabsences" readonly>
										</td>
										<td>
											<input type="text" name="total_absences" id="input_absencestotal" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Tardiness</td>
										<td>
											<input type="text" name="actual_tardines" id="input_tardinesactual" class="form-control font-style2 onlyNumber computetardines" readonly>
										</td>
										<td>
											<input type="text" name="adjust_tardines" id="input_tardinesadjust" class="form-control font-style2 onlyNumber computetardines" readonly>
										</td>
										<td>
											<input type="text" name="total_tardines" id="input_tardinestotal" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Undertime</td>
										<td>
											<input type="text" name="actual_undertime" id="input_actualundertime" class="form-control font-style2 onlyNumber computeundertime" readonly>
										</td>
										<td>
											<input type="text" name="adjust_undertime" id="input_adjustundertime" class="form-control font-style2 onlyNumber computeundertime" readonly>
										</td>
										<td>
											<input type="text" name="total_undertime" id="input_undertimetotal" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Overtime</td>
										<td>
											<input type="text" name="actual_overtime" id="actual_overtime" class="form-control font-style2 onlyNumber computeundertime" readonly>
										</td>
										<td>
											<input type="text" name="adjust_overtime" id="adjust_overtime" class="form-control font-style2 onlyNumber computeundertime" readonly>
										</td>
										<td>
											<input type="text" name="total_overtime_amount" id="total_overtime_amount" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td class="text-right">Basic Net Pay</td>
										<td><input type="text" name="basic_net_pay" id="basic_net_pay" class="form-control font-style2 onlyNumber" readonly></td>
									</tr>

								</table>
							</div>

							<div class="border-style2" style="margin-top: -25px;">
								<table class="table borderless">
									<tr class="text-center">
										<td></td>
										<td>Actual</td>
										<td>Adjustment</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>Total Contributions</td>
										<td>
											<input type="text" name="actual_contribution" id="input_actualcontribution" class="form-control font-style2 onlyNumber computecontribution" readonly>
										</td>
										<td>
											<input type="text" name="adjust_contribution" id="input_adjustcontribution" class="form-control font-style2 onlyNumber computecontribution" readonly>
										</td>
										<td>
											<input type="text" name="total_contribution" id="input_totalcontribution" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Total Loans</td>
										<td>
											<input type="text" name="actual_loan" id="input_actualloan" class="form-control font-style2 onlyNumber computeloan" readonly>
										</td>
										<td>
											<input type="text" name="adjust_loan" id="input_adjustloan" class="form-control font-style2 onlyNumber computeloan" readonly>
										</td>
										<td>
											<input type="text" name="total_loan" id="input_totalloans" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Total Other Deductions</td>
										<td>
											<input type="text" name="actual_otherdeduct" id="input_actualotherdeduct" class="form-control font-style2 onlyNumber computeotherdeduction" readonly>
										</td>
										<td>
											<input type="text" name="adjust_otherdeduct" id="input_adjustotherdeduct" class="form-control font-style2 onlyNumber computeotherdeduction" readonly>
										</td>
										<td>
											<input type="text" name="total_otherdeduct" id="input_totalotherdeduct" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td class="text-right">Total Deductions</td>
										<td><input type="text" name="net_deduction" id="net_deduction" class="form-control font-style2 onlyNumber" readonly></td>
									</tr>
								</table>
							</div>
							<br>
						</div>

						<div class="col-md-3">
							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Gross Pay</label>
								<span id="grosspay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Gross Taxable Pay</label>
								<span id="grosstaxablepay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Net Pay</label>
								<span id="netpay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label>Hold</label><br>
								<input type="checkbox" name="chk_holdpay" id="chk_holdpay" >

							</div>
						</div>

					</div>
				</div>
				<!-- PAYROLL SUMMARY -->
				<!-- ATTENDANCE -->
				<div id="attendance" class="tab-pane fade in">
					<div class="col-md-12">
						<div class="border-style2">
							<input type="hidden" name="employee_id" id="employee_id">
							<input type="hidden" name="attendance_id" id="attendance_id">
							<table class="table borderless">
								<tr>
									<!-- <td class="hidden"><label>Salary Rate</label></td> -->
									<!-- <td class="text-left"><label id="_salaryrate"></label></td> -->
									<td></td>
									<td></td>

								</tr>
								<tr>
									<td></td>
									<td class="text-center"><span>Actual</span></td>
									<td class="text-center"><span>Adjustment</span></td>
									<td class="text-center"><span>Total</span></td>
								</tr>
								<tr>
									<td>Work Days</td>
									<td class="newAttendance">
										<input type="text" name="actual_workdays" id="input_actualworkdays" class="form-control font-style2 input attendance workdays" maxlength="4">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_workdays" id="input_adjustworkdays" class="form-control font-style2 input attendance workdays isNumber" maxlength="4" readonly>
									</td>
									<td>
										<input type="text" name="total_workdays" id="input_totalworkdays" class="form-control font-style2 input attendance" placeholder="0.00" readonly>
									</td>
								</tr>
								<tr>
									<td>LWOP</td>
									<td class="newAttendance" >
										<input type="text" name="actual_absences" id="input_actualabsence" class="form-control font-style2 input attendance absences" maxlength="4" readonly>
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_absences" id="input_adjustabsence" class="form-control font-style2 input attendance absences" maxlength="4" readonly>
									</td>
									<td>
										<input type="text" name="total_absences" id="input_totalabsence" class="form-control font-style2 input attendance" placeholder="0.00" readonly>
									</td>
								</tr>
								<tr>
									<td>Tardiness</td>
									<td class="newAttendance">
										<input type="text" name="actual_tardines" id="input_actualwtardiness" class="form-control font-style2 input attendance computetardines ">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_tardines" id="input_adjusttardiness" class="form-control font-style2 input attendance computetardines" readonly>
									</td>
									<td>
										<input type="text" name="total_tardines" id="input_totaltardiness" class="form-control font-style2 input attendance " readonly placeholder="0.00">
									</td>
								</tr>
								<tr>
									<td>Undertime</td>
									<td class="newAttendance">
										<input type="text" name="actual_attendance_undertime" id="actual_attendance_undertime" class="form-control font-style2 input attendance computeundertime ">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_attendance_undertime" id="adjust_attendance_undertime" class="form-control font-style2 input attendance computeundertime " readonly>
									</td>
									<td>
										<input type="text" name="total_undertime" id="input_totalundertime" class="form-control font-style2 input attendance " readonly placeholder="0.00">
									</td>
								</tr>
								<tr>
									<td>Overtime</td>
									<td class="newAttendance">
										<input type="text" name="actual_overtime" id="input_actual_overtime" class="form-control font-style2 input attendance computeundertime ">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_overtime" id="input_adjust_overtime" class="form-control font-style2 input attendance computeundertime " readonly>
									</td>
									<td>
										<input type="text" name="total_overtime_amount" id="input_total_overtime_amount" class="form-control font-style2 input attendance " readonly placeholder="0.00">
									</td>
								</tr>
								<tr>
									<td colspan="3" class="text-right">
										<label>Basic Net Pay</label>
									</td>
									<td><label id="totalamount">0.00</label></td>

								</tr>
							</table>
						</div>
						<br>
					</div>
				</div>
				<!-- ATTENDANCE -->
				<!-- BENF /  ALLOW INFO -->
				<div id="benefitsinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 45px;">
						{!! $controller->showBenefitinfo() !!}
					</div>
					<br>
				</div>
				<!-- BENF /  ALLOW INFO -->
				<!-- CONTRIBUTIONS -->
				<div id="contribution" class="tab-pane fade in">
					<div class="col-md-12">
						<div class="border-style2">
							<table class="table borderless">
								<tr class="text-center">
									<td colspan="2"><label style="margin-left: 80px;">Employee Share</label></td>
									<td colspan="1"><label style="margin-right: 32px;">Employer Share</label></td>
									<td colspan="3"><label style="margin-right: 24px;">ECC</label></td>
								<!-- 	<td></td>
									<td></td> -->
								</tr>
								<tr>
									<td>GSIS</td>
									<td>
										<input type="text" name="gsis_ee_share" id="gsis_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsis"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsis"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="gsis_er_share" id="gsis_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmprShareGsis"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmprShareGsis"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="ecc" id="ecc" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEccGsis"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEccGsis"></a>
										</div>
									</td> -->
									<td colspan="2"></td>
								</tr>
								<tr>
									<td>Philhealth</td>
									<td>
										<input type="text" name="philhealth_ee_share" id="philhealth_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
								<!-- 	<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpSharePhhealth"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpSharePhhealth"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="philhealth_er_share" id="philhealth_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
								<!-- 	<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmprSharePhhealth"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmprSharePhhealth"></a>
										</div>
									</td> -->
									<td colspan="3"></td>
								</tr>
								<tr>
									<tr>
									<td>Pag-ibig</td>
									<td>
										<input type="text" name="pagibig_ee_share" id="pagibig_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpSharePagibig"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpSharePagibig"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="pagibig_er_share" id="pagibig_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmprSharePagibig"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmprSharePagibig"></a>
										</div>
									</td> -->
									<td colspan="3"></td>
								</tr>
								<tr>
									<td>Withholding Tax</td>
									<td>
										<input type="text" name="witholding_tax" id="witholding_tax" class="form-control font-style2 onlyNumber">
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareWtax"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareWtax"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>Provident Fund</td>
									<td>
										<input type="text" name="input_contribEmpSharePfund" id="input_contribEmpSharePfund" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpSharePfund"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpSharePfund"></a>
										</div>
									</td> -->
									<td>
										<input type="text" name="input_contribEmprSharePfund" id="input_contribEmprSharePfund" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmprSharePfund"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmprSharePfund"></a>
										</div>
									</td> -->
									<td colspan="3"></td>
								</tr>
								<tr>
									<td>Association Dues</td>
									<td>
										<input type="text" name="input_contribEmpShareAdues" id="input_contribEmpShareAdues" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareAdues"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareAdues"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>Pag-ibig Fund II</td>
									<td>
										<input type="text" name="pagibig2" id="pagibig2" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpSharePfund2"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpSharePfund2"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>Ajustment Add</td>
									<td>
										<input type="text" name="adjustment_add" id="adjustment_add" class="form-control font-style2 onlyNumber" >
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremI"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremI"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>Adjustment Less</td>
									<td>
										<input type="text" name="adjustment_less" id="adjustment_less" class="form-control font-style2 onlyNumber" >
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremII"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremII"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>AOM</td>
									<td>
										<input type="text" name="aom_amount" id="aom_amount" class="form-control font-style2 onlyNumber" >
									</td>
									<!-- <td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremIII"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremIII"></a>
										</div>
									</td> -->
									<td colspan="4"></td>
								</tr>
								<!-- <tr>
									<td>GSIS UOLI Premium IV</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremIV" id="input_contribEmpShareGsisUoliPremII" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremIV"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremIV"></a>
										</div>
									</td>
									<td colspan="4"></td>
								</tr> -->
								<!-- <tr>
									<td>GSIS UOLI Premium V</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremV" id="input_contribEmpShareGsisUoliPremV" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td>
										<div style="margin-top: 5px;">
											<a href="#" class="fa fa-plus" id="add_contribEmpShareGsisUoliPremV"></a>
											<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsisUoliPremV"></a>
										</div>
									</td>
									<td colspan="4"></td>
								</tr> -->
							</table>
						</div>
						<br>
					</div>
				</div>
				<!-- CONTRIBUTIONS -->
				<!-- LOANS INFO -->
				<div id="loansinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">

						{!! $controller->showLoaninfo() !!}

					</div>
				</div>
				<!-- LOANS INFO  -->
				<!-- DEDUCT INFO -->
				<div id="deducinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
					{!! $controller->showDeductioninfo() !!}
					</div>

				</div>
				<!-- DEDUCT INFO -->
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){

	var _grossPay;
	var _grossTaxable;
	var _netPay;
	var _taxContribution;
	var _allowances;

	var _totalHoursDay;
	var _totalWorkingDays;
	var _totalPayPerday;
	var _totalPayHour;

	var _basicNetPay;
	var _basicPayTotal;
	var	_basicPayActual;
	var	_basscPayAdjust;

	var _totalContribution;
	var _actualContribution;
	var	_adjustContribution;

	var _totalLoans;
	var _actualLoan;
	var	_adjustLoan;

	var _totalOtherDeductions;
	var _totalDeductions;
	var _actualDeduction;
	var	_adjustDeduction;

	var _actualWorkingDays;
	var _adjustWorkingDays;
	var _actualAbsence;
	var _adjustAbsence;
	var _totalAbsence ;

	var _actualTardines;
	var _adjustTardines;
	var _totalTardines;

	var _actualUndertime;
	var _adjustUndertime;
	var _totalUndertime;

	var _adjustTardinesPerHour;
	var	_adjustTardinesPerMinute;
	var	_actualUndertimePerHour;
	var	_actualUndertimePerMinute;
	var _salaryRate;
	var _regularDayRate = 1.25;
	var _specialHolidayRate = 1.50;
	var _regularHolidayRate = 1.50;
	var dailyRate;
	var ratePerHour;
	var taxRateOne;
	var taxRateTwo;
	var taxAmountOne;
	var taxAmountTwo;

	var overtimeAmount;

	var tDeduct;
	var tLoan;
	var tBenefit;

	var _bool = false;
	var _Year;
	var _Month;
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();
		if(_bool){
			$('.btnfilter').trigger('click');
		}

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
		if(_bool){
			$('.btnfilter').trigger('click');
		}

	})

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');
	var _payPeriod;
	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
	});

	var employee_status;
	$(document).on('change','#employee_status',function(){
		employee_status = $(this).val();
		$('.btnfilter').trigger('click');
	})

	$('.select2').select2();

	$('.newAttendance :input').attr('disabled',true);
	$('.newAttendance').attr('disabled',true);

	$('.isNumber').keypress(function (event) {
	    return isNumber(event, this)
	});

	$('#woutpayroll').prop('checked',false);

	$('.nav-tabs a').click(function(){
		$(this).tab('show');
	});

	$('.onlyNumber').keypress(function (event) {
	    return isNumber(event, this)
	});

	$('.onlyNumber').prop('placeholder','0.00');

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});

	$('.attendance').keypress(function (event) {
    	return isNumber(event, this)
	});

	var addAdjustment = 0;
	$('#adjustment_add').on('keyup',function(){
		addAdjustment = $(this).val().replace(/\,/g,'');

		$('#input_actualworkdays').trigger('keyup');
	});

	var lessAdjustment = 0;
	$('#adjustment_less').on('keyup',function(){
		lessAdjustment = $(this).val().replace(/\,/g,'');

		$('#input_actualworkdays').trigger('keyup');
	})

	var aomAdjustment = 0;
	$('#aom_amount').on('keyup',function(){
		aomAdjustment = $(this).val().replace(/\,/g,'');

		$('#input_actualworkdays').trigger('keyup');
	})


	$(document).on('keyup','#input_actualloan',function(){

		 $(this).val(_totalLoans);
		 $('#input_totalloans').val($(this).val());

		sum = compute_netdeduction();
		$('#net_deduction').val(commaSeparateNumber(sum.toFixed(2)));

	})

	$(document).on('keyup','#input_actualcontribution',function(){
		$(this).val(commaSeparateNumber(parseFloat(_totalContribution).toFixed(2)));

		$('#input_totalcontribution').val(commaSeparateNumber(parseFloat(_totalContribution).toFixed(2)));

		sum = compute_netdeduction();
		$('#net_deduction').val(commaSeparateNumber(sum.toFixed(2)));


	});

	$(document).on('keyup','#input_actualotherdeduct',function(){

		$(this).val(commaSeparateNumber(parseFloat(_totalOtherDeductions).toFixed(2)));
		$('#input_totalotherdeduct').val(commaSeparateNumber(parseFloat($(this).val())).toFixed(2));

		sum = compute_netdeduction();
		$('#net_deduction').val(commaSeparateNumber(sum.toFixed(2)));
	})


	var _actualWorkDays;
	var _adjustWorkDays;
	var _totalWorkDays;
	$(document).on('keyup','#input_actualworkdays',function(){
		_actualWorkingDays = $(this).val();

		_actualWorkDays = compute_workingdays(dailyRate,_actualWorkingDays);
		
		_totalWorkDays = _actualWorkDays;
		ratePerHour = (Number(dailyRate) / 8).toFixed(2);


		$('#totalamount').text(commaSeparateNumber(parseFloat(_actualWorkDays).toFixed(2)))

		$('#input_totalworkdays').val(commaSeparateNumber(parseFloat(_actualWorkDays).toFixed(2)));

		$('#input_basicpayactual').val(commaSeparateNumber(parseFloat(_actualWorkDays).toFixed(2)))
		$('#input_basicpayatotal').val(commaSeparateNumber(parseFloat(_actualWorkDays).toFixed(2)));
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_actualWorkDays).toFixed(2)));

		_basicNetPay = _actualWorkDays;
		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);

		_grossPay = compute_grosspay(_basicNetPay,_allowances);
		$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_actualWorkDays).toFixed(2)))

		_grossTaxable = compute_grosstaxable(_actualWorkDays,_totalContribution);

		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances,addAdjustment,lessAdjustment,aomAdjustment);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});

	$(document).on('keyup','#input_adjustworkdays',function(){
		_actualWorkingDays = $(this).val();

		_adjustWorkDays = compute_workingdays(dailyRate,_actualWorkingDays);

		//SUMMARY TAB
		$('#input_basicpayadjust').val(commaSeparateNumber(parseFloat(_adjustWorkDays).toFixed(2)))

		_totalWorkDays = compute_totalworkdays(_actualWorkDays,_adjustWorkDays);

		if(_totalWorkDays){
			adjustWorkDays = _totalWorkDays;
		}

		_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines,overtimeAmount);

		if(!_basicNetPay){
			_basicNetPay = _totalWorkDays;
		}
		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);

		$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))
		$('#input_totalworkdays').val(commaSeparateNumber(parseFloat(adjustWorkDays).toFixed(2)));

		//SUMMARY TAB
		$('#input_basicpayatotal').val(commaSeparateNumber(parseFloat(_totalWorkDays).toFixed(2)))
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

		_grossPay = compute_grosspay(adjustWorkDays,_allowances);
		$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

		_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances,addAdjustment,lessAdjustment,aomAdjustment);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});

	$(document).on('keyup','#input_actualabsence',function(){
		_actualWorkingDays = $(this).val();

		_actualAbsence = compute_workingdays(dailyRate,_actualWorkingDays);

		//SUMMARY TAB
		$('#input_absencesactual').val(commaSeparateNumber(parseFloat(_actualAbsence).toFixed(2)))

		_totalAbsence = compute_totalabsences(_actualAbsence, _adjustAbsence);

		if(!_totalAbsence){
			_totalAbsence = _actualAbsence
		}

		_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines,overtimeAmount);

		if(!_basicNetPay){
			_basicNetPay = _totalAbsence;
		}
		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);
		$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

		$('#input_totalabsence').val(commaSeparateNumber(parseFloat(_totalAbsence).toFixed(2)));

		//SUMMARY TAB
		$('#input_absencestotal').val(commaSeparateNumber(parseFloat(_totalAbsence).toFixed(2)))
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

		_grossPay = compute_grosspay(_basicNetPay,_allowances);
		$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

		_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances,addAdjustment,lessAdjustment,aomAdjustment);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});

	$(document).on('keyup','#input_adjustabsence',function(){
		_actualWorkingDays = $(this).val();

		_adjustAbsence = compute_workingdays(dailyRate,_actualWorkingDays);

		//SUMMARY TAB
		$('#input_absencesadjust').val(commaSeparateNumber(parseFloat(_adjustAbsence).toFixed(2)))

		_totalAbsence = compute_totalabsences(_actualAbsence, _adjustAbsence);

		if(!_totalAbsence){
			_totalAbsence = _actualAbsence
		}

		_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines,overtimeAmount);

		if(!_basicNetPay){
			_basicNetPay = _totalAbsence;
		}
		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);
		$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

		$('#input_totalabsence').val(commaSeparateNumber(parseFloat(_totalAbsence).toFixed(2)));

		//SUMMARY TAB
		$('#input_absencestotal').val(commaSeparateNumber(parseFloat(_totalAbsence).toFixed(2)))
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

		_grossPay = compute_grosspay(_basicNetPay,_allowances);
		$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

		_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances,addAdjustment,lessAdjustment,aomAdjustment);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});

	$(document).on('keyup','#input_actualwtardiness',function(){
		var payPerMinute = 0;
		_ratePerMinute 	 = $(this).val();

		_actualTardines  = rate_perminute(ratePerHour, _ratePerMinute);

		//SUMMARY TAB
		$('#input_tardinesactual').val(commaSeparateNumber(parseFloat(_actualTardines).toFixed(2)));

		_totalTardines = compute_totaltardines(_actualTardines,_adjustTardines);

		if(!_totalTardines){
			_totalTardines = _actualTardines;
		}

		_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines,overtimeAmount);

		if(!_basicNetPay){
			_basicNetPay = _totalTardines;
		}
		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);
		$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

		$('#input_totaltardiness').val(commaSeparateNumber(parseFloat(_totalTardines).toFixed(2)));

		//SUMMARY TAB
		$('#input_tardinestotal').val(commaSeparateNumber(parseFloat(_totalTardines).toFixed(2)))
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

		_grossPay = compute_grosspay(_basicNetPay,_allowances);
		$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

		_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});

	$(document).on('keyup','#input_adjusttardiness',function(){

		_ratePerMinute 	 = $(this).val();

		_adjustTardines = rate_perminute(ratePerHour, _ratePerMinute);

		//SUMMARY TAB
		$('#input_tardinesadjust').val(commaSeparateNumber(parseFloat(_adjustTardines).toFixed(2)));

		_totalTardines = compute_totaltardines(_actualTardines,_adjustTardines);

		if(!_totalTardines){
			_totalTardines = _adjustTardines;
		}

		_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines,overtimeAmount);


		if(!_basicNetPay){
			_basicNetPay = _totalTardines;
		}

		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);

		$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

		$('#input_totaltardiness').val(commaSeparateNumber(parseFloat(_totalTardines).toFixed(2)));

		//SUMMARY TAB
		$('#input_tardinestotal').val(commaSeparateNumber(parseFloat(_totalTardines).toFixed(2)))
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

		_grossPay = compute_grosspay(_basicNetPay,_allowances);
		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(_grossTaxable).toFixed(2)));

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});


	$(document).on('keyup','#actual_attendance_undertime',function(){

		_ratePerMinute 	 = $(this).val();
		_actualUndertime = rate_perminute(ratePerHour, _ratePerMinute);

		//SUMMARY TAB
		$('#input_actualundertime').val(commaSeparateNumber(parseFloat(_actualUndertime).toFixed(2)));

		_totalUndertime = compute_totalundertime(_actualUndertime,_adjustUndertime);

		if(!_totalUndertime){
			_totalUndertime = _actualUndertime;
		}

		_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines,overtimeAmount);

		if(!_basicNetPay){
			_basicNetPay = _totalUndertime;
		}

		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);
		$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

		$('#input_totalundertime').val(commaSeparateNumber(parseFloat(_totalUndertime).toFixed(2)));

		//SUMMARY TAB
		$('#input_undertimetotal').val(commaSeparateNumber(parseFloat(_totalUndertime).toFixed(2)))
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

		_grossPay = compute_grosspay(_basicNetPay,_allowances);
		$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

		_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances,addAdjustment,lessAdjustment,aomAdjustment);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});

	$(document).on('keyup','#adjust_attendance_undertime',function(){

		_ratePerMinute 	 = $(this).val();

		_adjustUndertime = rate_perminute(ratePerHour, _ratePerMinute);

		//SUMMARY TAB
		$('#input_adjustundertime').val(commaSeparateNumber(parseFloat(_adjustUndertime).toFixed(2)));
		_totalUndertime = compute_totalundertime(_actualUndertime,_adjustUndertime);

		if(!_totalUndertime){
			_totalUndertime = _adjustUndertime;
		}

		_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines,overtimeAmount);

		if(!_basicNetPay){
			_basicNetPay = _totalUndertime;
		}
		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);
		$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))
		$('#input_totalundertime').val(commaSeparateNumber(parseFloat(_totalUndertime).toFixed(2)));

		//SUMMARY TAB
		$('#input_undertimetotal').val(commaSeparateNumber(parseFloat(_totalUndertime).toFixed(2)))
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(total).toFixed(2)));

		_grossPay = compute_grosspay(total,_allowances);
		$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

		_grossTaxable = compute_grosstaxable(total,_totalContribution);
		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances,addAdjustment,lessAdjustment,aomAdjustment);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});

	$(document).on('keyup','#input_actual_overtime',function(){

		ratePerMinute 	 = $(this).val();

		overtimeAmount = rate_perminute(ratePerHour, ratePerMinute);

		//SUMMARY TAB
		$('#input_total_overtime_amount').val(commaSeparateNumber(parseFloat(overtimeAmount).toFixed(2)));


		_basicNetPay = compute_total(_totalWorkDays,_totalAbsence,_totalUndertime,_totalTardines,overtimeAmount);

		if(!_basicNetPay){
			_basicNetPay = overtimeAmount;
		}

		// _taxContribution = compute_taxcontribution(_basicNetPay,taxRateOne,taxRateTwo);
		$('#totalamount').text(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)))

		$('#actual_overtime').val(commaSeparateNumber(parseFloat(overtimeAmount).toFixed(2)));

		//SUMMARY TAB
		$('#total_overtime_amount').val(commaSeparateNumber(parseFloat(overtimeAmount).toFixed(2)))
		$('#basic_net_pay').val(commaSeparateNumber(parseFloat(_basicNetPay).toFixed(2)));

		_grossPay = compute_grosspay(_basicNetPay,_allowances);
		$('#totalGrossPay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

		_grossTaxable = compute_grosstaxable(_basicNetPay,_totalContribution);
		grossTaxable = (_grossTaxable) ? _grossTaxable : 0;
		$('#grosstaxablepay').text(commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)))

		_netPay = compute_netpay(_taxContribution,_totalLoans,_totalOtherDeductions,_grossTaxable,_allowances,addAdjustment,lessAdjustment,aomAdjustment);
		$('#netpay').text(commaSeparateNumber(parseFloat(_netPay).toFixed(2)));

		$('#grosspay').text(commaSeparateNumber(parseFloat(_grossPay).toFixed(2)))

	});




	function compute_grosspay(basicpay,allowance){
		basicpay = (basicpay) ? basicpay : 0;
		allowance = (allowance) ? allowance : 0;

		_grossPay = (parseFloat(basicpay) + parseFloat(allowance));

		return _grossPay;
	}


	function compute_totalundertime(actual,adjust){
		totalundertime = (actual + adjust);
		return totalundertime;
	}

	function compute_totaltardines(actual,adjust){
		total_tardines = (actual + adjust);
		return total_tardines;
	}

	function rate_perminute(payPerHour, ratePerMinute){
	
		payPerMinute = (Number(payPerHour) / 60).toFixed(2);
	
		payAmount = Number(payPerMinute) * Number(ratePerMinute);

		return payAmount;
	}

	function compute_taxcontribution(basic_net_pay,rate_one,rate_two) {
		taxAmountOne = parseFloat(basic_net_pay) * rate_one;
		taxAmountTwo = parseFloat(basic_net_pay) * rate_two;
		taxcontribution = (parseFloat(taxAmountOne) + parseFloat(taxAmountTwo));
		$('#witholding_tax').val(commaSeparateNumber(parseFloat(taxcontribution).toFixed(2)));
		return taxcontribution;
	}

	function compute_total(tWorkingDays,tAbsence,tUndertime,tTardiness,tOvertime){
		tWorkingDays = (tWorkingDays) ? tWorkingDays : 0;
		tAbsence 	 = (tAbsence) ? tAbsence : 0;
		tUndertime 	 = (tUndertime) ? tUndertime : 0;
		tTardiness 	 = (tTardiness) ? tTardiness : 0;
		tOvertime 	 = (tOvertime) ? tOvertime : 0;

		total = (parseFloat(tWorkingDays) + parseFloat(tOvertime) - (parseFloat(tAbsence) + parseFloat(tUndertime) + parseFloat(tTardiness)));
		return total;
	}


	function compute_netpay(taxAmount,tLoan,tOtherDeductions,gTaxable,allowances,adjustment_add,adjustment_less,aom_amount){
		taxAmount 			  = (taxAmount) ? taxAmount : 0;
		tLoan 	  			  = (tLoan) ? tLoan : 0;
		tOtherDeductions 	  = (tOtherDeductions) ? tOtherDeductions : 0;
		gTaxable 	  		  = (gTaxable) ? gTaxable : 0;
		allowances 			  = (allowances) ? allowances : 0;
		adjustment_add 		  = (adjustment_add) ? adjustment_add : 0;
		adjustment_less 	   = (adjustment_less) ? adjustment_less : 0;
		aom_amount 			  = (aom_amount) ? aom_amount : 0;

		if(gTaxable){

			netPay = ((parseFloat(gTaxable) + parseFloat(allowances) + parseFloat(adjustment_add)) - (parseFloat(taxAmount) + parseFloat(tLoan) + parseFloat(tOtherDeductions) + parseFloat(adjustment_less) + parseFloat(aom_amount)));
		}else{
			netPay = 0;
		}

		return netPay;

	}

	function compute_totalworkdays(actualworkdays,adjustworkdays){
		totalworkdays = (actualworkdays + adjustworkdays);
		return totalworkdays;
	}

	function compute_workingdays(totalPay, actualDays){
		payPerDay = (totalPay * actualDays);
		return payPerDay;

	}

	function compute_totalabsences(actual,adjust){
		totalAbsence = (actual + adjust);
		return totalAbsence;
	}

	function compute_netdeduction(){

		contributions 		= (_totalContribution) ? parseFloat(_totalContribution) : 0;
		loans 				= (_totalLoans) ? parseFloat(_totalLoans) : 0;
		otherdeductions 	= (_totalOtherDeductions) ? parseFloat(_totalOtherDeductions) : 0;

		sum = (contributions + loans + otherdeductions);

		return sum;
	}

	function compute_grosstaxable(basicnet,totalContribution){
		basicnet = (basicnet) ? basicnet : 0;
		totalContribution = (totalContribution) ? totalContribution : 0;
		grossTaxable = (parseFloat(basicnet) - parseFloat(totalContribution));
		return grossTaxable;
	}

	var _listId = [];
	$(document).on('click','#check_all',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

			$('#check_all').prop('checked',false);

		}else{
			if ($(this).is(':checked')) {
		        $('.emp_select').prop('checked', 'checked');
		        $('#transaction_id').val('');
		        $('.emp_select:checked').each(function(){
		        	_listId.push($(this).val())
		        });

		    } else {
		        $('.emp_select').prop('checked', false)
		        _listId = [];
		    }

		}

	});

$(document).on('click','.emp_select',function(){

	empid = $(this).val();
	index = $(this).data('key');
	if(!_Year && !_Month){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
		$('.emp_select').prop('checked',false);
	}else{
		if($(this).is(':checked')){
			_listId[index] =  empid;
		}else{
			delete _listId[index];
		}
	}
});


$(document).on('click','#process_payroll',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processPayroll();
			}else{
				return false;
			}
		});
	}
});

$.processPayroll = function(){
	$.ajax({
        type:'POST',
        data:{
        	'empid':_listId,
        	'_token':"{{ csrf_token() }}"
        	,'year':_Year,
        	'month':_Month,
        	'employee_id':$('#employee_id').val(),
        	'transaction_id':$('#transaction_id').val(),
        	'attendance_id':$('#attendance_id').val(),
        	'salaryinfo_id':$('#salaryinfo_id').val(),
        	'summary':{
        		'actual_basicpay':$('#input_basicpayactual').val(),
        		'adjust_basicpay':$('#input_basicpayadjust').val(),
        		'total_basicpay':$('#input_basicpayatotal').val(),
        		'actual_absences':$('#input_absencesactual').val(),
        		'adjust_absences':$('#input_absencesadjust').val(),
        		'total_absences':$('#input_absencestotal').val(),
        		'actual_tardines':$('#input_tardinesactual').val(),
        		'adjust_tardines':$('#input_tardinesadjust').val(),
        		'total_tardines':$('#input_tardinestotal').val(),
        		'actual_undertime':$('#input_actualundertime').val(),
        		'adjust_undertime':$('#input_adjustundertime').val(),
        		'total_undertime':$('#input_undertimetotal').val(),
        		'basic_net_pay':$('#basic_net_pay').val(),
        		'actual_contribution':$('#input_actualcontribution').val(),
        		'adjust_contribution':$('#input_adjustcontribution').val(),
        		'total_contribution':$('#input_totalcontribution').val(),
        		'actual_loan':$('#input_actualloan').val(),
        		'adjust_loan':$('#input_adjustloan').val(),
        		'total_loan':$('#input_totalloans').val(),
        		'actual_otherdeduct':$('#input_actualotherdeduct').val(),
        		'adjust_otherdeduct':$('#input_adjustotherdeduct').val(),
        		'total_otherdeduct':$('#input_totalotherdeduct').val(),
        		'actual_overtime_amount':$('#actual_overtime').val(),
        		'total_overtime_amount':$('#actual_overtime').val(),
        		'net_deduction':$('#net_deduction').val(),
        		'witholding_tax':$('#witholding_tax').val(),
        		'tax_rate_amount_one':taxAmountOne,
        		'tax_rate_amount_two':taxAmountTwo,
        		'net_pay':_netPay,
        		'gross_pay':_grossPay,
        		'gross_taxable_pay':_grossTaxable,
        		'pay_period':_payPeriod,
        		'hold':$('#chk_holdpay').val(),
        		'adjustment_add':addAdjustment,
        		'adjustment_less':lessAdjustment,
        		'aom_amount':aomAdjustment
        	},
        	'attendance':{
        		'actual_workdays':$('#input_actualworkdays').val(),
        		'adjust_workdays':$('#input_adjustworkdays').val(),
        		'total_workdays':$('#input_totalworkdays').val(),
        		'actual_absences':$('#input_actualabsence').val(),
        		'adjust_absences':$('#input_adjustabsence').val(),
        		'total_absences':$('#input_totalabsence').val(),
        		'actual_tardines':$('#input_actualwtardiness').val(),
        		'adjust_tardines':$('#input_adjusttardiness').val(),
        		'total_tardines':$('#input_totaltardiness').val(),
        		'actual_undertime':$('#actual_attendance_undertime').val(),
        		'adjust_undertime':$('#adjust_attendance_undertime').val(),
        		'total_undertime':$('#input_totalundertime').val(),
        		'actual_overtime':$('#input_actual_overtime').val(),
        		'year':_Year,
        		'month':_Month
        	}
        },
        url: base_url+module_prefix+module+'/processPayroll',
        beforeSend:function(){
        	$('#process_payroll').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
        success:function(data) {
        	par = JSON.parse(data);
        	if(par.status){
           		swal({
					  title: par.response,
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: "OK",
					  closeOnConfirm: false
				}).then(function(){
					$('#process_payroll').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
					$('.btn_save').addClass('hidden');
					$('.btn_cancel').addClass('hidden');
					$('.btn_edit').removeClass('hidden');
					$('#transaction_id').val('');
					$('.newAttendance :input').attr('disabled',true);
					$('.newAttendance').attr('disabled',true);
					clear_form_elements('attendance');
					$('.btnfilter').trigger('click');
					_listId = [];
				});
        	}else{
        		swal({
            	   title: par.response,
				   type: "warning",
				   showCancelButton: false,
				   confirmButtonClass: "btn-warning",
				   confirmButtonText: "OK",
				   closeOnConfirm: false
        		})
        		$('#process_payroll').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
        	}
        }
	});

}

	$(document).on('click','#namelist tr',function(){
    	_id = $(this).data('empid');
    	$('#d-name').text($(this).data('employee'));

    	$.ajax({
    		url:base_url+module_prefix+module+'/getEmployeesinfo',
    		data:{
    			'id':_id,
    			'year':_Year,
    			'month':_Month,
    			'pay_period':_payPeriod
    		},
    		type:'get',
    		dataType:'JSON',
    		success:function(data){
    			clear_form_elements('myForm');
    			$('#lbl_empname').text('');
    			$('#grosspay').text(0.00);
    			$('#netpay').text(0.00);
    			$('#grosstaxablepay').text(0.00);
    			$('#transaction_id').val('');
    			$('#attendance_id').val('');
    			$('#employee_id').val('');
				if(data.transaction !== null){
					fullname = data.transaction.employees.lastname+' '+data.transaction.employees.firstname+' '+ data.transaction.employees.middlename;
					employeeNumber = (data.transaction.employee_number) ? data.transaction.employee_number : '';
					$('#employee_number').val(employeeNumber);
					$('#lbl_empname').text(fullname);
					$('#transaction_id').val(data.transaction.id);
					$('#employee_id').val(data.transaction.employee_id);
					dailyRate = data.transaction.daily_rate;

					adjustWorkDays = (data.transaction.adjust_workdays) ? data.transaction.adjust_workdays : '';
					actualAbsence = (data.transaction.actual_absences) ? data.transaction.actual_absences : '';
					adjustAbsence = (data.transaction.adjust_absences) ? data.transaction.adjust_absences : '';
					actualTardiness = (data.transaction.actual_tardiness) ? data.transaction.actual_tardiness : '';
					adjustTardiness = (data.transaction.adjust_tardiness) ? data.transaction.adjust_tardiness : '';
					actualUndertime = (data.transaction.actual_undertime) ? data.transaction.actual_undertime : '';
					adjustUndertime = (data.transaction.adjust_undertime) ? data.transaction.adjust_undertime : '';
					actualOvertime = (data.transaction.actual_overtime) ? data.transaction.actual_overtime : '';
					taxAmountOne = (data.transaction.tax_rate_amount_one) ? data.transaction.tax_rate_amount_one : 0;
					taxAmountTwo = (data.transaction.tax_rate_amount_two) ? data.transaction.tax_rate_amount_two : 0;

					addAdjustment = (data.transaction.adjustment_add) ? data.transaction.adjustment_add : 0;
					lessAdjustment = (data.transaction.adjustment_less) ? data.transaction.adjustment_less : 0;
					aomAdjustment = (data.transaction.aom_amount) ? data.transaction.aom_amount : 0;
					_grossTaxable = (data.transaction.gross_taxable_pay) ? data.transaction.gross_taxable_pay : 0;
					_netPay = (data.transaction.net_pay) ? data.transaction.net_pay : 0;

					taxAmount = parseFloat(taxAmountOne) + parseFloat(taxAmountTwo);

					adjustment_add = (addAdjustment) ? commaSeparateNumber(parseFloat(addAdjustment).toFixed(2)) : '';
					adjustment_less = (lessAdjustment) ? commaSeparateNumber(parseFloat(lessAdjustment).toFixed(2)) : '';
					aom_amount = (aomAdjustment) ? commaSeparateNumber(parseFloat(aomAdjustment).toFixed(2)) : '';

					tax_amount = (taxAmount) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
					gross_taxable_pay = (_grossTaxable) ? commaSeparateNumber(parseFloat(_grossTaxable).toFixed(2)) : '';
					net_pay = (_netPay) ? commaSeparateNumber(parseFloat(_netPay).toFixed(2)) : '';

					$("#aom_amount").val(aom_amount);
					$('#adjustment_add').val(adjustment_add);
					$('#adjustment_less').val(adjustment_less);

					$('#attendance_id').val(data.transaction.id);
					$('#input_actualworkdays').val(data.transaction.actual_workdays).trigger('keyup');
					$('#input_adjustworkdays').val(adjustWorkDays).trigger('keyup');
					$('#input_actualabsence').val(actualAbsence).trigger('keyup');
					$('#input_adjustabsence').val(adjustAbsence).trigger('keyup');
					$('#input_actualwtardiness').val(actualTardiness).trigger('keyup');
					$('#input_adjusttardiness').val(adjustTardiness).trigger('keyup');
					$('#actual_attendance_undertime').val(actualUndertime).trigger('keyup');
					$('#adjust_attendance_undertime').val(adjustUndertime).trigger('keyup');
					$('#input_actual_overtime').val(actualOvertime).trigger('keyup');
					$('#witholding_tax').val(tax_amount);
					$('#grosstaxablepay').text(gross_taxable_pay);
					$('#netpay').text(net_pay);
				}

				if(data.employeeinfo !== null){
					taxRateOne = (data.employeeinfo.ewt_policy_id) ? data.employeeinfo.tax_policy_ewt.job_grade_rate : 0;
					taxRateTwo = (data.employeeinfo.pwt_policy_id) ? data.employeeinfo.tax_policy_pwt.job_grade_rate : 0;
				}


    			// GENERATE TR FOR DEDUCTION INFO TAB
				tDeduct = $('#tbl_deductTransact').DataTable();

				tDeduct.clear().draw();

				$.each(data.deductioninfo,function(k,v){

					tDeduct.row.add( [
			        	data.deductioninfo[k].deductions.name,
			        	data.deductioninfo[k].deduct_amount,
						data.deductioninfo[k].deduct_date_start,
						data.deductioninfo[k].deduct_date_end,
						data.deductioninfo[k].deduct_pay_period,
			        ]).draw( false );

			        tDeduct.rows(k).nodes().to$().attr("data-id", v.id);
			        tDeduct.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
			        tDeduct.rows(k).nodes().to$().attr("data-deductionid", v.deduction_id);
			        tDeduct.rows(k).nodes().to$().attr("data-amount", v.deduct_amount);
			        tDeduct.rows(k).nodes().to$().attr("data-datestart", v.deduct_date_start);
			        tDeduct.rows(k).nodes().to$().attr("data-dateend", v.deduct_date_end);
			        tDeduct.rows(k).nodes().to$().attr("data-payperiod", v.deduct_pay_period);
			        tDeduct.rows(k).nodes().to$().attr("data-dateterminated", v.deduct_date_terminated);
			        tDeduct.rows(k).nodes().to$().attr("data-btnnew", "newDeduct");
			        tDeduct.rows(k).nodes().to$().attr("data-btnsave", "saveDeduct");
			        tDeduct.rows(k).nodes().to$().attr("data-btnedit", "editDeduct");
			        tDeduct.rows(k).nodes().to$().attr("data-btncancel", "cancelDeduct");
				});

				// GENERATE TR FOR LOAN INFO TAB
				tLoan = $('#tbl_loanTransact').DataTable();

				tLoan.clear().draw();

				$.each(data.loaninfo,function(k,v){

					tLoan.row.add( [
			        	data.loaninfo[k].loans.name,
			        	data.loaninfo[k].loan_totalamount,
						data.loaninfo[k].loan_totalbalance,
						data.loaninfo[k].loan_amortization,
						data.loaninfo[k].loan_date_started,
						data.loaninfo[k].loan_date_end,
			        ]).draw( false );

			        tLoan.rows(k).nodes().to$().attr("data-id", v.id);
			        tLoan.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
			        tLoan.rows(k).nodes().to$().attr("data-loanamount", v.loan_totalamount);
			        tLoan.rows(k).nodes().to$().attr("data-loanbalance", v.loan_totalbalance);
			        tLoan.rows(k).nodes().to$().attr("data-amortization", v.loan_amortization);
			        tLoan.rows(k).nodes().to$().attr("data-datestart", v.loan_date_started);
			        tLoan.rows(k).nodes().to$().attr("data-dateend", v.loan_date_end);
			        tLoan.rows(k).nodes().to$().attr("data-loanid", v.loan_id);
			        tLoan.rows(k).nodes().to$().attr("data-payperiod", v.loan_pay_period);
			        tLoan.rows(k).nodes().to$().attr("data-dategranted", v.loan_date_granted);
			        tLoan.rows(k).nodes().to$().attr("data-dateterminated", v.loan_date_terminated);
			        tLoan.rows(k).nodes().to$().attr("data-btnnew", "newLoan");
			        tLoan.rows(k).nodes().to$().attr("data-btnsave", "saveLoan");
			        tLoan.rows(k).nodes().to$().attr("data-btnedit", "editLoan");
			        tLoan.rows(k).nodes().to$().attr("data-btncancel", "cancelLoan");
				});

				// GENERATE TR FOR BENEFITS TAB

				tBenefit = $('#tbl_benefitTransact').DataTable();

				tBenefit.clear().draw();

				$.each(data.benefitinfo,function(k,v){

					benefit_name = (data.benefitinfo[k].benefits) ? data.benefitinfo[k].benefits.name : '';

					tBenefit.row.add( [
			        	data.benefitinfo[k].benefit_effectivity_date,
			        	benefit_name,
						data.benefitinfo[k].benefit_description,
						data.benefitinfo[k].benefit_amount,
						data.benefitinfo[k].benefit_pay_period,
			        ]).draw( false );

			        tBenefit.rows(k).nodes().to$().attr("data-id", v.id);
			        tBenefit.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
			        tBenefit.rows(k).nodes().to$().attr("data-date", v.benefit_effectivity_date);
			        tBenefit.rows(k).nodes().to$().attr("data-name", benefit_name);
			        tBenefit.rows(k).nodes().to$().attr("data-description", v.benefit_description);
			        tBenefit.rows(k).nodes().to$().attr("data-payperiod", v.benefit_pay_period);
			        tBenefit.rows(k).nodes().to$().attr("data-paysub", v.benefit_pay_sub);
			        tBenefit.rows(k).nodes().to$().attr("data-amount", v.benefit_amount);
			        tBenefit.rows(k).nodes().to$().attr("data-benefitid", v.benefit_id);
			        tBenefit.rows(k).nodes().to$().attr("data-btnnew", "newBenefit");
			        tBenefit.rows(k).nodes().to$().attr("data-btnsave", "saveBenefit");
			        tBenefit.rows(k).nodes().to$().attr("data-btnedit", "editBenefit");
			        tBenefit.rows(k).nodes().to$().attr("data-btncancel", "cancelBenefit");
				});


    		}

    	})
    });

_checkpayroll = ""
 $('input[type=radio][name=chk_wpayroll]').change(function() {
 	if(!_Year){
 		swal({
			  title: 'Select year and month first',
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-warning",
			  confirmButtonText: "OK",
			  closeOnConfirm: false
		})
		$(this).prop('checked',false);
 	}else{

		_listId = [];
		if (this.value == 'wpayroll') {
			$('#process_payroll').prop('disabled',true);
			$('.editButton').removeClass('hidden');
			_bool = true;
        	_checkpayroll = 'wpayroll';
        }
        else if (this.value == 'wopayroll') {
        	$('#process_payroll').prop('disabled',false);
        	$('.editButton').addClass('hidden');
            _checkpayroll = 'wopayroll';
        }
        $('.btnfilter').trigger('click');
 	}
});

$(document).on('click','#delete_payroll',function(){
 	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deletePayroll();
			}else{
				return false;
			}
		});
	}
});

$.deletePayroll = function(){
 	$.ajax({
 		url:base_url+module_prefix+module+'/destroy',
 		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
 		type:'delete',
 		beforeSend:function(){
			$('#delete_payroll').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
 		success:function(response){
 			par = JSON.parse(response)

 			if(par.status){
 				swal({
					  title: par.response,
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: "OK",
					  closeOnConfirm: false
				})
 				_listId = [];
 				$('#delete_payroll').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('.btnfilter').trigger('click');
				clear_form_elements('myForm');
				tDeduct.clear().draw();
				tLoan.clear().draw();
				tBenefit.clear().draw();
				$("#_salaryrate").text(0.00)
				$("#totalamount").text(0.00)
				$("#totalOT").text(0.00)
				$("#totalGrossPay").text(0.00)
				$("#grosspay").text(0.00)
				$("#grosstaxablepay").text(0.00)
				$("#netpay").text(0.00)
				$('#transaction_id').val('');
				$('attendance_id').val('');
 			}
 		}
 	});
}

var timer;
$(document).on('click','.btnfilter',function(){
	tools  	  = $('#tools-form').serialize()
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();
	clearTimeout(timer);
	timer = setTimeout(
	function(){
		$.ajax({
		   type: "GET",
		   url: base_url+module_prefix+module+'/show',
		   data: {
		   	'year':year,
		   	'month':month,
		   	'checkpayroll':_checkpayroll,
		   	'employee_status':employee_status,
		   	'pay_period':_payPeriod,
		   },
		   success: function(res){
		      $(".namelist").html(res);
		   }
		});
	},500);
});


$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('._searchname').val(),
			   	'check_payroll':_checkpayroll,
			   	'empstatus':employee_status,
			   	'_year':_Year,
			   	'_month':_Month,
			   	'_pay_period':_payPeriod
			},
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});

$('.btn_new').on('click',function(){
	// $('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_cancel').on('click',function(){
	$('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).removeClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	clear_form_elements('nonplantilla');
	$('.error-msg').remove();

});

$('.update_payroll').on('click',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Update Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processPayroll();
			}else{
				return false;
			}
		});
	}
});


});




</script>
@endsection