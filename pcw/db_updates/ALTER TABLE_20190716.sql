ALTER TABLE `pms_nonplantilla_transactions`
	ADD COLUMN `daily_rate` DECIMAL(9,2) NULL DEFAULT NULL AFTER `total_overtime_amount`;