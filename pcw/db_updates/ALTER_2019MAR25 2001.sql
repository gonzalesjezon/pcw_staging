ALTER TABLE `pms_deductioninfo_transactions`
	ADD COLUMN `not_include` INT(11) NULL DEFAULT '0' AFTER `month`;

ALTER TABLE `pms_transactions`
	ADD COLUMN `salary_grade` INT(11) NULL DEFAULT NULL AFTER `responsibility_id`;