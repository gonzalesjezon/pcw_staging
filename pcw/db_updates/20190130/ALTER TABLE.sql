ALTER TABLE `pms_rata_adjustments`
	ADD COLUMN `remarks` VARCHAR(225) NULL DEFAULT NULL AFTER `month`;

ALTER TABLE `pms_beginning_balances`
	ADD COLUMN `gross_taxable_income` DECIMAL(9,2) NULL DEFAULT NULL AFTER `taxable_other_salaries`;