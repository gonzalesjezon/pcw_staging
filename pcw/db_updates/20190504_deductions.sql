-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pcw_db.pms_deductions
DROP TABLE IF EXISTS `pms_deductions`;
CREATE TABLE IF NOT EXISTS `pms_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.pms_deductions: ~10 rows (approximately)
/*!40000 ALTER TABLE `pms_deductions` DISABLE KEYS */;
INSERT INTO `pms_deductions` (`id`, `code`, `name`, `payroll_group`, `tax_type`, `itr_classification`, `alphalist_classification`, `amount`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(2, 'CU', 'CREDIT UNION', NULL, 'Non Taxable', NULL, NULL, 0.00, 'bill', '2018-11-16 13:41:55', '2019-05-03 08:20:56', NULL),
	(3, 'PCALL', 'PERSONAL CALLS', NULL, 'Non Taxable', NULL, NULL, 0.00, 'bill', '2018-11-17 03:50:40', '2018-11-17 03:53:29', NULL),
	(4, 'MEDICARD', 'MEDICARD', NULL, 'Non Taxable', NULL, NULL, 0.00, 'bill', '2018-11-17 03:50:54', '2018-11-17 03:52:54', NULL),
	(5, 'EA', 'EMPLOYEES ASSOCIATION', NULL, 'Non Taxable', NULL, NULL, 0.00, NULL, '2018-11-17 11:15:25', '2019-03-24 07:32:52', NULL),
	(6, 'EA MISC', 'MORTUARY ASSIST', NULL, 'Non Taxable', NULL, NULL, 100.00, 'bill', '2019-03-24 07:33:37', '2019-03-25 12:44:52', NULL),
	(7, 'EA LOAN', 'EA LOANS', NULL, NULL, NULL, NULL, 0.00, 'bill', '2019-05-03 08:11:36', '2019-05-03 08:11:36', NULL),
	(8, 'PHICADJ', 'Philhealth Adjusment', NULL, NULL, NULL, NULL, 0.00, 'bill', '2019-05-03 11:21:42', '2019-05-03 11:21:42', NULL),
	(9, 'PAGIBIGADJ', 'Pagibig Adjustment', NULL, NULL, NULL, NULL, 0.00, 'bill', '2019-05-03 11:22:10', '2019-05-03 11:22:10', NULL),
	(10, 'TAXADJ', 'Tax Adjustment', NULL, NULL, NULL, NULL, 0.00, 'bill', '2019-05-03 11:22:29', '2019-05-03 11:22:29', NULL),
	(11, 'GSISADJ', 'GSIS Adjustment', NULL, NULL, NULL, NULL, 0.00, 'bill', '2019-05-03 11:23:01', '2019-05-03 11:23:01', NULL);
/*!40000 ALTER TABLE `pms_deductions` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
