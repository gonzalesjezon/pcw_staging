ALTER TABLE `pms_transactions`
	ADD COLUMN `salary_diff_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `tax_amount`;