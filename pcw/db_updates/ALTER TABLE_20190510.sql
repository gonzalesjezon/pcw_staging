ALTER TABLE `pms_specialpayroll_transactions`
	ADD COLUMN `responsibility_id` INT(11) NULL DEFAULT NULL AFTER `employee_status_id`,
	ADD COLUMN `salary_grade` INT(11) NULL DEFAULT NULL AFTER `responsibility_id`;