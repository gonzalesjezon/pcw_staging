ALTER TABLE `pms_transactions`
	ADD COLUMN `gsis_ee_share` DECIMAL(9,2) NULL DEFAULT NULL AFTER `gross_pay`,
	ADD COLUMN `gsis_er_share` DECIMAL(9,2) NULL DEFAULT NULL AFTER `gsis_ee_share`;