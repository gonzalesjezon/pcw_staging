ALTER TABLE `pms_nonplantilla_transactions`
	ADD COLUMN `actual_overtime_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `total_undertime_amount`,
	ADD COLUMN `total_overtime_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `actual_overtime_amount`,
	ADD COLUMN `actual_overtime` DECIMAL(10,3) NULL DEFAULT NULL AFTER `actual_workdays`,
	CHANGE COLUMN `actual_absences` `actual_absences` DECIMAL(10,3) NULL DEFAULT NULL AFTER `adjust_workdays`;