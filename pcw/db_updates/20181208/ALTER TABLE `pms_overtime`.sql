ALTER TABLE `pms_overtime`
	ADD COLUMN `position_id` INT NULL DEFAULT NULL AFTER `employee_number`,
	ADD COLUMN `division_id` INT NULL DEFAULT NULL AFTER `position_id`;