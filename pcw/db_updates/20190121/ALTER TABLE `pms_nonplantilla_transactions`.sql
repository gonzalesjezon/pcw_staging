ALTER TABLE `pms_nonplantilla_transactions`
	ADD COLUMN `adjustment_add` DECIMAL(13,2) NULL DEFAULT NULL AFTER `adjust_undertime`,
	ADD COLUMN `adjustment_less` DECIMAL(13,2) NULL DEFAULT NULL AFTER `adjustment_add`,
	ADD COLUMN `aom_amount` DECIMAL(13,2) NULL DEFAULT NULL AFTER `adjustment_less`;