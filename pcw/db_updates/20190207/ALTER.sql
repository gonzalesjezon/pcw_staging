ALTER TABLE `pms_payroll_information`
	ADD COLUMN `responsibility_id` INT(11) NULL DEFAULT NULL AFTER `providentfund_id`;

ALTER TABLE `pms_transactions`
	ADD COLUMN `responsibility_id` INT(11) NULL DEFAULT NULL AFTER `employeeinfo_id`;

ALTER TABLE `pms_rata`
	CHANGE COLUMN `oic` `is_oic` INT(11) NULL DEFAULT '0' AFTER `month`;