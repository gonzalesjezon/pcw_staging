-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pcw_db.pms_responsibility_center
DROP TABLE IF EXISTS `pms_responsibility_center`;
CREATE TABLE IF NOT EXISTS `pms_responsibility_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.pms_responsibility_center: ~13 rows (approximately)
/*!40000 ALTER TABLE `pms_responsibility_center` DISABLE KEYS */;
INSERT INTO `pms_responsibility_center` (`id`, `code`, `name`, `remarks`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 'CAIRMD', 'CAIRMD', 'cos', '2019-02-01 02:46:13', '2019-02-07 03:54:27', NULL, 1),
	(2, 'IACVAWC', 'IACVAWC', 'cos', '2019-02-01 02:46:44', '2019-02-07 03:54:46', NULL, 1),
	(3, 'DDO', 'Deputy Director for Operations', 'cos', '2019-02-01 02:47:17', '2019-02-07 03:54:34', NULL, 1),
	(4, 'DOMS', 'DOMS', 'cos', '2019-02-01 02:47:32', '2019-02-07 03:54:40', NULL, 1),
	(5, 'SCD', 'SCD', 'cos', '2019-02-01 02:47:42', '2019-02-07 03:54:53', NULL, 1),
	(6, 'AFD', 'AFD', 'cos', '2019-02-01 02:47:53', '2019-02-07 03:54:16', NULL, 1),
	(7, '001', 'MANCOM', 'permanent', '2019-02-07 03:55:20', '2019-02-07 03:55:20', NULL, 1),
	(8, '002', 'OED', 'permanent', '2019-02-07 03:56:39', '2019-02-07 03:56:39', NULL, 1),
	(9, '003', 'AFD', 'permanent', '2019-02-07 04:14:13', '2019-02-07 04:14:13', NULL, 1),
	(10, '004', 'CAIRMD', 'permanent', '2019-02-07 04:14:38', '2019-02-07 04:14:38', NULL, 1),
	(11, '005', 'PDPMED', 'permanent', '2019-02-07 04:14:57', '2019-02-07 04:14:57', NULL, 1),
	(12, '006', 'TSRCD', 'permanent', '2019-02-07 04:15:12', '2019-02-07 04:15:12', NULL, 1),
	(13, '007', 'SCD', 'permanent', '2019-02-07 04:15:29', '2019-02-07 04:15:29', NULL, 1);
/*!40000 ALTER TABLE `pms_responsibility_center` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
