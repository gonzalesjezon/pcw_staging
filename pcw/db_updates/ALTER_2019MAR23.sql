ALTER TABLE `pms_beginning_balances`
	ADD COLUMN `tax_withheld` DECIMAL(9,2) NULL DEFAULT NULL AFTER `gross_taxable_income`;

ALTER TABLE `pms_beginning_balances`
	ADD COLUMN `mandatory_deduction` DECIMAL(9,2) NULL DEFAULT NULL AFTER `tax_withheld`;