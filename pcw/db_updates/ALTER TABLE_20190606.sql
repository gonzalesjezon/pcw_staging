
ALTER TABLE `pms_transactions`
	ADD COLUMN `phic_share` DECIMAL(9,2) NULL DEFAULT NULL AFTER `gsis_er_share`,
	ADD COLUMN `pagibig_share` DECIMAL(9,2) NULL DEFAULT NULL AFTER `phic_share`,
	ADD COLUMN `mp2_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `pagibig_share`,
	ADD COLUMN `mp1_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `mp2_amount`;