-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pcw_db.pms_nonplantilla_employeeinfo
DROP TABLE IF EXISTS `pms_nonplantilla_employeeinfo`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_employeeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `tax_policy_id` int(11) DEFAULT NULL,
  `ewt_policy_id` int(11) DEFAULT NULL,
  `pwt_policy_id` int(11) DEFAULT NULL,
  `responsibility_id` int(11) DEFAULT NULL,
  `atm_no` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `inputted_amount` decimal(9,2) DEFAULT NULL,
  `threshhold_amount` decimal(9,2) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.pms_nonplantilla_employeeinfo: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` DISABLE KEYS */;
INSERT INTO `pms_nonplantilla_employeeinfo` (`id`, `employee_number`, `employee_id`, `bank_id`, `tax_policy_id`, `ewt_policy_id`, `pwt_policy_id`, `responsibility_id`, `atm_no`, `daily_rate_amount`, `monthly_rate_amount`, `overtime_balance_amount`, `inputted_amount`, `threshhold_amount`, `tax_id_number`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, '20170429', 84, NULL, 3, NULL, NULL, 2, NULL, 1049.07, 23079.54, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:19:06', '2019-05-20 06:24:20', NULL, NULL),
	(2, '20180478', 106, NULL, 3, NULL, NULL, 6, NULL, 914.07, 20109.54, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:19:41', '2019-05-20 06:19:41', NULL, NULL),
	(3, '20180476', 81, NULL, 3, NULL, NULL, 1, NULL, 2216.56, 48764.32, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:20:11', '2019-05-20 06:20:30', NULL, NULL),
	(4, '20190492', 118, NULL, 3, NULL, NULL, 1, NULL, 1665.33, 36637.26, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:21:01', '2019-05-20 06:21:01', NULL, NULL),
	(5, '20180485', 109, NULL, 3, NULL, NULL, 2, NULL, 1665.33, 36637.26, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:21:35', '2019-05-20 06:21:35', NULL, NULL),
	(6, '20190489', 122, NULL, 3, NULL, NULL, 2, NULL, 3562.85, 78382.70, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:22:11', '2019-05-20 06:22:11', NULL, NULL),
	(7, '20020208', 80, NULL, 3, NULL, NULL, 1, NULL, 1376.29, 30278.38, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:22:32', '2019-05-20 06:22:52', NULL, NULL),
	(8, '20180455', 79, NULL, 3, NULL, NULL, 1, NULL, 1665.33, 36637.26, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:23:23', '2019-05-20 06:23:23', NULL, NULL),
	(9, '20170424', 83, NULL, 3, NULL, NULL, 2, NULL, 2216.56, 48764.32, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:24:01', '2019-05-20 06:24:01', NULL, NULL),
	(10, '20180474', 85, NULL, 3, NULL, NULL, 3, NULL, 3562.85, 78382.70, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:24:47', '2019-05-20 06:24:47', NULL, NULL),
	(11, '20180461', 86, NULL, 3, NULL, NULL, 4, NULL, 809.84, 17816.48, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:25:19', '2019-05-20 06:25:19', NULL, NULL),
	(12, '20180463', 88, NULL, 3, NULL, NULL, 6, NULL, 1665.33, 36637.26, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:25:48', '2019-05-20 06:25:48', NULL, NULL),
	(13, '20180459', 89, NULL, 3, NULL, NULL, 6, NULL, 1665.33, 36637.26, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:26:08', '2019-05-20 06:26:08', NULL, NULL),
	(14, '20170431', 91, NULL, 3, NULL, NULL, 6, NULL, 809.84, 17816.48, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:26:29', '2019-05-20 06:26:41', NULL, NULL),
	(15, '20130328', 92, NULL, 3, NULL, NULL, 6, NULL, 720.76, 15856.72, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:29:01', '2019-05-20 06:29:01', NULL, NULL),
	(16, '20180460', 93, NULL, 3, NULL, NULL, 6, NULL, 720.76, 15856.72, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:29:26', '2019-05-20 06:29:26', NULL, NULL),
	(17, '20190490', 116, NULL, 3, NULL, NULL, 6, NULL, 980.46, 21570.12, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:29:50', '2019-05-20 06:29:50', NULL, NULL),
	(18, '20190491', 117, NULL, 3, NULL, NULL, 6, NULL, 809.84, 17816.48, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:30:15', '2019-05-20 06:30:23', NULL, NULL),
	(19, '20190493', 119, NULL, 3, NULL, NULL, 6, NULL, 914.07, 20109.54, 0.00, 0.00, 250000.00, NULL, '2019-05-20 06:30:58', '2019-05-20 06:30:58', NULL, NULL);
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
