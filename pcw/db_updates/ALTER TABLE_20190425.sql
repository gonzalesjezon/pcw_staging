ALTER TABLE `pms_benefitsinfo`
	ADD COLUMN `created_by` INT(11) NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `pms_loansinfo`
	ADD COLUMN `terminated` INT(11) NULL DEFAULT '0' AFTER `loan_date_terminated`;

ALTER TABLE `pms_loansinfo`
	ADD COLUMN `created_by` INT(11) NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `pms_deductioninfo`
	ADD COLUMN `terminated` INT NULL DEFAULT NULL AFTER `deduct_date_terminated`,
	ADD COLUMN `created_by` INT(11) NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `pms_salaryinfo`
	CHANGE COLUMN `salary_description` `appointment_status_id` INT NULL DEFAULT NULL AFTER `step_inc`;