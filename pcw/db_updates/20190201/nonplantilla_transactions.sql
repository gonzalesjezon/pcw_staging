ALTER TABLE `pms_nonplantilla_transactions`
	ADD COLUMN `responsibility_id` INT(11) NULL DEFAULT NULL AFTER `employee_status_id`;
ALTER TABLE `pms_nonplantilla_employeeinfo`
	ADD COLUMN `responsibility_id` INT(11) NULL DEFAULT NULL AFTER `taxpolicy_two_id`;

CREATE TABLE `pms_responsibility_center` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`code` VARCHAR(50) NULL DEFAULT NULL,
	`name` VARCHAR(225) NULL DEFAULT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`created_by` INT(11) NULL DEFAULT NULL,
	`updated_by` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
;