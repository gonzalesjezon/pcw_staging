-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pcw_db.pms_nonplantilla_employeeinfo
DROP TABLE IF EXISTS `pms_nonplantilla_employeeinfo`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_employeeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `taxpolicy_two_id` int(11) DEFAULT NULL,
  `responsibility_id` int(11) DEFAULT NULL,
  `atm_no` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `tax_amount_one` decimal(9,2) DEFAULT NULL,
  `tax_amount_two` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.pms_nonplantilla_employeeinfo: ~18 rows (approximately)
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` DISABLE KEYS */;
INSERT INTO `pms_nonplantilla_employeeinfo` (`id`, `employee_id`, `employee_number`, `bank_id`, `taxpolicy_id`, `taxpolicy_two_id`, `responsibility_id`, `atm_no`, `daily_rate_amount`, `monthly_rate_amount`, `annual_rate_amount`, `tax_amount_one`, `tax_amount_two`, `overtime_balance_amount`, `tax_id_number`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(20, 81, NULL, NULL, 9, NULL, 1, NULL, 2077.36, 45701.92, 548423.04, 1371.06, 0.00, 274211.52, NULL, '2018-12-11 06:37:13', '2019-02-01 02:59:47', NULL, NULL),
	(21, 85, NULL, NULL, 9, NULL, 3, NULL, 3202.75, 70460.50, 845526.00, 2113.82, 0.00, 422763.00, NULL, '2018-12-11 06:37:32', '2019-02-01 03:01:45', NULL, NULL),
	(22, 87, NULL, NULL, 9, NULL, 5, NULL, 691.31, 15208.82, 182505.84, 456.26, 0.00, 91252.92, NULL, '2018-12-11 06:37:44', '2019-02-01 03:02:45', NULL, NULL),
	(23, 94, NULL, NULL, 9, NULL, NULL, NULL, 782.18, 17207.96, 206495.52, 516.24, 0.00, 103247.76, NULL, '2018-12-11 06:37:57', '2018-12-11 06:37:57', NULL, NULL),
	(24, 84, NULL, NULL, 9, NULL, 2, NULL, 1020.98, 22461.56, 269538.72, 673.85, 0.00, 134769.36, NULL, '2018-12-11 06:38:10', '2019-02-01 03:01:27', NULL, NULL),
	(25, 80, NULL, NULL, 9, NULL, 1, NULL, 1321.31, 29068.82, 348825.84, 872.06, 0.00, 174412.92, NULL, '2018-12-11 06:38:20', '2019-02-01 03:00:04', NULL, NULL),
	(26, 79, NULL, NULL, 9, NULL, 1, NULL, 1100.66, 24214.52, 290574.24, 726.44, 0.00, 145287.12, NULL, '2018-12-11 06:38:36', '2019-02-01 02:59:18', NULL, NULL),
	(27, 86, NULL, NULL, 9, NULL, 4, NULL, 782.18, 17207.96, 206495.52, 516.24, 0.00, 103247.76, NULL, '2018-12-11 06:38:49', '2019-02-01 03:02:25', NULL, NULL),
	(28, 77, NULL, NULL, 9, NULL, NULL, NULL, 1582.36, 34811.92, 417743.04, 1044.36, 0.00, 208871.52, NULL, '2018-12-11 06:38:59', '2018-12-11 06:38:59', NULL, NULL),
	(29, 90, NULL, NULL, 9, NULL, NULL, NULL, 888.11, 19538.42, 234461.04, 586.15, 0.00, 117230.52, NULL, '2018-12-11 06:39:36', '2018-12-11 06:39:36', NULL, NULL),
	(30, 91, NULL, NULL, 9, NULL, 6, NULL, 691.31, 15208.82, 182505.84, 456.26, 0.00, 91252.92, NULL, '2018-12-11 06:39:56', '2019-02-01 03:03:56', NULL, NULL),
	(31, 92, NULL, NULL, 9, NULL, 6, NULL, 691.31, 15208.82, 182505.84, 456.26, 0.00, 91252.92, NULL, '2018-12-11 06:40:10', '2019-02-01 03:04:10', NULL, NULL),
	(32, 78, NULL, NULL, 9, NULL, 1, NULL, 1582.36, 34811.92, 417743.04, 1044.36, 0.00, 208871.52, NULL, '2018-12-11 06:40:23', '2019-02-01 02:58:36', NULL, NULL),
	(33, 83, NULL, NULL, 9, NULL, 2, NULL, 2077.36, 45701.92, 548423.04, 1371.06, 0.00, 274211.52, NULL, '2018-12-11 06:40:41', '2019-02-01 03:01:10', NULL, NULL),
	(34, 88, NULL, NULL, 9, NULL, 6, NULL, 1582.36, 34811.92, 417743.04, 1044.36, 0.00, 208871.52, NULL, '2018-12-11 06:40:58', '2019-02-01 03:03:05', NULL, NULL),
	(35, 89, NULL, NULL, 9, NULL, 6, NULL, 1582.36, 34811.92, 417743.04, 1044.36, 0.00, 208871.52, NULL, '2018-12-11 06:41:12', '2019-02-01 03:03:24', NULL, NULL),
	(36, 82, NULL, NULL, 9, NULL, 2, NULL, 1582.36, 34811.92, 417743.04, 1044.36, 0.00, 208871.52, NULL, '2018-12-11 06:41:29', '2019-02-01 03:00:29', NULL, NULL),
	(37, 93, NULL, NULL, 9, NULL, 6, NULL, 691.31, 15208.82, 182505.84, 456.26, 0.00, 91252.92, NULL, '2018-12-11 06:41:51', '2019-02-01 03:04:26', NULL, NULL);
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` ENABLE KEYS */;

-- Dumping structure for table pcw_db.pms_responsibility_center
DROP TABLE IF EXISTS `pms_responsibility_center`;
CREATE TABLE IF NOT EXISTS `pms_responsibility_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.pms_responsibility_center: ~6 rows (approximately)
/*!40000 ALTER TABLE `pms_responsibility_center` DISABLE KEYS */;
INSERT INTO `pms_responsibility_center` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 'CAIRMD', 'CAIRMD', '2019-02-01 02:46:13', '2019-02-01 02:46:13', NULL, NULL),
	(2, 'IACVAWC', 'IACVAWC', '2019-02-01 02:46:44', '2019-02-01 02:46:44', NULL, NULL),
	(3, 'DDO', 'Deputy Director for Operations', '2019-02-01 02:47:17', '2019-02-01 02:47:17', NULL, NULL),
	(4, 'DOMS', 'DOMS', '2019-02-01 02:47:32', '2019-02-01 02:47:32', NULL, NULL),
	(5, 'SCD', 'SCD', '2019-02-01 02:47:42', '2019-02-01 02:47:42', NULL, NULL),
	(6, 'AFD', 'AFD', '2019-02-01 02:47:53', '2019-02-01 02:47:53', NULL, NULL);
/*!40000 ALTER TABLE `pms_responsibility_center` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
