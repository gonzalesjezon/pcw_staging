-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pcw_db.pms_nonplantilla_employeeinfo
DROP TABLE IF EXISTS `pms_nonplantilla_employeeinfo`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_employeeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `tax_policy_id` int(11) DEFAULT NULL,
  `ewt_policy_id` int(11) DEFAULT NULL,
  `pwt_policy_id` int(11) DEFAULT NULL,
  `responsibility_id` int(11) DEFAULT NULL,
  `atm_no` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `inputted_amount` decimal(9,2) DEFAULT NULL,
  `threshhold_amount` decimal(9,2) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pcw_db.pms_nonplantilla_transactions
DROP TABLE IF EXISTS `pms_nonplantilla_transactions`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `salaryinfo_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `responsibility_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `actual_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `total_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `actual_absences_amount` decimal(9,2) DEFAULT NULL,
  `adjust_absences_amount` decimal(9,2) DEFAULT NULL,
  `total_absences_amount` decimal(9,2) DEFAULT NULL,
  `actual_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `adjust_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `total_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `actual_undertime_amount` decimal(9,2) DEFAULT NULL,
  `adjust_undertime_amount` decimal(9,2) DEFAULT NULL,
  `total_undertime_amount` decimal(9,2) DEFAULT NULL,
  `actual_overtime_amount` decimal(9,2) DEFAULT NULL,
  `total_overtime_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `basic_net_pay` decimal(9,2) DEFAULT NULL,
  `actual_contribution` decimal(9,2) DEFAULT NULL,
  `adjust_contribution` decimal(9,2) DEFAULT NULL,
  `total_contribution` decimal(9,2) DEFAULT NULL,
  `actual_loan` decimal(9,2) DEFAULT NULL,
  `adjust_loan` decimal(9,2) DEFAULT NULL,
  `total_loan` decimal(9,2) DEFAULT NULL,
  `actual_otherdeduct` decimal(9,2) DEFAULT NULL,
  `adjust_otherdeduct` decimal(9,2) DEFAULT NULL,
  `total_otherdeduct` decimal(9,2) DEFAULT NULL,
  `net_deduction` decimal(9,2) DEFAULT NULL,
  `tax_rate_amount_one` decimal(9,2) DEFAULT NULL,
  `tax_rate_amount_two` decimal(9,2) DEFAULT NULL,
  `allowances` decimal(9,2) DEFAULT NULL,
  `gross_pay` decimal(9,2) DEFAULT NULL,
  `gross_taxable_pay` decimal(9,2) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `actual_overtime` decimal(10,3) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `actual_absences` decimal(10,3) DEFAULT NULL,
  `adjust_absences` int(11) DEFAULT NULL,
  `actual_tardiness` int(11) DEFAULT NULL,
  `adjust_tardiness` int(11) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `adjustment_add` decimal(13,2) DEFAULT NULL,
  `adjustment_less` decimal(13,2) DEFAULT NULL,
  `aom_amount` decimal(13,2) DEFAULT NULL,
  `threshhold_balance` decimal(13,2) DEFAULT NULL,
  `net_pay` decimal(9,2) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `sub_pay_period` varchar(225) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
