$(document).ready(function() {
   t = 100;
   $("#iPerInfo").click(function(){
      $("#panelPerInfo").slideToggle(t);
   });
   $("#iFamBG").click(function(){
      $("#panelFamBG").slideToggle(t);
   });
   $("#iEducBG").click(function(){
      $("#panelEducBG").slideToggle(t);
   });
   $("#iEligib").click(function(){
      $("#panelEligib").slideToggle(t);
   });
   $("#iTrainProg").click(function(){
      $("#panelTrainProg").slideToggle(t);
   });
   $("#iVolWork").click(function(){
      $("#panelVolWork").slideToggle(t);
   });
   $("#iWExp").click(function(){
      $("#panelWExp").slideToggle(t);
   });
   $("#iOtherInfo").click(function(){
      $("#panelOtherInfo").slideToggle(t);
   });
   $("#iPDSQ").click(function(){
      $("#panelPDSQ").slideToggle(t);
   });
   $("#iRef").click(function(){
      $("#panelRef").slideToggle(t);
   });
});

function selectMe(emprefid) {
   var url = "ctrl_201File_1.e2e.php?";
   url += "EmpRefId=" + emprefid;
   $("[name='hEmpRefId']").val(emprefid);
   $("#btnHolder").hide();
   //$("#RecordDetails").html("");
   $("#RecordDetails").load(url, function(responseTxt, statusTxt, xhr) {
      if(statusTxt == "error") {
         console.log("Ooops Error (selectMe): " + xhr.status + " : " + xhr.statusText);
         return;
      }
      if(statusTxt == "success") {
         $("#btnHolder").show();
      }
   });
}