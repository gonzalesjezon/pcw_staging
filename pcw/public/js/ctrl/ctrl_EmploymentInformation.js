$(document).ready(function(){
   remIconDL();
   var selected = $("#SelectedEMP").val();
   if (selected != "" && selected != undefined) {
      local_tr_Click(selected);      
   } else {
      $(".alert, #divView, #DataEntry, #EntryEmpInformation").hide();
      $("#btnSAVE").prop("disabled",true);   
   }
   $("#btnSAVE").click(function(){
      var emprefid = $("#hEmpRefid").val();
      if (emprefid > 0) {
         var t = "EntryEmpInformation";
         fldnval_add = getFieldEntry(t,"ADD");
         if ($("#hRefId").val() == 0) {
            fldnval_add += "hidden|sint_EmployeesRefId|"+$("#hEmpRefid").val()+"!";  // Add Purposes;
         }
         if ($("[name='date_ResignedDate']").val() != "") {
            if (confirm("Is Employee "+emprefid+" is now Inactive?")) {
               updateEmpActiveness(emprefid);
               $.notify("Employee "+emprefid+" is not Inactive","warn",{position:"top center"});
            }
         }
         if (saveProceed() == 0) {
            gSaveRecord(fldnval_add,"empinformation");   
         }
         
      } else {
         alert("Oooops!!! No Emp. Ref ID.");
      }
   });

   $("#btnPRINT").click(function(){
      $("#prnModal").modal();
      $("#rptContent").attr("src","ReportCaller.e2e.php?file=rptEmploymentInfo");
   });
   
});

function afterNewSave(newRefId) {
   alert("New Record Inserted\nRef. ID: " + newRefId);
   gotoscrn($("#hProg").val(),"");
}

function afterEditSave(refId) {
   alert("Record " + refId + " Succcessfully Updated");
   gotoscrn($("#hProg").val(),"");
}

function local_tr_Click(empRefId,mode) {
   $("#hEmpRefid").val(empRefId); // hidden
   $("#EmpRefId").html(empRefId); // span
   $("#EntryEmpInformation, #DataEntry").show();
   $("#btnSAVE").prop("disabled",false);
   $.post("ctrl_EmploymentInformation.e2e.php",
   {
      t:"viewRecord",
      emprefid:empRefId
   },
   function(data,status) {
      var EmpInformation = 0;
      if (data == false) {
         $("#spanEmpName").html(empRefId + " : NO Employment Information!!!");
         $("#hRefId").val(0);
         $("#hmode").val("ADD");

         $("[name='date_HiredDate']").val("");
         $("[name='date_AssumptionDate']").val("");
         $("[name='date_ResignedDate']").val("");
         $("[name='date_RehiredDate']").val("");
         $("[name='date_StartDate']").val("");
         $("[name='date_EndDate']").val("");
         $("[name='sint_AgencyRefId']").val(0);
         $("[name='sint_PositionItemRefId']").val(0);
         $("[name='sint_PositionRefId']").val(0);
         $("[name='sint_OfficeRefId']").val(0);
         $("[name='sint_DepartmentRefId']").val(0);
         $("[name='sint_DivisionRefId']").val(0);
         $("[name='sint_EmpStatusRefId']").val(0);
         $("[name='sint_SalaryGradeRefId']").val(0);
         $("[name='sint_StepIncrementRefId']").val(0);
         $("[name='bint_WorkScheduleRefId']").val(0);
         $("[name='sint_PayrateRefId']").val(0);
         $("[name='deci_SalaryAmount']").val(0);
      }
      else {
         try
         {
            //$("#dbg").html(data);
            data = JSON.parse(data);
            if (parseInt(data.RefId) > 0) {
               //alert(data.LastName + ", " + data.FirstName);
               $("#spanEmpName").html(data.LastName + ", " + data.FirstName);
               $("#hmode").val("EDIT");
               $("#hRefId").val(data.RefId);
               $("[name='date_HiredDate']").val(data.HiredDate);
               $("[name='date_AssumptionDate']").val(data.AssumptionDate);
               $("[name='date_ResignedDate']").val(data.ResignedDate);
               $("[name='date_RehiredDate']").val(data.RehiredDate);
               $("[name='date_StartDate']").val(data.StartDate);
               $("[name='date_EndDate']").val(data.EndDate);
               $("[name='sint_AgencyRefId']").val(data.AgencyRefId);
               $("[name='sint_PositionItemRefId']").val(data.PositionItemRefId);
               $("[name='sint_PositionRefId']").val(data.PositionRefId);
               $("[name='sint_OfficeRefId']").val(data.OfficeRefId);
               $("[name='sint_DepartmentRefId']").val(data.DepartmentRefId);
               $("[name='sint_DivisionRefId']").val(data.DivisionRefId);
               $("[name='sint_EmpStatusRefId']").val(data.EmpStatusRefId);
               $("[name='sint_SalaryGradeRefId']").val(data.SalaryGradeRefId);
               $("[name='sint_StepIncrementRefId']").val(data.StepIncrementRefId);
               $("[name='sint_PayrateRefId']").val(data.PayrateRefId);
               $("[name='deci_SalaryAmount']").val(data.SalaryAmount);
               $("[name='bint_WorkScheduleRefId']").val(data.WorkScheduleRefId);
            } else {
               alert("Ooops!!! No RefId");
            }
         } catch (e) {
             if (e instanceof SyntaxError) {
                 alert(e.message);
             }
         }
      }
   });
}

function selectMe(emprefid) {
   local_tr_Click(emprefid);
}

function updateEmpActiveness(emprefid) {
   $.post("trn.e2e.php",
   {
      fn:"updateEmpActiveness",
      emprefid:emprefid
   },
   function(data,status){
      if (status != 'success') {
         alert(data);
         return false;
      }
   });
}



