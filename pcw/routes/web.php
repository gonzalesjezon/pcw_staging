<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteSericeProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','Auth\LoginController@showLoginForm');
Auth::routes();

Route::group(['middleware' => 'auth'],function(){

	Route::resource('dashboards', 'DashboardsController');


	Route::group(['prefix' => 'payrolls/admin/filemanagers'],function(){

		Route::get('benefits/getItem','BenefitsController@getItem');
		Route::post('benefits/deleteItem','BenefitsController@deleteItem');
		Route::resource('benefits','BenefitsController');

		Route::get('adjustments/getItem','AdjustmentsController@getItem');
		Route::resource('adjustments','AdjustmentsController');

		Route::get('deductions/getItem','DeductionsController@getItem');
		Route::post('deductions/deleteItem','DeductionsController@deleteItem');
		Route::resource('deductions','DeductionsController');

		Route::get('loans/getItem','LoansController@getItem');
		Route::post('loans/deleteItem','LoansController@deleteItem');
		Route::resource('loans','LoansController');

		Route::get('responsibilitiescenter/getItem','ResponsibilitiesCenterController@getItem');
		Route::resource('responsibilitiescenter','ResponsibilitiesCenterController');

		Route::get('banks/getItem','BanksController@getItem');
		Route::resource('banks','BanksController');

		Route::get('bankbranches/getItem','BankBranchesController@getItem');
		Route::resource('bankbranches','BankBranchesController');

		Route::post('pagibig/storePolicy','PagibigController@storePolicy');
		Route::get('pagibig/showPolicy','PagibigController@showPolicy');
		Route::get('pagibig/getItem','PagibigController@getItem');
		Route::resource('pagibig','PagibigController');

		Route::post('philhealths/storePolicy','PhilhealthsController@storePolicy');
		Route::get('philhealths/showPolicy','PhilhealthsController@showPolicy');
		Route::get('philhealths/getItem','PhilhealthsController@getItem');
		Route::resource('philhealths','PhilhealthsController');

		Route::get('providentfundpolicies/getItem','ProvidentFundPoliciesController@getItem');
		Route::resource('providentfundpolicies','ProvidentFundPoliciesController');

		Route::get('salariesgrade/getSgstep','SalariesGradeController@getSgstep');
		Route::resource('salariesgrade','SalariesGradeController');

		Route::post('taxes/storeTaxstatus','TaxesController@storeTaxstatus');
		Route::get('taxes/showTaxstatus','TaxesController@showTaxstatus');
		Route::post('taxes/storeTaxstatusannual','TaxesController@storeTaxstatusannual');
		Route::get('taxes/showTaxstatusannual','TaxesController@showTaxstatusannual');
		Route::post('taxes/storeTaxpolicy','TaxesController@storeTaxpolicy');
		Route::get('taxes/showTaxpolicy','TaxesController@showTaxpolicy');

		Route::get('taxes/getTaxtable','TaxesController@getTaxtable');
		Route::get('taxes/getTaxstatus','TaxesController@getTaxstatus');
		Route::get('taxes/getTaxannual','TaxesController@getTaxannual');
		Route::get('taxes/getTaxpolicy','TaxesController@getTaxpolicy');
		Route::post('taxes/deleteTaxpolicy','TaxesController@deleteTaxpolicy');
		Route::resource('taxes','TaxesController');

		Route::get('wagerates/getItem','WageRatesController@getItem');
		Route::resource('wagerates','WageRatesController');

		Route::get('gsis/getItem','GsisController@getItem');
		Route::resource('gsis','GsisController');

		Route::get('jobgrades/getJgstep','GsisController@getJgstep');
		Route::resource('jobgrades','JobGradesController');

		Route::resource('divisions','DivisionsController');
		Route::resource('offices','OfficesController');
		Route::resource('employeestatus','EmployeeStatusController');
		Route::resource('positions','PositionsController');
		Route::resource('position_items','PositionItemsController');
		Route::resource('rates','RatesController');
		Route::resource('travelrates','TravelRateController');
	});

	Route::group(['prefix' => 'payrolls/reports/remittances'],function(){
		Route::get('gsis/getEmployeeinfo','GsisRemittancesController@getEmployeeinfo');
		Route::resource('gsis','GsisRemittancesController');
		Route::get('ecip/getEmployeeinfo','EcipRemittancesController@getEmployeeinfo');
		Route::resource('ecip','EcipRemittancesController');
		Route::get('pagibig/getEmployeeinfo','PagibigRemittancesController@getEmployeeinfo');
		Route::resource('pagibig','PagibigRemittancesController');
		Route::get('philhealth/getEmployeeinfo','PhilhealthRemittancesController@getEmployeeinfo');
		Route::resource('philhealth','PhilhealthRemittancesController');
	});

	Route::group(['prefix' => 'payrolls/reports/othercompensations'],function(){

		Route::get('communicationexpense/getEmployeeinfo','CommunicationExpenseReportController@getEmployeeinfo');
		Route::resource('communicationexpense','CommunicationExpenseReportController');

		Route::get('eme/getEmployeeinfo','EmeReportsController@getEmployeeinfo');
		Route::resource('eme','EmeReportsController');

		Route::get('cashmidyearbonus/getEmployeeinfo','CashGiftsAndMidYearReportController@getEmployeeinfo');
		Route::resource('cashmidyearbonus','CashGiftsAndMidYearReportController');
	});

	Route::group(['prefix' => 'payrolls/reports/nonplantillareports'], function(){

		Route::get('cocpayrollworksheets/getPayrollWorksheetReport','COCPayrollWorksheetReportController@getPayrollWorksheetReport');
		Route::resource('cocpayrollworksheets','COCPayrollWorksheetReportController');

		Route::get('cocgeneralpayroll/getGeneralPayrollReport','COCGeneralPayrollReportController@getGeneralPayrollReport');
		Route::resource('cocgeneralpayroll','COCGeneralPayrollReportController');
		Route::resource('costransmital','COSTransmitalReportsController');

	});

	Route::group(['prefix'=>'payrolls/reports/plantillareports'],function(){
		Route::get('payrollworksheets/getPayrollWorksheetReport','PayrollWorksheetReportController@getPayrollWorksheetReport');
		Route::resource('payrollworksheets','PayrollWorksheetReportController');
		Route::resource('generalpayroll','GeneralPayrollReportsController');
		Route::resource('regulartransmital','RegularTransmitalReportsController');
	});

	Route::group(['prefix'=>'payrolls/reports/overtimereports'],function(){

		Route::resource('overtimetransmital','OvertimeTransmitalReportsController');
		Route::resource('generalpayrollovertime','GeneralPayrollOvertimeReportsController');
		Route::resource('overtimepayrollworksheet','OvertimePayrollWorksheetReportsController');

	});

	Route::group(['prefix'=>'payrolls/reports/yearendreports'],function(){

		Route::resource('yearendgeneralpayroll','YearEndGeneralPayrollReportsReportsController');
		Route::resource('yearendtransmital','YearEndTransmitalReportsReportsController');

	});

	Route::group(['prefix'=>'payrolls/reports/monetizationreports'],function(){

		Route::resource('monetizationgeneralpayroll','MonetizationGeneralPayrollReportsReportsController');
		Route::resource('monetizationtransmital','MonetizationTransmitalReportsReportsController');

	});

	Route::group(['prefix'=>'payrolls/reports/cnapeireports'],function(){
		Route::resource('cnapeigeneralpayroll','CNAPEIReportsController');
		Route::resource('cnapeitransmital','CNAPEITransmitalReportsController');
	});

	Route::group(['prefix'=>'payrolls/reports'],function(){
		Route::resource('payslips','PayslipsController');
		Route::resource('ratapayroll','RataReportsController');
		Route::resource('midyearpayroll','MidYearGeneralPayrollReportsController');
		Route::resource('multipurposeloans','MultiPurposeLoanReportsController');
		Route::resource('taxesremittance','TaxesRemittanceReportsController');
		Route::resource('ssscontributions','SSSContributionReportsController');
		Route::resource('mwsscooperative','MultiPurposeCooperativeReportsController');
		Route::resource('welfarefund','WelfareFundReportsController');
		Route::resource('mwsstubig','MultiPurposeTubigReportsController');
		Route::resource('serviceloans','ServiceLoanReportsController');
		Route::resource('premiumcontributions','PremiumContributionReportsController');
		Route::resource('wasslai','WASSLAIReportsController');
		Route::resource('gippayrollworksheet','GIPPayrollWorksheetReportsController');

		Route::resource('generalpayrolltravelallowance','GeneralPayrollTravelAllowanceReportsController');

		Route::resource('deductedloanreports','DeductedLoanReportsController');
		Route::resource('loandetailsreports','LoanDetailsReportsController');
		Route::resource('gsisloanreports','GSISLoanReportsController');

		Route::get('lwopadjustments/getAdjustments','LWOPAdjustmentReportsController@getAdjustments');
		Route::resource('lwopadjustments','LWOPAdjustmentReportsController');
		Route::get('stepincrementreports/getStepIncrements','StepIncrementReportsController@getStepIncrements');
		Route::resource('stepincrementreports','StepIncrementReportsController');
		Route::resource('generalpayroll','GeneralPayrollReportsController');
		Route::resource('cospayroll','COSPayrollReportsController');
		Route::resource('peipayroll','PerformancesReportController');
		Route::resource('pbbpayroll','PBBReportsController');
		Route::resource('yearendpayroll','YearEndReportsController');
		Route::resource('gpanniversary','GPAnniversaryReportsController');
		Route::resource('gpuniformallowance','GPUniformAllowanceReportsController');
		Route::resource('gpovertime','GPOvertimeReportsController');
		Route::resource('recomputationtax','RecomputationTaxReportsController');
	});

	Route::group(['prefix'=>'payrolls/otherpayrolls'],function(){

		Route::get('initialsalaries/getInitialSalary','InitialSalariesController@getInitialSalary');
		Route::resource('initialsalaries','InitialSalariesController');
		Route::get('lastsalaries/getLastSalary','LastSalariesController@getLastSalary');
		Route::resource('lastsalaries','LastSalariesController');
		Route::get('cancelsalaries/getCancelSalary','CancelSalariesController@getCancelSalary');
		Route::resource('cancelsalaries','CancelSalariesController');

		// TRAVEL ALLOWANCE
		Route::get('leavemonetizations/getLeaveMonetization','LeaveMonetizationTransactionsController@getLeaveMonetization');
		Route::get('leavemonetizations/showLeaveMonetizationDatatable','LeaveMonetizationTransactionsController@showLeaveMonetizationDatatable');
		Route::post('leavemonetizations/processLeaveMonetization','LeaveMonetizationTransactionsController@processLeaveMonetization');
		Route::post('leavemonetizations/deleteLeaveMonetization','LeaveMonetizationTransactionsController@deleteLeaveMonetization');
		Route::resource('leavemonetizations','LeaveMonetizationTransactionsController');

		Route::get('salaryadjustments/getSalaryAdjustment','SalaryAdjustmentsController@getSalaryAdjustment');
		Route::get('salaryadjustments/getDaysInAMonth','SalaryAdjustmentsController@getDaysInAMonth');
		Route::get('salaryadjustments/getCountedDays','SalaryAdjustmentsController@getCountedDays');
		Route::post('salaryadjustments/deleteAdjustment','SalaryAdjustmentsController@deleteAdjustment');
		Route::resource('salaryadjustments','SalaryAdjustmentsController');
		Route::post('stepincrements/deleteStepIncrement','StepIncrementsController@deleteStepIncrement');
		Route::get('stepincrements/getStepIncrement','StepIncrementsController@getStepIncrement');
		Route::resource('stepincrements','StepIncrementsController');


	});

	Route::group(['prefix' => 'payrolls/specialpayrolls'],function(){

				// RATA PAYROLL
		Route::get('ratatransactions/getSearchby','RataTransactionsController@getSearchby');
		Route::get('ratatransactions/getRataInfo','RataTransactionsController@getRataInfo');
		Route::post('ratatransactions/storeRata','RataTransactionsController@storeRata');
		Route::post('ratatransactions/processRata','RataTransactionsController@processRata');
		Route::post('ratatransactions/deleteRata','RataTransactionsController@deleteRata');
		Route::resource('ratatransactions/showRataDatatable','RataTransactionsController@showRataDatatable');
		Route::resource('ratatransactions/filter','RataTransactionsController@filter');
		Route::resource('ratatransactions','RataTransactionsController');

		// EME PAYROLL
		Route::get('emetransactions/getEmeInfo','EmeTransactionsController@getEmeInfo');
		Route::get('emetransactions/showEmeDatatable','EmeTransactionsController@showEmeDatatable');
		Route::post('emetransactions/processEme','EmeTransactionsController@processEme');
		Route::post('emetransactions/deleteEme','EmeTransactionsController@deleteEme');
		Route::resource('emetransactions','EmeTransactionsController');

		// COMMUNICATION ALLOTMENT
		Route::get('communicationtransactions/getCeaInfo','CommunicationTransactionsController@getCeaInfo');
		Route::get('communicationtransactions/showCeaDatatable','CommunicationTransactionsController@showCeaDatatable');
		Route::post('communicationtransactions/processCea','CommunicationTransactionsController@processCea');
		Route::post('communicationtransactions/deleteCea','CommunicationTransactionsController@deleteCea');
		Route::resource('communicationtransactions','CommunicationTransactionsController');

		// PERFORMANCE INCENTIVE
		Route::get('peitransactions/getPeiInfo','PerformanceIncentiveTransactionsController@getPeiInfo');
		Route::get('peitransactions/showPeiDatatable','PerformanceIncentiveTransactionsController@showPeiDatatable');
		Route::post('peitransactions/processPei','PerformanceIncentiveTransactionsController@processPei');
		Route::post('peitransactions/deletePei','PerformanceIncentiveTransactionsController@deletePei');
		Route::resource('peitransactions','PerformanceIncentiveTransactionsController');

		// CASH GIFT AND YEAR END BONUS
		Route::get('cgyetransactions/getCgyeInfo','CashGiftAndYearEndTransactionsController@getCgyeInfo');
		Route::get('cgyetransactions/showCgyeDatatable','CashGiftAndYearEndTransactionsController@showCgyeDatatable');
		Route::post('cgyetransactions/processCgye','CashGiftAndYearEndTransactionsController@processCgye');
		Route::post('cgyetransactions/deleteCgye','CashGiftAndYearEndTransactionsController@deleteCgye');
		Route::resource('cgyetransactions','CashGiftAndYearEndTransactionsController');

		// MID YEAR END BONUS
		Route::get('midyeartransactions/getMidYear','MidYearBonusTransactionsController@getMidYear');
		Route::get('midyeartransactions/showMidYearDatatable','MidYearBonusTransactionsController@showMidYearDatatable');
		Route::post('midyeartransactions/processMidYear','MidYearBonusTransactionsController@processMidYear');
		Route::post('midyeartransactions/deleteMidYear','MidYearBonusTransactionsController@deleteMidYear');
		Route::resource('midyeartransactions','MidYearBonusTransactionsController');

		// TRAVEL ALLOWANCE
		Route::get('traveltransactions/getTravelAllowance','TravelRateTransactionsController@getTravelAllowance');
		Route::get('traveltransactions/showTravelAllowanceDatatable','TravelRateTransactionsController@showTravelAllowanceDatatable');
		Route::post('traveltransactions/deleteTravelAllowance','TravelRateTransactionsController@deleteTravelAllowance');
		Route::resource('traveltransactions','TravelRateTransactionsController');

		// Performance Base Bonus
		Route::get('pbbtransactions/getPbbInfo','PerformanceBasicBonusTransactionsController@getPbbInfo');
		Route::get('pbbtransactions/showPbbDatatable','PerformanceBasicBonusTransactionsController@showPbbDatatable');
		Route::post('pbbtransactions/processPbb','PerformanceBasicBonusTransactionsController@processPbb');
		Route::post('pbbtransactions/deletePbb','PerformanceBasicBonusTransactionsController@deletePbb');
		Route::resource('pbbtransactions','PerformanceBasicBonusTransactionsController');

		// HONORARIAS
		Route::get('honorarias/getHonoraria','HonorariaTransactionsController@getHonoraria');
		Route::get('honorarias/showHonoraria','HonorariaTransactionsController@showHonoraria');
		Route::post('honorarias/processHonoraria','HonorariaTransactionsController@processHonoraria');
		Route::post('honorarias/deleteHonoraria','HonorariaTransactionsController@deleteHonoraria');
		Route::resource('honorarias','HonorariaTransactionsController');

		// ANNIVERSARY BONUS
		Route::get('anniversarytransactions/getAnniversary','AnniversaryBonusTransactionsController@getAnniversary');
		Route::get('anniversarytransactions/showAnniversaryDatatable','AnniversaryBonusTransactionsController@showAnniversaryDatatable');
		Route::post('anniversarytransactions/processAnniversary','AnniversaryBonusTransactionsController@processAnniversary');
		Route::post('anniversarytransactions/deleteAnniversary','AnniversaryBonusTransactionsController@deleteAnniversary');
		Route::resource('anniversarytransactions','AnniversaryBonusTransactionsController');

		// UNIFORM ALLOWANCE
		Route::get('uniformtransactions/getUniform','UniformAllowanceTransactionsController@getUniform');
		Route::get('uniformtransactions/showUniformDatatable','UniformAllowanceTransactionsController@showUniformDatatable');
		Route::post('uniformtransactions/processUniform','UniformAllowanceTransactionsController@processUniform');
		Route::post('uniformtransactions/deleteUniform','UniformAllowanceTransactionsController@deleteUniform');
		Route::post('uniformtransactions/updateUniform','UniformAllowanceTransactionsController@updateUniform');
		Route::resource('uniformtransactions','UniformAllowanceTransactionsController');

	});


	Route::group(['prefix' => 'payrolls'], function(){

		// EMPLOYEE FILE
		Route::get('admin/employees_payroll_informations/getJgstep','EmployeePayrollInformationsController@getJgstep');
		Route::get('admin/employees_payroll_informations/getItem','EmployeePayrollInformationsController@getItem');
		Route::get('admin/employees_payroll_informations/getSgstep','EmployeePayrollInformationsController@getSgstep');
		Route::get('admin/employees_payroll_informations/getEmployeesinfo','EmployeePayrollInformationsController@getEmployeesinfo');
		Route::get('admin/employees_payroll_informations/getSearchby','EmployeePayrollInformationsController@getSearchby');
		Route::get('admin/employees_payroll_informations/filter','EmployeePayrollInformationsController@filter');
		Route::get('admin/employees_payroll_informations/showBenefitinfo','EmployeePayrollInformationsController@showBenefitinfo');
		Route::get('admin/employees_payroll_informations/showSalaryinfo','EmployeePayrollInformationsController@showSalaryinfo');
		Route::get('admin/employees_payroll_informations/showDeductioninfo','EmployeePayrollInformationsController@showDeductioninfo');
		Route::get('admin/employees_payroll_informations/showLoaninfo','EmployeePayrollInformationsController@showLoaninfo');
		Route::get('admin/employees_payroll_informations/computeEmployeeInfo','EmployeePayrollInformationsController@computeEmployeeInfo');
		Route::post('admin/employees_payroll_informations/storeBenefitinfo','EmployeePayrollInformationsController@storeBenefitinfo');
		Route::post('admin/employees_payroll_informations/deleteBenefitinfo','EmployeePayrollInformationsController@deleteBenefitinfo');
		Route::post('admin/employees_payroll_informations/deleteLoanInfo','EmployeePayrollInformationsController@deleteLoanInfo');
		Route::post('admin/employees_payroll_informations/deleteDeductInfo','EmployeePayrollInformationsController@deleteDeductInfo');
		Route::post('admin/employees_payroll_informations/storeSalaryinfo','EmployeePayrollInformationsController@storeSalaryinfo');
		Route::post('admin/employees_payroll_informations/storeDeductioninfo','EmployeePayrollInformationsController@storeDeductioninfo');
		Route::post('admin/employees_payroll_informations/storeLoaninfo','EmployeePayrollInformationsController@storeLoaninfo');
		Route::post('admin/employees_payroll_informations/storeNonPlantilla','EmployeePayrollInformationsController@storeNonPlantilla');
		Route::resource('admin/employees_payroll_informations','EmployeePayrollInformationsController');

		Route::resource('admin/employeesetup','EmployeeSetupController');

		Route::get('admin/annualtaxsetup/getAnnualTaxPolicy','AnnualTaxSetupController@getAnnualTaxPolicy');
		Route::get('admin/annualtaxsetup/getEmployeesinfo','AnnualTaxSetupController@getEmployeesinfo');
		Route::post('admin/annualtaxsetup/deleteTax','AnnualTaxSetupController@deleteTax');
		Route::resource('admin/annualtaxsetup','AnnualTaxSetupController');

		Route::resource('otherpayrolls','OtherPayrollsController');
		Route::resource('filemanagers','FileManagersController');

		Route::post('transactions/deletePayroll','TransactionsController@deletePayroll');
		Route::post('transactions/processPayroll','TransactionsController@processPayroll');
		Route::post('transactions/updatePayroll','TransactionsController@updatePayroll');
		Route::post('transactions/storeBenefitInfoTransaction','TransactionsController@storeBenefitInfoTransaction');
		Route::post('transactions/storeDeductionInfoTransaction','TransactionsController@storeDeductionInfoTransaction');
		Route::get('transactions/showLoaninfo','TransactionsController@showLoaninfo');
		Route::get('transactions/showDeductioninfo','TransactionsController@showDeductioninfo');
		Route::get('transactions/showBenefitinfo','TransactionsController@showBenefitinfo');
		Route::get('transactions/getSearchby','TransactionsController@getSearchby');
		Route::get('transactions/searchName','TransactionsController@searchName');
		Route::get('transactions/filter','TransactionsController@filter');
		Route::get('transactions/getEmployeesinfo','TransactionsController@getEmployeesinfo');
		Route::post('transactions/deleteLoan','TransactionsController@deleteLoan');
		Route::post('transactions/deleteBenefit','TransactionsController@deleteBenefit');
		Route::post('transactions/deleteDeduction','TransactionsController@deleteDeduction');
		Route::resource('transactions','TransactionsController');

		Route::resource('reports','ReportsController');

		Route::post('admin/users/deleteUser','UsersController@deleteUser');
		Route::resource('admin/users','UsersController');

		Route::resource('admin/payrollconfigurations','PayrollConfigurationsController');
		Route::get('admin/previousemployer/getPreviousEmployer','PreviousEmployerController@getPreviousEmployer');
		Route::resource('admin/previousemployer','PreviousEmployerController');
		Route::get('admin/beginningbalances/getBeginningBalances','BeginningBalancesController@getBeginningBalances');
		Route::resource('admin/beginningbalances','BeginningBalancesController');

		Route::get('admin/loanshistory/getLoansHistory','LoansAndDeductionsHistoryController@getLoansHistory');
		Route::resource('admin/loanshistory','LoansAndDeductionsHistoryController');

		Route::get('overtimepay/getOvertimeInfo','OvertimePaysController@getOvertimeInfo');
		Route::post('overtimepay/deleteOvertime','OvertimePaysController@deleteOvertime');
		Route::post('overtimepay/storeOvertimeInfo','OvertimePaysController@storeOvertimeInfo');
		Route::resource('overtimepay','OvertimePaysController');
		Route::post('nonplantilla/processPayroll','NonPlantillaController@processPayroll');
		Route::get('nonplantilla/getEmployeesinfo','NonPlantillaController@getEmployeesinfo');
		Route::resource('nonplantilla','NonPlantillaController');



	});


});

