<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnualTaxPolicy extends Model
{
    protected $table = 'pms_annualtax_policy';
    protected $fillable = [

		'from_year',
		'to_year',
		'below_amount',
		'above_amount',
		'rate_percentage',
		'rate_amount',
		'excess_amount',
		'effectivity_date',
		'remarks',
		'updated_by',
		'created_by'
    ];
}
