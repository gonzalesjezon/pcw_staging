<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelRate extends Model
{
    protected $table = 'pms_travelrates';
    protected $fillable = [
    	'rate_amount',
    	'status',
    	'created_by',
    	'updated_by'
    ];
}
