<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagibigPolicy extends Model
{
    protected $table = 'pms_pagibigpolicy';
    protected $fillable = [
    	'policy_name',
    	'pay_period',
    	'deduction_period',
    	'policy_type',
    	'based_on',
    	'value',
    	'remarks',
    ];
}
