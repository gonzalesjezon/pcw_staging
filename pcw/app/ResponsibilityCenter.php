<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponsibilityCenter extends Model
{
    protected $table = 'pms_responsibility_center';
    protected $fillable = [
    	'code',
    	'name',
    	'description',
    	'remarks'
    ];

    public function employeeinfo(){
    	return $this->belongsTo('App\EmployeeInfo');
    }
}
