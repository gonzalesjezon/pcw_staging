<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhilhealthPolicy extends Model
{
    protected $table = 'pms_philhealthpolicy';
    protected $fillable = [
    	'policy_name',
    	'pay_period',
    	'deduction_period',
    	'policy_type',
    	'based_on',
    	'below',
        'above',
    	'remarks'
    ];
}
