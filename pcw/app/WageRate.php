<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WageRate extends Model
{
    protected $table = 'pms_wagerates';
    protected $fillable = [
    	'wage_region',
    	'wage_rate',
    	'effectivity_date',
    	'remarks',
    ];
}
