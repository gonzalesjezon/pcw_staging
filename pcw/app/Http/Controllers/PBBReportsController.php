<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
use App\BenefitInfo;
use App\Benefit;
use App\Employee;
class PBBReportsController extends Controller
{
    function __construct(){
		$this->title = 'PERFORMANCE BASE BONUS REPORT';
    	$this->module = 'pbbpayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new SpecialPayrollTransaction;

        $query = $transaction
        ->with('offices','positions','divisions','salaryinfo','employees','employeeinfo')
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','pbb')
        ->get();

        $data = [];
        foreach ($query as $key => $value) {
            if(isset($value->division_id)){
                $data[$value->divisions->Name][$key] = $value;
            }else{
                $data = [];
            }
        }


        return json_encode([
            'transaction' => $data,
            'print_date'  => date('Y-m-d')
        ]);
    }
}
