<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Deduction;
use App\DeductionInfo;
use App\Transaction;
class SSSContributionReportsController extends Controller
{
    function __construct(){
		$this->title = 'SSS CONTRIBUTION REPORT';
    	$this->module = 'ssscontributions';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data = Input::all();
    	$transaction = new Transaction;
        $deduction =        new Deduction;
        $deductioninfo    = new DeductionInfo;

        $deduction_id = $deduction->where('code','SSS CONT')->select('id')->first();

        $employee_id = $deductioninfo->where('deduction_id',$deduction_id->id)->select('employee_id')->get()->toArray();

        $transaction_employee_id = $transaction->select('employee_id')
                                              ->whereIn('employee_id',$employee_id)
                                              ->where('year',$data['year'])
                                              ->where('month',$data['month'])
                                              ->get()->toArray();

        $query = $deductioninfo->with(['employeeinfo','employees'=>function($qry){ $qry->orderBy('lastname','asc'); }])
                            ->where('deduction_id',$deduction_id->id)
                            ->whereIn('employee_id',$transaction_employee_id)
                            ->get();


    	return json_encode($query);
    }
}
