<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\SalaryInfo;
use App\EmployeeInfo;
use App\DeductionInfo;
use App\LoanInfo;
use App\Deduction;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\StepIncrement;
use Carbon\Carbon;
use Auth;
class StepIncrementsController extends Controller
{
    function __construct(){
    	$this->title = 'STEP INCREMENT';
    	$this->module = 'stepincrements';
        $this->module_prefix = 'payrolls/otherpayrolls';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname'];

        $status              = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;
        $employee            = new Employee;

        $status = $status
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $arr_employeeid = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();


        $query = $employee
                ->whereIn('id',$arr_employeeid)
                ->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });


        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function store(Request $request){

        $stepincrement = new StepIncrement;

        $transaction_date   = $request->transaction_date;
        $employee_id        = $request->employee_id;
        $transaction_id     = $request->transaction_id;

        $new_basic_pay = (isset($request->new_rate_amount)) ? str_replace(',', '', $request->new_rate_amount) : 0;
        $old_basic_pay = (isset($request->old_rate_amount)) ? str_replace(',', '', $request->old_rate_amount) : 0;
        $salary_adjustment = (isset($request->adjustment_amount)) ? str_replace(',', '', $request->adjustment_amount) : 0;
        $gsis_cont_amount = (isset($request->gsis_cont_amount)) ? str_replace(',', '', $request->gsis_cont_amount) : 0;
        $philhealth_cont_amount = (isset($request->philhealth_cont_amount)) ? str_replace(',', '', $request->philhealth_cont_amount) : 0;
        // $provident_cont_amount = (isset($request->pf_cont_amount)) ? str_replace(',', '', $request->pf_cont_amount) : 0;
        $gross_pay = (isset($request->basic_amount)) ? str_replace(',', '', $request->basic_amount) : 0;
        $wtax_amount = (isset($request->tax_amount)) ? str_replace(',', '', $request->tax_amount) : 0;
        $first_amount = (isset($request->first_amount)) ? str_replace(',', '', $request->first_amount) : 0;
        $second_amount = (isset($request->second_amount)) ? str_replace(',', '', $request->second_amount) : 0;
        $third_amount = (isset($request->third_amount)) ? str_replace(',', '', $request->third_amount) : 0;
        $fourth_amount = (isset($request->fourth_amount)) ? str_replace(',', '', $request->fourth_amount) : 0;

        if(isset($transaction_id)){

            $stepincrement = $stepincrement->find($transaction_id);

            $stepincrement->employee_id                = $employee_id;
            $stepincrement->old_basic_pay_amount       = $old_basic_pay;
            $stepincrement->new_basic_pay_amount       = $new_basic_pay;
            $stepincrement->salary_adjustment_amount   = $salary_adjustment;
            $stepincrement->gsis_cont_amount           = $gsis_cont_amount;
            $stepincrement->philhealth_cont_amount     = $philhealth_cont_amount;
            // $stepincrement->provident_fund_amount      = $provident_cont_amount;
            $stepincrement->wtax_amount                = $wtax_amount;
            $stepincrement->first_deduction_amount     = $first_amount;
            $stepincrement->second_deduction_amount    = $second_amount;
            $stepincrement->third_deduction_amount     = $third_amount;
            $stepincrement->fourth_deduction_amount    = $fourth_amount;
            $stepincrement->gross_pay_amount           = $gross_pay;
            $stepincrement->date_from                  = $request->date_from;
            $stepincrement->date_to                    = $request->date_to;
            $stepincrement->transaction_date           = $transaction_date;
            $stepincrement->updated_by                 = Auth::User()->id;

            $stepincrement->save();

            $response = json_encode(['status'=>true,'response' => 'Update Successfully']);


        }else{

            $this->validate($request,[
                'transaction_date'  => 'required',
                'date_from'         => 'required',
                'date_to'           => 'required'
            ]);

            $stepincrement->employee_id                = $employee_id;
            $stepincrement->old_basic_pay_amount       = $old_basic_pay;
            $stepincrement->new_basic_pay_amount       = $new_basic_pay;
            $stepincrement->salary_adjustment_amount   = $salary_adjustment;
            $stepincrement->gsis_cont_amount           = $gsis_cont_amount;
            $stepincrement->philhealth_cont_amount     = $philhealth_cont_amount;
            // $stepincrement->provident_fund_amount      = $provident_cont_amount;
            $stepincrement->wtax_amount                = $wtax_amount;
            $stepincrement->first_deduction_amount     = $first_amount;
            $stepincrement->second_deduction_amount    = $second_amount;
            $stepincrement->third_deduction_amount     = $third_amount;
            $stepincrement->fourth_deduction_amount    = $fourth_amount;
            $stepincrement->gross_pay_amount           = $gross_pay;
            $stepincrement->date_from                  = $request->date_from;
            $stepincrement->date_to                    = $request->date_to;
            $stepincrement->transaction_date           = $transaction_date;
            $stepincrement->created_by                 = Auth::User()->id;

            $stepincrement->save();

            $response = json_encode(['status'=>true,'response' => 'Save Successfully']);

        }

        return $response;


    }

    public function showStepIncrement(){

         $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);


    }

    public function getStepIncrement(){

        $data = Input::all();

        $employee_id = $data['id'];

        $salaryinfo     = new SalaryInfo;
        $employeeinfo   = new EmployeeInfo;
        $deductioninfo  = new DeductionInfo;
        $deduction      = new Deduction;
        $loaninfo       = new LoanInfo;
        $stepincrement  = new StepIncrement;

        $now = Carbon::now();

        // $deduction = $deduction
        // ->where('code','PF')
        // ->first();

        $year = $now->year;
        $month = $now->month;

        $no_of_days = $this->countDays($year,$month,array(0,6));

        $query['salaryinfo'] = $salaryinfo
        ->where('employee_id',$employee_id)
        ->orderBy('salary_effectivity_date','desc')
        ->first();

        // $query['deductioninfo'] = $deductioninfo
        // ->with('deductions')
        // ->where('deduction_id',$deduction->id)
        // ->where('employee_id',$employee_id)
        // ->first();

        $start_date = $query['salaryinfo']->salary_effectivity_date;

        $end_date = date('Y-m-t',strtotime($start_date));

        $query['days_from_step'] = $this->getWorkingDays($start_date,$end_date);

        $query['employeeinfo'] = $employeeinfo
        ->where('employee_id',$employee_id)
        ->first();

        $query['days_in_month'] = $no_of_days;

        $query['stepincrement'] = $stepincrement
        ->where('employee_id',$employee_id)
        ->get();

        return json_encode($query);

    }

    public function deleteStepIncrement(){
        $data = Input::all();

        $date           = $data['date'];
        $id             = $data['id'];
        $employee_id    = $data['employee_id'];

        $stepincrement =  new StepIncrement;

        $stepincrement->destroy($id);

        $query['stepincrement'] = $stepincrement
        ->where('employee_id',$employee_id)
        ->get();

        return $query;

    }
}
