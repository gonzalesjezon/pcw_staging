<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Employee;
use Input;
class RecomputationTaxReportsController extends Controller
{
    function __construct(){
    	$this->title = 'RECOMPUTATION OF TAX';
    	$this->module = 'recomputationtax';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year   = $q['year'];
        $month  = $q['month'];

        $transaction    = new Transaction;

        $query = $transaction
        ->with([
            'employees' => function($qry){
                $qry->orderBy('lastname','asc');
                },
            'loaninfoTransaction',
            'deductioninfoTransaction',
            'positionitems',
            'positions',
            'divisions',
            'offices',
            'employeeinformation',
            'salaryinfo',
            'employeeinfo'
        ])
        ->where('year',$year)
        ->get();

        $data = [];
        foreach ($query as $key => $value) {
            if(isset($value->division_id)){
                $data[$value->divisions->Name][$key] = $value;
            }else{
                $data = [];
            }
        }

        return json_encode([
            'transaction'   => $data,
            'print_date'     => date('Y-m-d')
        ]);
    }
}
