<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Rata;
use App\RataAdjustment;
use App\Employee;
class RataReportsController extends Controller
{
    function __construct(){
    	$this->title = 'REPRESENTATION AND TRANSPORTATION ALLOWANCE';
    	$this->module = 'ratapayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new Rata;


        $query = $transaction->join('pms_payroll_information as emp','pms_rata.employee_id','=','emp.employee_id')
        ->with([
            'employees',
            'positions',
            'rataadjustment' => function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                        ->where('month',$month);
            },
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('emp.monthly_rate_amount','desc')
        ->get();

        $query2 = $transaction->join('pms_payroll_information as emp','pms_rata.employee_id','=','emp.employee_id')
        ->with([
            'employees',
            'positions',
            'rataadjustment' => function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                        ->where('month',$month);
            },
        ])
        ->where('is_oic',1)
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('emp.monthly_rate_amount','desc')
        ->get();

        return json_encode([
            'transaction'   => $query,
            'transaction2'  => $query2,
            'print_date'    => date('Y-m-d')
        ]);

    }
}
