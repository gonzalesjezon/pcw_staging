<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\NonPlantillaEmployeeInfo;
use App\NonPlantillaTransaction;
use Carbon\Carbon;
use App\AttendanceInfo;
use App\AnnualTaxRate;
use Session;

class NonPlantillaController extends Controller
{

    function __construct(){
        $this->title = 'NON PLANTILLA';
        $this->module = 'nonplantilla';
        $this->module_prefix = 'payrolls';
        $this->controller = $this;

    }

    public function index(){

        $benefit        = Benefit::orderBy('name','asc')->get();
        $loans          = Loan::orderBy('name','asc')->get();
        $deduction      = Deduction::orderBy('name','asc')->get();
        $empstatus      = EmployeeStatus::where('category',0)->get();


        $response = array(
                        'empstatus'     => $empstatus,
                        'benefit'       => $benefit,
                        'loans'         => $loans,
                        'deductions'    => $deduction,
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'months'             => config('params.months'),
                        'latest_year'        => $this->latestYear(),
                        'earliest_year'      => $this->earliestYear(),
                        'current_month'      => (int)date('m')
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function showBenefitinfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitdatatable',$response);
    }

    public function showLoaninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.loandatatable',$response);
    }

     public function showDeductioninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.deductdatatable',$response);
    }

    public function show(){
        $q                  = Input::get('q');
        $year               = Input::get('year');
        $month              = Input::get('month');
        $_year              = Input::get('_year');
        $_month             = Input::get('_month');
        $_pay_period        = Input::get('_pay_period');
        $pay_period         = Input::get('pay_period');
        $checkpayroll       = Input::get('checkpayroll');
        $check_payroll      = Input::get('check_payroll');
        $employee_status    = Input::get('employee_status');
        $empstatus          = Input::get('empstatus');

        $data = $this->searchName($q,$check_payroll,$empstatus = 2,$_year,$_month,$_pay_period);

        if(isset($year) || isset($month) || isset($checkpayroll) || isset($employee_status) || isset($pay_period)){
            $data = $this->filter($year,$month,$checkpayroll,$employee_status,$pay_period);
        }
        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function searchName($q,$checkpayroll,$empstatus,$year,$month,$pay_period){

        $employeestatus              = new EmployeeStatus;
        $employee_information        = new EmployeeInformation;
        $employee                    = new Employee;
        $nonplantilla_employeeinfo   = new NonPlantillaEmployeeInfo;
        $transaction                 = new NonPlantillaTransaction;

        $cols         = ['lastname','firstname','middlename'];

        $empstatus_id = $employeestatus
        ->where('category',0)
        ->where('RefId',$empstatus)
        ->first();

        $employee_info_id = $employee_information
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

        $transaction = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->where('pay_period',$pay_period)
        ->select('employee_id')
        ->whereIn('employee_id',$employee_info_id)
        ->get()
        ->toArray();

        $query = [];
        switch ($checkpayroll) {
            case 'wpayroll':

               $query = $employee->whereIn('id',$transaction);

                break;

            case 'wopayroll':

                $query = $employee
                ->whereNotIn('id',$transaction)
                ->whereIn('id',$employee_info_id);

                break;

            default:
                // $employee_id = $transaction->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_info_id);

                break;
        }


        $query = $query->where(function($qry) use($q, $cols){
            foreach ($cols as $key => $value) {
                $qry->orWhere($value,'like','%'.$q.'%');
            }
        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;
    }

   public function filter($year,$month,$checkpayroll,$employee_status,$pay_period){


        $employeestatus         = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $nonplantilla           = new NonPlantillaTransaction;
        $employee               = new Employee;
        $nonplantilla_employee_info  = new NonPlantillaEmployeeInfo;

        // --GET EMPLOYEE STATUS ID EX(JOB ORDER)
        $empstatus_id = $employeestatus
        ->where('category',0)
        ->where('RefId',$employee_status)
        ->select('RefId')
        ->get()
        ->toArray();

        $employee_id  = $employee_information->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();

        $response = "";

         $query = $nonplantilla
         ->select('employee_id')
         ->whereIn('employee_status_id',$empstatus_id)
         ->where('year',$year)
         ->where('month',$month)
         ->where('pay_period',$pay_period)
         ->select('employee_id')
         ->get()->toArray();

        switch ($checkpayroll) {
            case 'wpayroll':
                    $response = $employee
                    ->where('active',1)
                    ->whereIn('id',$query)
                    ->orderBy('lastname','asc')
                    ->get();
                break;

            case 'wopayroll':

                    $response = $employee
                    ->whereIn('id',$employee_id)
                    ->whereNotIn('id',$query)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }


    public function getEmployeesinfo(){

        $data = Input::all();

        $year           = @$data['year'];
        $month          = @$data['month'];
        $pay_period     = @$data['pay_period'];
        $employee_id    = @$data['id'];

        $transaction  = new NonPlantillaTransaction;
        $employeeinfo = new NonPlantillaEmployeeInfo;

        $query['transaction'] = $transaction
            ->with('employees')
            ->where('year',$year)
            ->where('month',$month)
            ->where('employee_id',$employee_id)
            ->where('pay_period',$pay_period)
            ->first();


        $query['employeeinfo'] = $employeeinfo
            ->with('tax_policy_ewt','tax_policy_pwt')
            ->where('employee_id',$employee_id)
            ->first();


        return json_encode($query);
    }

    public function processPayroll() {
        $data = Input::all();

        $year            = $data['year'];
        $month           = $data['month'];
        $pay_period      = @$data['summary']['pay_period'];

        $transactions =  new NonPlantillaTransaction;

        if(isset($data['transaction_id'])){

            $actual_workdays  = ($data['attendance']['actual_workdays']) ? $data['attendance']['actual_workdays'] : 0;

            $this->computeTransaction($data['employee_id'],$year,$month,$pay_period,$data,$actual_workdays);
            $response = json_encode(['status'=>true,'response'=>'Payroll Updated Successfully!']);

        }else{

            $ctr = 0;
            $error = 0;
            $error_id = [];
            foreach ($data['empid'] as $key => $_id) {

                if(isset($_id)){
                    $check = NonPlantillaEmployeeInfo::where('employee_id',$_id)->whereNotNull('daily_rate_amount')->first();
                    if($check === null){
                        $error++;
                        $error_id[] = $_id;
                    }
                }
            }


            if($error == 0){
                foreach ($data['empid'] as $key => $_id) {
                    if(isset($_id)){

                        $this->computeTransaction($_id,$year,$month,$pay_period,$data,11);
                        $ctr++;
                    }
                }
                $response = json_encode(['status'=>true,'response'=>'Processed Successfully! <br>'.$ctr.' Records Saved']);
            }else{
                $error_id = implode(',', $error_id);
                $response = json_encode(['status'=>false,'response'=>'Save Failed! <br>'.$error.' Records <br>No Employee Setup <br>Employee ID <br>['.$error_id.']']);
            }
        }

        return $response;
    }

    public function computeTransaction($employee_id,$year,$month,$pay_period,$data,$workdays){

        $employeeinfo = EmployeeInformation::where('employee_id',$employee_id)->first();
        $payrollinfo  = NonPlantillaEmployeeInfo::where('employee_id',$employee_id)->first();

        $taxPolicyId         = $payrollinfo->tax_policy_id;
        $ewtPolicyId         = $payrollinfo->ewt_policy_id;
        $pwtPolicyId         = $payrollinfo->pwt_policy_id;
        $dailyRate           = $payrollinfo->daily_rate_amount;
        $monthlyRate         = $dailyRate * $workdays;
        $annualRate          = $monthlyRate * 12;

        $ewtRate = TaxPolicy::where('id',$ewtPolicyId)->select('job_grade_rate')->first();
        $pwtRate = TaxPolicy::where('id',$pwtPolicyId)->select('job_grade_rate')->first();

        $taxAmountOne        = 0;
        $taxAmountTwo        = 0;
        $threshHoldBalance   = 0;
        $adjustmentAdd       = 0;
        $adjustmentLess      = 0;
        $aomAmount           = 0;
        $grossPay            = 0;

        $transaction = NonPlantillaTransaction::find(@$data['transaction_id']);
        if(empty($transaction)){

            $transaction = new NonPlantillaTransaction;

            switch ($taxPolicyId) {
                case '1': // Inputted policy
                    $taxAmountOne = $payrollinfo->inputted_amount;
                    break;
                case '2': // Standard Policy
                    $taxAmountOne = $monthlyRate * $ewtRate->job_grade_rate;
                    $taxAmountTwo = $monthlyRate * $pwtRate->job_grade_rate;
                    break;
                case '3': // Sworn declaration policy

                    $prevThreshold = NonPlantillaTransaction::where('employee_id',$employee_id)
                    ->where('year',$year)
                    ->where('month',$month)
                    ->where('pay_period',$pay_period)
                    ->select('threshhold_balance','actual_workdays')
                    ->orderBy('created_at','desc')
                    ->first();

                    if(isset($prevThreshold)){
                        $threshHoldBalance = $prevThreshold->threshhold_balance - $monthlyRate;
                    }else{

                        $threshHoldAmount = $payrollinfo->threshhold_amount;

                        if($threshHoldAmount > 0){
                            $threshHoldBalance = $threshHoldAmount - $monthlyRate;

                        }
                        else{
                            $threshHoldBalance = 250000;
                            $threshHoldBalance = $threshHoldBalance - $monthlyRate;
                        }
                    }

                    //* tax amount is less than 0 then (difference * 5%) = tax amount
                    if($threshHoldBalance <= 0){
                        // $taxAmountOne = abs($threshHoldBalance) * 0.05;
                        $taxAmountOne = abs($threshHoldBalance) * 0.05;
                        $threshHoldBalance = 0;
                    }

                    break;
            }

            $transaction->employee_id          = $employee_id;
            $transaction->employeeinfo_id      = $payrollinfo->id;
            $transaction->responsibility_id    = $payrollinfo->responsibility_id;
            $transaction->division_id          = $employeeinfo->division_id;
            $transaction->company_id           = $employeeinfo->company_id;
            $transaction->position_item_id     = $employeeinfo->position_item_id;
            $transaction->office_id            = $employeeinfo->office_id;
            $transaction->department_id        = $employeeinfo->department_id;
            $transaction->employee_status_id   = $employeeinfo->employee_status_id;
            $transaction->position_id          = $employeeinfo->position_id;
            $transaction->employee_number      = @$employeeinfo->employee_number;
            $grossPay = $monthlyRate;
            

        }else{

            $dailyRate = $monthlyRate / $workdays;

            $adjustmentAdd   = ($data['summary']['adjustment_add']) ? $data['summary']['adjustment_add'] : 0;
            $adjusmentLess   = ($data['summary']['adjustment_less']) ? $data['summary']['adjustment_less'] : 0;
            $aomAmount       = ($data['summary']['aom_amount']) ? $data['summary']['aom_amount'] : 0;
            $actualOvertime  = ($data['attendance']['actual_overtime']) ? $data['attendance']['actual_overtime'] : 0;
            $actualTardiness = ($data['attendance']['actual_tardines']) ? $data['attendance']['actual_tardines'] : 0;
            $actualUndertime = ($data['attendance']['actual_undertime']) ? $data['attendance']['actual_undertime'] : 0;
            $totalOvertimeAmount = ($data['summary']['total_overtime_amount']) ? str_replace(',', '', $data['summary']['total_overtime_amount']) : 0;
            $totalUndertime = ($data['summary']['total_undertime']) ? str_replace(',', '', $data['summary']['total_undertime']) : 0;
            $totalTardiness = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['total_tardines']) : 0;
            $monthlyRate    = ($data['summary']['total_basicpay']) ? str_replace(',', '', $data['summary']['total_basicpay']) : 0;

            $transaction->adjustment_add        = $adjustmentAdd;
            $transaction->adjustment_less       = $adjustmentLess;
            $transaction->aom_amount            = $aomAmount;
            $transaction->actual_overtime       = $actualOvertime;
            $transaction->total_overtime_amount = $totalOvertimeAmount;
            $transaction->actual_tardiness      = $actualTardiness;
            $transaction->actual_undertime      = $actualUndertime;
            $transaction->total_undertime_amount = $totalUndertime;
            $transaction->total_tardiness_amount = $totalTardiness;

            $taxAmountOne       = $transaction->tax_rate_amount_one;
            $taxAmountTwo       = $transaction->tax_rate_amount_two;
            $threshHoldBalance  = $transaction->threshhold_balance;
            $grossPay           = $monthlyRate + $totalOvertimeAmount - $totalTardiness - $totalUndertime;

            // if($taxPolicyId == '3'){

            //     if($threshHoldBalance > 0){
            //         $threshHoldBalance = $threshHoldBalance - $grossPay;
            //     }else{

            //         $threshHoldAmount = $payrollinfo->threshhold_amount;

            //         if($threshHoldAmount > 0){
            //             $threshHoldBalance = $threshHoldAmount - $grossPay;
            //         }
            //         else{
            //             $threshHoldBalance = 250000;
            //             $threshHoldBalance = $threshHoldBalance - $grossPay;
            //         }

            //     }

            //     //* tax amount is less than 0 then (difference * 5%) = tax amount
            //     if($threshHoldBalance <= 0){
            //         // $taxAmountOne = abs($threshHoldBalance) * 0.05;
            //         $taxAmountOne = abs($threshHoldBalance) * 0.05;
            //         $threshHoldBalance = 0;
            //     }
            // }

            # Inputted tax 
            $taxAmountOne       = ($data['summary']['witholding_tax']) ? str_replace(',', '', $data['summary']['witholding_tax']) : 0;
            

        }

        $netPay = $grossPay + $adjustmentAdd - $adjustmentLess - $taxAmountOne - $taxAmountTwo - $aomAmount;

        $transaction->actual_workdays      = $workdays;
        $transaction->pay_period           = $pay_period;
        $transaction->monthly_rate_amount  = $monthlyRate;
        $transaction->gross_pay            = $grossPay;
        $transaction->annual_rate_amount   = $annualRate;
        $transaction->tax_rate_amount_one  = $taxAmountOne;
        $transaction->tax_rate_amount_two  = $taxAmountTwo;
        $transaction->threshhold_balance   = $threshHoldBalance;
        $transaction->net_pay              = $netPay;
        $transaction->year                 = $year;
        $transaction->month                = $month;
        $transaction->daily_rate           = $dailyRate;
        $transaction->save();

    }


    public function destroy(){
        $data = Input::all();

        $transactions = new NonPlantillaTransaction;

        $attendance = new AttendanceInfo;

        foreach ($data['empid'] as $key => $value) {

            $transactions->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

}
