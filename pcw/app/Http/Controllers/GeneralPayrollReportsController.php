<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\EmployeeInfo;
use App\AttendanceInfo;
use App\Office;
use App\Benefit;
use App\LoanInfoTransaction;
use App\Deduction;
use App\DeductionInfoTransaction;
use App\Loan;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\Employee;
class GeneralPayrollReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL';
    	$this->module = 'generalpayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'        => config('params.months'),
                       'latest_year'    => $this->latestYear(),
                       'earliest_year'  => $this->earliestYear(),
                       'current_month'  => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    				);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year   = $q['year'];
        $month  = $q['month'];
        $status = $q['status'];

        $transaction    = new Transaction;
        $office         = new Office;
        $benefit        = new Benefit;
        $loaninfo       = new LoanInfoTransaction;
        $loan           = new Loan;
        $deduction      = new Deduction;
        $deductioninfo  = new DeductionInfoTransaction;
        $employeestatus = new EmployeeStatus;
        $employeeinfo   = new EmployeeInformation;

        $benefit = $benefit->where('name','PERA')->first();

        $query2 = $employeestatus;
        switch ($status) {
            case 'project':
                $query2  = $query2
                ->where('Code','PCT');
                break;

            default:
                $query2  = $query2
                ->where('Code','P');
                break;
        }
        $query2 = $query2->first();

        $arrEmployeeList = $employeeinfo
        ->where('employee_status_id',$query2->RefId)
        ->pluck('employee_id')
        ->toArray();

        $deductioninfo = $deductioninfo
        ->with('deductions')
        ->whereIn('employee_id',$arrEmployeeList)
        ->where('year',$year)
        ->where('month',$month)
        ->where('not_include',0)
        ->selectRaw('*,deduction_id,sum(amount) as net_amount')
        ->groupBy('deduction_id')
        ->get();


        $loaninfo = $loaninfo
        ->with('loans')
        ->whereIn('employee_id',$arrEmployeeList)
        ->where('year',$year)
        ->where('month',$month)
        ->selectRaw('*,loan_id,sum(amount) as net_amount')
        ->groupBy('loan_id')
        ->get();

        $loanCount = count($loaninfo);

        $deductionCount = count($deductioninfo);

        $query = $transaction
        ->with([
            'employees',
            'loaninfoTransaction' => function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            },
            'deductioninfoTransaction' => function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            },
            'benefitTransactions' => function($qry) use($benefit,$year,$month){
                $qry = $qry->where('benefit_id',$benefit->id)
                ->where('year',$year)
                ->where('month',$month);
            },
            'benefitinfo' => function($qry) use($benefit){
                $qry = $qry->where('benefit_id',$benefit->id);
            },
            'positionitems',
            'positions',
            'divisions',
            'offices',
            'employeeinformation',
            'responsibility',
            'salaryinfo' => function($qry){
                $qry = $qry->with('salarygrade');
            },
            'employeeinfo'
        ])
        ->where('empstatus_id',$query2->RefId)
        ->where('year',$year)
        ->where('month',$month);

        if($status == 'project'){
            $query = $query->orderBy('salary_grade','desc');
        }else{
            $query = $query->orderBy('responsibility_id','asc')
                ->orderBy('actual_basicpay_amount','desc');
        }

        $query = $query->get();


        $data = [];
        foreach ($query as $key => $value) {
            if(isset($value->responsibility_id)){
                $data[$value->responsibility->code][$key] = $value;
            }else{
                $data = [];
            }
        }

        // $data2 = [];
        // foreach ($query as $key => $value) {
        //     if(isset($value->responsibility_id)){
        //         $data2[$value->responsibility->code][$key] = $value;
        //     }else{
        //         $data2 = [];
        //     }
        // }

        return json_encode([
            'transaction'   => $data,
            // 'transaction2'   => $data2,
            'loanlists'     => $loaninfo,
            'loancount'     => $loanCount,
            'deductionlists' => $deductioninfo,
            'deductioncount' => $deductionCount,
            'print_date'     => date('Y-m-d')
        ]);
    }
}
