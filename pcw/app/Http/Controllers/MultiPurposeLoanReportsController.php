<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LoanInfo;
use App\Loan;
use App\Transaction;
use App\EmployeeInfo;
use Input;
class MultiPurposeLoanReportsController extends Controller
{
    function __construct(){
		$this->title = 'MULTI PURPOSE LOAN REPORT';
    	$this->module = 'multipurposeloans';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data = Input::all();
    	$transaction = new Transaction;
        $loan =        new Loan;
        $loaninfo    = new LoanInfo;

        $loan_id = $loan->where('code','MPL')->select('id')->first();

        $employee_id = $loaninfo->with('employees')->where('loan_id',$loan_id->id)->select('employee_id')->get()->toArray();

        $transaction_employee_id = $transaction->select('employee_id')
                                              ->where('year',$data['year'])
                                              ->where('month',$data['month'])
                                              ->get()->toArray();

        $query = $loaninfo->with(['employees'=>function($qry){ $qry->orderBy('lastname','asc'); }])
                            ->where('loan_id',$loan_id->id)
                            ->whereIn('employee_id',$transaction_employee_id)
                            ->get();


    	return json_encode($query);
    }
}
