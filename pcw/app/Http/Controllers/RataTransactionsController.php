<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\EmployeeInfo;
use App\BenefitInfo;
use App\Company;
use App\Division;
use App\Position;
use App\Department;
use App\Office;
use App\Rata;
use App\Leave;
use App\Transaction;
use Input;
use App\Deduction;
use App\RataAdjustment;
use Auth;

class RataTransactionsController extends Controller
{
    function __construct(){
        $this->title = 'RATA TRANSACTIONS';
        $this->controller = $this;
        $this->module = 'ratatransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){

        $deductions = new Deduction;
        $deductions = $deductions->where('payroll_group','rata')->get();


        $response = array(
           'deductions'     => $deductions,
           'title'          => $this->title,
           'controller'     => $this->controller,
           'module'         => $this->module,
           'module_prefix'  => $this->module_prefix,
           'months'             => config('params.months'),
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'current_month'      => (int)date('m')

       );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function storeRata(Request $request){

        $check = Leave::where('employee_id',$request['employee_id'])
                        ->where('leave_date',$request['leave']['inclusive_leave_date'])
                        ->first();

        if(!isset($check)){

            $rata = new Rata;

            $representation_amount = ($request['rata']['representation_amount'] * $request['rata']['percentage_of_rata_value']);
            $transportation_amount = ($request['rata']['transportation_amount'] * $request['rata']['percentage_of_rata_value']);


            $rata = Rata::find($request['rata_id']);

            $rata->number_of_actual_work = $request['rata']['no_of_actual_work'];
            $rata->number_of_work_days = $request['rata']['no_of_work_days'];
            $rata->number_of_used_vehicles = $request['rata']['no_of_used_vehicles'];
            $rata->percentage_of_rata = $request['rata']['percentage_of_rata'];
            $rata->percentage_of_rata_value = $request['rata']['percentage_of_rata_value'];
            $rata->year = $request['rata']['year'];
            $rata->month = $request['rata']['month'];

            $rata->representation_amount = $representation_amount;
            $rata->transportation_amount = $transportation_amount;

            $rata->save();

            if($request['leave']){
                $leave = new Leave;

                $leave->rata_id = $request['rata_id'];
                $leave->employee_id = $request['employee_id'];
                $leave->leave_type = $request['leave']['leave_type'];
                $leave->leave_date = $request['leave']['inclusive_leave_date'];
                $leave->number_of_leave_field = $request['leave']['no_of_leave_field'];

                $leave->save();
            }

            $query['rata'] = Rata::where('employee_id',$request['employee_id'])
                        ->where('year',$request['rata']['year'])
                        ->where('month',$request['rata']['month'])
                        ->first();

            $query['leave'] = Leave::where('rata_id',$request['rata_id'])
                                ->where('employee_id',$request['employee_id'])
                                ->select('leave_type','leave_date','number_of_leave_field')
                                ->get();

            $leave_list = [];
            foreach ($query['leave'] as $key => $value) {
                $leave_list[$value->leave_type][$key] = date('d',strtotime($value->leave_date));

            }

            $query['leave_list'] = $leave_list;

            $response = json_encode(['status'=>true,'response'=>'Save Successfully!','rata'=>$query]);
        }else{
            $response = json_encode(['status'=>false,'response'=>'Date already taken!']);
        }

        return $response;

    }

    public function show(){

        $q = Input::get('q');
        $check_rata = Input::get('check_rata');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');
        $year = Input::get('year');
        $month = Input::get('month');

        $data = "";

        $data = $this->searchName($q,$check_rata,$year,$month);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$check_rata,$year,$month){

        $cols = ['lastname','firstname'];

        $benefit     = new Benefit;
        $benefitinfo = new BenefitInfo;
        $rata        = new Rata;
        $employee    = new Employee;
        $employeeinfo = new EmployeeInfo;

        $oic_employee = $employeeinfo
        ->where('check_oic',1)
        ->select('employee_id')
        ->get()->toArray();

        $benefit_id = $benefit
        ->whereIn('code',['RA','TA'])
        ->select('id')
        ->get()->toArray();

        $employee_id = $benefitinfo
        ->whereIn('benefit_id',$benefit_id)
        ->select('employee_id')
        ->groupBy('employee_id')
        ->get()->toArray();

        $employee_array = array_merge($employee_id,$oic_employee);

        $rata_employee_id = $rata
        ->whereIn('employee_id',$employee_array)
        ->where('year',$year)
        ->where('month',$month)
        ->select('employee_id')
        ->get()->toArray();

        $arr_employee_id = array();

        switch ($check_rata) {
            case 'wrata':
                $query = $employee->whereIn('id',$rata_employee_id);
                break;

            default:
                $query = $employee->whereNotIn('id',$rata_employee_id)
                                ->whereIn('id',$employee_array);
                break;

        }



        $query = $query->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });

        $response = $query->where('active','!=',0)->orderBy('lastname','asc')->get();
        return $response;
    }

    public function showRataDatatable(){

        $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.ratadatatable',$response);
    }

    public function getRataInfo(){

        $data = Input::all();

        $transaction =  new Rata;
        $adjustment  =  new RataAdjustment;

        $query['rata'] = $transaction
        ->where('employee_id',@$data['id'])
        ->where('year',$data['year'])
        ->where('month',$data['month'])
        ->first();

        $query['adjustment'] = $adjustment
        ->where('employee_id',@$data['id'])
        ->where('year',$data['year'])
        ->where('month',$data['month'])
        ->first();


        return json_encode($query);

    }

    public function processRata(){

        $data = Input::all();
        $year = $data['year'];
        $month = $data['month'];

        $benefit             = new Benefit;
        $employee            = new Employee;
        $rata                = new Rata;
        $benefitInfo         = new BenefitInfo;
        $employeeinformation = new EmployeeInformation;
        $payrollinfo         = new EmployeeInfo;
        $transaction         = new Transaction;

        $benefit = $benefit
        ->whereIn('code',['RA','TA'])
        ->select('id')
        ->get()->toArray();

        if(isset($data['rata_id'])){

            $rata = $rata->find($data['rata_id']);

            $benefitInfo = $benefitInfo
            ->whereIn('benefit_id',$benefit)
            ->where('employee_id',$data['employee_id'])
            ->select('benefit_amount','benefit_id')
            ->get()->toArray();

            $employeeinfo = $employeeinformation
            ->where('employee_id',$data['employee_id'])
            ->select('office_id','department_id')
            ->first();

            $checkoic = $transaction
            ->where('year',$year)
            ->where('month',$month)
            ->where('hold',1)
            ->first();

            $is_oic = 0;
            if($checkoic){
                $is_oic = 1;
            }

            $payrollinfo = $payrollinfo
            ->where('employee_id',$data['employee_id'])
            ->first();

            $representation_amount = (isset($benefitInfo[0]['benefit_amount'])) ? $benefitInfo[0]['benefit_amount'] : NULL;
            $transportation_amount = (isset($benefitInfo[1]['benefit_amount'])) ? $benefitInfo[1]['benefit_amount'] : NULL;

            $rata->office_id = (isset($employeeinfo->office_id)) ? $employeeinfo->office_id : NULL;

            // $rata->position_item_id      = (isset($employeeinfo->position_item_id)) ? $employeeinfo->position_item_id : NULL;
            $rata->department_id         = @$employeeinfo->department_id;
            $rata->employee_id           = $data['employee_id'];
            $rata->representation_amount = $representation_amount;
            $rata->transportation_amount = $transportation_amount;
            $rata->number_of_work_days   = $payrollinfo->no_ofdays_inamonth;
            $rata->number_of_actual_work = $payrollinfo->no_ofdays_inamonth;
            $rata->percentage_of_rata_value = 1;
            $rata->year                  = $year;
            $rata->month                 = $month;
            $rata->is_oic                = $is_oic;
            $rata->save();
            $response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
        }else{

            foreach ($data['list_id'] as $key => $value) {
                if($value !== null){

                    $benefitInfo = new BenefitInfo;

                    $benefitInfo = $benefitInfo
                     ->whereIn('benefit_id',$benefit)
                     ->where('employee_id',$value)
                     ->select('benefit_amount','benefit_id')
                     ->get()->toArray();

                     $employeeinfo = $employeeinformation->where('employee_id',$value)->first();
                     $employee  = $employee->where('id',$value)->first();

                     $payrollinfo = $payrollinfo->where('employee_id',$value)->first();

                     $rata = new Rata;

                     $representation_amount = @$benefitInfo[0]['benefit_amount'];
                     $transportation_amount = @$benefitInfo[1]['benefit_amount'];
                     $rata->office_id             = @$employeeinfo->office_id;
                     $rata->department_id         = @$employeeinfo->department_id;
                     $rata->position_id           = @$employeeinfo->position_id;
                     $rata->employee_number       = @$employee->employee_number;
                     $rata->is_oic                = @$payrollinfo->check_oic;
                     $rata->employee_id           = $value;
                     $rata->representation_amount = $representation_amount;
                     $rata->transportation_amount = $transportation_amount;
                     $rata->number_of_work_days   = $payrollinfo->no_ofdays_inamonth;
                     $rata->number_of_actual_work = $payrollinfo->no_ofdays_inamonth;
                     $rata->percentage_of_rata_value = 1;
                     $rata->year                  = $year;
                     $rata->month                 = $month;

                     $rata->save();
                }
            }
            $response = json_encode(['status'=>true,'response'=>'Process Successfully!']);
        }


       return $response;
    }

    public function deleteRata(){
        $data = Input::all();
        $leave = new Leave;

        foreach ($data['empid'] as $key => $value) {

            $transaction = new Rata;
            $adjustment  = new RataAdjustment;

            if(isset($value)){
                $transaction
                ->where('employee_id',$value)
                ->where('month',$data['month'])
                ->where('year',$data['year'])
                ->delete();

                $adjustment
                ->where('employee_id',$value)
                ->where('month',$data['month'])
                ->where('year',$data['year'])
                ->delete();
            }
        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
   }

   public function store(Request $request){

        $ta_deduction = (isset($request->ta_deduction)) ? str_replace(',','', $request->ta_deduction) : NULL;
        $ra_deduction = (isset($request->ra_deduction)) ? str_replace(',','', $request->ra_deduction) : NULL;
        $refund_amount = (isset($request->refund_amount)) ? str_replace(',','', $request->refund_amount) : NULL;
        $number_of_used_vehicles = (isset($request->number_of_used_vehicles)) ? str_replace(',','', $request->number_of_used_vehicles) : NULL;

        // ADD TO PROCESSED RATA
        $adjustment = RataAdjustment::find($request->rata_id);
        $message = 'Update Successfully.';
        if(empty($adjustment)){
            $adjustment = new RataAdjustment;
            // $this->validate($request,[
            //     'ta_deduction' => 'required',
            // ]);
            $message = 'Save Successfully.';
        }

        $adjustment->fill($request->all());
        $adjustment->used_vehicle_amount      = $number_of_used_vehicles;
        $adjustment->ta_additional_amount     = $ta_deduction;
        $adjustment->ra_additional_amount     = $ra_deduction;
        $adjustment->refund_amount            = $refund_amount;

        if($message == 'Update Successfully.'){
            $adjustment->updated_by = Auth::id();
        }else{
            $adjustment->created_by = Auth::id();
        }
        $adjustment->save();

        return json_encode(['status'=>true,'response'=>$message]);
   }

}
