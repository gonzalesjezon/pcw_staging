<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\EmployeeInfo;
use App\Transaction;
use Carbon\Carbon;
use App\AttendanceInfo;
use App\BenefitInfoTransaction;
use App\DeductionInfoTransaction;
use App\LoanInfoTransaction;
use App\AnnualTaxRate;
use App\PostedReport;
use Session;
use Auth;

class TransactionsController extends Controller
{

    function __construct(){
    	$this->title = 'REGULAR PAYROLL';
    	$this->module = 'transactions';
        $this->module_prefix = 'payrolls';
    	$this->controller = $this;

    }

    public function index(){

        $benefit        = Benefit::where('remarks','allowance')->orderBy('name','asc')->get();
        $loans          = Loan::orderBy('name','asc')->get();
        $deduction      = Deduction::where('remarks','bill')->orderBy('name','asc')->get();


    	$response = array(
                        'benefit'       => $benefit,
                        'loans'         => $loans,
                        'deductions'     => $deduction,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q              = Input::get('q');
        $year           = Input::get('year');
        $month          = Input::get('month');
        $_year           = Input::get('_year');
        $_month          = Input::get('_month');
        $subperiod      = Input::get('subperiod');
        $period         = Input::get('period');
        $checkpayroll   = Input::get('checkpayroll');
        $check_payroll  = Input::get('check_payroll');

        $data = $this->searchName($q,$check_payroll,$_year,$_month);

        if(isset($year) || isset($month) || isset($checkpayroll)){
            $data = $this->filter($year,$month,$checkpayroll);
        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }
    public function searchName($q,$checkpayroll,$year,$month){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $salaryinfo                 = new SalaryInfo;
        $transaction                = new Transaction;

        $cols = ['lastname','firstname'];

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();

        $employee_info_id = $employee_info->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();

        // $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();
        $employee_id = $transaction
        ->whereIn('employee_id',$employee_info_id)
        ->where('year',$year)
        ->where('month',$month)
        ->select('employee_id')
        ->get()->toArray();

        $query = [];
        switch ($checkpayroll) {
            case 'wpayroll':
               $query = $employee->whereIn('id',$employee_id);

                break;

            default:
                $query = $employee->whereNotIn('id',$employee_id);
                break;
        }

      $query = $query->whereIn('id',$employee_info_id)->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($year,$month,$checkpayroll){

        $employeestatus         = new EmployeeStatus;
        $employeeinfo           = new EmployeeInformation;
        $transaction            = new Transaction;
        $employee               = new Employee;
        $salaryinfo             = new SalaryInfo;

        $empstatus_id = $employeestatus->where('category',1)->select('RefId')->get()->toArray();
        $employee_id  = $employeeinfo->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();
        $salaryinfo = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkpayroll) {
            case 'wpayroll':

                $query =  $transaction->select('employee_id');

                    if(count($empstatus_id) > 0){
                        $query = $query->whereIn('empstatus_id',$empstatus_id);
                    }

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)
                                        ->where('active',1)
                                        ->orderBy('lastname','asc')->get();


                break;

            case 'wopayroll':

                 $query =  $transaction->select('employee_id');

                    if(count($empstatus_id) > 0){
                        $query = $query->whereIn('empstatus_id',$empstatus_id);
                    }

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employee_id)
                                        ->whereIn('id',$salaryinfo)
                                        ->whereNotIn('id',$query)
                                        ->where('active',1)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }

    public function showBenefitinfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitdatatable',$response);
    }

    public function showLoaninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.loandatatable',$response);
    }

     public function showDeductioninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.deductdatatable',$response);
    }



     public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){

        $data = Input::all();

        $employee_id = @$data['id'];
        $employee_number = @$data['employee_number'];

        $transaction    = new Transaction;
        $employeeinfo   = new EmployeeInfo;
        $salaryinfo     = new SalaryInfo;
        $benefitinfo    = new BenefitInfoTransaction;
        $deductioninfo  = new DeductionInfoTransaction;
        $loaninfo       = new LoanInfoTransaction;
        $benefit        = new Benefit;

        $query['transaction'] = $transaction->with('employees')
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->first();

        $query['employeeinfo'] = $employeeinfo
                            ->where('employee_id',@$employee_id)
                            // ->where('employee_number',@$employee_number)
                            ->first();

        $effectivity_date = Carbon::now()->toDateTimeString();
        $query['salaryinfo'] = $salaryinfo
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->where('salary_effectivity_date','<=',$effectivity_date)
                            ->first();

        $query['benefitinfo'] = $benefitinfo->with('benefits','benefitinfo')
                                ->where('employee_id',@$employee_id)
                                // ->where('employee_number',@$employee_number)
                                ->where('year',@$data['year'])
                                ->where('month',@$data['month'])
                                ->get();

        $query['deductioninfo'] = $deductioninfo->with('deductions','deductioninfo')
                                ->where('employee_id',@$employee_id)
                                // ->where('employee_number',@$employee_number)
                                ->where('year',@$data['year'])
                                ->where('month',@$data['month'])
                                ->get();

        $query['loaninfo'] = $loaninfo
                            ->with('loans','loaninfo')
                            // ->where('employee_number',@$employee_number)
                            ->where('employee_id',@$employee_id)
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            ->get();

        $year  = $data['year'];
        $month = $data['month'];

        $y = date('Y', strtotime($year));
        $m = date('m', strtotime($month));

        $workdays = $this->countDays($y,$m,array(0,6));

        $query['workdays'] = $workdays;

        return json_encode($query);
    }

    public function updatePayroll(){
        $data = Input::all();

        $actual_basicpay_amount     = ($data['summary']['actual_basicpay']) ? str_replace(',', '', $data['summary']['actual_basicpay']) : 0;
        $adjust_basicpay_amount     = ($data['summary']['adjust_basicpay']) ? str_replace(',', '', $data['summary']['adjust_basicpay']) : 0;
        $total_basicpay_amount      = ($data['summary']['total_basicpay']) ? str_replace(',', '', $data['summary']['total_basicpay']) : 0;

        $actual_absences_amount     = ($data['summary']['actual_absences']) ? str_replace(',', '', $data['summary']['actual_absences']) : 0;
        $adjust_absences_amount     = ($data['summary']['adjust_absences']) ? str_replace(',', '', $data['summary']['adjust_absences']) : 0;
        $total_absences_amount      = ($data['summary']['total_absences']) ? str_replace(',', '', $data['summary']['total_absences']) : 0;

        $actual_tardines_amount     = ($data['summary']['actual_tardines']) ? str_replace(',', '', $data['summary']['actual_tardines']) : 0;
        $adjust_tardines_amount     = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['adjust_tardines']) : 0;
        $total_tardines_amount      = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['total_tardines']) : 0;

        $actual_undertime_amount     = ($data['summary']['actual_undertime']) ? str_replace(',', '', $data['summary']['actual_undertime']) : 0;
        $adjust_undertime_amount     = ($data['summary']['adjust_undertime']) ? str_replace(',', '', $data['summary']['adjust_undertime']) : 0;
        $total_undertime_amount      = ($data['summary']['total_undertime']) ? str_replace(',', '', $data['summary']['total_undertime']) : 0;

        $actual_contribution     = ($data['summary']['actual_contribution']) ? str_replace(',', '', $data['summary']['actual_contribution']) : 0;
        $adjust_contribution     = ($data['summary']['adjust_contribution']) ? str_replace(',', '', $data['summary']['adjust_contribution']) : 0;
        $total_contribution      = ($data['summary']['total_contribution']) ? str_replace(',', '', $data['summary']['total_contribution']) : 0;

        $actual_loan     = ($data['summary']['actual_loan']) ? str_replace(',', '', $data['summary']['actual_loan']) : 0;
        $adjust_loan     = ($data['summary']['adjust_loan']) ? str_replace(',', '', $data['summary']['adjust_loan']) : 0;
        $total_loan      = ($data['summary']['total_loan']) ? str_replace(',', '', $data['summary']['total_loan']) : 0;

        $actual_otherdeduct     = ($data['summary']['actual_otherdeduct']) ? str_replace(',', '', $data['summary']['actual_otherdeduct']) : 0;
        $adjust_otherdeduct     = ($data['summary']['adjust_otherdeduct']) ? str_replace(',', '', $data['summary']['adjust_otherdeduct']) : 0;
        $total_otherdeduct      = ($data['summary']['total_otherdeduct']) ? str_replace(',', '', $data['summary']['total_otherdeduct']) : 0;

        $basic_net_pay      = ($data['summary']['basic_net_pay']) ? str_replace(',', '', $data['summary']['basic_net_pay']) : 0;
        $net_deduction      = ($data['summary']['net_deduction']) ? str_replace(',', '', $data['summary']['net_deduction']) : 0;
        $gross_pay      = ($data['summary']['gross_pay']) ? str_replace(',', '', $data['summary']['gross_pay']) : 0;
        $gross_taxable_pay      = ($data['summary']['gross_taxable_pay'])? str_replace(',', '', $data['summary']['gross_taxable_pay']) : 0;
        $salary_diff_amount      = ($data['summary']['salary_diff_amount'])? str_replace(',', '', $data['summary']['salary_diff_amount']) : 0;
        $net_pay      = ($data['summary']['net_pay']) ? str_replace(',', '', $data['summary']['net_pay']) : 0;

        $adjust_workdays = $data['attendance']['adjust_workdays'];
        $actual_absences = $data['attendance']['actual_absences'];
        $adjust_absences = $data['attendance']['adjust_absences'];
        $actual_tardiness = $data['attendance']['actual_tardines'];
        $adjust_tardiness = $data['attendance']['adjust_tardines'];
        $actual_undertime = $data['attendance']['actual_undertime'];
        $adjust_undertime = $data['attendance']['adjust_undertime'];

        if(isset($actual_absences)){
            $benefitinfo = new BenefitInfoTransaction;
            $benefitinfo = $benefitinfo->find($data['pera_id']);

            $old_pera_amount = $benefitinfo->amount;
            $daily_pera_amount = $old_pera_amount / 22;
            $deduct_pera_amount = $daily_pera_amount * $actual_absences;
            $new_pera_amont = $old_pera_amount - $deduct_pera_amount;
            $benefitinfo->amount = $new_pera_amont;
            $benefitinfo->save();
        }


        $transactions = Transaction::find($data['transaction_id']);

        $transactions->actual_basicpay_amount     = $actual_basicpay_amount;
        $transactions->adjust_basicpay_amount     = $adjust_basicpay_amount;
        $transactions->total_basicpay_amount      = $total_basicpay_amount;
        $transactions->actual_absences_amount     = $actual_absences_amount;
        $transactions->adjust_absences_amount     = $adjust_absences_amount;
        $transactions->total_absences_amount      = $total_absences_amount;
        $transactions->actual_tardines_amount     = $actual_tardines_amount;
        $transactions->adjust_tardines_amount     = $adjust_tardines_amount;
        $transactions->total_tardines_amount      = $total_tardines_amount;
        $transactions->actual_undertime_amount    = $actual_undertime_amount;
        $transactions->adjust_undertime_amount    = $adjust_undertime_amount;
        $transactions->total_undertime_amount     = $total_undertime_amount;
        $transactions->actual_absences_amount     = $actual_absences_amount;
        $transactions->adjust_workdays            = $adjust_workdays;
        $transactions->actual_absences            = $actual_absences;
        $transactions->adjust_absences            = $adjust_absences;
        $transactions->actual_tardiness           = $actual_tardiness;
        $transactions->adjust_tardiness           = $adjust_tardiness;
        $transactions->actual_undertime           = $actual_undertime;
        $transactions->adjust_undertime           = $adjust_undertime;
        $transactions->basic_net_pay              = $basic_net_pay;
        $transactions->actual_contribution        = $actual_contribution;
        $transactions->adjust_contribution        = $adjust_contribution;
        $transactions->total_contribution         = $total_contribution;
        $transactions->actual_loan                = $actual_loan;
        $transactions->adjust_loan                = $adjust_loan;
        $transactions->total_loan                 = $total_loan;
        $transactions->actual_otherdeduct         = $actual_otherdeduct;
        $transactions->adjust_otherdeduct         = $adjust_otherdeduct;
        $transactions->total_otherdeduct          = $total_otherdeduct;
        $transactions->net_deduction              = $net_deduction;
        $transactions->gross_pay                  = $gross_pay;
        $transactions->gross_taxable_pay          = $gross_taxable_pay;
        $transactions->salary_diff_amount         = $salary_diff_amount;
        $transactions->net_pay                    = $net_pay;

        $transactions->save();


        $response = json_encode(['status'=>true,'response'=>'Payroll Updated Successfully!']);

        return $response;

    }

    public function processPayroll() {
        // getting values from form (like $record_num)
        $data = Input::all();
        // Session::put('progress', 0);
        // Session::save(); // Remember to call save()

        // for ($i = 1; $i < $record_num; $i++) {
            // $record = new Record();

            // // adding attributes...

            // $record->save();
        //     Session::put('progress', $i);
        //     Session::save(); // Remember to call save()
        // }

        // $response = Response::make();
        // $response->header('Content-Type', 'application/json');
        // return $response;

        $transactions =  new Transaction;
        $ctr = 0;
        $error = 0;
        $error_id = [];
        foreach ($data['empid'] as $key => $_id) {

            if(isset($_id)){
                $check = SalaryInfo::where('employee_id',$_id)->first();
                if($check === null){
                    $error++;
                    $error_id[$key] = $_id;
                }
            }
        }

        $year  = $data['year'];
        $month = $data['month'];

        $y = date('Y', strtotime($year));
        $m = date('m', strtotime($month));

        if($m > 1 && $m != 12){
            $m = $m -1;
        }

        $workdays = $this->countDays($y,(int)$m,array(0,6));
        $effectivity_date    = Carbon::now()->toDateTimeString();

        if($error == 0){
            foreach ($data['empid'] as $key => $_id) {
                if(isset($_id)){
                    $employeeinfo        = new EmployeeInfo;
                    $employeeinformation = new EmployeeInformation;
                    $salaryinfo          = new SalaryInfo;
                    $loaninfo            = new LoanInfo;
                    $taxrates            = new AnnualTaxRate;

                    $employeeinfo = $employeeinfo
                    ->with('taxpolicy')
                    ->where('employee_id',$_id)
                    ->first();

                    $employeeinformation = $employeeinformation
                    ->where('employee_id',$_id)
                    ->first();

                    $salaryinfo = $salaryinfo
                        ->where('salary_effectivity_date','<=',$effectivity_date)
                        ->where('employee_id',$_id)
                        ->orderBy('created_at','desc')
                        ->first();

                    $employee_number = @$employeeinfo->employee_number;

                    $taxAmount = 0;
                    if($employeeinfo->taxpolicy->policy_name == 'ANNUAL TAX POLICY'){

                        $taxrates = $taxrates
                        ->where('employee_id',$_id)
                        ->where('for_year',$year)
                        ->where('for_month',$month)
                        ->select('tax_amount')
                        ->first();

                        $taxAmount = @$taxrates->tax_amount;

                    }else{

                        $taxAmount = @$employeeinfo->tax_contribution;

                    }

                    $basic_amount = @$salaryinfo->salary_new_rate;
                    $salary_grade = str_replace('SG', '', @$salaryinfo->salarygrade->Code);

                    $totalContribution = (@$employeeinfo->pagibig_contribution + @$employeeinfo->philhealth_contribution + @$employeeinfo->gsis_contribution + @$employeeinfo->pagibig2 + @$employeeinfo->pagibig_personal);

                    $totalLoan = $this->storeLoaninfoTransaction($_id,$year,$month,$employee_number);

                    $totalDeduction = $this->storeDeduction($_id,$year,$month,$employee_number);

                    $netDeduction = $totalContribution + $totalLoan + $totalDeduction;

                    $pera_amount = $this->storePera($_id,$year,$month,$employee_number);

                    $gross_taxable = $basic_amount - $totalContribution;

                    if(isset($amount)){
                        $allowances = (@$pera_amount) ? @$pera_amount: 0;
                    }else{
                        $allowances = 0;
                    }

                    $net_pay = ($gross_taxable + $allowances ) - (@$taxAmount);

                    $gross_pay = ($basic_amount + @$allowances);


                    $transactions->employee_id        = $_id;
                    $transactions->salaryinfo_id      = $salaryinfo->id;
                    $transactions->employeeinfo_id    = $employeeinfo->id;
                    $transactions->employee_number    = @$employeeinfo->employee_number;
                    $transactions->responsibility_id  = @$employeeinfo->responsibility_id;
                    $transactions->salary_grade       = $salary_grade;
                    $transactions->gsis_ee_share      = @$employeeinfo->gsis_contribution;
                    $transactions->pagibig_share      = @$employeeinfo->pagibig_contribution;
                    $transactions->phic_share         = @$employeeinfo->philhealth_contribution;
                    $transactions->mp1_amount         = @$employeeinfo->pagibig_personal;
                    $transactions->mp2_amount         = @$employeeinfo->pagibig2;
                    $transactions->gsis_er_share      = $employeeinfo->er_gsis_share;
                    $transactions->position_id        = @$employeeinformation->position_id;
                    $transactions->division_id        = @$employeeinformation->division_id;
                    $transactions->company_id         = @$employeeinformation->company_id;
                    $transactions->position_item_id   = @$employeeinformation->position_item_id;
                    $transactions->office_id          = @$employeeinformation->office_id;
                    $transactions->department_id      = @$employeeinformation->department_id;
                    $transactions->empstatus_id       = @$employeeinformation->employee_status_id;
                    $transactions->actual_workdays    = $workdays;
                    $transactions->tax_amount         = @$taxAmount;
                    $transactions->total_contribution = $totalContribution;
                    $transactions->ecc_amount         = 100;
                    $transactions->total_loan         = (float)$totalLoan;
                    $transactions->total_otherdeduct  = (float)$totalDeduction;
                    $transactions->net_deduction      = $netDeduction;
                    $transactions->basic_net_pay      = (float)$basic_amount;
                    $transactions->actual_basicpay_amount = (float)$basic_amount;
                    $transactions->net_pay            = $net_pay;
                    $transactions->gross_taxable_pay  = $gross_taxable;
                    $transactions->gross_pay          = $gross_pay;
                    $transactions->year               = $year;
                    $transactions->month              = $month;


                    Transaction::create($transactions->toArray());

                    $ctr++;
                }

            }
            $response = json_encode(['status'=>true,'response'=>'Processed Successfully! <br>'.$ctr.' Records Saved']);
        }else{
            $error_id = implode(',', $error_id);
            $response = json_encode(['status'=>false,'response'=>'Save Failed! <br>'.$error.' Records <br>No Employee Setup <br>Employee ID <br>['.$error_id.']']);
        }


        return $response;
    }

    public function storeLoaninfoTransaction($employee_id,$year,$month,$employee_number){
        $loaninfo =  new LoanInfo;

        $query = $loaninfo
        ->where('employee_id',$employee_id)
        ->whereNull('loan_date_terminated')
        ->whereNull('loan_date_end')
        ->get();

        $amount = 0;

        foreach ($query as $key => $value) {

            $transaction =  new LoanInfoTransaction;

            $transaction->employee_id        = $employee_id;
            $transaction->employee_number    = $employee_number;
            $transaction->loan_info_id       = $value->id;
            $transaction->loan_id            = $value->loan_id;
            $transaction->employee_number    = $employee_number;
            $transaction->amount             = $value->loan_amortization;
            $transaction->year               = $year;
            $transaction->month              = $month;
            $transaction->created_by         = Auth::User()->id;
            $transaction->save();

            $amount += (float)$value->loan_amortization;
        }


        return $amount;

    }

    public function storePera($employee_id,$year,$month,$employee_number){

        $benefitInfo =  new BenefitInfo;
        $transaction =  new BenefitInfoTransaction;

        $query = $benefitInfo::where('employee_number',$employee_number)
                            ->with(['benefits' => function($qry){
                                $qry->where('name','PERA');
                            }])->first();
        $pera_amount = 0;
        if(isset($query)){
            $transaction->employee_id        = $employee_id;
            $transaction->employee_number    = $employee_number;
            $transaction->benefit_info_id    = @$query->id;
            $transaction->benefit_id         = $query->benefit_id;
            $transaction->amount             = $query->benefit_amount;
            $transaction->status             = 'pera';
            $transaction->year               = $year;
            $transaction->month              = $month;
            $transaction->created_by         = Auth::User()->id;
            $transaction->save();

            $pera_amount = $query->benefit_amount;
        }

        return $pera_amount;

    }

    public function storeDeduction($employee_id,$year,$month,$employee_number){

        $deductionInfo  =  new DeductionInfo;

        $query = $deductionInfo
        ->with('deductions')
        ->where('employee_id',$employee_id)
        ->where('employee_number',$employee_number)
        ->get();

        $amount = 0;
        foreach ($query as $key => $value) {
            $transaction    =  new DeductionInfoTransaction;

            if($value->deductions->remarks == 'bill'){
                $transaction->not_include = 1;
            }else{
                $transaction->not_include = 0;
            }

            if($value->deductions->code == 'CU'){
                $transaction->not_include = 0;
            }

            $transaction->employee_id        = $employee_id;
            $transaction->deduction_info_id  = $value->id;
            $transaction->employee_number    = $employee_number;
            $transaction->deduction_id       = $value->deduction_id;
            $transaction->amount             = $value->deduct_amount;
            $transaction->status             = 'deductions';
            $transaction->year               = $year;
            $transaction->month              = $month;
            $transaction->created_by         = Auth::User()->id;
            $transaction->save();

            $amount += (float)$value->deduct_amount;
        }

        return $amount;

    }

    public function deletePayroll(){
        $data = Input::all();

        $transactions   = new Transaction;
        $benefitinfo    = new BenefitInfoTransaction;
        $deductioninfo  = new DeductionInfoTransaction;
        $loaninfo       = new LoanInfoTransaction;

        $posted = PostedReport::where('year',$data['year'])
        ->where('month',$data['month'])
        ->first();

        if(isset($posted)){
            $response = json_encode(['status'=>false,'response'=>'Transaction cannot be deleted cause its already posted.' ]);
        }else{

            foreach ($data['empid'] as $key => $value) {

                $transactions->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();
                $deductioninfo->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();
                $benefitinfo->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();

                $loaninfo->where('employee_id',$data['empid'][$key])
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();
            }
            $response = json_encode(['status'=>true,'response'=>'Delete Successfully']);
        }

        return $response;
    }

    public function storeBenefitInfoTransaction(Request $request){

        $allowance_amount = ($request->bt_amount) ? str_replace(',', '', $request->bt_amount) :  0;

        $transaction = new BenefitInfoTransaction;

        if(isset($request->benefit_transaction_id)){

            $transaction                 = $transaction->find($request->benefit_transaction_id);
            $transaction->benefit_id     = $request->benefit_id;
            $transaction->transaction_id = $request->transaction_id;
            $transaction->amount         = $allowance_amount;
            $transaction->year           = $request->year;
            $transaction->month          = $request->month;
            $transaction->updated_by     = Auth::User()->id;


            $transaction->save();

             $transaction['benefitinfo'] = $transaction
            ->with('benefits','benefitinfo')
            ->where('employee_id',$request->employee_id)
            ->where('year',$request->year)
            ->where('month',$request->month)
            ->get();

            $response[] = json_encode(['status'=>true,'response'=>'Update Successfully','transaction'=>$transaction]);

        }else{

            $this->validate($request,[
                'bt_amount'     => 'required',
                'benefit_id' => 'required'
            ]);

            $transaction->benefit_id     = $request->benefit_id;
            $transaction->transaction_id = $request->transaction_id;
            $transaction->employee_id    = $request->employee_id;
            $transaction->amount         = $allowance_amount;
            $transaction->status         = 'allowance';
            $transaction->year           = $request->year;
            $transaction->month          = $request->month;
            $transaction->created_by     = Auth::User()->id;

            $transaction->save();

            $transaction['benefitinfo'] = $transaction
            ->with('benefits','benefitinfo')
            ->where('employee_id',$request->employee_id)
            ->where('year',$request->year)
            ->where('month',$request->month)
            ->get();

            $response = json_encode(['status'=>true,'response'=>'Save Successfully','transaction'=>$transaction]);

        }

        return $response;
    }

    public function storeDeductionInfoTransaction(Request $request){

        $this->validate($request,[
            'deduction_id'      => 'required',
            'deduction_amount'  => 'required'
        ]);

        $deduction_amount = ($request->deduction_amount) ? str_replace(',', '', $request->deduction_amount) :  0;

        $deduction = Deduction::where('id',$request->deduction_id)->first();

        $transaction = DeductionInfoTransaction::find($request->id);
        $message = ' Update Successfully.';
        if(empty($transaction)){
            $transaction = new DeductionInfoTransaction;
            $this->validate($request,[
                'deduction_amount'     => 'required',
                'deduction_id'          => 'required'
            ]);
            $message = ' Save Successfully.';
        }
        $transaction->fill($request->all());
        $transaction->amount         = $deduction_amount;
        $transaction->not_include    = 1;
        if(in_array($deduction->code, ['EA','CU'])){
            $transaction->not_include    = 0;
        }

        if(in_array($deduction->code, ['PHICADJ','PAGIBIGADJ','TAXADJ','GSISADJ'])){
            $transaction->not_include    = 2;
        }

        if($message == 'Update Successfully.'){
            $transaction->updated_by = Auth::id();
        }else{
            $transaction->created_by = Auth::id();
        }

        $transaction->save();

         $transaction['deductioninfo'] = $transaction
        ->with('deductions','deductioninfo')
        ->where('employee_id',$request->employee_id)
        ->where('year',$request->year)
        ->where('month',$request->month)
        ->get();

        return json_encode(['status'=>true,'response'=>$message,'transaction'=>$transaction]);
    }

    public function deleteLoan(){
        $data = Input::all();

        $transaction = new LoanInfoTransaction;

        $query = $transaction
        ->where('id',$data['id'])
        ->delete();

        $data2 = $transaction
                ->with('loans','loaninfo')
                ->where('employee_id',@$data['employee_id'])
                ->where('year',@$data['year'])
                ->where('month',@$data['month'])
                ->get();

        return json_encode(['status'=>'loans', 'data'=> $data2]);
    }

    public function deleteDeduction(){
        $data = Input::all();

        $transaction = new DeductionInfoTransaction;

        $query['deductioninfo'] = $transaction
        ->where('id',$data['id'])
        ->delete();

        $data2 = $transaction
                ->with('deductions','deductioninfo')
                ->where('employee_id',@$data['employee_id'])
                ->where('year',@$data['year'])
                ->where('month',@$data['month'])
                ->get();

        return json_encode(['status'=>'deductions', 'data'=> $data2]);
    }

    public function deleteBenefit(){
        $data = Input::all();

        $transaction = new BenefitInfoTransaction;

        $query = $transaction
        ->where('id',$data['id'])
        ->delete();

        $data2 = $transaction
                ->with('benefits','benefitinfo')
                ->where('employee_id',@$data['employee_id'])
                ->where('year',@$data['year'])
                ->where('month',@$data['month'])
                ->get();

        return json_encode(['status'=>'benefits', 'data'=> $data2]);
    }

}
