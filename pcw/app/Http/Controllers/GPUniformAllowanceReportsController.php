<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpecialPayrollTransaction;
use App\EmployeeStatus;
use App\Employee;
use Input;
class GPUniformAllowanceReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL UNIFORM ALLOWANCE';
    	$this->module = 'gpuniformallowance';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'        => config('params.months'),
                       'latest_year'    => $this->latestYear(),
                       'earliest_year'  => $this->earliestYear(),
                       'current_month'  => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $status = $q['status'];
        $year   = $q['year'];
        $month  = $q['month'];

        $transaction    = new SpecialPayrollTransaction;
        $employeestatus = new EmployeeStatus;

        $query2 = $employeestatus;
        switch ($status) {
            case 'project':
                $query2  = $query2
                ->where('Code','PCT');
                break;

            default:
                $query2  = $query2
                ->where('Code','P');
                break;
        }
        $query2 = $query2->first();

        $query = $transaction
        ->with([
            'employees' => function($qry){
                $qry->orderBy('lastname','asc');
            },
            'positions',
            'divisions',
            'employeeinfo'
        ])
        ->where('status','ua')
        ->where('employee_status_id',$query2->RefId)
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data = [];
        foreach ($query as $key => $value) {
            if(isset($value->division_id)){
                $data[$value->divisions->Name][$key] = $value;
            }else{
                $data = [];
            }
        }

        $data2 = [];
        foreach ($data as $key => $value) {
            $data2[$key] = array_values($value);
        }


        $sameOffice = '';
        $data3 = [];
        $ctr = 1;
        $ctr2 = 1;
        foreach ($data2 as $key => $value) {
            foreach ($value as $k => $val) {
                if($ctr <= 20){
                    $data3[$key][$ctr2][$ctr] = $val;
                }else{
                    $ctr = 0;
                    $ctr2++;
                }
                $ctr++;
            }
            $ctr  = 1;
            $ctr2 = 1;
        }


        return json_encode([
            'transaction'   => $data3,
            'print_date'    => date('Y-m-d')
        ]);
    }
}
