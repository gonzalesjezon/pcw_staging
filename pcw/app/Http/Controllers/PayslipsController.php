<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaction;
use App\Office;
use App\Benefit;
use App\LoanInfoTransaction;
use App\Loan;
use App\Deduction;
use App\DeductionInfoTransaction;
use App\Employee;
use App\PostedReport;
use Input;
use Auth;

class PayslipsController extends Controller
{
    function __construct(){
    	$this->title = 'PAYSLIP';
    	$this->module = 'payslips';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employee = new Employee;

        $employee = $employee->orderBy('lastname','asc')->get();
        $posted = PostedReport::where('report_type','payslip')->latest()->get();

    	$response = array(
                        'employee'          => $employee,
    					'module'            => $this->module,
    					'controller'        => $this->controller,
                        'module_prefix'     => $this->module_prefix,
    					'title'		        => $this->title,
                        'months'            => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'posted'             => $posted
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year        = @$q['year'];
        $month       = @$q['month'];
        $employee_id = @$q['employee_id'];

        $transaction = new Transaction;
        $office      = new Office;
        $benefit     = new Benefit;
        $loaninfo    = new LoanInfoTransaction;
        $loan        = new Loan;
        $deduction   = new Deduction;
        $deductioninfo = new DeductionInfoTransaction;

        $benefit = $benefit->where('name','PERA')->first();

        $gsisloans = $loan
        ->where('loan_type','GSIS')
        ->select('id')
        ->get()
        ->toArray();

        $pagibigloans = $loan
        ->where('loan_type','PAGIBIG')
        ->select('id')
        ->get()
        ->toArray();


        $query = $transaction
        ->with([
            'employees' => function($qry){
                $qry->orderBy('lastname','asc');
                },
            'gsisloans' => function($qry) use($gsisloans,$year,$month){
                $qry = $qry->whereIn('loan_id',$gsisloans)
                ->where('year',$year)
                ->where('month',$month);
            },
            'pagibigloans' => function($qry) use($pagibigloans,$year,$month){
                $qry = $qry->whereIn('loan_id',$pagibigloans)
                ->where('year',$year)
                ->where('month',$month);
            },
            'deductioninfoTransaction',
            'benefitTransactions' => function($qry) use($benefit){
                $qry = $qry->where('benefit_id',$benefit->id);
            },
            'benefitinfo' => function($qry) use($benefit){
                $qry = $qry->where('benefit_id',$benefit->id);
            },
            'positionitems',
            'positions',
            'divisions',
            'offices',
            'employeeinformation',
            'salaryinfo',
            'employeeinfo'
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->where('employee_id',$employee_id)
        ->first();


        return json_encode([
            'transaction'   => $query,
            'print_date'    => date('Y-m-d')
        ]);
    }

    /*
    * Post Payslip
    */
    public function store(Request $request){

        $this->validate($request,[
            'employee_id' => 'required'
        ]);

        $transaction = Transaction::where('year',$request->year)
        ->where('month',$request->month)
        ->get();

        if(count($transaction) > 0){

            foreach ($transaction as $key => $value) {

                $posted = PostedReport::where('year',$request->year)
                ->where('employee_id',$value->employee_id)
                ->where('month',$request->month)
                ->where('report_type','payslip')
                ->first();

                if(!$posted){
                    $posted = new PostedReport;
                    $posted->fill($request->all());
                    $posted->employee_id    = $value->employee_id;
                    $posted->report_type    = 'payslip';
                    $posted->created_by     = Auth::id();

                    if($posted->save()){
                        $transaction = Transaction::where('year',$request->year)
                        ->where('month',$request->month)
                        ->where('employee_id',$value->employee_id)
                        ->update(['posted' => 1]);
                    }
                }
            }

            $response = json_encode(['status'=>true,'response'=> 'Report Posted Successfully.']);
        }else{
            $response = json_encode(['status'=>false,'response'=> 'No Transaction Found.']);
        }

        return $response;
    }

}
