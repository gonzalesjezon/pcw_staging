<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OtherPayrollsController extends Controller
{
    function __construct(){
    	$this->title = 'OTHER PAYROLL';
    	$this->controller = $this;
    	$this->module = 'otherpayrolls';
        $this->module_prefix = 'payrolls';
    }

    public function index(){

    	$response = array(
    					'controller' => $this->controller,
    					'module'	 => $this->module,
    					'title'		 => $this->title, 
    				);
    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }
}
