<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\PreviousEmployer;
class PreviousEmployerController extends Controller
{
    function __construct(){
    	$this->title = 'EMPLOYEE PREVIOUS EMPLOYER';
    	$this->module = 'previousemployer';
        $this->module_prefix = 'payrolls/admin';
    	$this->controller = $this;
    }

    public function index(){

    	$employee = Employee::orderBy('LastName')->get();


    	$response = array(
    						'employee'			=> $employee,
	    					'module'        	=> $this->module,
	    					'controller'   	 	=> $this->controller,
	                        'module_prefix' 	=> $this->module_prefix,
	    					'title'		    	=> $this->title,
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

        public function show(){

        $q = Input::get('q');

        $data = $this->searchName($q);

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname'];

        $id = PreviousEmployer::select('employee_id')->get()->toArray();

        $query = Employee::whereIn('id',$id)
        			->where(function($query) use($cols,$q){

                        $query = $query->where(function($qry) use($q, $cols){
                            foreach ($cols as $key => $value) {
                                $qry->orWhere($value,'like','%'.$q.'%');
                            }
                        });
                    });


        $response = $query->orderBy('lastname','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	$this->validate(request(),[
    		'employee_id' 				 => 'required',
    		'allowances' 				 => 'required',
    		'thirteen_month_pay' 		 => 'required',
    		'deminimis_amount' 			 => 'required',
    		'tax_witheld' 				 => 'required',
    		'premium_amount' 			 => 'required',
    		'taxable_basic_pay' 		 => 'required',
    		'taxable_allowances' 		 => 'required',
    		'taxable_thirteen_month_pay' => 'required',
    		'as_of_date' 				 => 'required',
    	]);

    	$previousemployer = new PreviousEmployer;

  		if(isset($request->previousemployer_id)){
  			$previousemployer = PreviousEmployer::find($request->previousemployer_id);
			$previousemployer->employee_id 					= $request->employee_id;
			$previousemployer->allowances 					= str_replace(',', '', $request->allowances);
			$previousemployer->thirteen_month_pay 			= str_replace(',', '', $request->thirteen_month_pay);
			$previousemployer->deminimis_amount 			= str_replace(',', '', $request->deminimis_amount);
			$previousemployer->tax_witheld 					= str_replace(',', '', $request->tax_witheld);
			$previousemployer->premium_amount 				= str_replace(',', '', $request->premium_amount);
			$previousemployer->taxable_basic_pay 			= str_replace(',', '', $request->taxable_basic_pay);
			$previousemployer->taxable_thirteen_month_pay 	= str_replace(',', '', $request->taxable_thirteen_month_pay);
			$previousemployer->taxable_allowances 			= str_replace(',', '', $request->taxable_allowances);
			$previousemployer->as_of_date 					= $request->as_of_date;

			$previousemployer->save();

			$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);

  		}else{

  			$previousemployer->employee_id 					= $request->employee_id;
			$previousemployer->allowances 					= str_replace(',', '', $request->allowances);
			$previousemployer->thirteen_month_pay 			= str_replace(',', '', $request->thirteen_month_pay);
			$previousemployer->deminimis_amount 			= str_replace(',', '', $request->deminimis_amount);
			$previousemployer->tax_witheld 					= str_replace(',', '', $request->tax_witheld);
			$previousemployer->premium_amount 				= str_replace(',', '', $request->premium_amount);
			$previousemployer->taxable_basic_pay 			= str_replace(',', '', $request->taxable_basic_pay);
			$previousemployer->taxable_thirteen_month_pay 	= str_replace(',', '', $request->taxable_thirteen_month_pay);
			$previousemployer->taxable_allowances 			= str_replace(',', '', $request->taxable_allowances);
			$previousemployer->as_of_date 					= $request->as_of_date;

			$previousemployer->save();

			$response = json_encode(['status'=>true,'response'=>'Saving Successfully!']);
  			
  		}

  		return $response;

    }

    public function getPreviousEmployer(){
    	$id = Input::all();

    	$query = PreviousEmployer::where('employee_id',$id)->first();

    	return json_encode($query);
    }

}
