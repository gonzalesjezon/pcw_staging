<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\EmployeeInfo;
use App\AttendanceInfo;
class RegularTransmitalReportsController extends Controller
{
   function __construct(){
    	$this->title = 'REGULAR TRANSMITAL';
    	$this->module = 'regulartransmital';
        $this->module_prefix = 'payrolls/reports/plantillareports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new Transaction;
    	$employeeinfo = new EmployeeInfo;
    	$attendanceinfo = new AttendanceInfo;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $query['transaction'] = $transaction->where('year',$year)
								        ->where('month',$month)
								        ->sum('net_pay');
       return json_encode($query);
    }
}
