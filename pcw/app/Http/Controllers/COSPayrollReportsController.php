<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NonPlantillaTransaction;
use App\Employee;
use Input;
class COSPayrollReportsController extends Controller
{
    function __construct(){
    	$this->title = 'COS PAYROLL REPORT';
    	$this->module = 'cospayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year       = $q['year'];
        $month      = $q['month'];
        $pay_period = $q['pay_period'];

        $transaction = new NonPlantillaTransaction;

        $query = $transaction
        ->with([
            'employees' => function($qry){
                $qry = $qry->orderBy('lastname','asc');
            },
            'positions',
            'employeeinfo',
            'overtimepay',
            'responsibility'
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->where('pay_period',$pay_period)
        ->get();

        $data = [];
        foreach ($query as $key => $value) {
            if(isset($value->responsibility_id)){
                $data[@$value->responsibility->name][$key] = $value;
            }
        }

        $printDate = date('Y-m-d');

        return json_encode([
            'transaction'   => $data,
            'print_date'    => $printDate
        ]);
    }
}
