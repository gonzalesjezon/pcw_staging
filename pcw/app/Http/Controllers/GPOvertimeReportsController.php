<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\OvertimePay;
use App\Transaction;
use Input;
class GPOvertimeReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL OVERTIME';
    	$this->module = 'gpovertime';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new Transaction;
        $overtimepay = new OvertimePay;

        $query2 = $overtimepay
        ->where('year',$year)
        ->select('employee_id')
        ->get()->toArray();

        $query = $transaction
        ->with([
            'employees' => function($qry){
                $qry->orderBy('lastname','asc');
            },
            'positions',
            'divisions',
            'employeeinfo',
            'overtimepay'
        ])
        ->where('year',$year)
        ->whereIn('employee_id',$query2)
        ->groupBy('employee_id')
        ->get();

        $data = [];
        foreach ($query as $key => $value) {
            if(isset($value->division_id)){
                $data[@$value->divisions->Name][$key] = $value;
            }else{
                $data = [];
            }
        }

        $data2 = [];
        foreach ($data as $key => $value) {
            $data2[$key] = array_values($value);
        }


        $sameOffice = '';
        $data3 = [];
        $ctr = 1;
        $ctr2 = 1;
        foreach ($data2 as $key => $value) {
            foreach ($value as $k => $val) {
                if($ctr <= 20){
                    $data3[$key][$ctr2][$ctr] = $val;
                }else{
                    $ctr = 0;
                    $ctr2++;
                }
                $ctr++;
            }
            $ctr  = 1;
            $ctr2 = 1;
        }


        return json_encode([
            'transaction'   => $data3,
            'print_date'    => date('Y-m-d')
        ]);
    }
}
