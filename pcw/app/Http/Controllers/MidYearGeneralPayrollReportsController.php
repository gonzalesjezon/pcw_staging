<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpecialPayrollTransaction;
use Input;
use App\EmployeeStatus;
use App\Employee;
class MidYearGeneralPayrollReportsController extends Controller
{
    function __construct(){
    	$this->title = 'MID YEAR PAYROLL';
    	$this->module = 'midyearpayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m'),
                       'employees'      => Employee::where('active',1)->orderBy('lastname','asc')->get()
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $status = $q['status'];
        $year   = $q['year'];
        $month   = $q['month'];

        $transaction    = new SpecialPayrollTransaction;
        $employeestatus = new EmployeeStatus;

        $query2 = $employeestatus;
        switch ($status) {
            case 'project':
                $query2  = $query2
                ->where('Code','PCT');
                break;

            default:
                $query2  = $query2
                ->where('Code','P');
                break;
        }
        $query2 = $query2->first();

        $query = $transaction
        ->with('salaryinfo','positions','employees','divisions','employeeinfo')
        ->where('employee_status_id',$query2->RefId)
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','midyearbonus');

        if($status == 'project'){
            $query = $query->orderBy('salary_grade','desc');
        }else{
            $query = $query->orderBy('responsibility_id','asc')
                ->orderBy('actual_basicpay_amount','desc');
        }

        $query = $query->get();


        $data = [];
        foreach ($query as $key => $value) {
            if(isset($value->responsibility_id)){
                $data[$value->responsibility->code][$key] = $value;
            }else{
                $data = [];
            }
        }


        return json_encode([
            'transaction'   => $data,
            'print_date'    => date('Y-m-d')
        ]);
    }
}
