<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\Employee;
use App\LoanInfo;
use App\Loan;
use Input;
class ServiceLoanReportsController extends Controller
{
    function __construct(){
    	$this->title = 'SERVICE LOAN';
    	$this->module = 'serviceloans';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();


        $query = Transaction::with(['employees','positionitems'=>function($qry){ 
									        	$qry->with('positions'); 
									        },'offices','employeeinformation','salaryinfo'=>function($qry){
									        	$qry->with('salarygrade');
									        }])
									        ->where('year',$q['year'])
									        ->where('month',$q['month']);

        $query = $query->with(['employeeinfo'=>function($qry){ $qry = $qry->with(['loaninfo' => function($qry){
                                $qry->with('loans:id,name')->get();
                            }]);
                        }])->orderBy('office_id','asc')->get();


        return json_encode(['transactions'=>collect($query)->reverse()->toArray(),'loans'=>Loan::orderBy('name','asc')->get()]);
    }
}
