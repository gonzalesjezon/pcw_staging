<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\EmployeeInfo;
use App\SalaryGrade;
use App\PositionItem;
use App\JobGrade;
use App\NonPlantillaEmployeeInfo;
use App\ResponsibilityCenter;
use Carbon\Carbon;
use Auth;

class EmployeePayrollInformationsController extends Controller
{

    function __construct(){
        $this->module_prefix = 'payrolls/admin';
        $this->module = 'employees_payroll_informations';
        $this->title = 'EMPLOYEE FILE';
        $this->controller  = $this;

    }

    public function index(){

        $gsis           = GsisPolicy::get();
        $pagibig        = PagibigPolicy::get();
        $philhealth     = PhilhealthPolicy::get();
        $bank           = Bank::get();
        $benefit        = Benefit::get();
        $taxperiod      = TaxTable::get();
        $wagerate       = WageRate::get();
        $loans          = Loan::get();
        $deductions     = Deduction::where('category',null)->get();
        $sg_data        = SalaryGrade::get();
        $positionitem   = PositionItem::orderBy('name','asc')->get();
        $position       = Position::orderBy('name','asc')->get();
        $jg_data        = JobGrade::get();
        $ewt_tax_policy = TaxPolicy::whereNotNull('job_grade_rate')->where('policy_name','like','%'.'EWT'.'%')->get();
        $pwt_tax_policy = TaxPolicy::whereNotNull('job_grade_rate')->where('policy_name','like','%'.'PWT'.'%')->get();
        $tax_policy             = TaxPolicy::whereNull('job_grade_rate')->get();
        $cos_responsibility     = ResponsibilityCenter::where('remarks','cos')->get();
        $perma_responsibility   = ResponsibilityCenter::whereIn('remarks',['permanent','project'])->get();

    	$response = array(
                        'ewt_tax_policy' => $ewt_tax_policy,
                        'pwt_tax_policy' => $pwt_tax_policy,
                        'jg_data'       => $jg_data,
                        'sg_data'       => $sg_data,
                        'positionitem'  => $positionitem,
                        'position'      => $position,
                        'deductions'    => $deductions,
                        'loans'         => $loans,
                        'wagerate'      => $wagerate,
                        'taxperiod'     => $taxperiod,
                        'cos_responsibility' => $cos_responsibility,
                        'perma_responsibility' => $perma_responsibility,
                        'bank'          => $bank,
                        'philhealth'    => $philhealth,
                        'pagibig'       => $pagibig,
                        'gsis'          => $gsis,
                        'benefit'       => $benefit,
                        'tax_policy'    => $tax_policy,
    					'title' 		=> $this->title,
    					'controller'	=> $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function showBenefitinfo(){

        $benefitinfo    = BenefitInfo::with('benefits')->get();

        $response       = array(
                            'benefitinfo'   => $benefitinfo,
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitinfo_datatable',$response);
    }

    public function showLoaninfo(){

        $loaninfo = LoanInfo::with('loans')->get();
        $response = array(
                        'loaninfo'   => $loaninfo,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );
        return view($this->module_prefix.'.'.$this->module.'.loaninfo_datatable',$response);
    }


     public function showSalaryinfo(){

        $salaryinfo   = SalaryInfo::with('salarygrade','jobgrade')->get();
        $response     = array(
                        'salaryinfo'   => $salaryinfo,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );
        return view($this->module_prefix.'.'.$this->module.'.salaryinfo_datatable',$response);
    }

     public function showDeductioninfo(){

        $deductioninfo   = DeductionInfo::with('deductions')->get();
        $response        = array(
                            'deductioninfo'   => $deductioninfo,
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );
        return view($this->module_prefix.'.'.$this->module.'.deductioninfo_datatable',$response);
    }


    public function store(Request $request){

        if(!isset($request->employee_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $pagibig_contribution       =   (isset($request->pagibig_contribution) ? str_replace(',', '', $request->pagibig_contribution) : 0.00);
            $philhealth_contribution    = (isset($request->philhealth_contribution) ? str_replace(',', '', $request->philhealth_contribution) : 0.00);
            $gsis_contribution          = (isset($request->gsis_contribution) ? str_replace(',', '', $request->gsis_contribution) : 0.00);
            $tax_contribution           = (isset($request->tax_contribution) ? str_replace(',', '', $request->tax_contribution) : 0.00);
            $pagibig2                   = (isset($request->pagibig2) ? str_replace(',', '', $request->pagibig2) : 0.00);
            $tax_bracket_amount         = (isset($request->tax_bracket_amount) ? str_replace(',', '', $request->tax_bracket_amount) : 0.00);
            $pagibig_personal           = (isset($request->pagibig_personal) ? str_replace(',', '', $request->pagibig_personal) : 0.00);

            $tax_inexcess               = (isset($request->tax_inexcess) ? str_replace(',', '', $request->tax_inexcess) : 0.00);
            $er_pagibig_share           = (isset($request->er_pagibig_share) ? str_replace(',', '', $request->er_pagibig_share) : 0.00);
            $er_gsis_share              = (isset($request->er_gsis_share) ? str_replace(',', '', $request->er_gsis_share) : 0.00);
            $er_philhealth_share        = (isset($request->er_philhealth_share) ? str_replace(',', '', $request->er_philhealth_share) : 0.00);
            $overtime_balance_amount    = (isset($request->overtime_balance_amount) ? str_replace(',', '', $request->overtime_balance_amount) : 0.00);
            $daily_rate_amount          = (isset($request->daily_rate_amount) ? str_replace(',', '', $request->daily_rate_amount) : 0);

            $monthly_rate_amount        = (isset($request->monthly_rate_amount) ? str_replace(',', '', $request->monthly_rate_amount) : 0);
            $annual_rate_amount         = (isset($request->annual_rate_amount) ? str_replace(',', '', $request->annual_rate_amount) : 0);
            $check_oic                  = (isset($request->check_oic)) ? 1 : 0;

            $this->validate($request,[
                'responsibility_id' => 'required',
            ]);

            $employeeInfo = EmployeeInfo::find($request->id);
            $message = 'Update Successfully.';
            if(empty($employeeInfo)){
                $employeeInfo = new EmployeeInfo;
                $message = 'Save Successfully.';
            }

            $employeeInfo->fill($request->all());

            $employeeInfo->account_number           = $request->atm_no;
            $employeeInfo->daily_rate_amount        = $daily_rate_amount;
            $employeeInfo->monthly_rate_amount      = $monthly_rate_amount;
            $employeeInfo->pagibig_contribution     = $pagibig_contribution;
            $employeeInfo->annual_rate_amount       = $annual_rate_amount;
            $employeeInfo->philhealth_contribution  = $philhealth_contribution;
            $employeeInfo->gsis_contribution        = $gsis_contribution;
            $employeeInfo->tax_contribution         = $tax_contribution;
            $employeeInfo->pagibig2                 = $pagibig2;
            $employeeInfo->tax_bracket_amount       = $tax_bracket_amount;
            $employeeInfo->tax_inexcess             = $tax_inexcess;
            $employeeInfo->pagibig_personal         = $pagibig_personal;
            $employeeInfo->er_pagibig_share         = $er_pagibig_share;
            $employeeInfo->er_gsis_share            = $er_gsis_share;
            $employeeInfo->er_philhealth_share      = $er_philhealth_share;
            $employeeInfo->overtime_balance_amount  = $overtime_balance_amount;
            $employeeInfo->check_oic                = $check_oic;

            if($message == 'Update Successfully.'){
                $employeeInfo->updated_by = Auth::id();
            }else{
                $employeeInfo->created_by = Auth::id();
            }

            $employeeInfo->save();

            $response = json_encode(['status' => true, 'response' => $message,'employeeload'=>true]);
        }

        return $response;


    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');
        $emp_status = Input::get('emp_status');

        $data = "";

        $data = $this->searchName($q,$emp_status);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$empstatus){
        $cols = ['lastname','firstname'];

        $employee       = new Employee;
        $employeeinfo   = new EmployeeInformation;
        $salaryinfo     = new SalaryInfo;
        $employeestatus      = new EmployeeStatus;

        $salaryinfo = $salaryinfo->select('employee_id')->whereNotNull('salarygrade_id')->get()->toArray();

        $empstatus_id = [];
        switch ($empstatus) {
            case 'nonplantilla':
                $empstatus_id = $employeestatus
                ->where('category',0)
                ->select('RefId')
                ->get()->toArray();

                break;

            default:
                $empstatus_id = $employeestatus
                ->where('category',1)
                ->select('RefId')
                ->get()->toArray();

                break;
        }

        $employeeinfo = $employeeinfo
        ->select('employee_id')
        ->whereIn('employee_status_id',$empstatus_id)
        ->get()->toArray();

        $query = $employee
                ->whereIn('id',$employeeinfo)
                ->where(function($query) use($cols,$q){

                        $query = $query->where(function($qry) use($q, $cols){
                            foreach ($cols as $key => $value) {
                                $qry->orWhere($value,'like','%'.$q.'%');
                            }
                        });
                    });


        $response = $query->orderBy('lastname','asc')->where('active',1)->get();

        return $response;

    }

    public function filter($empstatus,$category,$emp_type,$searchby){

        $employeestatus = new EmployeeStatus;
        $employeeinfo   = new EmployeeInformation;
        $employee       = new Employee;


        $empstatus_id = [];
        switch($empstatus){
            case 'plantilla':
                $empstatus_id = $employeestatus
                ->where('category',1)
                ->select('RefId')
                ->get()->toArray();

            break;

            case 'nonplantilla':
                $empstatus_id = $employeestatus
                ->where('category',0)
                ->select('RefId')
                ->get()->toArray();

            break;
        }

        $employeeinfo = $employeeinfo->select('employee_id')
                    ->where(function($qry) use($category,$searchby){
                        switch ($searchby) {
                            case 'company':
                                $qry =  $qry->where('company_id',$category);
                                break;
                            case 'position':
                                $qry =  $qry->where('position_id',$category);
                                break;
                            case 'division':
                                $qry =  $qry->where('division_id',$category);
                                break;
                            case 'office':
                                $qry =  $qry->where('office_id',$category);
                                break;
                            case 'department':
                                $qry =  $qry->where('department_id',$category);
                                break;
                        }

                    });
        if(count($empstatus_id) > 0){
            $employeeinfo = $employeeinfo->whereIn('employee_status_id',$empstatus_id);
        }

        $employeeinfo = $employeeinfo->get()->toArray();

        $query = $employee
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        return $query;

    }


    public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('Name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('Name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('Name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('Name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('Name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){

        $data = Input::all();

        $empstatus   = @$data['empstatus'];
        $employee_id = @$data['id'];

        $employeeinformation = new EmployeeInformation;
        $employeeinfo        = new EmployeeInfo;
        $loaninfo            = new LoanInfo;
        $deductioninfo       = new DeductionInfo;
        $benefitinfo         = new BenefitInfo;
        $salaryinfo          = new SalaryInfo;
        $nonplantilla        = new NonPlantillaEmployeeInfo;
        $taxtable            = new TaxTable;

        $data['employeeinfo'] = $employeeinformation
        ->with([
            'employees',
            'divisions',
            'positionitems',
            'employeestatus',
            'salaryinfo',
            'offices',
            'positions'
        ])
        ->where('employee_id',$employee_id)
        ->first();

        $effectivity_date = Carbon::now()->toDateString();

        switch ($empstatus) {
                case 'nonplantilla':
                        $data['nonplantilla'] = $nonplantilla
                        ->where('employee_id',$employee_id)
                        ->first();
                    break;

                default:

                    $data['pmsemployeeinfo'] = $employeeinfo
                    ->with('gsispolicy','pagibigpolicy','philhealthpolicy','taxpolicy','banks','wages','providentfunds','responsibility')
                    ->where('employee_id',$employee_id)
                    ->first();

                    if(isset($data['pmsemployeeinfo'])){

                        $data['loaninfo'] = $loaninfo
                        ->with('loans')
                        ->where('employee_id',$employee_id)
                        ->get();

                        $data['deductioninfo'] = $deductioninfo
                        ->with('deductions')
                        ->where('employee_id',$employee_id)
                        ->get();

                        $data['benefitinfo'] = $benefitinfo
                        ->with('benefits')
                        ->where('employee_id',$employee_id)
                        ->get();

                        $data['salaryinfo'] = $salaryinfo
                        ->with('salarygrade','jobgrade','appointment_status','positions')
                        ->where('employee_id',$employee_id)
                        ->where('salary_effectivity_date','<=',$effectivity_date)
                        ->orderBy('created_at','desc')
                        ->first();

                        $data['salarylist'] = $salaryinfo
                        ->with('salarygrade','jobgrade','appointment_status','positions')
                        ->where('employee_id',$employee_id)
                        ->get();

                    }
            break;

        }

        $taxtable = $taxtable
            ->get()->toArray();


            $data['cl'] = [
                // 'dailyCL'       => ['0-685 ' . '',
                //                     '686-1095 '  . $taxtable[0]['salary_bracket_level2'].'-'.'.20',
                //                     '1096-2191 '  . $taxtable[0]['salary_bracket_level3'].'-'.'.25',
                //                     '2192-5471'  . $taxtable[0]['salary_bracket_level4'].'-'.'.30',
                //                     '5479-21917,' . $taxtable[0]['salary_bracket_level5'].'-'.'.32',
                //                     '21918' . $taxtable[0]['salary_bracket_level6'].'-'.'.35'
                //                     ],
                // 'weeklyCL'      => ['0-4807'  . '',
                //                     '4808-7691'  . $taxtable[3]['salary_bracket_level2'].'-'.'.20',
                //                     '7692-15384' . $taxtable[3]['salary_bracket_level3'].'-'.'.25',
                //                     '15385-38461' . $taxtable[3]['salary_bracket_level4'].'-'.'.30',
                //                     '38462-153845' . $taxtable[3]['salary_bracket_level5'].'-'.'.32',
                //                     '153846' . $taxtable[3]['salary_bracket_level6'].'-'.'.35'
                //                     ],
                // 'semimonthlyCL' => ['0-10416' . '',
                //                     '10417-16666' . $taxtable[2]['salary_bracket_level2'].'-'.'.20',
                //                     '16667-33332' . $taxtable[2]['salary_bracket_level3'].'-'.'.25',
                //                     '33333-83334' . $taxtable[2]['salary_bracket_level4'].'-'.'.30',
                //                     '83333-333332' . $taxtable[2]['salary_bracket_level5'].'-'.'.32',
                //                     '333333' .$taxtable[2]['salary_bracket_level6'].'-'.'.35'
                //                 ],
                'monthlyCL'     => [
                                    '0'.'-'.'0',
                                    $taxtable[1]['salary_bracket_level2'].'-'.'.20',
                                    $taxtable[1]['salary_bracket_level3'].'-'.'.25',
                                    $taxtable[1]['salary_bracket_level4'].'-'.'.30',
                                    $taxtable[1]['salary_bracket_level5'].'-'.'.32',
                                    $taxtable[1]['salary_bracket_level6'].'-'.'.35'
                                    ]
            ];

            $data['first']  = ['0','20833.33','33333.33','66666.66','166666.66','666666.66'];
            $data['second'] = ['20832','33332','66666','166666','666666','9999999'] ;





        return json_encode($data);
    }


    public function storeBenefitinfo(Request $request){

        if(!isset($request->employeeinfo_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $this->validate($request,[
                'benefit_id'                => 'required',
                'benefit_effectivity_date'  => 'required',
                'benefit_amount'            => 'required',
            ]);

            $amount = str_replace(',', '', $request->benefit_amount);

            $benefitinfo = BenefitInfo::find($request->id);
            $message = 'Update Successfully.';

            if(empty($benefitinfo)){
                $benefitinfo = new BenefitInfo;
                $message = 'Save Successfully.';
            }

            $benefitinfo->fill($request->all());
            $benefitinfo->benefit_amount = $amount;

            if($message == 'Update Successfully.'){
                $benefitinfo->updated_by = Auth::id();
            }else{
                $benefitinfo->created_by = Auth::id();
            }
            $benefitinfo->save();

            $query['benefitinfo'] = $benefitinfo->with('benefits')
                ->where('employee_id',$request->employee_id)
                ->orderBy('benefit_effectivity_date','asc')
                ->get();

            $response = json_encode(['status'=>true,'response'=> $message ,'transactions'=> $query, 'myform'=>'myForm3']);

        }

        return $response;

    }

    public function deleteBenefitinfo(){
        $data = Input::all();

        $id = $data['id'];

        $benefitinfo = new BenefitInfo;

        $benefitinfo->destroy($id);

        $benefitinfo = $benefitinfo
        ->with('benefits')
        ->where('employee_id',$data['employee_id'])
        ->get();

         return json_encode(['status'=>'benefits', 'data'=> $benefitinfo]);

    }

    public function storeSalaryinfo(Request $request){

        if(!isset($request->employee_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $this->validate(request(),[
                'salary_effectivity_date'  => 'required',
                'salary_description'       => 'required',
            ]);

            $salaryInfo = new SalaryInfo;

            $salary_old_rate       = (isset($request->salary_old_rate) ? str_replace(',', '', $request->salary_old_rate) : 0.00 );
            $salary_adjustment     = (isset($request->salary_adjustment) ? str_replace(',', '', $request->salary_adjustment) : 0.00);
            $salary_new_rate       = (isset($request->salary_new_rate) ? str_replace(',', '', $request->salary_new_rate) : 0.00);
            $salarygrade_id        =  (isset($request->salarygrade_id) ? $request->salarygrade_id : NULL);
            $jobgrade_id           = (isset($request->jobgrade_id) ? $request->jobgrade_id : NULL);
            $position_id           =  (isset($request->position_id) ? $request->position_id : NULL);
            $positionitem_id       =  (isset($request->positionitem_id) ? $request->positionitem_id : NULL);
            $step_inc              =  (isset($request->jgstep_inc) ? $request->jgstep_inc : NULL);
// dd($request->all());
            if(isset($request->sgjginfo_id)){

                $salaryInfo = SalaryInfo::find($request->sgjginfo_id);

                $salaryInfo->salarygrade_id          = $salarygrade_id;
                $salaryInfo->jobgrade_id             = $jobgrade_id;
                $salaryInfo->employee_id             = $request->employee_id;
                $salaryInfo->employee_number         = $request->employee_number;
                $salaryInfo->salary_effectivity_date = $request->salary_effectivity_date;
                $salaryInfo->salary_description      = $request->salary_description;
                $salaryInfo->salary_old_rate         = $salary_old_rate;
                $salaryInfo->salary_adjustment       = $salary_adjustment;
                $salaryInfo->salary_new_rate         = $salary_new_rate;
                $salaryInfo->positionitem_id         = $positionitem_id;
                $salaryInfo->position_id             = $position_id;
                $salaryInfo->step_inc                = $step_inc;


                $salaryInfo->save();

                $response = json_encode(['status'=>true,'response'=>'Update Successfully!','salaryinfo'=> SalaryInfo::with('salarygrade','jobgrade')->where('employee_id',$request->employee_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm2']);

            }else{

                $request->merge([
                    'salary_old_rate'   => $salary_old_rate,
                    'salary_adjustment' => $salary_adjustment,
                    'salary_new_rate'   => $salary_new_rate,
                    'jobgrade_id'       => $jobgrade_id,
                    'salarygrade_id'    => $salarygrade_id,
                    'positionitem_id'   => $positionitem_id,
                    'position_id'       => $position_id,
                    'step_inc'          => $step_inc,
                ]);

                $salaryInfo->fill($request->all())->save();

                $response = json_encode(['status'=>true,'response'=>'Save Successfully!','salaryinfo'=> SalaryInfo::with('salarygrade','jobgrade')->where('employee_id',$request->employee_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm2']);

            }
        }

        return $response;
    }

    public function storeLoaninfo(Request $request){

        if(!isset($request->employeeinfo_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $this->validate($request,[
                'loan_id'           => 'required',
                'loan_amortization' => 'required',
                'loan_date_started' => 'required'
            ]);

            $loanTotalAmount        = (isset($request->loan_totalamount) ? str_replace(',', '', $request->loan_totalamount) : 0.00);
            $loanTotalBalance       = (isset($request->loan_totalbalance) ? str_replace(',', '', $request->loan_totalbalance) : 0.00);
            $loanAmortization       = (isset($request->loan_amortization) ? str_replace(',', '', $request->loan_amortization) : 0.00);

            $loanInfo = LoanInfo::find($request->id);
            $message = 'Update Successfully.';

            if(empty($loanInfo)){
                $loanInfo = new LoanInfo;
                $message = 'Save Successfully';
            }

            $loanInfo->fill($request->all());
            $loanInfo->loan_totalamount       = $loanTotalAmount;
            $loanInfo->loan_totalbalance      = $loanTotalBalance;
            $loanInfo->loan_amortization      = $loanAmortization;
            $loanInfo->terminated             = ($request->chk_loanterminated) ? 1 : 0;

            if($message == 'Update Successfully.'){
                $loanInfo->updated_by = Auth::id();
            }else{
                $loanInfo->created_by = Auth::id();
            }

            $loanInfo->save();

            $query['loaninfo'] = $loanInfo
                ->with('loans')
                ->where('employee_id',$request->employee_id)
                ->orderBy('created_at','desc')->get();

            $response = json_encode(['status'=>true,'response'=> $message,'transactions'=> $query, 'myform'=>'myForm4']);

        }

        return $response;

    }


    public function deleteLoanInfo(){
        $data = Input::all();

        $id = $data['id'];

        $loaninfo = new LoanInfo;

        $loaninfo->destroy($id);

        $data2 = $loaninfo
                ->with('loans')
                ->where('employee_id',@$data['employee_id'])
                ->get();

        return json_encode(['status'=>'loans', 'data'=> $data2]);
    }


    public function storeDeductioninfo(Request $request){

        if(!isset($request->employeeinfo_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $this->validate($request,[
                'deduction_id'      => 'required',
                'deduct_amount'     => 'required',
                'deduct_date_start' => 'required',
            ]);

            $deductAmount  = (isset($request->deduct_amount) ? str_replace(',', '', $request->deduct_amount) : 0.00);

            $deductionInfo = DeductionInfo::find($request->id);
            $message = 'Update Successfully.';

            if(empty($deductionInfo)){
                $deductionInfo = new DeductionInfo;
                $message = 'Save Successfully';
            }

            $deductionInfo->fill($request->all());
            $deductionInfo->deduct_amount        = $deductAmount;
            $deductionInfo->terminated           = ($request->chk_deduct) ? 1 : 0;

            if($message == 'Update Successfully.'){
                $deductionInfo->updated_by = Auth::id();
            }else{
                $deductionInfo->created_by = Auth::id();
            }

            $deductionInfo->save();

            $query['deductioninfo'] = $deductionInfo->with('deductions')
            ->where('employee_id',$request->employee_id)
            ->orderBy('created_at','desc')
            ->get();

            $response = json_encode(['status'=>true,'response'=> $message,'transactions'=> $query, 'myform'=>'myForm5']);
        }

        return $response;

    }

    public function deleteDeductInfo(){
        $data = Input::all();

        $id = $data['id'];

        $deductinfo = new DeductionInfo;
        $deductinfo->destroy($id);

        $deductinfo = $deductinfo
        ->with('deductions')
        ->where('employee_id',$data['employee_id'])
        ->get();

        return json_encode(['status'=>'deductions', 'data'=> $deductinfo]);

    }

     public function getSgstep(){

        $id = Input::get('id');
        $step_no = Input::get('step_no');

        $amount = SalaryGrade::where('id',$id)->first([$step_no])->toArray();

        return $amount;
    }

    public function getJgstep(){

        $id = Input::get('id');
        $step_no = Input::get('step_no');

        $amount = JobGrade::where('id',$id)->first([$step_no])->toArray();

        return $amount;
    }

    public function getItem(){
        $id = Input::get('id');

        $query = PositionItemSetup::where('id',$id)->first();

        return json_encode($query);
    }

    //* Store function for Contract of Service and Job Order Employees
    public function storeNonPlantilla(Request $request){

        $this->validate($request,[
            'daily_rate_amount'     => 'required',
        ]);

        $monthly_rate_amount = ($request->monthly_rate_amount) ? str_replace(',', '', $request->monthly_rate_amount) : 0;
        $daily_rate_amount = ($request->daily_rate_amount) ? str_replace(',', '', $request->daily_rate_amount) : 0;
        $inputted_amount = ($request->inputted_amount) ? str_replace(',', '', $request->inputted_amount) : 0;
        $overtime_balance_amount = ($request->overtime_balance_amount) ? str_replace(',', '', $request->overtime_balance_amount) : 0;
        $threshhold_amount = ($request->threshhold_amount) ? str_replace(',', '', $request->threshhold_amount) : 0;


        $nonplantilla = NonPlantillaEmployeeInfo::find($request->id);
        $message = 'Update Successfully.';
        if(empty($nonplantilla)){
            $nonplantilla = new NonPlantillaEmployeeInfo;
            $message = 'Save Successfully.';
        }

        $nonplantilla->fill($request->all());
        $nonplantilla->monthly_rate_amount      = $monthly_rate_amount;
        $nonplantilla->daily_rate_amount        = $daily_rate_amount;
        $nonplantilla->overtime_balance_amount  = $overtime_balance_amount;
        $nonplantilla->inputted_amount          = $inputted_amount;
        $nonplantilla->threshhold_amount        = $threshhold_amount;
        $nonplantilla->save();


        return json_encode(['status'=> true, 'response'=> $message, 'employeeload'=>true]);

    }

    public function computeEmployeeInfo(){

        $data = Input::all();

        $status = $data['emp_status'];

        $employeeinfo        = new EmployeeInfo;
        $employee            = new Employee;
        $employeeinformation = new EmployeeInformation;
        $phicpolicy          = new PhilhealthPolicy;
        $pagibigpolicy       = new PagibigPolicy;
        $philhealthpolicy    = new PhilhealthPolicy;
        // $responsibilities    = new ResponsibilityCenter;
        $employeestatus      = new EmployeeStatus;
        $gsispolicy          = new GsisPolicy;
        $taxpolicy           = new TaxPolicy;

        $employee = $employee
        ->whereIn('id',$data['empid'])
        ->where('active',1)
        ->select('id')
        ->get()->toArray();

        $num = 1;
        if($status == 'nonplantilla'){
            $num = 0;
        }

        $employeestatus = $employeestatus
        ->where('category',$num)
        ->select('RefId')
        ->get()->toArray();

        $query = $employeeinformation
        ->whereIn('employee_status_id',$employeestatus)
        ->whereIn('employee_id',$employee)
        ->get();

        $philhealthpolicy = $philhealthpolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $pagibigpolicy = $pagibigpolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $gsispolicy = $gsispolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $taxpolicy = $taxpolicy
        ->where('policy_name','ANNUAL TAX POLICY')
        ->first();

        if($status == 'plantilla'){
            foreach ($query as $key => $value) {
                $employeeinfo        = new EmployeeInfo;

                $employee_id = $value->employee_id;

                $salaryinfo = SalaryInfo::where('employee_id',$employee_id)
                    ->orderBy('created_at','desc')
                    ->first();

                $basicOne = (isset($salaryinfo->salary_new_rate)) ? $salaryinfo->salary_new_rate : 0;

                $basicAmount = $basicOne;

                $dailyRateAmount  = 0;
                if(isset($basicAmount)){
                    $dailyRateAmount = $basicAmount / 22;
                }else{
                    dd('No Salary Info Employee Id '.$employee_id);
                }

                $monthlyRateAmount = $basicAmount;
                $annualRateAmount = $monthlyRateAmount * 12;

                $phic = $this->computePhic($monthlyRateAmount);
                $gsis = $this->computeGsis($monthlyRateAmount);

                $phic_ee_share = $phic['ee_share'];
                $phic_er_share = $phic['er_share'];
                $gsis_ee_share = $gsis['ee_share'];
                $gsis_er_share = $gsis['er_share'];
                $pagibig_ee_share = 100;

                $totalDeduction = $phic_ee_share + $gsis_ee_share + $pagibig_ee_share;
                $grossSalary = $monthlyRateAmount - $totalDeduction;

                // $tax = $this->computeTax($grossSalary);

                $empinfo = $employeeinfo
                ->where('employee_id',$employee_id)
                ->first();

                $employeenumber = new Employee;

                $employeenumber = $employeenumber
                ->where('id',$value->employee_id)
                ->first();

                if(isset($empinfo)){

                    $employeeinfo = $employeeinfo->find($empinfo->id);

                    $employeeinfo->employee_id                  = $value->employee_id;
                    $employeeinfo->gsispolicy_id                = $gsispolicy->id;
                    $employeeinfo->pagibigpolicy_id             = $pagibigpolicy->id;
                    $employeeinfo->philhealthpolicy_id          = $philhealthpolicy->id;
                    $employeeinfo->taxpolicy_id                 = $taxpolicy->id;
                    $employeeinfo->employee_number              = $employeenumber->employee_number;
                    $employeeinfo->tax_contribution             = 0;
                    $employeeinfo->daily_rate_amount            = $dailyRateAmount;
                    $employeeinfo->monthly_rate_amount          = $monthlyRateAmount;
                    $employeeinfo->annual_rate_amount           = $annualRateAmount;
                    $employeeinfo->pagibig_contribution         = $pagibig_ee_share;
                    $employeeinfo->philhealth_contribution      = $phic_ee_share;
                    $employeeinfo->gsis_contribution            = $gsis_ee_share;
                    // $employeeinfo->tax_contribution             = $tax;
                    $employeeinfo->er_pagibig_share             = $pagibig_ee_share;
                    $employeeinfo->er_philhealth_share          = $phic_er_share;
                    $employeeinfo->er_gsis_share                = $gsis_er_share;
                    $employeeinfo->overtime_balance_amount      = $annualRateAmount;

                    $employeeinfo->save();

                }else{

                    $employeeinfo->employee_id                  = $value->employee_id;
                    $employeeinfo->gsispolicy_id                = $gsispolicy->id;
                    $employeeinfo->pagibigpolicy_id             = $pagibigpolicy->id;
                    $employeeinfo->philhealthpolicy_id          = $philhealthpolicy->id;
                    $employeeinfo->taxpolicy_id                 = $taxpolicy->id;
                    $employeeinfo->employee_number              = $employeenumber->employee_number;
                    $employeeinfo->tax_contribution             = 0;
                    // $employeeinfo->employee_status_id           = $value->employee_status_id;
                    $employeeinfo->daily_rate_amount            = $dailyRateAmount;
                    $employeeinfo->monthly_rate_amount          = $monthlyRateAmount;
                    $employeeinfo->annual_rate_amount           = $annualRateAmount;
                    $employeeinfo->pagibig_contribution         = $pagibig_ee_share;
                    $employeeinfo->philhealth_contribution      = $phic_ee_share;
                    $employeeinfo->gsis_contribution            = $gsis_ee_share;
                    // $employeeinfo->tax_contribution             = $tax;
                    $employeeinfo->er_pagibig_share             = $pagibig_ee_share;
                    $employeeinfo->er_philhealth_share          = $phic_er_share;
                    $employeeinfo->er_gsis_share                = $gsis_er_share;
                    $employeeinfo->overtime_balance_amount      = $annualRateAmount;

                    $employeeinfo->save();

                }
            }

        }else{
            
            foreach ($query as $key => $value) {

                $salaryinfo = SalaryInfo::where('employee_id',$value->employee_id)
                ->orderBy('created_at','desc')
                ->first();

                // if($value->employee_id == 166){
                //     dd($salaryinfo);
                // }

                $dailyRateAmount  = 0;
                if(isset($salaryinfo->salary_new_rate)){
                    $dailyRateAmount   = $salaryinfo->salary_new_rate;
                    $monthlyRateAmount = $dailyRateAmount * 22;
                    $annualRateAmount  = $monthlyRateAmount * 12;

                    $nonEmpInfo = NonPlantillaEmployeeInfo::where('employee_id',$value->employee_id)->first();

                    $nonplantilla = NonPlantillaEmployeeInfo::find(@$nonEmpInfo->id);
                    if(empty($nonplantilla)){
                        $nonplantilla = new NonPlantillaEmployeeInfo;
                    }

                    $nonplantilla->employee_id                     = $value->employee_id;
                    $nonplantilla->daily_rate_amount               = $dailyRateAmount;
                    $nonplantilla->monthly_rate_amount             = $monthlyRateAmount;
                    // $nonplantilla->annual_rate_amount              = $annualRateAmount;
                    $nonplantilla->overtime_balance_amount         = $annualRateAmount;
                    $nonplantilla->save();
                }

            }
        }

        return json_encode(['status' => true, 'response' => 'Update Successfully']);

    }

}
