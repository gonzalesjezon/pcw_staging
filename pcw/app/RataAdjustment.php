<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RataAdjustment extends Model
{
    protected $table = 'pms_rata_adjustments';
    protected $fillable = [

        'employee_number',
        'employee_id',
        'deduction_id',
        'rata_id',
        'tax_deduction',
        'ra_additional_amount',
        'ta_additional_amount',
        'used_vehicle_amount',
        'refund_amount',
        'remarks',
        'year',
        'month',
        'created_by',
        'updated_by',
    ];
}
